# Backend

Kode proses bisnis API dari sistem pencatatan administrasi CV Mulyo Farm

## Dokumentasi Penggunaan

Penggunaan API untuk mendukung proses bisnis pada sistem pencatatan ini dapat diakses [disini](https://documenter.getpostman.com/view/27793116/2s9YkgEkzc)

## Format Penamaan 🖋️

Penamaan file dan variable menggunakan format Camel Case, contoh: **_AsetController.js_**

## Struktur Folder 📒

```
├── config
├── controller
├── error
├── middleware
├── prisma
├── routes
├── service
├── utils
├── index.js
├── package.json
├── package-lock.json
├── vercel.json
├── .env.example
└── .gitignore
```

### 1) config

Konfigurasi singletone pemanggilan database. Terdapat 1 file yaitu [db.js](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/config/db.js), file ini mengatur instansiasi kelas prisma client.

### 2) controller

Menyimpan proses bisnis pada sistem. File pada folder ini sebagai berikut:

| No  | Nama File               | Keterangan                                                                                                                                                                                                                                                                                                           | Lokasi                   |
| --- | ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| 1.  | AsetController.js       | File yang berisi proses bisnis yang berkaitan dengan aset. Hal ini mencakup proses bisnis dalam meminta semua data, membuat, update informasi aset, menghapus aset, menampilkan riwayat penghapusan aset, menambah riwayat operasional aset, menampilkan semua riwayat operasional aset, dan jual aset               | [AsetController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/AsetController.js)       |
| 2.  | AuthController.js       | File yang berisi proses bisnis yang berkaitan dengan otentikasi pengguna. Hal ini mencakup proses bisnis dalam login, reset password, logout                                                                                                                                                                         | [AuthController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/AuthController.js)       |
| 3.  | InventarisController.js | File yang berisi proses bisnis yang berkaitan dengan pencatatan transaksi dan operasional yang berkaitan dengan pupuk dan pakan. Hal ini meliputi menambah stok pakan dan pupuk, menggunakan pakan dan pupuk, dan penjualan pupuk dan pakan                                                                          | [InventorisController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/InventorisController.js) |
| 4.  | PegawaiController.js    | File yang berisi proses bisnis yang berkaitan dengan pencatatan pegawai. Hal ini meliputi pencatatan pegawai baru, pencatatan penggajian, penghapusan data pegawai, dan update data pegawai.                                                                                                                         | [PegawaiController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/PegawaiController.js)    |
| 5.  | PeternakanController.js | File yang berisi proses bisnis yang berkaitan dengan pencatatan ekspansi peternakan. Hal ini mencakup pencatatan peternakan baru, update informasi peternakan, menghapus peternakan, menambah modal peternakan, dan menampilkan informasi detail untuk setiap peternakan                                             | [PeternakanController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/PeternakanController.js) |
| 6.  | TernakController.js     | File yang berisi proses bisnis yang berkaitan dengan ternak. Hal ini mencakup proses bisnis dalam meminta semua data, membuat, update informasi ternak, menghapus ternak, menampilkan riwayat penghapusan ternak, menambah riwayat operasional ternak, menampilkan semua riwayat operasional ternak, dan jual ternak | [TernakController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/TernakController.js)     |
| 7.  | TransaksiController.js  | File yang berisi proses bisnis yang berkaitan dengan menu transaksi dan laporan. Hal ini meliputi menampilkan transaksi, menampilkan ringkasan agregat transaksi 1 bulan terakhir, download pdf dan excel, dan pencatatan transaksi lain.                                                                            | [TransaksiController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/TransaksiController.js)  |
| 8.  | UserController.js       | File yang berisi proses bisnis yang berkaitan dengan manajemen akun pengguna. Hal ini meliputi menambahkan pengguna, menghapus pengguna, dan menampilkan informasi pengguna.                                                                                                                                         | [UserController](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/controller/UserController.js)       |

### 3) error

Custom error untuk membedakan response. File pada folder ini antara lain:

| No  | Nama File          | Keterangan                                                                                                                                           | Lokasi              |
| --- | ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------- |
| 1.  | BadRequestError.js | File yang berisi custom error untuk mengelompokkan kesalahan input user yang gagal melalui validasi.                                                 | [BadRequestError](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/error/BadRequestError.js) |
| 2.  | ConflictError.js   | File yang berisi custom error untuk mengelompokkan kesalahan input user yang gagal menambahkan record baru karena gagal memnuhi constraint tertentu. | [ConflictError](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/error/ConflictError.js)   |
| 3.  | NotFoundError.js   | File yang berisi custom error untuk mengelompokkan kesalahan input user ketika data yang diminta tidak ditemukan                                     | [NotFoundError](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/error/NotFoundError.js)   |

### 4) middleware

Verifikasi akses token sebelum request masuk ke controller. File pada folder ini:

| No  | Nama File    | Keterangan                                                                       | Lokasi        |
| --- | ------------ | -------------------------------------------------------------------------------- | ------------- |
| 1.  | VerifyJWT.js | File yang berisi kumpulan fungsi yang digunakan untuk verifikasi akses pengguna. | [VerifyJWT](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/middleware/VerifyJwt.js) |

### 5) prisma

Folder ini mengatur pembuatan schema model dan mengatur konfigurasi serta migrasi database.

| No  | Nama File     | Keterangan                                                                             | Lokasi            |
| --- | ------------- | -------------------------------------------------------------------------------------- | ----------------- |
| 1.  | schema.prisma | File yang berisi konfigurasi database dan model beserta constraint untuk tiap entitas. | [schema.prisma](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/prisma/schema.prisma) |
| 2.  | migrations    | Folder yang berisi migrasi terakhir dari database                                      | [migrations](https://git.stis.ac.id/222112280/rpl/-/tree/main/backend/prisma/migrations)    |

### 6) routes

Mendefinisikan dan mengelompokkan route untuk setiap api berdasarkan entitas. File pada folder tersebut:

| No  | Nama File          | Keterangan                                                                                               | Lokasi              |
| --- | ------------------ | -------------------------------------------------------------------------------------------------------- | ------------------- |
| 1.  | AsetRoute.js       | File yang berisi konfigurasi route yang berkaitan dengan entitas aset                                    | [AsetRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/AsetRoute.js)       |
| 2.  | InventorisRoute.js | File yang berisi konfigurasi route yang berkaitan dengan entitas OperasionalInventoris (pakan dan pupuk) | [InventorisRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/InventorisRoute.js) |
| 3.  | PegawaiRoute.js    | File yang berisi konfigurasi route yang berkaitan dengan entitas Pegawai                                 | [PegawaiRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/PegawaiRoute.js)    |
| 4.  | PeternakanRoute.js | File yang berisi konfigurasi route yang berkaitan dengan entitas Peternakan                              | [PeternakanRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/TernakRoute.js) |
| 5.  | TernakRoute.js     | File yang berisi konfigurasi route yang berkaitan dengan entitas Ternak                                  | [TernakRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/TernakRoute.js)     |
| 6.  | TransaksiRoute.js  | File yang berisi konfigurasi route yang berkaitan dengan entitas Transaksi                               | [TransaksiRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/TransaksiRoute.js)  |
| 7.  | UserRoute.js       | File yang berisi konfigurasi route yang berkaitan dengan entitas User                                    | [UserRoute](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/routes/UserRoutes.js)       |

### 7) service

Kumpulan fungsi reuseable untuk setiap proses bisnis. Setiap file dikelompokkan menurut entitas yang terdaftar.

| No  | Nama File          | Keterangan                                                                                               | Lokasi              |
| --- | ------------------ | -------------------------------------------------------------------------------------------------------- | ------------------- |
| 1.  | AsetService.js       | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas aset                                    | [AsetService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/AsetService.js)       |
| 2.  | KasService.js | File yang berisi kumpulan fungsi penambahan yang berkaitan dengan entitas Kas (pakan dan pupuk) | [KasService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/KasService.js) |
| 3.  | PegawaiService.js    | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas Pegawai                                 | [PegawaiService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/PegawaiService.js)    |
| 4.  | PeternakanService.js | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas Peternakan                              | [PeternakanService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/PeternakanService.js) |
| 5.  | TernakService.js     | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas Ternak                                  | [TernakService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/TernakService.js)     |
| 6.  | TransaksiService.js  | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas Transaksi                               | [TransaksiService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/TransaksiService.js)  |
| 7.  | UserService.js       | File yang berisi kumpulan fungsi reuseable yang berkaitan dengan entitas User                                    | [UserService](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/service/UserService.js)       |

### 8) utils

Konfigurasi konstanta dan fungsi yang tidak terikat pada entitas tertentu.

| No  | Nama File          | Keterangan                                                                                               | Lokasi              |
| --- | ------------------ | -------------------------------------------------------------------------------------------------------- | ------------------- |
| 1.  | Constant.js       | File yang berisi konstanta yang akan dipakai berulang kali                                    | [Constant](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/utils/Constant.js)       |
| 2.  | Mailer.js | File yang berisi fungsi untuk menghubungkan dengan Google API untuk pengiriman link reset password | [Mailer](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/utils/Mailer.js) |

### 9) [index.js](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/index.js)

File berisi konfigurasi port, integrasi route, dan pengaturan CORS.

### 9) [vercel.json](https://git.stis.ac.id/222112280/rpl/-/blob/main/backend/vercel.json)

File berisi konfigurasi untuk deployment pada vercel.
