import prisma from "../config/db.js";
import AsetService from "../service/AsetService.js";
import TransaksiService from "../service/TransaksiService.js";
import ConflictError from "../error/ConflictError.js";

/**
 * Proses bisnis dan pengaturan response menu pencatatan aset
 */

const transaksiService = new TransaksiService();
const asetSetvice = new AsetService();

//Mendapatkan semua aset
export const getAset = async (req, res) => {
  try {
    const { peternakan } = req.query;

    const data = await asetSetvice.getAllAset(peternakan);

    res.status(200).json({
      message: "Berhasil mendapatkan Aset",
      data,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//mendapatkan aset berdasarkan ID
export const getAsetById = async (req, res) => {
  const { id } = req.params; // Mengambil id dari URL
  try {
    const data = await asetSetvice.getAsetById(id);

    res.status(200).json({
      data,
      message: `Berhasil mendapatkan aset dengan id ${id}`,
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(400).json({ message: error.message });
  }
};

//Menambahkan aset baru
export const tambahAset = async (req, res) => {
  const {
    nama,
    jenis,
    harga_beli,
    pihak_terkait,
    status_kepemilikan,
    deskripsi,
    metode_pembayaran,
    tanggal_dapat,
    id_peternakan,
  } = req.body;

  const date = new Date(tanggal_dapat);

  try {
    //Ketika pembayaran cash
    if (metode_pembayaran === "Cash") {
      await transaksiService.createTransaksi(
        tanggal_dapat,
        "Pengeluaran",
        harga_beli,
        pihak_terkait,
        id_peternakan,
        `Pembelian Aset ${jenis} ${deskripsi}`
      );
    }

    // Simpan data ke database
    const data = await asetSetvice.createAset(
      nama,
      jenis,
      harga_beli,
      status_kepemilikan,
      deskripsi,
      metode_pembayaran,
      tanggal_dapat,
      id_peternakan
    );

    // Berhasil
    return res.status(201).json(data);
  } catch (error) {
    console.log(error);
    if (error instanceof ConflictError) {
      return res.status(409).json({ message: error.message });
    } else {
      return res.status(500).json({ message: "Terjadi kesalahan internal." });
    }
  }
};

//Penjualan aset
export const jualAset = async (req, res) => {
  const {
    kategori,
    subkategori,
    detail,
    nominal,
    tanggal_transaksi,
    pembeli,
    id_peternakan,
  } = req.body;
  const { id } = await req.params;
  try {
    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pemasukan",
      nominal,
      pembeli,
      id_peternakan,
      `Penjualan aset ${kategori} ${detail}`
    );

    await asetSetvice.deleteAset(id);

    res.status(201).json({ transaksi });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

//Mendapatkan operasional aset
export const getOperasional = async (req, res) => {
  const { id } = req.params;
  let sum = 0;

  try {
    const data = await asetSetvice.getOperasional(id);

    data.forEach((item) => (sum += item.transaksi.jumlah));

    return res.status(200).json({
      message: "Berhasil mendapatkan data operasional",
      data,
      sum,
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

//Menambahkan operasional aset
export const addOperasional = async (req, res) => {
  try {
    await prisma.$connect();
    const {
      tanggal_transaksi,
      id_peternakan,
      jumlah,
      pihak_terkait,
      deskripsi,
    } = req.body;

    const { id } = req.params;

    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pengeluaran",
      jumlah,
      pihak_terkait,
      id_peternakan,
      `${deskripsi}`
    );

    const data = await asetSetvice.addOperasional(id, transaksi.id);

    res.status(200).json({
      data: {
        id: data.id,
        pihak_terkait,
        jumlah,
        tanggal_transaksi,
        deskripsi,
      },
      message: "Operasional berhasil ditambahkan",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: error.message,
    });
  }
};

//Menghapus aset
export const tambahPenghapusanAset = async (req, res) => {
  const { id, jenis, kategori, tanggal, deskripsi, id_peternakan } = req.body;

  try {
    await asetSetvice.penghapusanAset(
      jenis,
      kategori,
      deskripsi,
      tanggal,
      id_peternakan
    );

    await asetSetvice.deleteAset(id);

    res.status(201).json({
      message: "Berhasil menambahkan riwayat penghapusan",
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

//Mengedit aset
export const editAset = async (req, res) => {
  const {
    nama,
    jenis,
    harga_beli,
    status_kepemilikan,
    deskripsi,
    metode_pembayaran,
    tanggal_dapat,
    id_peternakan,
  } = req.body;
  const { id } = req.params;
  try {
    await prisma.$connect();
    const data = await prisma.aset.update({
      where: {
        id: +id,
      },
      data: {
        nama: nama,
        jenis: jenis,
        harga_beli: +harga_beli,
        status_kepemilikan: status_kepemilikan,
        deskripsi: deskripsi,
        metode_pembayaran,
        tanggal_dapat: new Date(tanggal_dapat).toISOString(),
        id_peternakan,
      },
    });

    await prisma.$disconnect();

    if (!data) {
      res.status(404).json({
        message: "Data aset tidak ditemukan",
      });
    } else {
      res.status(200).json({
        data,
        message: "Aset berhasil diupdate",
      });
    }
  } catch (error) {
    await prisma.$disconnect();
    res.status(500).json({ message: error.message });
  }
};
