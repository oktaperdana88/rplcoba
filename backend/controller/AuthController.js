import prisma from "../config/db.js";
import argon2 from "argon2";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import { createTransporter } from "../utils/Mailer.js";
import ConflictError from "../error/ConflictError.js";

dotenv.config();

//login
export const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    if (!email && !password)
      return res.status(400).json({
        message: "Email dan password tidak boleh kosong",
      });

    //mendapatkan user
    await prisma.$connect();
    const user = await prisma.user.findFirst({
      select: {
        id: true,
        password: true,
        role: true,
      },
      where: {
        email: email,
      },
    });

    await prisma.$disconnect();

    //verifikasi user
    if (!user)
      return res.status(406).json({
        message: "Tolong cek kembali email atau password Anda",
      });

    //Verifikasi password
    const confirm = await argon2.verify(user.password, password);

    if (!confirm)
      return res.status(406).json({
        message: "Tolong cek kembali email atau password Anda",
      });

    //Membuat access token
    const accessToken = jwt.sign(
      {
        id: user.id,
        email: email,
        role: user.role,
      },
      process.env.ACCESS_TOKEN_SECRET,
      {
        expiresIn: "1d",
      }
    );

    //Membuat refresh token
    const refreshToken = jwt.sign(
      {
        id: user.id,
        email: email,
        role: user.role,
      },
      process.env.REFRESH_TOKEN_SECRET,
      { expiresIn: "1d" }
    );

    //Memasukkan token ke cookie
    res.cookie("refresh", refreshToken, {
      httpOnly: true,
      maxAge: 24 * 60 * 60 * 1000,
    });
    res.status(200).json({
      id: user.id,
      role: user.role,
      accessToken,
    });
  } catch (e) {
    await prisma.$disconnect();
    res.status(500).json({
      message: "Internal server Error",
      error: e.message,
    });
  }
};

//Mendapatkan akses token baru
export const refresh = async (req, res) => {
  try {
    const refreshToken = req.cookies.refresh;
    const { id } = req.body;

    if (!refreshToken) return res.status(401);

    //Mendapatkan data user
    await prisma.$connect();

    const user = await prisma.user.findUnique({
      where: {
        id: +id,
      },
    });

    await prisma.$disconnect();

    if (!user)
      return res.status(404).json({
        message: "Akun tidak ditemukan",
      });

    //verifikasi refresh token
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
      if (err) return res.status(403);

      //Membuat ulang akses token
      const accessToken = jwt.sign(
        { username: user.email },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: "1m",
        }
      );

      res.json({ accessToken });
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(500);
    console.error(error.message);
  }
};

//Membuat akun baru
export const register = async (req, res) => {
  try {
    const { email, password, confirm, name, role } = req.body;

    if (!email || !password || !name || !role)
      return res.status(400).json({
        message: "Email, password, nama, dan role tidak boleh kosong",
      });

    //Verifikasi password
    if (password !== confirm)
      return res.status(400).json({
        message: "password harus sesuai dengan konfirmasi password",
      });

    await prisma.$connect();
    const user = await prisma.user.findUnique({
      where: {
        email: email,
      },
    });

    if (user) throw new ConflictError("Email telah digunakan");

    //hash password
    const hash = await argon2.hash(password);

    //Mencatat di database

    await prisma.user.create({
      data: {
        email: email,
        nama: name,
        password: hash,
        role: role,
      },
    });

    await prisma.$disconnect();

    return res.status(201).json({
      data: {
        nama: name,
        email: email,
        role: role,
      },
      message: "Akun berhasil dibuat",
    });
  } catch (error) {
    await prisma.$disconnect();
    if (error instanceof ConflictError) {
      res.status(400).json({ message: error.message });
    }
    res.status(500).json({
      message: error.message,
    });
  }
};

//mengirimkan email link ganti password
export const resetPassword = async (req, res) => {
  const { email } = req.body;

  //Mengirimkan email
  try {
    await prisma.$connect();
    const user = await prisma.user.findFirst({
      where: {
        email: email,
      },
    });

    if (!user) {
      return res.status(400).json({
        message: "Mohon cek email Anda",
      });
    }

    const resetToken = jwt.sign(
      {
        email: email,
        id: user.id,
      },
      process.env.RESET_PASSWORD_TOKEN_SECRET,
      {
        expiresIn: "10m",
      }
    );

    const link = `https://rplcoba.vercel.app/change-password?key=${resetToken}`;
    const transporter = await createTransporter();
    const info = await transporter.sendMail({
      from: process.env.EMAIL,
      to: email,
      subject: "Ganti Password",
      html: `<div style="width: 100%; padding: 2rem; text-align: center;"><h1 style:"font-size: 2rem;">
      <b>Permintaan Reset Password</b>
      </h1>
      <p>Klik tombol ini<p>
      <a href=${link} style="width: max-content; padding: 1rem; background-color: #219C90; color: white; display: block; margin: 2rem auto; font-style: normal; text-decoration: none;">Reset Password</a>
      <p>Link akan bertahan selama 10 menit<p>
      </div>`,
    });

    await prisma.$disconnect();

    return res.status(200).json({
      message: info.messageId,
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    return res.status(500).json({
      message: error.message,
    });
  }
};

//Ganti password saat login
export const changePassword = async (req, res) => {
  try {
    await prisma.$connect();
    const id = req.user;
    const { password, confirm } = req.body;
    if (password !== confirm) {
      return res.status(400).json({
        message: "Password tidak sesuai dengan konfirmasi password",
      });
    }

    const hash = await argon2.hash(password);
    await prisma.user.update({
      data: {
        password: hash,
      },
      where: {
        id: id,
      },
    });

    await prisma.$disconnect();
    return res.status(200).json({
      message: "Reset password berhasil",
    });
  } catch (error) {
    await prisma.$disconnect();
    return res.status(500).json({
      message: error.message,
    });
  }
};

//Logout
export const logout = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id)
      return res.status(403).json({
        message: "Unauthorized",
      });
    await prisma.$connect();
    const user = await prisma.user.findUnique({
      where: {
        id: +id,
      },
    });
    if (!user)
      return res.status(404).json({
        message: "User Not Found",
      });
    await prisma.user.update({
      data: {
        nama: user.nama,
        email: user.email,
        password: user.password,
        refresh_token: "",
      },
      where: {
        id: user.id,
      },
    });

    await prisma.$disconnect();

    res.clearCookie("refresh");
    res.status(200).json({
      message: "Logout success",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      message: "Failed",
    });
  }
};
