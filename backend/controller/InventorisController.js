import prisma from "../config/db.js";
import TransaksiService from "../service/TransaksiService.js";

/**
 * Proses bisnis pencatatan pupuk dan pakan
 */

const transaksiService = new TransaksiService();

//Mendapatkan riwayat pupuk dan pakan
export const getInventoris = async (req, res) => {
  const { peternakan, riwayat, jenis } = req.query;
  try {
    await prisma.$connect();
    let where;
    if (jenis === "Semua Jenis") {
      where = {
        id_peternakan: +peternakan,
        riwayat: {
          contains: riwayat,
        },
        jenis: {
          not: {
            contains: "Pupuk",
          },
        },
      };
    } else {
      where = {
        id_peternakan: +peternakan,
        riwayat: {
          contains: riwayat,
        },
        jenis: {
          contains: jenis,
        },
      };
    }

    const data = await prisma.operasionalInventoris.findMany({
      where,
    });
    await prisma.$disconnect();
    res.status(200).json({
      data,
      message: "Berhasil ambil data operasional inventoris",
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Menambahkan pupuk dan/atau pakan
export const tambahInventoris = async (req, res) => {
  const {
    jenis,
    id_peternakan,
    cara_peroleh,
    ternak,
    berat,
    pihak_terkait,
    tanggal,
    harga,
    deskripsi,
    riwayat,
  } = req.body;

  const date = new Date(tanggal);

  try {
    await prisma.$connect();
    const existingPeternakan = await prisma.peternakan.findUnique({
      where: {
        id: +id_peternakan,
      },
    });

    if (!existingPeternakan) {
      return res.status(404).json({ message: "Peternakan tidak ditemukan" });
    }

    let inventori;
    let updatedPeternakan = { ...existingPeternakan };
    let transaksi;

    if (riwayat === "Penambahan") {
      //ketika beli
      if (cara_peroleh === "Beli") {
        transaksi = await prisma.transaksi.create({
          data: {
            jenis_transaksi: "Pengeluaran",
            deskripsi: `Pembelian ${jenis}: ${deskripsi}`,
            tanggal_transaksi: new Date(tanggal).toISOString(),
            pihak_terkait: pihak_terkait,
            jumlah: +harga,
            id_peternakan: +id_peternakan,
          },
        });

        //menambahkan riwayat penambahan
        inventori = await prisma.operasionalInventoris.create({
          data: {
            jenis: jenis,
            berat: +berat,
            riwayat: riwayat,
            tanggal: new Date(tanggal).toISOString(),
            deskripsi: deskripsi,
            peternakan: { connect: { id: +id_peternakan } },
            transaksi: {
              connect: {
                id: transaksi.id ? transaksi.id : null,
              },
            },
          },
        });

        //menambahkan kas
        const kas = await prisma.kas.findFirst({
          where: {
            id_peternakan: +id_peternakan,
            bulan: date.getMonth() === 12 ? "1" : String(date.getMonth() + 1),
            tahun:
              date.getMonth() === 12
                ? String(date.getFullYear() + 1)
                : String(date.getFullYear()),
          },
        });

        if (!kas) {
          const allKas = await prisma.kas.findMany({
            where: {
              id_peternakan: +id_peternakan,
            },
          });

          allKas.sort((a, b) => {
            if (a.tahun < b.tahun) {
              return -1;
            } else if (a.tahun > b.tahun) {
              return 1;
            } else {
              return a.bulan - b.bulan;
            }
          });

          let index = allKas.findIndex((item) => {
            return (
              (item.tahun == date.getFullYear() &&
                item.bulan > date.getMonth() + 1) ||
              item.tahun > date.getFullYear()
            );
          });

          if (index == -1) {
            index = 1;
          }

          let jml = 0;

          const bulan = date.getMonth() + 1;
          const tahun = date.getFullYear();
          jml = allKas[index - 1].jumlah - Number(harga);
          await prisma.kas.create({
            data: {
              id_peternakan: +id_peternakan,
              bulan: String(bulan),
              tahun: String(tahun),
              jumlah: jml,
            },
          });
        } else {
          kas.jumlah -= Number(harga);
          await prisma.kas.update({
            data: kas,
            where: {
              id: kas.id,
            },
          });
        }

        if (
          date.getMonth() !== new Date().getMonth() ||
          date.getFullYear() !== new Date().getFullYear()
        ) {
          const allKas = await prisma.kas.findMany({
            where: {
              tahun: {
                gte: String(date.getFullYear()),
                lte: String(new Date().getFullYear()),
              },
              id_peternakan: +id_peternakan,
            },
          });

          allKas.sort((a, b) => {
            if (a.tahun < b.tahun) {
              return -1;
            } else if (a.tahun > b.tahun) {
              return 1;
            } else {
              return a.bulan - b.bulan;
            }
          });

          for (let i = 0; i < allKas.length; i++) {
            if (
              allKas[i].tahun == String(date.getFullYear()) &&
              allKas[i].bulan <= date.getMonth() + 1
            ) {
              continue;
            }

            if (
              allKas[i].tahun == String(new Date().getFullYear()) &&
              allKas[i].bulan > new Date().getMonth() + 1
            ) {
              continue;
            }

            allKas[i].jumlah -= Number(harga);

            await prisma.kas.update({
              data: allKas[i],
              where: {
                id: allKas[i].id,
              },
            });
          }
        }
      } else {
        inventori = await prisma.operasionalInventoris.create({
          data: {
            jenis: jenis,
            berat: +berat,
            riwayat: riwayat,
            tanggal: new Date(tanggal).toISOString(),
            deskripsi: deskripsi,
            peternakan: { connect: { id: +id_peternakan } },
          },
        });
      }

      //menambahkan stok
      switch (jenis) {
        case "Rumput":
          updatedPeternakan.stok_rumput += Number(berat);
          break;
        case "Fermentasi":
          updatedPeternakan.stok_fermentasi += +berat;
          break;
        case "Konsentrat":
          updatedPeternakan.stok_konsentrat += +berat;
          break;
        default:
          updatedPeternakan.stok_pupuk += +berat;
          break;
      }
    } else {
      switch (jenis) {
        case "Rumput":
          if (updatedPeternakan.stok_rumput < +berat) {
            return res
              .status(400)
              .json({ message: "Stok rumput tidak mencukupi" });
          }
          updatedPeternakan.stok_rumput -= +berat;
          break;
        case "Fermentasi":
          if (updatedPeternakan.stok_fermentasi < +berat) {
            return res
              .status(400)
              .json({ message: "Stok fermentasi tidak mencukupi" });
          }
          updatedPeternakan.stok_fermentasi -= +berat;
          break;
        case "Konsentrat":
          if (updatedPeternakan.stok_konsentrat < +berat) {
            return res
              .status(400)
              .json({ message: "Stok konsentrat tidak mencukupi" });
          }
          updatedPeternakan.stok_konsentrat -= +berat;
          break;
        default:
          if (updatedPeternakan.stok_pupuk < +berat) {
            return res
              .status(400)
              .json({ message: "Stok pupuk tidak mencukupi" });
          }
          updatedPeternakan.stok_pupuk -= +berat;
          break;
      }
      inventori = await prisma.operasionalInventoris.create({
        data: {
          jenis: jenis,
          berat: +berat,
          riwayat: riwayat,
          ternak: ternak,
          tanggal: new Date(tanggal).toISOString(),
          deskripsi: deskripsi,
          peternakan: { connect: { id: +id_peternakan } },
        },
      });
    }

    await prisma.peternakan.update({
      where: {
        id: +id_peternakan,
      },
      data: updatedPeternakan,
    });

    await prisma.$disconnect();

    res.status(201).json(inventori);
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};


// Penjualan pakan dan/atau pupuk
export const jualInventoris = async (req, res) => {
  const {
    nominal,
    detail,
    tanggal_transaksi,
    pembeli,
    id_peternakan,
    kategori,
    deskripsi,
    subkategori,
  } = req.body;
  const date = new Date(tanggal_transaksi);
  try {
    await prisma.$connect();

    console.log(subkategori);

    const peternakan = await prisma.peternakan.findUnique({
      where: {
        id: +id_peternakan,
      },
    });

    if (kategori === "Pakan") {
      switch (subkategori) {
        case "Rumput":
          if (peternakan.stok_rumput < +detail) {
            return res
              .status(400)
              .json({ message: "Stok rumput tidak mencukupi" });
          }
          peternakan.stok_rumput -= +detail;
          break;
        case "Fermentasi":
          if (peternakan.stok_fermentasi < +detail) {
            return res
              .status(400)
              .json({ message: "Stok fermentasi tidak mencukupi" });
          }
          peternakan.stok_fermentasi -= +detail;
          break;
        case "Konsentrat":
          if (peternakan.stok_konsentrat < +detail) {
            return res
              .status(400)
              .json({ message: "Stok konsentrat tidak mencukupi" });
          }
          peternakan.stok_konsentrat -= +detail;
          break;
        default:
          break;
      }

    }

    if (kategori === "Pupuk") {
      if (peternakan.stok_pupuk < +detail) {
        return res.status(400).json({ message: "Stok pupuk tidak mencukupi" });
      }
      peternakan.stok_pupuk -= +detail;
    }

    let catatan;

    if (kategori === "Pakan") {
      catatan = `Penjualan ${kategori} ${subkategori} ${detail} Kg`;
    } else {
      catatan = `Penjualan ${kategori} ${detail} sak`;
    }

    switch (subkategori) {
      case "Rumput":
        peternakan.stok_rumput -= Number(detail);
        break;
      case "Fermentasi":
        peternakan.stok_fermentasi -= +detail;
        break;
      case "Konsentrat":
        peternakan.stok_konsentrat -= +detail;
        break;
      default:
        peternakan.stok_pupuk -= +detail;
        break;
    }

    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pemasukan",
      nominal,
      pembeli,
      id_peternakan,
      catatan
    );

    await prisma.peternakan.update({
      data: peternakan,
      where: {
        id: +id_peternakan,
      },
    });

    await prisma.$disconnect();

    res.status(200).json({
      transaksi,
    });
  } catch (error) {
    await prisma.$disconnect();
    console.log(error);
    res.status(500).json({ message: error });
  }
};


//Mendapatkan rekap operasional
export const getSummary = async (req, res) => {
  const { peternakan } = req.query;

  const now = new Date();
  const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
  const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);

  try {
    await prisma.$connect();
    const rekap_week = await prisma.operasionalInventoris.groupBy({
      by: ["riwayat", "jenis"],
      _sum: {
        berat: true,
      },

      where: {
        id_peternakan: +peternakan,
        tanggal: {
          lte: new Date(),
          gte: new Date(now.setDate(now.getDate() - 7)),
        },
      },
    });

    const rekap_month = await prisma.operasionalInventoris.groupBy({
      by: ["riwayat", "jenis"],
      _sum: {
        berat: true,
      },
      where: {
        id_peternakan: +peternakan,
        tanggal: {
          lte: lastDay,
          gte: firstDay,
        },
      },
    });

    await prisma.$disconnect();

    res.status(200).json({
      week: rekap_week,
      month: rekap_month,
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(400);
  }
};
