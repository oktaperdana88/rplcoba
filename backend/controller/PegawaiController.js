import prisma from "../config/db.js";
import BadRequestError from "../error/BadRequestError.js";
import ConflictError from "../error/ConflictError.js";
import PegawaiService from "../service/PegawaiService.js";
import TransaksiService from "../service/TransaksiService.js";

/**
 * Proses bisnis pencatatan pegawai
 */

const pegawaiService = new PegawaiService();
const transaksiService = new TransaksiService();

//Mendapatkan semua pegawai
export const getPegawai = async (req, res) => {
  const { peternakan } = req.query;
  try {
    const data = await pegawaiService.getAllPegawai(peternakan);

    res.status(200).json({
      message: "Berhasil mendapatkan daftar Pegawai",
      data,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//Mendapatkan pegawai berdasarkan id
export const getpegawaiById = async (req, res) => {
  try {
    const data = await pegawaiService.getPegawaiById(req.params.id);

    res.status(200).json({ data, message: "pegawai ditemukan" });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: "Internal server error" });
  }
};

//menambhakan pegawai
export const tambahpegawai = async (req, res) => {
  const { nama, alamat, nik, no_telpon, id_peternakan, role, tanggal_masuk } =
    req.body;
  try {
    // Validasi input
    if (
      !nama ||
      !alamat ||
      !nik ||
      !no_telpon ||
      !tanggal_masuk ||
      !id_peternakan
    ) {
      throw new BadRequestError(
        `Nama, alamat, NIK, nomor telepon, tanggal masuk, dan id peternakan harus diisi`
      );
    }

    // Jika NIK belum terdaftar, buat data pegawai
    const data = await pegawaiService.createPegawai(
      nama,
      alamat,
      no_telpon,
      nik,
      role,
      tanggal_masuk,
      id_peternakan
    );

    res.status(201).json(data);
  } catch (error) {
    await prisma.$disconnect();
    if (error instanceof ConflictError) {
      res
        .status(400)
        .json({ message: error.message });
    }
    res.status(500).json({ message: "Terjadi kesalahan internal." });
  }
};

//mendapatkan riwayat penggajian
export const getRiwayatGaji = async (req, res) => {
  const { id } = req.params;

  try {
    const data = await pegawaiService.getRiwayatGaji(id);

    return res.status(200).json({
      message: "Data Gaji ditemukan",
      data,
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(500).json({
      message: error.message,
    });
  }
};

//Menambah riwayat penggajian
export const addRiwayatGaji = async (req, res) => {
  const { tanggal_transaksi, id_peternakan, jumlah, pihak_terkait, deskripsi } =
    req.body;

  const { id } = req.params;

  try {
    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pengeluaran",
      jumlah,
      pihak_terkait,
      id_peternakan,
      `Penggajian pegawai atas nama ${pihak_terkait}: ${deskripsi}`
    );

    const data = pegawaiService.addRiwayatGaji(id, transaksi.id);

    res.status(200).json({
      data: {
        id: data.id,
        pihak_terkait,
        jumlah,
        tanggal_transaksi,
        deskripsi,
      },
      message: "Operasional Gaji berhasil ditambahkan",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: error.message,
    });
  }
};

//mengedit data pegawai
export const editPegawai = async (req, res) => {
  const { nama, alamat, no_telpon, nik, id_peternakan, role, tanggal_masuk } =
    req.body;
  const { id } = req.params;
  try {
    const data = pegawaiService.updatePegawai(
      nama,
      alamat,
      no_telpon,
      nik,
      role,
      tanggal_masuk,
      id_peternakan,
      id
    );

    res.status(200).json({
      data,
      message: "pegawai berhasil diupdate",
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

//Menghapus pegawai
export const tambahPenghapusanPegawai = async (req, res) => {
  const { id, kategori, jenis, tanggal, deskripsi, id_peternakan } = req.body;
  // const id_peternakan = req.body.id_peternakan; // Pastikan variabel ini diambil dari req.body

  try {
    await prisma.$connect();
    const today = new Date(tanggal);
    await prisma.penghapusan.create({
      data: {
        tipe: "Pegawai",
        tanggal: today,
        id_peternakan: +id_peternakan, // Pastikan variabel ini diambil dari req.body
        jenis: jenis,
        kategori: kategori,
        deskripsi: deskripsi,
      },
    });

    await pegawaiService.deletePegawai(id);

    await prisma.$disconnect();

    res.status(201).json({
      message: "Berhasil menambahkan riwayat penghapusan",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};
