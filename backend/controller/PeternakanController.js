import prisma from "../config/db.js";
import BadRequestError from "../error/BadRequestError.js";
import ConflictError from "../error/ConflictError.js";
import NotFoudError from "../error/NotFoundError.js";
import AsetService from "../service/AsetService.js";
import KasService from "../service/KasService.js";
import PeternakanService from "../service/PeternakanService.js";
import TernakService from "../service/TernakService.js";

const peternakanService = new PeternakanService();
const ternakService = new TernakService();
const asetService = new AsetService();
const kasService = new KasService();

/**
 * Proses bisnis pencatatan peternakan
 */

//Mendapatkan detail peternakan berdasarkan id
export const getPeternakanById = async (req, res) => {
  const { id } = req.params;
  const now = new Date();
  try {
    if (!id)
      throw new NotFoudError(`Peternakan dengan id ${id} tidak ditemukan`);

    await prisma.$connect();
    const peternakan = await peternakanService.getPeternakanById(id);

    const jumlah_sapi = await ternakService.countTernak("Sapi", id);

    const jumlah_kambing = await ternakService.countTernak("Kambing", id);

    const jumlah_ayam = await ternakService.countTernak("Ayam", id);

    const jumlah_transportasi = await asetService.countAset("Transportasi", id);
    const jumlah_teknologi = await asetService.countAset("Teknologi", id);
    const jumlah_alat = await asetService.countAset("Alat & Mesin", id);
    const jumlah_lainnya = await asetService.countAset("Lainnya", id);

    let kas = await kasService.getOneKas(
      now.getMonth() + 1,
      now.getFullYear(),
      id
    );

    res.status(200).json({
      peternakan: peternakan,
      jumlah_kambing: jumlah_kambing,
      jumlah_sapi: jumlah_sapi,
      jumlah_ayam: jumlah_ayam,
      jumlah_transportasi: jumlah_transportasi,
      jumlah_teknologi: jumlah_teknologi,
      jumlah_alat: jumlah_alat,
      jumlah_lainnya: jumlah_lainnya,
      kas: kas.jumlah,
    });
  } catch (error) {
    console.error(error);
    if (error instanceof NotFoudError) {
      res.status(404).json({ message: error.message });
    }
    res.status(500).json({ message: "internal server error" });
  }
};

//Mendapatkan semua peternakan
export const getAllPeternakan = async (req, res) => {
  try {
    const { user } = req.query;

    const data = await peternakanService.getAllPeternakan(user);

    return res.status(200).json({
      data,
      message: "berhasil mendapatkan peternakan",
    });
  } catch (error) {
    res.status(500);
    console.error(error.message);
  }
};

//Membuat peternakan baru
export const createPeternakan = async (req, res) => {
  try {
    await prisma.$connect();
    const {
      name,
      lokasi,
      id_user,
      kapasitas_ayam,
      kapasitas_kambing,
      kapasitas_sapi,
      stok_fermentasi,
      stok_pupuk,
      stok_konsentrat,
      stok_rumput,
      tanggal,
      kas,
    } = req.body;

    if (
      !name ||
      !lokasi ||
      kapasitas_ayam === null ||
      kapasitas_kambing === null ||
      kapasitas_sapi === null ||
      stok_fermentasi === null ||
      stok_konsentrat === null ||
      stok_pupuk === null ||
      stok_rumput === null ||
      tanggal === null ||
      kas === null
    )
      throw new BadRequestError(
        "Nama, lokasi, kapasitas ayam, sapi, kambing, stok rumput, pupuk, konsentrat, fermentasi, dan kas tidak bolah kososng"
      );

    const tglBerdiri = new Date(tanggal);
    const now = new Date();

    const data = await peternakanService.createPeternakan(req.body, tglBerdiri);

    await prisma.transaksi.create({
      data: {
        tanggal_transaksi: tglBerdiri,
        jenis_transaksi: "Pemasukan",
        jumlah: +kas,
        pihak_terkait: "Pemilik Mulyo Farm",
        id_peternakan: data.id,
        deskripsi: "Modal Awal",
      },
    });

    await kasService.createKas(
      tglBerdiri.getMonth() + 1,
      tglBerdiri.getFullYear(),
      data.id,
      kas
    );

    if (
      now.getFullYear() !== tglBerdiri.getFullYear() ||
      now.getMonth() !== tglBerdiri.getMonth()
    ) {
      await kasService.createKas(
        now.getMonth() + 1,
        now.getFullYear(),
        data.id,
        kas
      );
    }
    await prisma.$disconnect();

    return res.status(201).json({
      data: {
        name,
        lokasi,
        id_manager: id_user,
      },
      message: "Berhasil menambahkan peternakan",
    });
  } catch (error) {
    console.log(error.message);
    if (error instanceof BadRequestError) {
      return res.status(400).json({
        message: error.message,
      });
    } else if (error instanceof ConflictError) {
      return res.status(409).json({ message: error.message });
    } else {
      return res.status(500).json({
        message: error.message,
      });
    }
  }
};

//Edit detail peternakan
export const editPeternakan = async (req, res) => {
  try {
    await prisma.$connect();
    const {
      name,
      lokasi,
      id_user,
      kapasitas_ayam,
      kapasitas_kambing,
      kapasitas_sapi,
    } = req.body;

    const { id } = req.params;

    if (
      !name ||
      !lokasi ||
      !id ||
      kapasitas_ayam === null ||
      kapasitas_kambing === null ||
      kapasitas_sapi === null
    )
      throw new BadRequestError(
        "id, Nama, lokasi, kapasitas ternak tidak boleh kosong"
      );

    const data = await peternakanService.updatePeternakan(req.body, id);

    return res.status(201).json({
      data,
      message: "Berhasil mengupdate peternakan",
    });
  } catch (error) {
    if (error instanceof BadRequestError) {
      res.status(400).json({
        message: error.message,
      });
    }
    res.status(500).json({
      message: error.message,
    });
  }
};

//menghapus peternakan
export const deletePeternakan = async (req, res) => {
  const { id } = req.params;
  try {
    if (!id) throw new BadRequestError("Peternakan tidak ditemukan");

    await peternakanService.deletePeternakan(id);

    return res.status(201).json({
      message: `Berhasil menghapus Peternakan dengan id: ${id}`,
    });
  } catch (error) {
    if (error instanceof BadRequestError) {
      res.status(400).json({
        message: error.message,
      });
    }
    return res.status(500).json({
      message: error.message,
    });
  }
};
