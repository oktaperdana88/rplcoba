import prisma from "../config/db.js";
import ConflictError from "../error/ConflictError.js";
import NotFoudError from "../error/NotFoundError.js";
import PeternakanService from "../service/PeternakanService.js";
import TernakService from "../service/TernakService.js";
import TransaksiService from "../service/TransaksiService.js";

const ternakService = new TernakService();
const transaksiService = new TransaksiService();
const peternakanService = new PeternakanService();

//Mendapatkan data seluruh ternak
export const getTernak = async (req, res) => {
  const { kategori, jenis, peternakan } = req.query;
  try {
    if (!peternakan) throw new NotFoudError("Peternakan tidak ditemukan");

    const data = await ternakService.getAllTernak(peternakan, kategori, jenis);

    return res.status(200).json({
      data,
      message: "Berhasil mengambil data",
    });
  } catch (error) {
    if (error instanceof NotFoudError) {
      return res.status(400).json({
        message: error.message,
      });
    }
    res.status(500).json({ message: error.message });
  }
};

//Menambahkan ternak
export const tambahTernak = async (req, res) => {
  const {
    nomor,
    jenis,
    berat,
    pihak_terkait,
    kategori,
    kelamin,
    cara_perolehan,
    induk_jantan,
    induk_betina,
    harga_beli,
    umur,
    tanggal_transaksi,
    id_peternakan,
  } = req.body;
  const date = new Date(tanggal_transaksi);
  try {
    const peternakan = await peternakanService.getPeternakanById(id_peternakan);

    //Mengecek kapasitas maksimal ternak
    const count = await ternakService.countTernak(kategori, id_peternakan);

    if (kategori === "Sapi") {
      if (count == peternakan.kapasitas_sapi)
        throw new ConflictError("Kapasitas Sapi penuh");

      await ternakService.getNomorSapi(nomor, id_peternakan);
    } else if (kategori === "Kambing") {
      if (count == peternakan.kapasitas_kambing) {
        throw new ConflictError("Kapasitas Kambing penuh");
      }
    } else {
      if (count == peternakan.kapasitas_ayam) {
        throw new ConflictError("Kapasitas Ayam penuh");
      }
    }

    // Membuat entri Transaksi pembelian
    if (cara_perolehan === "Beli") {
      await transaksiService.createTransaksi(
        tanggal_transaksi,
        "Pengeluaran",
        harga_beli,
        pihak_terkait,
        id_peternakan,
        `Pembelian ${kategori}`
      );
    }

    // Membuat entri ternak
    if (kategori === "Sapi") {
      await prisma.ternak.create({
        data: {
          nomor: +nomor,
          kategori,
          jenis,
          kelamin,
          tanggal_masuk: date.toISOString(),
          berat: +berat,
          cara_perolehan: cara_perolehan,
          induk_betina,
          induk_jantan,
          harga_beli: +harga_beli,
          umur: +umur,
          id_peternakan: +id_peternakan,
        },
      });
    } else {
      await prisma.ternak.create({
        data: {
          nomor: 0,
          kategori,
          jenis,
          tanggal_masuk: date.toISOString(),
          berat: +berat,
          kelamin,
          cara_perolehan: cara_perolehan,
          induk_betina,
          induk_jantan,
          harga_beli: +harga_beli,
          umur: +umur,
          id_peternakan: +id_peternakan,
        },
      });
    }

    await prisma.$disconnect();

    res.status(201).json({
      message: "Berhasil menambahkan ternak",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};

//Mendapatkan detail ternak
export const getDetailTernakById = async (req, res) => {
  const { id } = req.params; // Mengambil id dari URL
  try {
    const data = await ternakService.getTernakById(id);

    res.status(200).json({
      data,
      message: `Berhasil mendapatkan ternak dengan id ${id}`,
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(400).json({ message: error.message });
  }
};

//Mendapatkan pemeliharaan ternak
export const getPemeliharaanTernak = async (req, res) => {
  const { id } = req.params; // Ambil id_ternak dari parameter URL
  let sum = 0;
  try {
    const data = await ternakService.getPemeliharaanTernak(id);

    data.forEach((item) => (sum += item.transaksi.jumlah));

    res.status(200).json({
      data,
      sum: sum,
      message: "Berhasil mendapatkan riwayat pengobatan",
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ message: "Internal server error" });
  }
};

//Menambahkan riwayat pemeliharaan ternak
export const tambahpemeliharaanTernak = async (req, res) => {
  const {
    //untuk tabel transaksi
    // biaya,
    pihak_terkait,
    tanggal_transaksi,
    id_peternakan,
    //untuk tabel pemeliharaan sapi
    penyakit,
    obat,
    jumlah,
    deskripsi,
  } = req.body;

  const id_ternak = req.params.id;
  try {
    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pengeluaran",
      jumlah,
      pihak_terkait,
      id_peternakan,
      deskripsi
    );

    const data = await ternakService.addPengobatanTernak(
      obat,
      penyakit,
      transaksi.id,
      id_ternak
    );

    res.status(201).json({
      data,
      message: "Berhasil menambahkan riwayat pengobatan",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

// Mendapatkan riwayat penghapusan
export const getPenghapusanTernak = async (req, res) => {
  const { peternakan, tipe } = req.query;
  try {
    const data = await ternakService.getPenghapusan(peternakan, tipe);
    res.status(200).json({
      data,
      message: "Berhasil ambil data penghapusan ternak",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Penghapusan ternak
export const hapusTernak = async (req, res) => {
  const { id, jenis, kategori, tanggal, deskripsi, id_peternakan } = req.body;

  try {
    await prisma.penghapusan.create({
      data: {
        tipe: "Ternak",
        tanggal: new Date(tanggal),
        id_peternakan: +id_peternakan, // Pastikan variabel ini diambil dari req.body
        jenis: jenis,
        kategori: kategori,
        deskripsi: deskripsi,
      },
    });

    await ternakService.deleteTernak(id);

    res.status(200).json({
      message: `Ternak dengan ID ${id} berhasil dihapus.`,
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

// Edit detail ternak
export const editTernak = async (req, res) => {
  const {
    nomor,
    jenis,
    berat,
    kategori,
    kelamin,
    cara_perolehan,
    induk_jantan,
    induk_betina,
    harga_beli,
    umur,
    tanggal_transaksi,
    id_peternakan,
  } = req.body;
  const id = req.params.id;

  try {
    await prisma.$connect();
    const data = await prisma.ternak.update({
      where: {
        id: +id,
      },
      data: {
        nomor: +nomor,
        kategori: kategori,
        jenis: jenis,
        berat: +berat,
        kelamin: kelamin,
        cara_perolehan: cara_perolehan,
        induk_jantan: induk_jantan,
        induk_betina: induk_betina,
        harga_beli: +harga_beli,
        umur: +umur,
        tanggal_masuk: new Date(tanggal_transaksi).toISOString(),
        id_peternakan: +id_peternakan,
      },
    });

    await prisma.$disconnect();

    res.status(200).json({
      data,
      message: `Update ternak nomor ${nomor} sukses`,
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

//Penjualan tenak
export const jualTernak = async (req, res) => {
  const {
    kategori,
    subkategori,
    detail,
    tanggal_transaksi,
    nominal,
    pembeli,
    id_peternakan,
  } = req.body;
  const { id } = await req.params;
  try {
    const transaksi = await transaksiService.createTransaksi(
      tanggal_transaksi,
      "Pemasukan",
      nominal,
      pembeli,
      id_peternakan,
      `Penjualan ${kategori} ${subkategori} ${detail}`
    );
    await ternakService.deleteTernak(id);

    res.status(201).json({ transaksi });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(400).json({ msg: error.message });
  }
};
