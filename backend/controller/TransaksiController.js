import prisma from "../config/db.js";
import PDFDocument from "pdfkit-table";
import TransaksiService from "../service/TransaksiService.js";

const transaksiService = new TransaksiService();

process.env.TZ = "Asia/Jakarta";

// mendapatkan semua transaksi
export const getTransaksi = async (req, res) => {
  const { peternakan, month, year, jenis, deskripsi } = req.query;
  let where;
  let sum = {
    _sum: {
      jumlah: 0,
    },
  };
  try {
    await prisma.$connect();
    if (month && year) {
      const firstDay = new Date(+year, +month - 1, 1);
      const lastDay = new Date(+year, +month, 1);

      where = {
        tanggal_transaksi: {
          gte: firstDay,
          lte: lastDay,
        },
        jenis_transaksi: {
          contains: jenis,
        },
        id_peternakan: +peternakan,
      };

      sum = await prisma.transaksi.aggregate({
        _sum: {
          jumlah: true,
        },
        where,
      });
    } else {
      where = {
        jenis_transaksi: {
          contains: jenis,
        },
        deskripsi: {
          contains: deskripsi,
        },
        id_peternakan: +peternakan,
      };
    }

    const data = await prisma.transaksi.findMany({
      where,
    });

    let kas;

    if (jenis === "Pemasukan") {
      kas = await prisma.kas.findFirst({
        where: {
          bulan: String(month == 1 ? "12" : month - 1),
          tahun: String(month == 1 ? year - 1 : year),
          id_peternakan: +peternakan,
        },
      });
    } else {
      kas = await prisma.kas.findFirst({
        where: {
          bulan: String(month),
          tahun: String(year),
          id_peternakan: +peternakan,
        },
      });
    }

    if (!kas) {
      kas = {
        jumlah: 0,
      };
    }

    await prisma.$disconnect();
    res.status(200).json({
      message: "berhasil mendapatkan data transaksi",
      data,
      kas: kas.jumlah,
      sum: sum?._sum?.jumlah,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

export const getTransaksiById = async (req, res) => {
  try {
    await prisma.$connect();
    const response = await prisma.transaksi.findUnique({
      where: {
        id: Number(req.params.id),
      },
    });
    await prisma.$disconnect();
    res.status(200).json(response);
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(505).json({ message: "Internal server error" });
  }
};

// membuat transaksi baru
export const createTransaksi = async (req, res) => {
  const {
    tanggal_transaksi,
    jenis,
    harga,
    pihak_terkait,
    id_peternakan,
    deskripsi,
  } = req.body;
  try {
    const response = await transaksiService.createTransaksi(
      tanggal_transaksi,
      jenis,
      harga,
      pihak_terkait,
      id_peternakan,
      deskripsi
    );
    res.status(200).json(response);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

// Download neraca pdf
export const downloadTransaksiPDF = async (req, res) => {
  const { peternakan, bulan, tahun } = req.query;
  try {
    let pdfDoc = new PDFDocument({ margin: 30, size: "A4" });
    res.setHeader("Content-Type", "application/pdf");
    res.setHeader("Content-Disposition", "attachment; filename=transaksi.pdf");
    pdfDoc.pipe(res);
    pdfDoc = await transaksiService.downloadPDF(
      peternakan,
      bulan,
      tahun,
      pdfDoc
    );
    pdfDoc.end();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//Download neraca excel
export const downloadTransaksiExcel = async (req, res) => {
  const { peternakan, bulan, tahun } = req.query;
  try {
    const workbook = await transaksiService.downloadExcel(
      peternakan,
      bulan,
      tahun
    );
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader("Content-Disposition", "attachment; filename=transaksi.xlsx");

    await workbook.xlsx.write(res);
    res.end();
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};

//Ringkasan transaksi
export const getSummary = async (req, res) => {
  const { peternakan } = req.query;

  const now = new Date();

  const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
  const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 1);

  try {
    await prisma.$connect();
    const summary = await prisma.transaksi.groupBy({
      by: ["jenis_transaksi"],
      _sum: {
        jumlah: true,
      },
      where: {
        tanggal_transaksi: {
          gte: firstDay,
          lte: lastDay,
        },
        id_peternakan: +peternakan,
      },
    });

    const pet = await prisma.peternakan.findUnique({
      where: {
        id: +peternakan,
      },
      select: {
        kas: true,
        nama: true,
      },
    });

    await prisma.$disconnect();

    const arr = [0, 0];

    {
      summary
        .filter((item) => item.jenis_transaksi === "Pemasukan")
        .map((item, index) => {
          arr[index] = item._sum.jumlah;
        });
    }

    {
      summary
        .filter((item) => item.jenis_transaksi === "Pengeluaran")
        .map((item, index) => {
          arr[index + 1] = item._sum.jumlah;
        });
    }

    let kas = await prisma.kas.findFirst({
      where: {
        id_peternakan: +peternakan,
        bulan: String(now.getMonth() + 1),
        tahun: String(now.getFullYear()),
      },
    });

    if (!kas) {
      const allKas = await prisma.kas.findMany({
        where: {
          id_peternakan: +peternakan,
        },
      });

      const index = allKas.length;

      const kasBefore = allKas[index - 1];

      const bulan = now.getMonth() + 1;
      const tahun = now.getFullYear();

      await prisma.kas.create({
        data: {
          bulan: String(bulan),
          tahun: String(tahun),
          jumlah: kasBefore.jumlah,
          id_peternakan: kasBefore.id_peternakan,
        },
      });
    }

    res.status(200).json({
      kas: kas.jumlah,
      pemasukan: arr[0],
      pengeluaran: arr[1],
      nama: pet.nama || "",
    });
  } catch (error) {
    await prisma.$disconnect();
    console.error(error);
    res.status(500).json({
      message: error.message,
    });
  }
};

export const getSummaryByYearRange = async (req, res) => {
  const { peternakan, dateStart, dateEnd } = req.query;

  const start = new Date(dateStart);
  const end = new Date(dateEnd);

  // Menghitung selisih bulan antara dateStart dan dateEnd
  const monthDiff =
    (end.getFullYear() - start.getFullYear()) * 12 +
    (end.getMonth() - start.getMonth());
  // Membuat array bulan dengan format "MM-YYYY"
  const labels = Array.from({ length: monthDiff + 1 }, (_, index) => {
    const date = new Date(start.getFullYear(), start.getMonth() + index);
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const year = date.getFullYear();
    return `${month}-${year}`;
  });

  // Inisialisasi array pemasukan dan pengeluaran dengan nilai 0
  const pemasukan = Array(monthDiff + 1).fill(0);
  const pengeluaran = Array(monthDiff + 1).fill(0);

  try {
    await prisma.$connect();
    const summary = await prisma.transaksi.groupBy({
      by: ["jenis_transaksi", "tanggal_transaksi"],
      _sum: {
        jumlah: true,
      },
      where: {
        tanggal_transaksi: {
          gte: start,
          lte: end,
        },
        id_peternakan: +peternakan,
      },
    });

    summary.forEach((item) => {
      const monthIndex = labels.indexOf(
        `${(item.tanggal_transaksi.getMonth() + 1)
          .toString()
          .padStart(2, "0")}-${item.tanggal_transaksi.getFullYear()}`
      );

      if (monthIndex !== -1) {
        if (item.jenis_transaksi === "Pemasukan") {
          pemasukan[monthIndex] += item._sum.jumlah;
        } else if (item.jenis_transaksi === "Pengeluaran") {
          pengeluaran[monthIndex] += item._sum.jumlah;
        }
      }
    });

    await prisma.$disconnect();

    res.status(200).json({
      labels,
      pemasukan,
      pengeluaran,
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(500).json({
      message: error.message,
    });
  }
};
