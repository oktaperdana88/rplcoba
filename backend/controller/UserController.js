import prisma from "../config/db.js";
import argon2 from "argon2";
import UserService from "../service/UserService.js";
import NotFoudError from "../error/NotFoundError.js";
import BadRequestError from "../error/BadRequestError.js";

const userService = new UserService();

//Menghapus user
export const deleteUser = async (req, res) => {
  const { id } = req.params;
  try {
    await userService.delete(id);
    return res.status(201).json({
      message: `Berhasil menghapus User dengan id: ${id}`,
    });
  } catch (error) {
    await prisma.$disconnect();
    res.status(404).json({
      message: error.message,
    });
  }
};

//mendapatkan semua user
export const getAllUser = async (req, res) => {
  const { role } = req.query;
  try {
    const data = await userService.getAllUser(role);

    return res.status(200).json({
      data,
      message: "Berhasil mendapatkan data user",
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

//Mengubah password ketika sudah login
export const changePassword = async (req, res) => {
  try {
    const { id } = req.params;
    const { password, baru, confirm } = req.body;

    if (!id) throw new NotFoudError("User tidak ditemukan");

    if (!password || !baru || !confirm)
      throw new NotFoudError("Password tidak boleh kosong");

    const user = await userService.getUserById(id);

    const match = await argon2.verify(user.password, password);

    if (!match) throw new BadRequestError("Password saat ini tidak sesuai");

    if (baru !== confirm)
      throw new BadRequestError(
        "Password baru dan konfirmasi password harus sama"
      );

    const hash = await argon2.hash(baru);

    const data = await userService.updateUser(hash, user.id);

    return res.status(201).json({
      data,
      message: "Berhasil mengganti password",
    });
  } catch (error) {
    console.error(error);
    if (error instanceof NotFoudError) {
      return res.status(404).json({
        message: error.message,
      });
    } else if (error instanceof BadRequestError) {
      return res.status(400).json({
        message: error.message,
      });
    }
    res.status(500).json({
      message: error.message,
    });
  }
};

//Mengganti role user
export const changeRole = async (req, res) => {
  const { id } = req.params;

  const { role } = req.body;
  try {
    if (!id || !role)
      throw new BadRequestError("User dan role tidak boleh kosong");

    const data = await userService.changeRole(role, id);

    return res.status(201).json({
      data,
      message: "Berhasil mengganti role",
    });
  } catch (error) {
    if (error instanceof BadRequestError) {
      return res.status(400).json({
        message: error.message,
      });
    }
    res.status(500).json({
      message: error.message,
    });
  }
};

//Mendapatkan detail user berdasarkan id
export const getUserById = async (req, res) => {
  const { id } = req.params;

  try {
    const data = await userService.getUserById(id);

    return res.status(200).json({
      data,
      message: "Berhasil mendapatkan data user",
    });
  } catch (error) {
    if (error instanceof NotFoudError) {
      return res.status(404).json({
        message: error.message,
      });
    }
    res.status(500).json({
      message: error.message,
    });
  }
};
