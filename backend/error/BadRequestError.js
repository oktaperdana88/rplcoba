/**
 * Custom error untuk menangani kesalahan input klien
 */

class BadRequestError extends Error {
    constructor(message) {
        super(message)
    }
}

export default BadRequestError