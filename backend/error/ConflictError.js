/**
 * Kesalahan validasi terhadap data di database gagal, contoh kapasitas penuh
 */

class ConflictError extends Error {
    constructor(message) {
        super(message)
    }
}

export default ConflictError