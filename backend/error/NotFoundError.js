/**
 * Kustom error untuk menangani data tidak ditemukan
 */

class NotFoudError extends Error {
    constructor(message) {
        super(message)
    }
}

export default NotFoudError