import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import InventorisRoute from "./routes/InventorisRoute.js";
import AsetRoute from "./routes/AsetRoute.js";
import TernakRoute from "./routes/TernakRoute.js";
import peternakanRouter from "./routes/PeternakanRoute.js";
import pegawaiRouter from "./routes/PegawaiRoute.js";
import transaksiRouter from "./routes/TransaksiRoute.js";
import {
  changePassword,
  login,
  logout,
  refresh,
  register,
  resetPassword,
} from "./controller/AuthController.js";
import cookieParser from "cookie-parser";
import userRouter from "./routes/UserRoutes.js";
import { verifyJWT, verifyResetPassword } from "./middleware/VerifyJwt.js";

dotenv.config();

const app = express();

app.use(
  cors({
    credentials: true,
    origin: process.env.ORIGIN || "https://rplcoba.vercel.app",
  })
);

app.use(cookieParser());
app.use(express.json());
app.post("/api/login", login);
app.post("/api/logout", logout);
app.post("/api/refresh", refresh);
app.post("/api/register", register);
app.post("/api/reset", resetPassword);
app.post("/api/change-password", verifyResetPassword, changePassword);
app.use("/api/inventoris", verifyJWT, InventorisRoute);
app.use("/api/aset", verifyJWT, AsetRoute);
app.use("/api/ternak", verifyJWT, TernakRoute);
app.use("/api/peternakan/", verifyJWT, peternakanRouter);
app.use("/api/pegawai/", verifyJWT, pegawaiRouter);
app.use("/api/users", verifyJWT, userRouter);
app.use("/api/transaksi", verifyJWT, transaksiRouter);

app.listen(process.env.APP_PORT, () => {
  console.log("Server running");
  console.log(new Date());
  console.log(process.env.APP_PORT);
});
