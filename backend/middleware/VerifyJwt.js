import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config();

//Verifikasi akses token
export const verifyJWT = (req, res, next) => {
  const authHeader = req?.headers["authorization"];
  if (!authHeader) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }

  const token = authHeader.split(" ")[1];
  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    next();
  } catch (error) {
    return res.status(403).json({
      message: error.message,
    });
  }
};

//Verifikasi reset password
export const verifyResetPassword = (req, res, next) => {
  const authHeader = req?.headers["authorization"];
  if (!authHeader) {
    return res.status(401);
  }

  const token = authHeader.split(" ")[1];
  try {
    const decoded = jwt.verify(token, process.env.RESET_PASSWORD_TOKEN_SECRET);
    req.user = decoded.id;
    next();
  } catch (error) {
    console.error(error);
    return res.status(403).json({
      message: "Token kadaluarsa, Harap kirimkan email ulang",
    });
  }
};

//Verifikasi role pemilik
export const checkPemilik = (req, res, next) => {
  const role = req.user.role;

  if (role === "Pemilik") next();
  else return res.status(403);
};
