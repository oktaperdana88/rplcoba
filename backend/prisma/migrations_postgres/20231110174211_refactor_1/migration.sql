-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "email" VARCHAR(100) NOT NULL,
    "password" VARCHAR(512) NOT NULL,
    "role" VARCHAR(7) NOT NULL,
    "refresh_token" TEXT,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Peternakan" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "lokasi" TEXT NOT NULL,
    "kapasitas_sapi" SMALLINT NOT NULL,
    "kapasitas_kambing" SMALLINT NOT NULL,
    "kapasitas_ayam" SMALLINT NOT NULL,
    "stok_rumput" REAL NOT NULL,
    "stok_konsentrat" REAL NOT NULL,
    "stok_fermentasi" REAL NOT NULL,
    "stok_pupuk" REAL NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_user" INTEGER,

    CONSTRAINT "Peternakan_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Pegawai" (
    "id" SERIAL NOT NULL,
    "nama" VARCHAR(50) NOT NULL,
    "nik" VARCHAR(16) NOT NULL,
    "no_telpon" VARCHAR(13) NOT NULL,
    "alamat" TEXT NOT NULL,
    "role" VARCHAR(10) NOT NULL,
    "tanggal_masuk" DATE NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_peternakan" INTEGER NOT NULL,

    CONSTRAINT "Pegawai_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Aset" (
    "id" SERIAL NOT NULL,
    "nama" VARCHAR(50) NOT NULL,
    "jenis" VARCHAR(18) NOT NULL,
    "harga_beli" INTEGER,
    "tanggal_dapat" DATE,
    "status_kepemilikan" VARCHAR(14) NOT NULL,
    "deskripsi" TEXT,
    "metode_pembayaran" VARCHAR(50),
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_peternakan" INTEGER NOT NULL,

    CONSTRAINT "Aset_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Transaksi" (
    "id" SERIAL NOT NULL,
    "deskripsi" TEXT,
    "jenis_transaksi" VARCHAR(12) NOT NULL,
    "pihak_terkait" VARCHAR(12),
    "tanggal_transaksi" DATE NOT NULL,
    "jumlah" INTEGER NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_peternakan" INTEGER NOT NULL,

    CONSTRAINT "Transaksi_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Ternak" (
    "id" SERIAL NOT NULL,
    "nomor" SMALLINT,
    "jenis" VARCHAR(16) NOT NULL,
    "kategori" VARCHAR(16) NOT NULL,
    "berat" DOUBLE PRECISION NOT NULL,
    "kelamin" VARCHAR(6) NOT NULL,
    "induk_jantan" VARCHAR(16) NOT NULL DEFAULT 'Tidak Diketahui',
    "induk_betina" VARCHAR(16) NOT NULL DEFAULT 'Tidak Diketahui',
    "cara_perolehan" VARCHAR(10) NOT NULL,
    "harga_beli" INTEGER NOT NULL DEFAULT 0,
    "umur" SMALLINT NOT NULL DEFAULT 0,
    "tanggal_dapat" DATE NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_peternakan" INTEGER NOT NULL,

    CONSTRAINT "Ternak_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PengobatanTernak" (
    "id" SERIAL NOT NULL,
    "obat" VARCHAR(50) NOT NULL,
    "penyakit" VARCHAR(50) NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_transaksi" INTEGER NOT NULL,
    "id_ternak" INTEGER NOT NULL,

    CONSTRAINT "PengobatanTernak_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OperasionalAset" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_aset" INTEGER NOT NULL,
    "id_transaksi" INTEGER NOT NULL,

    CONSTRAINT "OperasionalAset_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OperasionalGaji" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_transaksi" INTEGER NOT NULL,
    "id_pegawai" INTEGER NOT NULL,

    CONSTRAINT "OperasionalGaji_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OperasionalInventoris" (
    "id" SERIAL NOT NULL,
    "jenis" VARCHAR(50) NOT NULL,
    "ternak" VARCHAR(50),
    "riwayat" VARCHAR(12) NOT NULL,
    "tanggal" TIMESTAMPTZ(6) NOT NULL,
    "deskripsi" TEXT,
    "berat" INTEGER NOT NULL,
    "createdAt" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(6) NOT NULL,
    "id_peternakan" INTEGER NOT NULL,
    "id_transaksi" INTEGER,

    CONSTRAINT "OperasionalInventoris_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Penghapusan" (
    "id" SERIAL NOT NULL,
    "jenis" VARCHAR(50) NOT NULL,
    "tipe" VARCHAR(50) NOT NULL,
    "kategori" VARCHAR(50) NOT NULL,
    "tanggal" DATE NOT NULL,
    "deskripsi" TEXT NOT NULL,
    "id_peternakan" INTEGER NOT NULL,

    CONSTRAINT "Penghapusan_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Kas" (
    "id" SERIAL NOT NULL,
    "bulan" VARCHAR(2) NOT NULL,
    "tahun" VARCHAR(4) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "jumlah" INTEGER NOT NULL,

    CONSTRAINT "Kas_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "PengobatanTernak_id_transaksi_key" ON "PengobatanTernak"("id_transaksi");

-- CreateIndex
CREATE UNIQUE INDEX "OperasionalAset_id_transaksi_key" ON "OperasionalAset"("id_transaksi");

-- CreateIndex
CREATE UNIQUE INDEX "OperasionalGaji_id_transaksi_key" ON "OperasionalGaji"("id_transaksi");

-- CreateIndex
CREATE UNIQUE INDEX "OperasionalInventoris_id_transaksi_key" ON "OperasionalInventoris"("id_transaksi");

-- AddForeignKey
ALTER TABLE "Peternakan" ADD CONSTRAINT "Peternakan_id_user_fkey" FOREIGN KEY ("id_user") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Pegawai" ADD CONSTRAINT "Pegawai_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Aset" ADD CONSTRAINT "Aset_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Transaksi" ADD CONSTRAINT "Transaksi_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Ternak" ADD CONSTRAINT "Ternak_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PengobatanTernak" ADD CONSTRAINT "PengobatanTernak_id_ternak_fkey" FOREIGN KEY ("id_ternak") REFERENCES "Ternak"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PengobatanTernak" ADD CONSTRAINT "PengobatanTernak_id_transaksi_fkey" FOREIGN KEY ("id_transaksi") REFERENCES "Transaksi"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalAset" ADD CONSTRAINT "OperasionalAset_id_aset_fkey" FOREIGN KEY ("id_aset") REFERENCES "Aset"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalAset" ADD CONSTRAINT "OperasionalAset_id_transaksi_fkey" FOREIGN KEY ("id_transaksi") REFERENCES "Transaksi"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalGaji" ADD CONSTRAINT "OperasionalGaji_id_pegawai_fkey" FOREIGN KEY ("id_pegawai") REFERENCES "Pegawai"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalGaji" ADD CONSTRAINT "OperasionalGaji_id_transaksi_fkey" FOREIGN KEY ("id_transaksi") REFERENCES "Transaksi"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalInventoris" ADD CONSTRAINT "OperasionalInventoris_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OperasionalInventoris" ADD CONSTRAINT "OperasionalInventoris_id_transaksi_fkey" FOREIGN KEY ("id_transaksi") REFERENCES "Transaksi"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Penghapusan" ADD CONSTRAINT "Penghapusan_id_peternakan_fkey" FOREIGN KEY ("id_peternakan") REFERENCES "Peternakan"("id") ON DELETE NO ACTION ON UPDATE CASCADE;
