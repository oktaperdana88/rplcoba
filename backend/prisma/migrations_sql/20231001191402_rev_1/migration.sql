-- CreateTable
CREATE TABLE `User` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,
    `email` VARCHAR(191) NOT NULL,
    `password` VARCHAR(191) NOT NULL,
    `role` VARCHAR(191) NOT NULL,
    `refresh_token` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Peternakan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `lokasi` TEXT NOT NULL,
    `kapasitas_sapi` SMALLINT NOT NULL,
    `kapasitas_kambing` SMALLINT NOT NULL,
    `kapasitas_ayam` SMALLINT NOT NULL,
    `stok_rumput` DOUBLE NOT NULL,
    `stok_konsentrat` DOUBLE NOT NULL,
    `stok_fermentasi` DOUBLE NOT NULL,
    `stok_pupuk` DOUBLE NOT NULL,
    `id_user` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Pakan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tanggal` VARCHAR(50) NOT NULL,
    `berat` DOUBLE NOT NULL,
    `jenis` VARCHAR(50) NOT NULL,
    `deskripsi` VARCHAR(100) NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Aset` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `jenis` VARCHAR(18) NOT NULL,
    `harga_beli` INTEGER NOT NULL,
    `tanggal` DATE NULL,
    `status_kepemilikan` VARCHAR(14) NOT NULL,
    `deskripsi` TEXT NOT NULL,
    `metode_pembayaran` TEXT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Transaksi` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `deskripsi` TEXT NOT NULL,
    `jenis_transaksi` VARCHAR(12) NOT NULL,
    `tanggal` DATE NULL,
    `harga` INTEGER NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Sapi` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nomor_sapi` SMALLINT NOT NULL,
    `jenis` VARCHAR(16) NOT NULL,
    `berat` DOUBLE NOT NULL,
    `kelamin` VARCHAR(6) NOT NULL,
    `cara_perolehan` VARCHAR(8) NOT NULL,
    `harga_beli` INTEGER NOT NULL,
    `umur` SMALLINT NOT NULL,
    `tanggal_masuk` DATE NULL,
    `total_biaya` INTEGER NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `PemeliharaanSapi` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `obat` VARCHAR(50) NOT NULL,
    `penyakit` VARCHAR(50) NOT NULL,
    `pihak_terkait` VARCHAR(50) NOT NULL,
    `id_transaksi` INTEGER NOT NULL,
    `id_sapi` INTEGER NOT NULL,

    UNIQUE INDEX `PemeliharaanSapi_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Pegawai` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `alamat` TEXT NOT NULL,
    `gaji_terakhir` INTEGER NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `RiwayatGaji` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `id_transaksi` INTEGER NOT NULL,
    `id_pegawai` INTEGER NOT NULL,

    UNIQUE INDEX `RiwayatGaji_id_transaksi_key`(`id_transaksi`),
    UNIQUE INDEX `RiwayatGaji_id_pegawai_key`(`id_pegawai`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Limbah` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `stok` DOUBLE NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Penjualan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `id_transaksi` INTEGER NOT NULL,
    `pembeli` VARCHAR(50) NOT NULL,

    UNIQUE INDEX `Penjualan_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `OperasionalAset` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `pihak_terkait` VARCHAR(50) NOT NULL,
    `id_transaksi` INTEGER NOT NULL,
    `id_aset` INTEGER NOT NULL,

    UNIQUE INDEX `OperasionalAset_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `perkawinan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `kambing` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Pakan` ADD CONSTRAINT `Pakan_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Aset` ADD CONSTRAINT `Aset_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Transaksi` ADD CONSTRAINT `Transaksi_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Sapi` ADD CONSTRAINT `Sapi_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PemeliharaanSapi` ADD CONSTRAINT `PemeliharaanSapi_id_sapi_fkey` FOREIGN KEY (`id_sapi`) REFERENCES `Sapi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PemeliharaanSapi` ADD CONSTRAINT `PemeliharaanSapi_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Pegawai` ADD CONSTRAINT `Pegawai_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `RiwayatGaji` ADD CONSTRAINT `RiwayatGaji_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `RiwayatGaji` ADD CONSTRAINT `RiwayatGaji_id_pegawai_fkey` FOREIGN KEY (`id_pegawai`) REFERENCES `Pegawai`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Limbah` ADD CONSTRAINT `Limbah_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Penjualan` ADD CONSTRAINT `Penjualan_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalAset` ADD CONSTRAINT `OperasionalAset_id_aset_fkey` FOREIGN KEY (`id_aset`) REFERENCES `Aset`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalAset` ADD CONSTRAINT `OperasionalAset_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
