/*
  Warnings:

  - A unique constraint covering the columns `[id_transaksi]` on the table `Pakan` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE `pakan` ADD COLUMN `id_transaksi` INTEGER NULL;

-- CreateIndex
CREATE UNIQUE INDEX `Pakan_id_transaksi_key` ON `Pakan`(`id_transaksi`);

-- AddForeignKey
ALTER TABLE `Pakan` ADD CONSTRAINT `Pakan_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
