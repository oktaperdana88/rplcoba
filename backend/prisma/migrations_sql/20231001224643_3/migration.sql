/*
  Warnings:

  - You are about to alter the column `tanggal` on the `pakan` table. The data in that column could be lost. The data in that column will be cast from `VarChar(50)` to `DateTime(3)`.

*/
-- AlterTable
ALTER TABLE `pakan` MODIFY `tanggal` DATETIME(3) NOT NULL;
