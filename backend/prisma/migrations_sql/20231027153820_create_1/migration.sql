/*
  Warnings:

  - You are about to drop the column `tanggal` on the `aset` table. All the data in the column will be lost.
  - You are about to drop the column `harga` on the `transaksi` table. All the data in the column will be lost.
  - You are about to drop the column `tanggal` on the `transaksi` table. All the data in the column will be lost.
  - You are about to drop the `kambing` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `limbah` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `pakan` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `pemeliharaansapi` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `penjualan` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `perkawinan` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `sapi` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `id_pegawai` to the `Aset` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lokasi` to the `Aset` table without a default value. This is not possible if the table is not empty.
  - Added the required column `plat` to the `Aset` table without a default value. This is not possible if the table is not empty.
  - Added the required column `umur` to the `Aset` table without a default value. This is not possible if the table is not empty.
  - Added the required column `nik` to the `Pegawai` table without a default value. This is not possible if the table is not empty.
  - Added the required column `jumlah` to the `Transaksi` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `limbah` DROP FOREIGN KEY `Limbah_id_peternakan_fkey`;

-- DropForeignKey
ALTER TABLE `pakan` DROP FOREIGN KEY `Pakan_id_peternakan_fkey`;

-- DropForeignKey
ALTER TABLE `pakan` DROP FOREIGN KEY `Pakan_id_transaksi_fkey`;

-- DropForeignKey
ALTER TABLE `pemeliharaansapi` DROP FOREIGN KEY `PemeliharaanSapi_id_sapi_fkey`;

-- DropForeignKey
ALTER TABLE `pemeliharaansapi` DROP FOREIGN KEY `PemeliharaanSapi_id_transaksi_fkey`;

-- DropForeignKey
ALTER TABLE `penjualan` DROP FOREIGN KEY `Penjualan_id_transaksi_fkey`;

-- DropForeignKey
ALTER TABLE `sapi` DROP FOREIGN KEY `Sapi_id_peternakan_fkey`;

-- AlterTable
ALTER TABLE `aset` DROP COLUMN `tanggal`,
    ADD COLUMN `id_pegawai` INTEGER NOT NULL,
    ADD COLUMN `lokasi` VARCHAR(14) NOT NULL,
    ADD COLUMN `plat` VARCHAR(14) NOT NULL,
    ADD COLUMN `tanggal_dapat` DATE NULL,
    ADD COLUMN `umur` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `pegawai` ADD COLUMN `nik` VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE `transaksi` DROP COLUMN `harga`,
    DROP COLUMN `tanggal`,
    ADD COLUMN `jumlah` INTEGER NOT NULL,
    ADD COLUMN `pihak_terkait` VARCHAR(12) NULL,
    ADD COLUMN `tanggal_dicatat` DATE NULL,
    ADD COLUMN `tanggal_transaksi` DATE NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `refresh_token` VARCHAR(191) NULL;

-- DropTable
DROP TABLE `kambing`;

-- DropTable
DROP TABLE `limbah`;

-- DropTable
DROP TABLE `pakan`;

-- DropTable
DROP TABLE `pemeliharaansapi`;

-- DropTable
DROP TABLE `penjualan`;

-- DropTable
DROP TABLE `perkawinan`;

-- DropTable
DROP TABLE `sapi`;

-- CreateTable
CREATE TABLE `Inventoris` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tanggal` DATETIME(3) NOT NULL,
    `berat` DOUBLE NOT NULL,
    `jenis` VARCHAR(50) NOT NULL,
    `deskripsi` VARCHAR(100) NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Ternak` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nomor` SMALLINT NOT NULL,
    `jenis` VARCHAR(16) NOT NULL,
    `kategori` VARCHAR(16) NOT NULL,
    `berat` DOUBLE NOT NULL,
    `kelamin` VARCHAR(6) NOT NULL,
    `induk_jantan` VARCHAR(16) NULL,
    `induk_betina` VARCHAR(16) NULL,
    `cara_perolehan` VARCHAR(8) NOT NULL,
    `harga_beli` INTEGER NOT NULL,
    `umur` SMALLINT NOT NULL,
    `tanggal_dapat` DATE NULL,
    `total_biaya` INTEGER NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `PengobatanTernak` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `obat` VARCHAR(50) NOT NULL,
    `penyakit` VARCHAR(50) NOT NULL,
    `id_transaksi` INTEGER NOT NULL,
    `id_ternak` INTEGER NOT NULL,

    UNIQUE INDEX `PengobatanTernak_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Laporan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `file` VARCHAR(50) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Inventoris` ADD CONSTRAINT `Inventoris_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Ternak` ADD CONSTRAINT `Ternak_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PengobatanTernak` ADD CONSTRAINT `PengobatanTernak_id_ternak_fkey` FOREIGN KEY (`id_ternak`) REFERENCES `Ternak`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PengobatanTernak` ADD CONSTRAINT `PengobatanTernak_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
