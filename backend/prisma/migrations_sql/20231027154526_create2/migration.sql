/*
  Warnings:

  - You are about to drop the column `id_pegawai` on the `aset` table. All the data in the column will be lost.
  - You are about to drop the column `lokasi` on the `aset` table. All the data in the column will be lost.
  - You are about to drop the column `plat` on the `aset` table. All the data in the column will be lost.
  - You are about to alter the column `tanggal` on the `inventoris` table. The data in that column could be lost. The data in that column will be cast from `DateTime(3)` to `DateTime`.
  - You are about to alter the column `name` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(50)`.
  - You are about to alter the column `email` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(100)`.
  - You are about to alter the column `role` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(4)`.
  - A unique constraint covering the columns `[id_transaksi]` on the table `Inventoris` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `pihak_terkait` to the `PengobatanTernak` table without a default value. This is not possible if the table is not empty.
  - Made the column `tanggal_dicatat` on table `transaksi` required. This step will fail if there are existing NULL values in that column.
  - Made the column `tanggal_transaksi` on table `transaksi` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `aset` DROP COLUMN `id_pegawai`,
    DROP COLUMN `lokasi`,
    DROP COLUMN `plat`,
    MODIFY `harga_beli` INTEGER NULL,
    MODIFY `deskripsi` TEXT NULL;

-- AlterTable
ALTER TABLE `inventoris` ADD COLUMN `id_transaksi` INTEGER NULL,
    MODIFY `tanggal` DATETIME NOT NULL;

-- AlterTable
ALTER TABLE `pengobatanternak` ADD COLUMN `pihak_terkait` VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE `peternakan` MODIFY `stok_rumput` FLOAT NOT NULL,
    MODIFY `stok_konsentrat` FLOAT NOT NULL,
    MODIFY `stok_fermentasi` FLOAT NOT NULL,
    MODIFY `stok_pupuk` FLOAT NOT NULL;

-- AlterTable
ALTER TABLE `transaksi` MODIFY `deskripsi` TEXT NULL,
    MODIFY `tanggal_dicatat` DATE NOT NULL,
    MODIFY `tanggal_transaksi` DATE NOT NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `name` VARCHAR(50) NOT NULL,
    MODIFY `email` VARCHAR(100) NOT NULL,
    MODIFY `password` VARCHAR(512) NOT NULL,
    MODIFY `role` VARCHAR(4) NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX `Inventoris_id_transaksi_key` ON `Inventoris`(`id_transaksi`);

-- AddForeignKey
ALTER TABLE `Inventoris` ADD CONSTRAINT `Inventoris_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
