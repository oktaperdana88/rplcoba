/*
  Warnings:

  - You are about to alter the column `tanggal` on the `inventoris` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - Added the required column `name` to the `Peternakan` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `inventoris` MODIFY `tanggal` DATETIME NOT NULL;

-- AlterTable
ALTER TABLE `peternakan` ADD COLUMN `name` VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `role` VARCHAR(7) NOT NULL;

-- AddForeignKey
ALTER TABLE `Peternakan` ADD CONSTRAINT `Peternakan_id_user_fkey` FOREIGN KEY (`id_user`) REFERENCES `User`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
