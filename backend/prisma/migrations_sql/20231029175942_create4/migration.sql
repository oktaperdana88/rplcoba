/*
  Warnings:

  - You are about to alter the column `tanggal` on the `inventoris` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to drop the column `total_biaya` on the `ternak` table. All the data in the column will be lost.
  - Made the column `tanggal_dapat` on table `ternak` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `inventoris` MODIFY `tanggal` DATETIME NOT NULL;

-- AlterTable
ALTER TABLE `ternak` DROP COLUMN `total_biaya`,
    MODIFY `harga_beli` INTEGER NULL,
    MODIFY `umur` SMALLINT NULL,
    MODIFY `tanggal_dapat` DATE NOT NULL;
