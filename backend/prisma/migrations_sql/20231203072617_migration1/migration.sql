-- CreateTable
CREATE TABLE `User` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `password` VARCHAR(512) NOT NULL,
    `role` VARCHAR(7) NOT NULL,
    `refresh_token` VARCHAR(191) NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Peternakan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `lokasi` TEXT NOT NULL,
    `kapasitas_sapi` SMALLINT NOT NULL,
    `kapasitas_kambing` SMALLINT NOT NULL,
    `kapasitas_ayam` SMALLINT NOT NULL,
    `stok_rumput` FLOAT NOT NULL,
    `stok_konsentrat` FLOAT NOT NULL,
    `stok_fermentasi` FLOAT NOT NULL,
    `stok_pupuk` FLOAT NOT NULL,
    `kas` FLOAT NOT NULL DEFAULT 1000000,
    `tanggal_berdiri` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_user` INTEGER NULL,

    UNIQUE INDEX `Peternakan_nama_key`(`nama`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Pegawai` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL,
    `nik` VARCHAR(16) NOT NULL,
    `no_telpon` VARCHAR(13) NOT NULL,
    `alamat` TEXT NOT NULL,
    `role` VARCHAR(10) NOT NULL,
    `tanggal_masuk` DATE NOT NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Aset` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(50) NOT NULL DEFAULT 'AAAAAA',
    `jenis` VARCHAR(18) NOT NULL,
    `harga_beli` INTEGER NULL,
    `tanggal_dapat` DATE NULL,
    `status_kepemilikan` VARCHAR(14) NOT NULL,
    `deskripsi` TEXT NULL,
    `metode_pembayaran` VARCHAR(10) NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Transaksi` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `deskripsi` TEXT NULL,
    `jenis_transaksi` VARCHAR(12) NOT NULL,
    `pihak_terkait` VARCHAR(12) NULL,
    `tanggal_transaksi` DATE NOT NULL,
    `jumlah` INTEGER NOT NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_peternakan` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Ternak` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nomor` SMALLINT NULL,
    `kategori` VARCHAR(16) NOT NULL,
    `jenis` VARCHAR(16) NOT NULL,
    `berat` DOUBLE NOT NULL,
    `kelamin` VARCHAR(6) NOT NULL,
    `induk_jantan` VARCHAR(16) NOT NULL DEFAULT 'Tidak Diketahui',
    `induk_betina` VARCHAR(16) NOT NULL DEFAULT 'Tidak Diketahui',
    `cara_perolehan` VARCHAR(10) NOT NULL,
    `harga_beli` INTEGER NOT NULL DEFAULT 0,
    `umur` SMALLINT NOT NULL DEFAULT 0,
    `tanggal_masuk` DATE NOT NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_peternakan` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `PengobatanTernak` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `obat` VARCHAR(100) NOT NULL,
    `penyakit` VARCHAR(100) NOT NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_transaksi` INTEGER NOT NULL,
    `id_ternak` INTEGER NOT NULL,

    UNIQUE INDEX `PengobatanTernak_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `OperasionalAset` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_aset` INTEGER NOT NULL,
    `id_transaksi` INTEGER NOT NULL,

    UNIQUE INDEX `OperasionalAset_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `OperasionalGaji` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_transaksi` INTEGER NOT NULL,
    `id_pegawai` INTEGER NOT NULL,

    UNIQUE INDEX `OperasionalGaji_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `OperasionalInventoris` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `jenis` VARCHAR(50) NOT NULL,
    `ternak` VARCHAR(50) NULL,
    `riwayat` VARCHAR(12) NOT NULL,
    `tanggal` DATETIME NOT NULL,
    `deskripsi` TEXT NULL,
    `berat` INTEGER NOT NULL,
    `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME NOT NULL,
    `id_peternakan` INTEGER NOT NULL,
    `id_transaksi` INTEGER NULL,

    UNIQUE INDEX `OperasionalInventoris_id_transaksi_key`(`id_transaksi`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Penghapusan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `jenis` VARCHAR(50) NOT NULL,
    `tipe` VARCHAR(50) NOT NULL,
    `kategori` VARCHAR(50) NOT NULL,
    `tanggal` DATE NOT NULL,
    `deskripsi` TEXT NOT NULL,
    `id_peternakan` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Kas` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `bulan` VARCHAR(2) NOT NULL,
    `tahun` VARCHAR(4) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `jumlah` INTEGER NOT NULL,
    `id_peternakan` INTEGER NULL DEFAULT 1,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Peternakan` ADD CONSTRAINT `Peternakan_id_user_fkey` FOREIGN KEY (`id_user`) REFERENCES `User`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Pegawai` ADD CONSTRAINT `Pegawai_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Aset` ADD CONSTRAINT `Aset_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Transaksi` ADD CONSTRAINT `Transaksi_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Ternak` ADD CONSTRAINT `Ternak_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PengobatanTernak` ADD CONSTRAINT `PengobatanTernak_id_ternak_fkey` FOREIGN KEY (`id_ternak`) REFERENCES `Ternak`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PengobatanTernak` ADD CONSTRAINT `PengobatanTernak_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalAset` ADD CONSTRAINT `OperasionalAset_id_aset_fkey` FOREIGN KEY (`id_aset`) REFERENCES `Aset`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalAset` ADD CONSTRAINT `OperasionalAset_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalGaji` ADD CONSTRAINT `OperasionalGaji_id_pegawai_fkey` FOREIGN KEY (`id_pegawai`) REFERENCES `Pegawai`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalGaji` ADD CONSTRAINT `OperasionalGaji_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalInventoris` ADD CONSTRAINT `OperasionalInventoris_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `OperasionalInventoris` ADD CONSTRAINT `OperasionalInventoris_id_transaksi_fkey` FOREIGN KEY (`id_transaksi`) REFERENCES `Transaksi`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Penghapusan` ADD CONSTRAINT `Penghapusan_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Kas` ADD CONSTRAINT `Kas_id_peternakan_fkey` FOREIGN KEY (`id_peternakan`) REFERENCES `Peternakan`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
