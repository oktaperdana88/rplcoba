import express from "express";
import { tambahPenghapusanAset, tambahAset, jualAset, editAset, getAset, getAsetById, addOperasional, getOperasional } from "../controller/AsetController.js";

const router = express.Router();

router.post("/create", tambahAset);
router.post("/:id/jual", jualAset);
router.get("/", getAset);
router.get("/:id", getAsetById);
router.put("/:id", editAset);
router.post("/hapus", tambahPenghapusanAset);
router.post("/:id/operasional", addOperasional);
router.get("/:id/operasional", getOperasional);

export default router;
