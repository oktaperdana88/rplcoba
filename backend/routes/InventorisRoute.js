import express from "express";
import { tambahInventoris, getInventoris, getSummary, jualInventoris } from "../controller/InventorisController.js";

const router = express.Router();

router.post("/tambah", tambahInventoris);
router.post("/jual", jualInventoris);
router.get("/summary", getSummary);
router.get("/", getInventoris);

export default router;
