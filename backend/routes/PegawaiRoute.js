import express from "express";
import { tambahpegawai, tambahPenghapusanPegawai, editPegawai, getPegawai, getpegawaiById, addRiwayatGaji, getRiwayatGaji } from "../controller/PegawaiController.js";

const router = express.Router();

router.post("/tambah", tambahpegawai);
router.put("/:id", editPegawai);
router.get("/", getPegawai);
router.get("/:id", getpegawaiById);
router.post("/hapus", tambahPenghapusanPegawai);
router.post("/:id/operasional", addRiwayatGaji);
router.get("/:id/operasional", getRiwayatGaji);

export default router;
