import express from "express";
import {
  createPeternakan,
  deletePeternakan,
  editPeternakan,
  getAllPeternakan,
  getPeternakanById,
} from "../controller/PeternakanController.js";

const peternakanRouter = express.Router();

peternakanRouter.get("/:id", getPeternakanById);
peternakanRouter.post("/create", createPeternakan);
peternakanRouter.get("/", getAllPeternakan);
peternakanRouter.put("/:id", editPeternakan);
peternakanRouter.delete("/:id", deletePeternakan);

export default peternakanRouter;
