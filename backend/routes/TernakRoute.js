import express from "express";
import {
  tambahTernak,
  hapusTernak,
  tambahpemeliharaanTernak,
  getTernak,
  editTernak,
  getPemeliharaanTernak,
  jualTernak,
  getDetailTernakById,
  getPenghapusanTernak,
} from "../controller/TernakController.js";

const router = express.Router();

router.post("/create", tambahTernak);
router.post("/:id/jual", jualTernak);
router.post("/:id/pemeliharaan", tambahpemeliharaanTernak);
router.get("/:id/pemeliharaan", getPemeliharaanTernak);
router.get("/", getTernak);
router.get("/penghapusan", getPenghapusanTernak);
router.post("/hapus", hapusTernak);
router.get("/:id", getDetailTernakById);
router.put("/:id", editTernak);

export default router;
