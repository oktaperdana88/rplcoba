import express from "express";
import {
  getTransaksi,
  getTransaksiById,
  createTransaksi,
  downloadTransaksiPDF,
  downloadTransaksiExcel,
  getSummary,
  getSummaryByYearRange,
} from "../controller/TransaksiController.js";

const router = express.Router();

router.get("/", getTransaksi);
router.post("/", createTransaksi);
router.get("/summary", getSummary);
router.get("/summaryByYear", getSummaryByYearRange);
router.get("/:id", getTransaksiById);
router.get("/download/pdf", downloadTransaksiPDF);
router.get("/download/excel", downloadTransaksiExcel);

export default router;
