import express from "express";
import {
  changePassword,
  changeRole,
  deleteUser,
  getAllUser,
  getUserById,
} from "../controller/UserController.js";

const userRouter = express.Router();

userRouter.get("/", getAllUser);
userRouter.get("/:id", getUserById);
userRouter.delete("/:id", deleteUser);
userRouter.put("/:id/changepassword", changePassword);
userRouter.put("/:id/changerole", changeRole);

export default userRouter;
