import prisma from "../config/db.js";
import ConflictError from "../error/ConflictError.js";
import NotFoudError from "../error/NotFoundError.js";

class AsetService {
  //menghitung aset berdasarkan kategori
  async countAset(kategori, id) {
    await prisma.$connect();
    return await prisma.aset.count({
      where: {
        jenis: kategori,
        id_peternakan: +id,
      },
    });
  }

  //Mendapatkan semua aset
  async getAllAset(peternakan) {
    await prisma.$connect();

    //Mendapatkan seluruh data aset untuk menu penjualan
    const data = await prisma.aset.findMany({
      where: {
        id_peternakan: +peternakan,
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Mendapatkan aset berdasarkan id
  async getAsetById(id) {
    await prisma.$connect();
    const data = await prisma.aset.findFirst({
      where: {
        id: +id,
      },
      include: {
        peternakan: {
          select: {
            nama: true,
          },
        },
      },
    });
    await prisma.$disconnect();

    if (!data) throw new NotFoudError(`Aset id ${id} tidak ditemukan`);

    return data;
  }

  //Membuat aset baru
  async createAset(
    nama,
    jenis,
    harga_beli,
    status_kepemilikan,
    deskripsi,
    metode_pembayaran,
    tanggal_dapat,
    id_peternakan
  ) {
    await prisma.$connect();
    const aset = await prisma.aset.findFirst({
      where: {
        nama,
        id_peternakan : +id_peternakan,
      },
    });

    if (aset) throw new ConflictError("Nama aset sudah terpakai");

    const data = await prisma.aset.create({
      data: {
        nama: nama,
        jenis: jenis,
        harga_beli: +harga_beli,
        status_kepemilikan: status_kepemilikan,
        deskripsi: deskripsi,
        metode_pembayaran,
        tanggal_dapat: new Date(tanggal_dapat).toISOString(),
        id_peternakan: +id_peternakan,
      },
    });
    await prisma.$disconnect();

    return data;
  }

  //Menghapus aset
  async deleteAset(id) {
    await prisma.$connect();
    await prisma.aset.delete({
      where: {
        id: +id,
      },
    });
    await prisma.$disconnect();
  }

  async getOperasional(id) {
    await prisma.$connect();

    const data = await prisma.operasionalAset.findMany({
      select: {
        id: true,
        transaksi: {
          select: {
            tanggal_transaksi: true,
            jumlah: true,
            pihak_terkait: true,
            deskripsi: true,
          },
        },
      },
      where: {
        id_aset: +id,
      },
    });
    await prisma.$disconnect();

    return data;
  }

  //menambah operasional
  async addOperasional(id, id_transaksi) {
    await prisma.$connect();
    const data = await prisma.operasionalAset.create({
      data: {
        id_aset: +id,
        id_transaksi: +id_transaksi,
      },
      select: {
        id: true,
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Menambah riwayat penghapusan ke tabel penghapusan
  async penghapusanAset(jenis, kategori, deskripsi, tanggal, id_peternakan) {
    await prisma.$connect();
    const today = new Date(tanggal);
    await prisma.penghapusan.create({
      data: {
        tipe: "Aset",
        tanggal: today,
        id_peternakan: +id_peternakan, // Pastikan variabel ini diambil dari req.body
        jenis: jenis,
        kategori: kategori,
        deskripsi: deskripsi,
      },
    });

    await prisma.$disconnect();
  }
}

export default AsetService;
