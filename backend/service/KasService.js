import prisma from "../config/db.js";

class KasService {
  //Mendapatkan kas salah satu bulan
  async getOneKas(bulan, tahun, id) {
    await prisma.$connect();
    let kas = await prisma.kas.findFirst({
      where: {
        id_peternakan: +id,
        tahun: String(tahun),
        bulan: String(bulan),
      },
    });
    await prisma.$disconnect();

    if (!kas) {
      kas = {
        jumlah: 0,
      };
    }

    return kas;
  }

  //membuat kas baru
  async createKas(bulan, tahun, id, kas) {
    await prisma.$connect();
    await prisma.kas.create({
      data: {
        bulan: String(bulan),
        tahun: String(tahun),
        id_peternakan: +id,
        jumlah: +kas,
      },
    });
    await prisma.$disconnect();
  }
}

export default KasService;
