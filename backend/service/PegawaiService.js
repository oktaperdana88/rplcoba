import prisma from "../config/db.js";
import ConflictError from "../error/ConflictError.js";
import NotFoudError from "../error/NotFoundError.js";

class PegawaiService {
  //Mendapatkan semua pegawai
  async getAllPegawai(peternakan) {
    await prisma.$connect();

    const data = await prisma.pegawai.findMany({
      where: {
        id_peternakan: +peternakan,
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Mendapatkan pegawai berdasarkan id
  async getPegawaiById(id) {
    await prisma.$connect();
    const data = await prisma.pegawai.findUnique({
      where: {
        id: Number(id),
      },
      select: {
        id: true,
        nama: true,
        no_telpon: true,
        nik: true,
        alamat: true,
        tanggal_masuk: true,
        role: true,
        peternakan: {
          select: {
            nama: true,
            id: true,
          },
        },
      },
    });

    await prisma.$disconnect();

    if (!data) throw new NotFoudError(`Pegawai tidak ditemukan`);

    return data;
  }

  //Menambah pegawai baru
  async createPegawai(
    nama,
    alamat,
    no_telpon,
    nik,
    role,
    tanggal_masuk,
    id_peternakan
  ) {
    await prisma.$connect();
    const pegawaiNIK = await prisma.pegawai.findFirst({
      where: {
        nik: String(nik),
      },
    });

    if (pegawaiNIK) throw new ConflictError("NIK telah terdaftar");

    const pegawaiTelp = await prisma.pegawai.findFirst({
      where: {
        no_telpon: String(no_telpon),
      },
    });

    if (pegawaiTelp) throw new ConflictError("Nomor telepon telah terdaftar");

    const data = await prisma.pegawai.create({
      data: {
        nama: nama,
        alamat: alamat,
        no_telpon: String(no_telpon),
        nik: String(nik),
        role: role,
        tanggal_masuk: new Date(tanggal_masuk).toISOString(),
        id_peternakan: +id_peternakan, // Menggunakan nilai dari request, bukan 1 secara statis
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //mendapatkan riwayat penggajian 1 pegawai
  async getRiwayatGaji(id) {
    await prisma.$connect();

    const data = await prisma.operasionalGaji.findMany({
      select: {
        id: true,
        transaksi: {
          select: {
            tanggal_transaksi: true,
            pihak_terkait: true,
            jumlah: true,
            deskripsi: true,
          },
        },
      },
      where: {
        id_pegawai: +id,
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //menambah riwayat penggajian 1 pegawai
  async addRiwayatGaji(id, id_transaksi) {
    await prisma.$connect();
    const data = await prisma.operasionalGaji.create({
      data: {
        id_pegawai: +id,
        id_transaksi: +id_transaksi,
      },
      select: {
        id: true,
      },
    });

    await prisma.$disconnect();
    return data;
  }

  //mengupdate data detail 1 pegawai
  async updatePegawai(
    nama,
    alamat,
    no_telpon,
    nik,
    role,
    tanggal_masuk,
    id_peternakan,
    id
  ) {
    await prisma.$connect();
    const data = await prisma.pegawai.update({
      where: {
        id: +id,
      },
      data: {
        nama: nama,
        alamat: alamat,
        no_telpon: String(no_telpon),
        nik: String(nik),
        role: role,
        tanggal_masuk: new Date(tanggal_masuk).toISOString(),
        id_peternakan,
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Menghapus pegawai
  async deletePegawai(id) {
    await prisma.pegawai.delete({
      where: {
        id: +id,
      },
    });
  }
}

export default PegawaiService;
