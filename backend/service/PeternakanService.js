import prisma from "../config/db.js";
import ConflictError from "../error/ConflictError.js";

class PeternakanService {
  //mendapatkan detail peternakan berdasarkan id
  async getPeternakanById(id) {
    await prisma.$connect();
    const peternakan = await prisma.peternakan.findUnique({
      where: {
        id: +id,
      },
      include: {
        user: {
          select: {
            nama: true,
          },
        },
      },
    });
    await prisma.$disconnect();

    if (!peternakan)
      throw new NotFoudError(`Peternakan dengan id ${id} tidak ditemukan`);

    return peternakan;
  }

  //Mendapatkan semua peternakan
  async getAllPeternakan(user) {
    let where;

    if (!user) {
      where = {};
    } else {
      where = {
        id_user: +user,
      };
    }

    await prisma.$connect();

    const data = await prisma.peternakan.findMany({
      select: {
        id: true,
        nama: true,
        lokasi: true,
        tanggal_berdiri: true,
        user: {
          select: {
            nama: true,
          },
        },
      },
      where,
      orderBy: {
        id: "asc",
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Menambah peternakan baru
  async createPeternakan(data, tanggal) {
    await prisma.$connect();
    const peternakan = await prisma.peternakan.findFirst({
      where: {
        nama: data.name,
      },
    });

    if (peternakan) throw new ConflictError("Nama peternakan telah terdaftar");

    const result = await prisma.peternakan.create({
      data: {
        nama: data.name,
        lokasi: data.lokasi,
        kapasitas_ayam: +data.kapasitas_ayam,
        kapasitas_kambing: +data.kapasitas_kambing,
        kapasitas_sapi: +data.kapasitas_sapi,
        stok_fermentasi: +data.stok_fermentasi,
        stok_pupuk: +data.stok_pupuk,
        stok_konsentrat: +data.stok_konsentrat,
        stok_rumput: +data.stok_rumput,
        kas: +data.kas,
        tanggal_berdiri: tanggal,
        id_user: +data.id_user,
      },
    });
    await prisma.$disconnect();

    return result;
  }

  //Update data peternakan berdasarkan id
  async updatePeternakan(data, id) {
    await prisma.$connect();
    const pet = await prisma.peternakan.update({
      data: {
        nama: data.name,
        lokasi: data.lokasi,
        id_user: +data.id_user,
        kapasitas_ayam: +data.kapasitas_ayam,
        kapasitas_kambing: +data.kapasitas_kambing,
        kapasitas_sapi: +data.kapasitas_sapi,
      },
      where: {
        id: +id,
      },
      select: {
        nama: true,
        lokasi: true,
      },
    });
    await prisma.$disconnect();

    return pet;
  }

  //Menghapus peternakan
  async deletePeternakan(id) {
    await prisma.$connect();

    await prisma.peternakan.delete({
      where: {
        id: +id,
      },
    });

    await prisma.$disconnect();
  }
}

export default PeternakanService;
