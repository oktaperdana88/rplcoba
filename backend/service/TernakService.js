import prisma from "../config/db.js";
import ConflictError from "../error/ConflictError.js";
import NotFoudError from "../error/NotFoundError.js";

class TernakService {
  //Menghitung ternak berdasarkan kategori
  async countTernak(kategori, id) {
    await prisma.$connect();
    return await prisma.ternak.count({
      where: {
        kategori: kategori,
        id_peternakan: +id,
      },
    });
  }

  //Mendapatkan semua ternak
  async getAllTernak(peternakan, kategori, jenis) {
    await prisma.$connect();

    const data = await prisma.ternak.findMany({
      where: {
        id_peternakan: +peternakan,
        kategori: {
          contains: kategori,
        },
        jenis: {
          contains: jenis,
        },
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Mengecek nomor sapi terdaftar
  async getNomorSapi(nomor, peternakan) {
    await prisma.$connect();

    const data = await prisma.ternak.findFirst({
      where: {
        nomor: +nomor,
        id_peternakan: +peternakan,
      },
      select: {
        nomor: true,
        id_peternakan: true,
      },
    });

    await prisma.$disconnect();

    if (data) {
      throw new ConflictError(`Sapi dengan nomor ${nomor}sudah terdaftar`);
    }

    return data;
  }

  //mendapatkan detail ternak berdasarkan id
  async getTernakById(id) {
    await prisma.$connect();
    const data = await prisma.ternak.findFirst({
      where: {
        id: +id,
      },
    });

    await prisma.$disconnect();

    if (!data) throw new NotFoudError("Ternak tidak ditemukan");

    return data;
  }

  //Mendapatkan riwayat pemeliharaan ternak berdasarkan id
  async getPemeliharaanTernak(id) {
    await prisma.$connect();

    const data = await prisma.pengobatanTernak.findMany({
      where: {
        id_ternak: +id,
      },
      include: {
        transaksi: {
          select: {
            pihak_terkait: true,
            tanggal_transaksi: true,
            jumlah: true,
          },
        },
      },
    });

    await prisma.$disconnect();

    return data;
  }

  //Menghapus ternak
  async deleteTernak(id) {
    await prisma.$connect();
    await prisma.ternak.delete({
      where: {
        id: parseInt(id),
      },
    });
    await prisma.$disconnect();
  }

  //menambah riwayat pengobatan ternak
  async addPengobatanTernak(obat, penyakit, id_transaksi, id_ternak) {
    await prisma.$connect();
    const data = await prisma.pengobatanTernak.create({
      data: {
        obat,
        penyakit,
        id_transaksi: id_transaksi, // Menggunakan id transaksi yang baru saja dibuat
        id_ternak: parseInt(id_ternak),
      },
      select: {
        obat: true,
        penyakit: true,
        id: true,
        transaksi: {
          select: {
            tanggal_transaksi: true,
            jumlah: true,
          },
        },
      },
    });
    await prisma.$disconnect();

    return data;
  }

  //Mendapat riwayat penghapusan
  async getPenghapusan(peternakan, tipe) {
    await prisma.$connect();
    const data = await prisma.penghapusan.findMany({
      where: {
        id_peternakan: +peternakan,
        tipe: {
          contains: tipe,
        },
      },
      include: {
        peternakan: {
          select: {
            nama: true,
          },
        },
      },
    });
    await prisma.$disconnect();
    return data;
  }
}

export default TernakService;
