import prisma from "../config/db.js";
import ExcelJS from "exceljs";
import { bulans } from "../utils/Constant.js";

class TransaksiService {

  //Menambahkan transaksi
  async createTransaksi(
    tanggal_transaksi,
    jenis,
    harga,
    pihak_terkait,
    id_peternakan,
    deskripsi
  ) {
    const date = new Date(tanggal_transaksi);
    await prisma.$connect();
    const data = await prisma.transaksi.create({
      data: {
        jenis_transaksi: jenis,
        jumlah: +harga,
        tanggal_transaksi: new Date(tanggal_transaksi),
        pihak_terkait: pihak_terkait,
        id_peternakan: +id_peternakan,
        deskripsi,
      },
    });

    //menemukan riwayat kas
    const kas = await prisma.kas.findFirst({
      where: {
        id_peternakan: +id_peternakan,
        bulan: date.getMonth() === 12 ? "1" : String(date.getMonth() + 1),
        tahun:
          date.getMonth() === 12
            ? String(date.getFullYear() + 1)
            : String(date.getFullYear()),
      },
    });

    //Kas tidak ditemukan
    if (!kas) {
      //mendapatkan kas dari peternakan berdiri hingga sekarang
      const allKas = await prisma.kas.findMany({
        where: {
          id_peternakan: +id_peternakan,
        },
      });

      //mengurutkan kas
      allKas.sort((a, b) => {
        if (a.tahun < b.tahun) {
          return -1;
        } else if (a.tahun > b.tahun) {
          return 1;
        } else {
          return a.bulan - b.bulan;
        }
      });

      //menemukan kas bulan terakhir tercatat sebelum saat ini
      let index = allKas.findIndex((item) => {
        return (
          (item.tahun == date.getFullYear() &&
            item.bulan > date.getMonth() + 1) ||
          item.tahun > date.getFullYear()
        );
      });

      if (index == -1) {
        index = 1;
      }

      let jml = 0;

      const bulan = date.getMonth() + 1;
      const tahun = date.getFullYear();

      if (jenis === "Pemasukan") {
        jml = allKas[index - 1].jumlah + Number(harga);
        await prisma.kas.create({
          data: {
            id_peternakan: +id_peternakan,
            bulan: String(bulan),
            tahun: String(tahun),
            jumlah: jml,
          },
        });
      } else {
        jml = allKas[index - 1].jumlah - Number(harga);
        await prisma.kas.create({
          data: {
            id_peternakan: +id_peternakan,
            bulan: String(bulan),
            tahun: String(tahun),
            jumlah: jml,
          },
        });
      }
    } else {
      if (jenis === "Pemasukan") kas.jumlah += Number(harga);
      else kas.jumlah -= Number(harga);
      await prisma.kas.update({
        data: kas,
        where: {
          id: kas.id,
        },
      });
    }

    //mengupdate seluruh riwayat kas
    if (
      date.getMonth() !== new Date().getMonth() ||
      date.getFullYear() !== new Date().getFullYear()
    ) {
      const allKas = await prisma.kas.findMany({
        where: {
          tahun: {
            gte: String(date.getFullYear()),
            lte: String(new Date().getFullYear()),
          },
          id_peternakan: +id_peternakan,
        },
      });

      allKas.sort((a, b) => {
        if (a.tahun < b.tahun) {
          return -1;
        } else if (a.tahun > b.tahun) {
          return 1;
        } else {
          return a.bulan - b.bulan;
        }
      });

      for (let i = 0; i < allKas.length; i++) {
        if (
          allKas[i].tahun == String(date.getFullYear()) &&
          allKas[i].bulan <= date.getMonth() + 1
        ) {
          continue;
        }

        if (
          allKas[i].tahun == String(new Date().getFullYear()) &&
          allKas[i].bulan > new Date().getMonth() + 1
        ) {
          continue;
        }

        if (jenis === "Pemasukan") allKas[i].jumlah += Number(harga);
        else allKas[i].jumlah -= Number(harga);

        await prisma.kas.update({
          data: allKas[i],
          where: {
            id: allKas[i].id,
          },
        });
      }
    }

    await prisma.$disconnect();

    return data;
  }

  async downloadPDF(peternakan, bulan, tahun, pdfDoc) {
    const firstDay = new Date(+tahun, +bulan - 1, 1);
    const lastDay = new Date(+tahun, +bulan, 1);
    const date = new Date();

    //Mendpatkan transaksi
    const transaksiData = await prisma.transaksi.findMany({
      where: {
        id_peternakan: +peternakan,
        tanggal_transaksi: {
          gte: firstDay,
          lte: lastDay,
        },
      },
      orderBy: {
        tanggal_transaksi: "asc",
      },
    });

    const pemasukan = [];
    const pengeluaran = [];
    let masuk = 0;
    let keluar = 0;

    //mengelompokkan transaksi pemasukan dan pengeluaran
    transaksiData.forEach((transaksi) => {
      const jumlah = (transaksi.jumlah ? transaksi.jumlah : 0).toLocaleString(
        "id-ID",
        {
          style: "currency",
          currency: "IDR",
        }
      );

      const formattedDate = new Date(
        transaksi.tanggal_transaksi
      ).toLocaleDateString("id-ID", {
        year: "numeric",
        month: "long",
        day: "numeric",
      });

      if (transaksi.jenis_transaksi === "Pemasukan") {
        masuk++;
        pemasukan.push({
          nomor: masuk,
          tanggal_transaksi: formattedDate,
          deskripsi: transaksi.deskripsi,
          pihak_terkait: transaksi.pihak_terkait,
          jumlah: jumlah,
        });
      } else {
        keluar++;
        pengeluaran.push({
          nomor: keluar,
          tanggal_transaksi: formattedDate,
          deskripsi: transaksi.deskripsi,
          pihak_terkait: transaksi.pihak_terkait,
          jumlah: jumlah,
        });
      }
    });

    // Calculate the total jumlah Pemasukan
    const totalPemasukan = transaksiData
      .filter((transaksi) => transaksi.jenis_transaksi === "Pemasukan")
      .reduce(
        (acc, pemasukan) =>
          acc +
          parseFloat(
            (pemasukan.jumlah || 0).toString().replace(/[^0-9.]/g, "")
          ),
        0
      );

    // Calculate the total jumlah Pengeluaran
    const totalPengeluaran = transaksiData
      .filter((transaksi) => transaksi.jenis_transaksi === "Pengeluaran")
      .reduce(
        (acc, pengeluaran) =>
          acc +
          parseFloat(
            (pengeluaran.jumlah || 0).toString().replace(/[^0-9.]/g, "")
          ),
        0
      );

    // Calculate the saldo saat ini (totalPemasukan - totalPengeluaran)
    const saldoSaatIni = await prisma.kas.findFirst({
      where: {
        id_peternakan: +peternakan,
        bulan: bulan,
        tahun: tahun,
      },
    });

    const allKas = await prisma.kas.findMany({
      where: {
        id_peternakan: +peternakan,
      },
    });

    allKas.sort((a, b) => {
      if (a.tahun < b.tahun) {
        return -1;
      } else if (a.tahun > b.tahun) {
        return 1;
      } else {
        return a.bulan - b.bulan;
      }
    });

    let index = allKas.findIndex((item) => {
      return (
        (item.tahun == date.getFullYear() &&
          item.bulan > date.getMonth() + 1) ||
        item.tahun > date.getFullYear()
      );
    });

    let saldoSebelumnya = {
      jumlah: totalPengeluaran + saldoSaatIni.jumlah - totalPemasukan,
    };

    if (index != 1 && index != -1) {
      saldoSebelumnya = allKas[index - 1];
    }

    //tabel pemasukan
    const tablePemasukan = {
      title: "Pemasukan",
      headers: [
        { label: "Nomor", property: "nomor", width: 60, renderer: null },
        {
          label: "Tanggal Transaksi",
          property: "tanggal_transaksi",
          width: 100,
          renderer: null,
        },
        {
          label: "Deskripsi",
          property: "deskripsi",
          width: 240,
          renderer: null,
        },
        {
          label: "Pihak Terkait",
          property: "pihak_terkait",
          width: 100,
          renderer: null,
        },
        { label: "Jumlah", property: "jumlah", width: 100, renderer: null },
      ],
      // complex data
      datas: pemasukan,
    };

    //tabel pengeluaran
    const tablePengeluaran = {
      title: "Pengeluaran",
      headers: [
        { label: "Nomor", property: "nomor", width: 60, renderer: null },
        {
          label: "Tanggal Transaksi",
          property: "tanggal_transaksi",
          width: 100,
          renderer: null,
        },
        {
          label: "Deskripsi",
          property: "deskripsi",
          width: 140,
          renderer: null,
        },
        {
          label: "Pihak Terkait",
          property: "pihak_terkait",
          width: 100,
          renderer: null,
        },
        { label: "Jumlah", property: "jumlah", width: 100, renderer: null },
      ],
      // complex data
      datas: pengeluaran,
    };

    //Penataan layout pdf
    pdfDoc
      .fontSize(14)
      .text(
        `Laporan Neraca Keuangan Peternakan ${peternakan} Mulyo Farm ${
          bulans[bulan - 1]
        } ${tahun}`
      );
    pdfDoc.moveDown();

    if (pemasukan.length !== 0) {
      pdfDoc.table(tablePemasukan, {
        prepareHeader: () => pdfDoc.font("Helvetica-Bold").fontSize(8),
        prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
          pdfDoc.font("Helvetica").fontSize(8);
          indexColumn === 0 && pdfDoc.addBackground(rectRow, "white", 0.15);
        },
      });
      pdfDoc.moveDown();
    } else {
      pdfDoc.fontSize(12).font("Helvetica-Bold").text("Pemasukan");
    }

    pdfDoc
      .font("Helvetica")
      .fontSize(10)
      .text(
        `Saldo bulan sebelumnya: ${saldoSebelumnya.jumlah.toLocaleString(
          "id-ID",
          {
            style: "currency",
            currency: "IDR",
          }
        )}`
      );

    pdfDoc.fontSize(10).text(
      `Total Pemasukan: ${(
        saldoSebelumnya.jumlah + totalPemasukan
      ).toLocaleString("id-ID", {
        style: "currency",
        currency: "IDR",
      })}`
    );

    pdfDoc.moveDown();

    pdfDoc.table(tablePengeluaran, {
      prepareHeader: () => pdfDoc.font("Helvetica-Bold").fontSize(8),
      prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
        pdfDoc.font("Helvetica").fontSize(8);
        indexColumn === 0 && pdfDoc.addBackground(rectRow, "white", 0.15);
      },
    });

    pdfDoc.moveDown();

    pdfDoc.fontSize(10).text(
      `Saldo bulan ini: ${saldoSaatIni.jumlah.toLocaleString("id-ID", {
        style: "currency",
        currency: "IDR",
      })}`
    );
    pdfDoc.fontSize(10).text(
      `Total Pemasukan: ${(
        saldoSaatIni.jumlah + totalPengeluaran
      ).toLocaleString("id-ID", {
        style: "currency",
        currency: "IDR",
      })}`
    );

    return pdfDoc;
  }

  async downloadExcel(peternakan, bulan, tahun) {
    const firstDay = new Date(+tahun, +bulan - 1, 1);
    const lastDay = new Date(+tahun, +bulan, 1);
    const date = new Date();

    //Mendapatkan semua transaksi
    const transaksiData = await prisma.transaksi.findMany({
      where: {
        id_peternakan: +peternakan,
        tanggal_transaksi: {
          gte: firstDay,
          lte: lastDay,
        },
      },
      orderBy: {
        tanggal_transaksi: "asc",
      },
    });
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("Transaksi");

    //Setup kolom excel
    worksheet.mergeCells(1, 1, 1, 10);
    worksheet.mergeCells(2, 1, 2, 5);
    worksheet.mergeCells(2, 6, 2, 10);
    worksheet.getCell("A1").alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    worksheet.getCell(
      "A" + 1
    ).value = `Laporan Neraca Keuangan Peternakan ${peternakan} Mulyo Farm ${
      bulans[bulan - 1]
    } ${tahun}`;
    worksheet.getCell("A" + 2).value = "Pemasukan";
    worksheet.getCell("F" + 2).value = "Pengeluaran";
    worksheet.getCell("A2").alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    worksheet.getCell("F2").alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    worksheet.getCell("A2").font = {
      size: 14,
      bold: true,
    };
    worksheet.getCell("F2").font = {
      size: 14,
      bold: true,
    };

    worksheet.getCell("A3").value = "No";
    worksheet.getCell("B3").value = "Tanggal Transaksi";
    worksheet.getCell("C3").value = "Deskripsi";
    worksheet.getCell("D3").value = "Pihak Terkait";
    worksheet.getCell("E3").value = "Jumlah";
    worksheet.getCell("F3").value = "No";
    worksheet.getCell("G3").value = "Tanggal Transaksi";
    worksheet.getCell("H3").value = "Deskripsi";
    worksheet.getCell("I3").value = "Pihak Terkait";
    worksheet.getCell("J3").value = "Jumlah";

    let masuk = 0;
    let keluar = 0;

    //Pengelompokan transaksi
    transaksiData.forEach((transaksi) => {
      const formattedDate = new Date(
        transaksi.tanggal_transaksi
      ).toLocaleDateString("id-ID", {
        year: "numeric",
        month: "long",
        day: "numeric",
      });

      if (transaksi.jenis_transaksi === "Pemasukan") {
        masuk++;
        worksheet.getCell("A" + (masuk + 3)).value = masuk;
        worksheet.getCell("B" + (masuk + 3)).value = formattedDate;
        worksheet.getCell("C" + (masuk + 3)).value = transaksi.deskripsi;
        worksheet.getCell("D" + (masuk + 3)).value = transaksi.pihak_terkait;
        worksheet.getCell("E" + (masuk + 3)).value = transaksi.jumlah;
      } else {
        keluar++;
        worksheet.getCell("F" + (keluar + 3)).value = keluar;
        worksheet.getCell("G" + (keluar + 3)).value = formattedDate;
        worksheet.getCell("H" + (keluar + 3)).value = transaksi.deskripsi;
        worksheet.getCell("I" + (keluar + 3)).value = transaksi.pihak_terkait;
        worksheet.getCell("J" + (keluar + 3)).value = transaksi.jumlah;
      }
    });

    // Calculate the total jumlah Pemasukan
    const totalPemasukan = transaksiData
      .filter((transaksi) => transaksi.jenis_transaksi === "Pemasukan")
      .reduce(
        (acc, pemasukan) =>
          acc +
          parseFloat(
            (pemasukan.jumlah || 0).toString().replace(/[^0-9.]/g, "")
          ),
        0
      );

    // Calculate the total jumlah Pengeluaran
    const totalPengeluaran = transaksiData
      .filter((transaksi) => transaksi.jenis_transaksi === "Pengeluaran")
      .reduce(
        (acc, pengeluaran) =>
          acc +
          parseFloat(
            (pengeluaran.jumlah || 0).toString().replace(/[^0-9.]/g, "")
          ),
        0
      );

    // Calculate the saldo saat ini (totalPemasukan - totalPengeluaran)
    const saldoSaatIni = await prisma.kas.findFirst({
      where: {
        id_peternakan: +peternakan,
        bulan: bulan,
        tahun: tahun,
      },
    });

    const allKas = await prisma.kas.findMany({
      where: {
        id_peternakan: +peternakan,
      },
    });

    allKas.sort((a, b) => {
      if (a.tahun < b.tahun) {
        return -1;
      } else if (a.tahun > b.tahun) {
        return 1;
      } else {
        return a.bulan - b.bulan;
      }
    });

    let index = allKas.findIndex((item) => {
      return (
        (item.tahun == date.getFullYear() &&
          item.bulan > date.getMonth() + 1) ||
        item.tahun > date.getFullYear()
      );
    });

    let saldoSebelumnya = {
      jumlah: totalPengeluaran + saldoSaatIni.jumlah - totalPemasukan,
    };

    if (index != 1 && index != -1) {
      saldoSebelumnya = allKas[index - 1];
    }

    //Saldo dan total pemasukan serta pengeluaran
    worksheet.getCell("A" + (Math.max(keluar, masuk) + 4)).value =
      "Saldo Bulan Lalu";
    worksheet.getCell("E" + (Math.max(keluar, masuk) + 4)).value =
      saldoSebelumnya.jumlah;
    worksheet.getCell("A" + (Math.max(keluar, masuk) + 5)).value =
      "Total Pemasukan";
    worksheet.getCell("E" + (Math.max(keluar, masuk) + 5)).value =
      saldoSebelumnya.jumlah + totalPemasukan;

    worksheet.getCell("F" + (Math.max(keluar, masuk) + 4)).value =
      "Saldo Bulan ini";
    worksheet.getCell("J" + (Math.max(keluar, masuk) + 4)).value =
      saldoSaatIni.jumlah;
    worksheet.getCell("F" + (Math.max(keluar, masuk) + 5)).value =
      "Total Pengeluaran";
    worksheet.getCell("J" + (Math.max(keluar, masuk) + 5)).value =
      saldoSaatIni.jumlah + totalPengeluaran;

    worksheet.getColumn(5).numFmt = '"Rp"#,##0.00;[Red]-"Rp"#,##0.00';
    worksheet.getColumn(10).numFmt = '"Rp"#,##0.00;[Red]-"Rp"#,##0.00';

    return workbook;
  }
}

export default TransaksiService;
