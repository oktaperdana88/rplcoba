import prisma from "../config/db.js";
import NotFoudError from "../error/NotFoundError.js";

class UserService {
  //Mendapatkan semua user
  async getAllUser(role) {
    prisma.$connect();
    const data = await prisma.user.findMany({
      select: {
        id: true,
        nama: true,
        email: true,
        role: true,
      },
      where: {
        role: {
          contains: role,
        },
      },
    });
    prisma.$disconnect();

    return data;
  }

  //Mendapatkan user berdasarkan id
  async getUserById(id) {
    prisma.$connect();
    const data = await prisma.user.findUnique({
      where: {
        id: +id,
      },
    });
    prisma.$disconnect();

    if (!data) throw new NotFoudError(`User dengan id ${id} tidak ditemukan`);

    return data;
  }

  //update user
  async updateUser(hash, id) {
    prisma.$connect();
    const data = await prisma.user.update({
      data: {
        password: hash,
      },
      where: {
        id: id,
      },
      select: {
        id: true,
        nama: true,
        email: true,
        role: true,
      },
    });
    prisma.$disconnect();

    return data;
  }

  //hapus akun user
  async delete(id) {
    await prisma.$connect();

    await prisma.user.delete({
      where: {
        id: +id,
      },
    });

    await prisma.$disconnect();
  }

  //ganti role user
  async changeRole(role, id) {
    await prisma.$connect();

    const data = await prisma.user.update({
      data: {
        role: role,
      },
      where: {
        id: +id,
      },
      select: {
        id: true,
        nama: true,
        email: true,
        role: true,
      },
    });

    await prisma.$disconnect();

    return data;
  }
}

export default UserService;
