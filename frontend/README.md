# Frontend 

Folder ini berisi tentang kode program untuk tampilan antarmuka pengguna. Kode ditulis menggunakan library [React.js](https://react.dev/) dan [Material UI](https://mui.com/).

### Format Penulisan 🖋️
Nama file ditulis dengan format kebab case dengan urutan penamaan </menu/fitur/>-</bagian/>.</format file/>. Spasi dipisahkan dengan menggunakan tanda '-'. Contoh: **penjualan-form.jsx**

Untuk penamaan variabel dalam file, ditulis menggunakan format camel case. Contoh: **handleOpenMenu**

## Struktur Folder 📒

```
├── src
│   ├── components
│   ├── constant
│   ├── hooks
│   ├── layouts
│   ├── pages
│   ├── theme
│   ├── utils
│   ├── views
│   ├── App.jsx
│   ├── index.css
│   ├── main.jsx
├── public
│   ├── background
│   ├── images
│   ├── error
│   ├── _redirect
│   ├── favicon.ico
├── index.html
├── package.json
├── package-lock.json 
├── vercel.json 
├── vite.config.js
└── .gitignore
```

### 1) public
Folder public semua menyimpan gambar yang digunakan dalam aliran proses bisnis dari web. Gambar dikelompokkan dalam folder sesuai kegunaannya:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | error | Folder gambar ketika terjadi request error | [error](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/public/error) |
| 2. | background | Folder gambar yang digunakan untuk background | [background](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/public/background) |
| 3. | img | Folder gambar logo Mulyo Farm | [img](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/public/img) |
| 4. | favicon.ico | File favicon sistem | [favicon](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/public/favicon.ico) |

### 2) components
Folder berisi tentang komponen, yang memiliki skala lebih kecil daripada view, seperti icon, alert, dll, yang bisa dipakai berkali-kali (reuseable). Terbagi dalam folder:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | alert | Konfigurasi alert berupa toast | [alert](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/alert) |
| 2. | card | Mengatur custom card yang digunakan untuk menampilkan angka agregat ringkasan pada menu ternak, aset, dll. | [card](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/card) |
| 3. | chart | Konfigurasi line chart yang dipakai dalam dashboard | [chart](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/chart) |
| 4. | currency_field | Konfigurasi input berupa mata uang | [currency_field](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/currency_field) |
| 5. | iconify | Konfigurasi penggunaan icon dari react-iconify | [iconify](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/iconify) |
| 6. | logo | Konfigurasi penggunaan logo CV. Mulyo Farm | [logo](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/logo) |
| 7. | scrollbar | Konfigurasi scrollbar yang digunakan dalam sidebar | [scrollbar](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/scrollbar) |
| 8. | auth | Konfigurasi komponen untuk memverifikasi akses halaman yang dilindungi | [auth](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/auth) |
| 9. | table | Konfigurasi tabel dasar, seperti [tabel ketika kosong](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/components/table/table-no-data.jsx), [head tabel](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/components/table/table-head.jsx), [baris tabel kosong](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/components/table/table-empty-row.jsx) dan fungsi untuk [membandingka antar alemen tabel](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/components/table/utils.js) | [table](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/components/table) |

### 3) constant
Folder berisi konstanta daftar kategori yang digunakan berulang-ulang dalam program.

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | constant | Kumpulan konstanta reuseable | [constant](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/constant) |

### 4) context
Folder ini berisi pengaturan untuk state global.

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | AuthContext.jsx | Membuat context untuk menyimpan state global pada halaman yang harus memerlukan otentikasi | [AuthContext](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/context) |

### 5) hooks
Folder dengan isi custom hooks yang dibuat. Custom hooks yang dibuat antara lain:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | use-auth.js | Memanggil state global dari context | [use-auth](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-auth.js) |
| 2. | use-axios-private.js | Memanggil axios yang telah dimodifikasi sehingga memiliki header yang mengandung access token dan menghandle error ketika token tidak valid | [use-axios-private](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-axios-private.js) |
| 3. | use-pathname.js | Mendapatkan nama url path | [use-pathname](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-pathname.js) |
| 4. | use-refresh-token.js | Mendapatkan access token ketika access token expire | [use-refresh-token](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-refresh-token.js) |
| 5. | use-responsive.js | Mendapatkan kategori layar berdasarkan kelebaran | [use-responsive](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-responsive.js) |
| 6. | use-scroll-to-top.js | Shortcut scroll hingga ke atas | [use-scroll-to-top](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/hooks/use-scroll-to-top.js) |

### 6) layouts
Layout dasar untuk halaman yang diakses ketika pengguna telah login. Isi folder:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | breadcrumbs-header.jsx | Komponen widget breadcrumbs pada header | [breadcrumbs](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/common/breadcumbs-header.jsx) |
| 2. | select-peternakan.jsx | Input select untuk memilih dan berganti peternakan | [peternakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/common/select-peternakan.jsx) |
| 3. | config-layout.jsx | Konstanta ukuran untuk layout | [config-layout](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/config-layout.js) |
| 4. | config-navigation.jsx | Konstanta route untuk komponen pilihan menu di sidebar | [config-navigation](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/config-navigation.jsx) |
| 5. | header.jsx | Tatanan header | [header](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/header.jsx) |
| 6. | nav.jsx | Tatanan sidebar | [nav](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/nav.jsx) |
| 7. | index.jsx | Tatanan dari layout yang digunakan, mulai dari sidebar dan header | [index](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/layouts/admin/index.jsx) |

### 7) pages
Folder berisi pengaturan tampilan view dan metadata untuk setiap halaman. Dikelompokkan menjadi:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | daftar | Berisi metadata untuk halaman dengan konten utama menampilkan tabel. Terdiri dari: [daftar akun](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/akun.jsx), [daftar aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/aset.jsx), [neraca](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/neraca.jsx), [daftar operasional pakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/pakan.jsx), [daftar pegawai](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/pegawai.jsx), [daftar riwayat penghapusan aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/penghapusan-aset.jsx), [daftar riwayat penghapusan pegawai](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/penghapusan-pegawai.jsx). [daftar riwayat penghapusan ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/penghapusan-ternak.jsx), [daftar riwayat penjualan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/penjualan.jsx), [daftar peternakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/peternakan.jsx), [daftar riwayat operasional pupuk](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/pupuk.jsx), [daftar rekap pakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/rekap-pakan.jsx), [daftar rekap pupuk](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/rekap-pupuk.jsx), [daftar ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/ternak.jsx), dan [daftar riwayat transaksi](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/daftar/transaksi.jsx) | [daftar](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/pages/daftar) |
| 2. | detail | Berisi metadata untuk halaman dengan konten utama menampilkan detail suatu entitas. Terdiri dari: [detail aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/detail/aset.jsx), [detail pegawai](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/detail/pegawai.jsx), [detail peternakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/detail/peternakan.jsx), dan [detail ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/detail/ternak.jsx) | [detail](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/pages/detail) |
| 3. | detail | Berisi metadata untuk halaman dengan konten utama form mengedit informasi suatu entitas. Terdiri dari: [edit aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/aset.jsx), [edit akun](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/akun.jsx), [edit pegawai](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/pegawai.jsx), [edit peternakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/peternakan.jsx), [ganti password](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/ganti-password.jsx), dan [edit ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/edit/ternak.jsx) | [edit](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/pages/edit) |
| 4. | tambah | Berisi metadata untuk halaman dengan konten utama form untuk menambahkan record baru di database. Terdiri dari: [tambah akun](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/akun.jsx), [tambah aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/aset.jsx), [tambah operasional aset](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/operasional-aset.jsx), [tambah operasional pakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/operasional-pakan.jsx), [tambah operasional pupuk](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/operasional-pupuk.jsx), [tambah pegawai](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/pegawai.jsx), [tambah pupuk](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/pupuk.jsx), [tambah pakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/pakan.jsx), [tambah penggajian karyawan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/penggajian-karyawan.jsx), [tambah riwayat pengobatan ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/pengobatan-ternak.jsx), [tambah riwayat penjualan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/penjualan.jsx), [tambah peternakan](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/peternakan.jsx), [tambah ternak](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/ternak.jsx), dan [tambah riwayat transaksi](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/tambah/transaksi.jsx) | [tambah](https://git.stis.ac.id/222112280/rpl/-/tree/main/frontend/src/pages/tambah) |
| 5. | Login.jsx | Berisi metadata untuk halaman login | [Login](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/Login.jsx) |
| 6. | app.jsx | Berisi metadata untuk halaman dashboard | [app](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/app.jsx) |
| 7. | change-password.jsx | Berisi metadata untuk halaman ubah password ketika pengguna belum login | [change-password](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/change-password.jsx) |
| 8. | page-not-found.jsx | Berisi metadata untuk halaman yang tidak ditemukan | [page-not-found](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/page-not-found.jsx) |
| 9. | reset-password.jsx | Berisi metadata untuk halaman permintaan email untuk mengganti password ketika dalam keadaan belum login | [reset-password](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/pages/reset-password.jsx) |


### 8) theme
Folder pengaturan tema dasar untuk material ui. Isi folder:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | css.js | Styling untuk komponen yang reuseable | [css](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/css.js) |
| 2. | custom-shadows.js | Konfigurasi custom shadow | [custom-shadows](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/custom-shadows.js) |
| 3. | overrides.js | Konfigurasi style dasar MUI | [overrides](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/overrides.js) |
| 4. | palette.js | Konfigurasi pallete warna | [palette](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/palette.js) |
| 5. | typography.js | Konfigurasi komponen Typography | [typography](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/typography.js) |
| 6. | shadows.js | Konfigurasi override default shadow | [shadows](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/theme/shadows.js) |

### 9) utils
Folder berisi tentang kumpulan fungsi reusable. Isi folder:

| No | Nama File | Keterangan | Lokasi |
| -- | --------- | ---------- | ------ |
| 1. | axios.js | Konfigurasi base path axios | [axios](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/utils/axios.js) |
| 2. | format-number.js | Kumpulan fungsi untuk mempersingkat angka | [format-number](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/utils/format-number.js) |
| 3. | format-time.js | Kumpulan fungsi untuk manipulasi tanggal/waktu| [format-time](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/utils/format-time.js) |

### 10) views
Folder ini berisi komponen tampilan. View dikkelompokkan berdasarkan menu:
1. akun: menu manajemen akun
1. aset: menu manajemen pencatatan aset
1. dashboard: halaman dashboard
1. error: tampilan error
1. karyawan: menu manajemen pencatatan karyawan
1. laporan: menu laporan transaksi dan keuangan
1. login: menu yang tidak memerlukan login untuk akses (login, reset password)
1. pakan: menu pencatatan operasional pakan
1. penghapusan: halaman tampilan riwayat penghapusan karyawan, aset, dan ternak
1. penjualan: menu penjualan aset, ternak, pakan, atau pupuk
1. peternakan: menu pencatatan ekspansi peternakan
1. pupuk: menu penatatan operasional pupuk
1. tenak: menu penatatan operasional ternak
### 11) [App.jsx](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/App.jsx)
File ini berisi kumpulan konfigurasi route untuk program ini.
### 12) [main.jsx](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/src/main.jsx)
File ini berguna dalam inisialisasi React.js ke dalam file index.html

### 13) [vite.config.js](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/vite.config.js)

Konfigurasi vite dalam mengimplementasikan React.js

### 14) [vercel.json](https://git.stis.ac.id/222112280/rpl/-/blob/main/frontend/vercel.json)

File berisi konfigurasi untuk deployment pada vercel.
