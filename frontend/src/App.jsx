import { BrowserRouter, Route, Routes } from "react-router-dom";
import DashboardLayout from "./layouts/admin";
import ThemeProvider from "./theme";
import AppPage from "./pages/app";
import TernakPage from "./pages/daftar/ternak";
import DaftarPeternakanPage from "./pages/daftar/peternakan";
import TambahPeternakanPage from "./pages/tambah/peternakan";
import DetailPeternakanPage from "./pages/detail/peternakan";
import EditPeternakanPage from "./pages/edit/peternakan";
import DaftarAsetPage from "./pages/daftar/aset";
import TambahAsetPage from "./pages/tambah/aset";
import DetailAsetPage from "./pages/detail/aset";
import EditAsetPage from "./pages/edit/aset";
import TambahOperasionalAsetPage from "./pages/tambah/operasional-aset";
import TambahTernakPage from "./pages/tambah/ternak";
import DetailTernakPage from "./pages/detail/ternak";
import EditTernakPage from "./pages/edit/ternak";
import TambahPengobatanPage from "./pages/tambah/pengobatan-ternak";
import OperasionalPupukPage from "./pages/daftar/pupuk";
import OperasionalPakanPage from "./pages/daftar/pakan";
import PenghapusanAsetPage from "./pages/daftar/penghapusan-aset";
import PenghapusanTernakPage from "./pages/daftar/penghapusan-ternak";
import DaftarPegawaiPage from "./pages/daftar/pegawai";
import TambahPegawaiPage from "./pages/tambah/pegawai";
import PenghapusanPegawaiPage from "./pages/daftar/penghapusan-pegawai";
import DetailPegawaiPage from "./pages/detail/pegawai";
import EditPegawaiPage from "./pages/edit/pegawai";
import TransaksiPage from "./pages/daftar/transaksi";
import TambahTransaksiPage from "./pages/tambah/transaksi";
import DaftarPenjualanPage from "./pages/daftar/penjualan";
import TambahPenjualanPage from "./pages/tambah/penjualan";
import DaftarAkunPage from "./pages/daftar/akun";
import TambahAkunPage from "./pages/tambah/akun";
import GantiPasswordPage from "./pages/edit/ganti-password";
import TambahPakanPage from "./pages/tambah/pakan";
import TambahOperasionalPakanPage from "./pages/tambah/operasional-pakan";
import RekapPakan from "./pages/daftar/rekap-pakan";
import RekapPupuk from "./pages/daftar/rekap-pupuk";
import TambahPupukPage from "./pages/tambah/pupuk";
import TambahOperasionalPupukPage from "./pages/tambah/operasional-pupuk";
import LoginPage from "./pages/Login";
import ForgetPasswordPage from "./pages/reset-password";
import ChangePasswordPage from "./pages/change-password";
import NeracaPage from "./pages/daftar/neraca";
import PenggajianKaryawanPage from "./pages/tambah/penggajian-karyawan";
import EditAkunPage from "./pages/edit/akun";
import RequiredAuth from "./components/auth/required-path";
import NotFoundView from "./views/error/not-found-view";
import RequiredNonAuth from "./components/auth/auth-path";
import ChangePasswordAuth from "./components/auth/change-password";

/*
  File untuk konfigurasi route
*/

function App() {
  return (
    <ThemeProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/">
            {/* Route untuk halaman yang tidak memerlukan login, yang telah login harus logout untuk mengakses halaman ini */}
            <Route element={<RequiredNonAuth />}>
              <Route index element={<LoginPage />} />
              <Route path="forget-password" element={<ForgetPasswordPage />} />
              <Route element={<ChangePasswordAuth />}>
                <Route
                  path="change-password"
                  element={<ChangePasswordPage />}
                />
              </Route>
            </Route>
          </Route>
          <Route path="/admin" element={<DashboardLayout />}>
            {/* Route untuk menu pencatatan ternak, dapat diakses oleh pemilik dan manager */}
            <Route
              element={<RequiredAuth allowedRoles={["Pemilik", "Manager"]} />}
            >
              <Route index element={<AppPage />} />
              <Route path="ternak">
                <Route index element={<TernakPage />} />
                <Route path="tambah" element={<TambahTernakPage />} />
                <Route
                  path="riwayat-hapus"
                  element={<PenghapusanTernakPage />}
                />
                <Route path=":id">
                  <Route index element={<DetailTernakPage />} />
                  <Route path="edit" element={<EditTernakPage />} />
                  <Route
                    path="tambah-operasional"
                    element={<TambahPengobatanPage />}
                  />
                </Route>
              </Route>
            </Route>

            {/* Route untuk menu pencatatan peternakan, hanya dapat diakses oleh pemilik */}
            <Route element={<RequiredAuth allowedRoles={["Pemilik"]} />}>
              <Route path="peternakan">
                <Route index element={<DaftarPeternakanPage />} />
                <Route path="tambah" element={<TambahPeternakanPage />} />
                <Route path=":id">
                  <Route index element={<DetailPeternakanPage />} />
                  <Route path="edit" element={<EditPeternakanPage />} />
                </Route>
              </Route>
            </Route>

            {/* Route untuk menu pencatatan aset, dapat diakses oleh pemilik dan manager */}
            <Route
              element={<RequiredAuth allowedRoles={["Pemilik", "Manager"]} />}
            >
              <Route path="aset">
                <Route index element={<DaftarAsetPage />} />
                <Route path="tambah" element={<TambahAsetPage />} />
                <Route path="riwayat-hapus" element={<PenghapusanAsetPage />} />
                <Route path=":id">
                  <Route index element={<DetailAsetPage />} />
                  <Route path="edit" element={<EditAsetPage />} />
                  <Route
                    path="tambah-operasional"
                    element={<TambahOperasionalAsetPage />}
                  />
                </Route>
              </Route>

              {/* Route untuk menu pencatatan pakan, dapat diakses oleh pemilik dan manager */}
              <Route path="pakan">
                <Route index element={<OperasionalPakanPage />} />
                <Route path="tambah" element={<TambahPakanPage />} />
                <Route
                  path="tambah-operasional"
                  element={<TambahOperasionalPakanPage />}
                />
                <Route path="rekap" element={<RekapPakan />} />
              </Route>

              {/* Route untuk menu pencatatan pupuk, dapat diakses oleh pemilik dan manager */}
              <Route path="pupuk">
                <Route index element={<OperasionalPupukPage />} />
                <Route path="tambah" element={<TambahPupukPage />} />
                <Route
                  path="tambah-operasional"
                  element={<TambahOperasionalPupukPage />}
                />
                <Route path="rekap" element={<RekapPupuk />} />
              </Route>

              {/* Route untuk menu pencatatan pegawai dan karyawan, dapat diakses oleh pemilik dan manager */}
              <Route path="pegawai">
                <Route index element={<DaftarPegawaiPage />} />
                <Route path="tambah" element={<TambahPegawaiPage />} />
                <Route
                  path="riwayat-hapus"
                  element={<PenghapusanPegawaiPage />}
                />
                <Route path=":id">
                  <Route index element={<DetailPegawaiPage />} />
                  <Route path="edit" element={<EditPegawaiPage />} />
                  <Route
                    path="tambah-penggajian"
                    element={<PenggajianKaryawanPage />}
                  />
                </Route>
              </Route>

              {/* Route untuk menu pencatatan dan laporan transaksi, dapat diakses oleh pemilik dan manager */}
              <Route path="transaksi">
                <Route index element={<TransaksiPage />} />
                <Route path="tambah" element={<TambahTransaksiPage />} />
                <Route path="neraca" element={<NeracaPage />} />
              </Route>

              {/* Route untuk menu pencatatan penjualan ternak, aset, pakan, dan pupuk, dapat diakses oleh pemilik dan manager */}
              <Route path="penjualan">
                <Route index element={<DaftarPenjualanPage />} />
                <Route path="tambah" element={<TambahPenjualanPage />} />
              </Route>
            </Route>

            {/* Route untuk menu pencatatan akun, hanya dapat diakses oleh pemilik */}
            <Route path="akun">
              <Route element={<RequiredAuth allowedRoles={["Pemilik"]} />}>
                <Route index element={<DaftarAkunPage />} />
                <Route path="tambah" element={<TambahAkunPage />} />
                <Route path=":id" element={<EditAkunPage />} />
              </Route>
              <Route
                element={<RequiredAuth allowedRoles={["Pemilik", "Manager"]} />}
              >
                <Route path="ganti-password" element={<GantiPasswordPage />} />
              </Route>
            </Route>
          </Route>

          {/* Route untuk url yang tidak terdaftar */}
          <Route path="/*" element={<NotFoundView />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
