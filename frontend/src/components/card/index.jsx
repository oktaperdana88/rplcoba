import PropTypes from "prop-types";

import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

import { fShortenNumber } from "../../utils/format-number";

/*
  File yang mengatur custom card untuk keperluan ringkasan

  @params title: String => judul deskripsi ringkasan
  @params total: Number => ringkasan numerik
  @params max: Number? => Nilai maksimum dari ringkasan numerik
  @params route: String? => URL navigasi ketika ada tombol yang ditampilkan
  @params sx: Object? => Tambahan style lain
  @params other: Object? => Tambahan properti card lain
*/

const AppWidgetSummary = ({ title, total, max, sx, route, ...other }) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(route);
  };
  return (
    <Card
      component={Stack}
      spacing={3}
      direction="row"
      justifyContent={"space-between"}
      alignItems={"center"}
      sx={{
        px: 3,
        py: 5,
        borderRadius: 2,
        ...sx,
      }}
      {...other}
    >
      <Stack spacing={0.5}>
        <Typography>
          {title}
        </Typography>
        <Typography variant="h4">
          {max
            ? (total || 0) + "/" + max
            : typeof total === "string" ? total : (fShortenNumber(total) || 0)}
        </Typography>
      </Stack>

      {/* <FontAwesomeIcon icon={icon} width={64} height={64} /> */}
      <Button
        variant="contained"
        color="inherit"
        onClick={handleClick}
        size="small"
        sx={{
          fontSize: 12,
          maxHeight: 40,
        }}
      >
        Tambah
      </Button>
    </Card>
  );
};

AppWidgetSummary.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  sx: PropTypes.object,
  title: PropTypes.string,
  max: PropTypes.number,
  total: PropTypes.any,
  route: PropTypes.string,
};

export default AppWidgetSummary;
