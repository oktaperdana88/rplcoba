import React from "react";
import { NumericFormat } from "react-number-format";
import PropTypes from "prop-types";

/*
  Custom input form dengan format mata uang

  @params name: String => Input name
  @params onChange: String => Fungsi perubahan state juka isian berubah
*/

const NumericFormatCustom = React.forwardRef(function NumericFormatCustom(
  props,
  ref
) {
  const { onChange, ...other } = props;

  return (
    <NumericFormat
      {...other}
      getInputRef={ref}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator="."
      decimalSeparator=","
      valueIsNumericString
      prefix="Rp"
    />
  );
});

NumericFormatCustom.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default NumericFormatCustom;
