import PropTypes from "prop-types";

import Paper from "@mui/material/Paper";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";

/*
  Konfigurasi isi baris tabel ketika data kosong

  @params query: String => text yang akan ditampilkan ketika data kosong
  @params col: String => panjang kolom yang akan digabungkan untuk menyajikan data
*/

const TableNoData = ({ query, col }) => {
  return (
    <TableRow>
      <TableCell align="center" colSpan={col} sx={{ py: 3 }}>
        <Paper
          sx={{
            textAlign: "center",
          }}
        >
          <Typography variant="h6" paragraph>
            Tidak Ditemukan
          </Typography>

          <Typography variant="body2">
            Data &nbsp;
            <strong>&quot;{query}&quot;</strong> &nbsp; tidak ditemukan.
          </Typography>
        </Paper>
      </TableCell>
    </TableRow>
  );
};

TableNoData.propTypes = {
  query: PropTypes.string,
  col: PropTypes.number,
};

export default TableNoData;
