/*
  File berisi konstanta reuseable

  @property kategoriTernak: Kategori ternak
  @property jenisSapi: jenis spesies Sapi
  @property jenisKambing: jenis spesies Kambing
  @property jenisAyam: jenis spesies Ayam
  @property asets: kategori aset
  @property statusKepemilikan: Status kepemilikan aset
  @property metode_pembayaran: metode pembayaran aset
  @property caraDapatSapi: Cara mendapatkan sapi
  @property kelaminTernak: Cara mendapatkan pakan dan pupuk
  @property roles: kategori role akun
  @property jenisTransaksi: kategori transaksi
  @property pakans: kategori pakan
  @property penjualan: kategori penjualan
  @property bulans: nama bulan
*/

export const kategoriTernak = ["Sapi", "Ayam", "Kambing"];
export const jenisSapi = [
  "Brahmana",
  "Simental",
  "Limosin",
  "Ongole",
  "Peranakan Ongole",
  "Bali",
  "Madura",
  "Angus",
  "Brangus",
];
export const jenisKambing = [
  "Kacang",
  "Etawa",
  "Jawarandu",
  "Peranakan Etawa",
  "Boer",
  "Saanen",
  "Gembrong",
  "Boerawa",
  "Boerka",
  "Muara",
  "Kosta",
  "Marica",
  "Samosir",
];
export const jenisAyam = [
  "Kampung",
  "Broiler",
  "Hybrid",
  "Arab",
  "Cemani",
  "Pelung",
  "Bangkok",
  "Brahma",
];

//Jenis aset
export const asets = ["Transportasi", "Teknologi", "Alat & Mesin", "Lainnya"];

//Status kepemilikan aset
export const statusKepemilikan = ["Milik Sendiri", "Sewa"];

//Metode pembayaran aset
export const metode_pembayaran = ["Cash", "Kredit"];

//Cara mendapatkan sapi
export const caraDapatSapi = ["Beli", "Breeding"];

//Jenis kelamin ternak
export const jenisKelamin = ["Jantan", "Betina"];

//Cara mendapatkan pakan dan pupuk
export const caraDapatPakan = ["Beli", "Produksi Mandiri"];

//Role akun
export const roles = ["Manager", "Pemilik", "Karyawan"];

export const hapusTernak = ["mati", "hilang", "diambil maling", "lainnya"];

export const jenisTransaksi = ["Pemasukan", "Pengeluaran"];

//Jenis Pakan
export const pakans = ["Rumput", "Konsentrat", "Fermentasi"];

//Kategori penjualan
export const penjualan = ["Ternak", "Aset", "Pakan", "Pupuk"];

//Bulan dalam tahun
export const bulans = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];
