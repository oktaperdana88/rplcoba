import { node } from "prop-types";
import { createContext, useState } from "react";

/**
 * File untuk membuat state global
 * 
 * @property auth: Object => state yang berisi:
 *  - user: id user yang login
 *  - role: role user yang login
 *  - peternakan: peternakan yang dikunjungi
 *  - tanggal: tanggal berdiri peternakan yang dikunjungi
 *  - token: access token api
 * 
 * Token disimpan di localstorage, sementara lainnya di session storage
 */

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({
    user: sessionStorage.getItem("_mulyo_farm_u_"),
    role: [sessionStorage.getItem("_mulyo_farm_r_")],
    peternakan: sessionStorage.getItem("_mulyo_farm_p_"),
    tanggal: sessionStorage.getItem("_mulyo_farm_p_t_"),
    token: localStorage.getItem("_mulyo_farm_sess_id_"),
  });

  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: node,
};

export default AuthContext;
