import { Breadcrumbs, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { usePathname } from "../../../hooks/use-pathname";

/**
 * Mengatur path breadcrumbs pada layout
 * 
 * @returns {Component}
 */

const Breadcumbs = () => {
  const path = usePathname();
  const pathArray = path.split("/");
  pathArray.shift()
  return (
    <Breadcrumbs separator=">">
      <Link to={"/"}>
        <FontAwesomeIcon icon={faHome} color="#115e59" />
      </Link>
      {pathArray.map((link, index) => {
        const route = `/${pathArray.slice(0, index + 1).join("/")}`;
        const isLast = index == pathArray.length - 1;
        return isLast ? (
          <Typography key={link} color="text.primary" sx={{textDecoration: 'none'}}>
            {link}
          </Typography>
        ) : (
          <Link key={link} style={{textDecoration: "none", color: "#115e59"}} to={route}>
            {link}
          </Link>
        );
      })}
    </Breadcrumbs>
  );
};

export default Breadcumbs;
