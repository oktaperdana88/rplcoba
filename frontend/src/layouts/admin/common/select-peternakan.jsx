import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import { Select } from "@mui/material";
import { useEffect, useState } from "react";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

/**
 * Mengatur pemilihan peternakan yang akan dikunjungi pada layout
 * 
 * @returns {Component}
 */

const SelectPeternakan = () => {
  //Konfigurasi API
  const apiPrivate = useAxiosPrivate();
  //State global
  const { auth, setAuth } = useAuth();
  //Pilihan peternakan
  const [options, setOptions] = useState([]);

  //Ketika peternakan diubah
  const handleChange = (event) => {
    const selected = options.find((item) => item.id === event.target.value);
    sessionStorage.setItem("_mulyo_farm_p_", selected.id);
    sessionStorage.setItem("_mulyo_farm_p_t_", selected.tanggal_berdiri);
    setAuth({
      ...auth,
      peternakan: selected.id,
      tanggal: selected.tanggal_berdiri,
    });
  };

  //Mengambil data peternakan dari API
  const fetcher = async () => {
    let res;
    if (auth.role.find((role) => role === "Pemilik")) {
      res = await apiPrivate.get(`peternakan`);
    } else {
      res = await apiPrivate.get(`peternakan?user=${auth.user}`);
    }
    setOptions(res.data.data);
  };

  useEffect(() => {
    fetcher();
  }, []);

  return (
    <Box sx={{ width: "max", display: "flex", alignItems: "center" }}>
      <FormControl variant="standard" fullWidth>
        <Select
          id="select"
          value={auth.peternakan}
          onChange={handleChange}
          variant="standard"
          disableUnderline={true}
          sx={{
            borderBottom: "none",
            fontSize: 18,
          }}
          aria-describedby="Pilih Peternakan"
          aria-labelledby="Pilih Peternakan"
        >
          {options?.map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item.nama}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

export default SelectPeternakan;
