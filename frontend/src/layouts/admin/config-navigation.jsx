import { faCartShopping, faCoins, faCow, faFile, faFolderOpen, faHome, faHouseChimney, faNotesMedical, faSackXmark, faScaleBalanced, faSeedling, faUnlock, faUserGroup, faUserPlus, faUsers } from "@fortawesome/free-solid-svg-icons"

/**
 * Kumpulan kategori menu
 */


export const beranda = [
  {
    title: 'dashboard',
    path: '/admin',
    icon: faHome,
  },
];

export const peternkan = [
  {
    title: 'peternakan',
    path: '/admin/peternakan',
    icon: faHouseChimney,
  },
]

export const ternak = [
  {
    title: 'daftar ternak',
    path: '/admin/ternak',
    icon: faCow,
  },
  {
    title: 'riwayat hapus ternak',
    path: '/admin/ternak/riwayat-hapus',
    icon: faNotesMedical,
  },
]

export const aset = [
  {
    title: 'daftar aset',
    path: '/admin/aset',
    icon: faCoins,
  },
  {
    title: 'riwayat hapus aset',
    path: '/admin/aset/riwayat-hapus',
    icon: faNotesMedical,
  },
]

export const pakan = [
  {
    title: 'operasional pakan',
    path: '/admin/pakan',
    icon: faSeedling,
  },
  {
    title: 'rekap pakan',
    path: '/admin/pakan/rekap',
    icon: faFile,
  },
]

export const pupuk = [
  {
    title: 'operasional pupuk',
    path: '/admin/pupuk',
    icon: faSackXmark,
  },
  {
    title: 'rekap pupuk',
    path: '/admin/pupuk/rekap',
    icon: faFile,
  },
]

export const karyawan = [
  {
    title: 'karyawan',
    path: '/admin/pegawai',
    icon: faUsers,
  },
  {
    title: 'riwayat hapus karyawan',
    path: '/admin/pegawai/riwayat-hapus',
    icon: faNotesMedical,
  },
]

export const penjualan = [
  {
    title: 'penjualan',
    path: '/admin/penjualan',
    icon: faCartShopping,
  },
]

export const laporan = [
  {
    title: 'transaksi',
    path: '/admin/transaksi',
    icon: faFolderOpen,
  },
  {
    title: 'neraca',
    path: '/admin/transaksi/neraca',
    icon: faScaleBalanced,
  },
]

export const akun = [
  {
    title: 'akun terdaftar',
    path: '/admin/akun',
    icon: faUserGroup,
  },
  {
    title: 'buat akun',
    path: '/admin/akun/tambah',
    icon: faUserPlus,
  },
  {
    title: 'ganti password',
    path: '/admin/akun/ganti-password',
    icon: faUnlock,
  },
] 
