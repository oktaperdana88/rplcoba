import { useState } from "react";

import Nav from "./nav";
import Header from "./header";

import { NAV, HEADER } from "./config-layout";
import { useResponsive } from "../../hooks/use-responsive";
import { Outlet, useNavigate } from "react-router-dom";
import { Box, CircularProgress } from "@mui/material";
import { api } from "../../utils/axios";
import useAuth from "../../hooks/use-auth";
import { toast } from "../../components/alert";

/**
 * Layout untuk pengguna yang telah login
 */

const SPACING = 8;

const AdminLayout = () => {
  //Status sidebar terbuka
  const [openNav, setOpenNav] = useState(false);
  //Status loading
  const [loading, setLoading] = useState(false);
  //Status kategori layar
  const lgUp = useResponsive("up", "lg");

  //State global
  const { auth, setAuth } = useAuth();
  const navigate = useNavigate();

  //Fungsi logout
  const logout = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      await api.post("logout", {
        id: auth.user,
      });
      //Menghapus state global dan browser storage
      setAuth({ user: "", role: "" });
      localStorage.clear();
      sessionStorage.clear();
      setLoading(false);
      navigate("/");
    } catch (error) {
      setLoading(false);
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100vh",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <>
      {/* header */}
      <Header onOpenNav={() => setOpenNav(true)} />

      <Box
        sx={{
          minHeight: 1,
          display: "flex",
          flexDirection: { xs: "column", lg: "row" },
        }}
      >
        {/* Sidebar */}
        <Nav
          openNav={openNav}
          onCloseNav={() => setOpenNav(false)}
          logout={logout}
        />
        {/* Komponen main dan loading */}

        <Box
          component="main"
          sx={{
            flexGrow: 1,
            minHeight: 1,
            display: "flex",
            flexDirection: "column",
            py: `${HEADER.H_MOBILE + SPACING}px`,
            ...(lgUp && {
              px: 2,
              py: `${HEADER.H_DESKTOP + SPACING}px`,
              width: `calc(100% - ${NAV.WIDTH}px)`,
            }),
          }}
        >
          <Outlet />
        </Box>
      </Box>
    </>
  );
};

export default AdminLayout;
