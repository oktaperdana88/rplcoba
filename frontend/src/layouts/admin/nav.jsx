import { useEffect } from "react";
import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import Drawer from "@mui/material/Drawer";
import { alpha } from "@mui/material/styles";
import ListItemButton from "@mui/material/ListItemButton";

import { useResponsive } from "../../hooks/use-responsive";

import Logo from "../../components/logo";
import Scrollbar from "../../components/scrollbar";

import { NAV } from "./config-layout";
import { Box, Button, Typography } from "@mui/material";
import {
  aset,
  beranda,
  karyawan,
  laporan,
  pakan,
  penjualan,
  pupuk,
  ternak,
} from "./config-navigation";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouseChimney,
  faRightFromBracket,
  faUnlock,
  faUserGroup,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";
import { usePathname } from "../../hooks/use-pathname";
import useAuth from "../../hooks/use-auth";

/**
 * Konfigurasi komponen sidebar
 *
 * @param {boolean} openNav => status sidebar
 * @param {func} onCloseNav => buka tutup sidebar
 * @param {func} logout => fungsi logout
 * @returns {component}
 */

export default function Nav({ openNav, onCloseNav, logout }) {
  const pathname = usePathname();
  const { auth } = useAuth();

  const upLg = useResponsive("up", "lg");

  useEffect(() => {
    if (openNav) {
      onCloseNav();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const renderMenu = (
    <Stack component="nav" spacing={0.5} sx={{ px: 2 }}>
      <Typography sx={{ margin: 3 }}>Beranda</Typography>
      {beranda.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      {auth.role.find((role) => role === "Pemilik") && (
        <>
          <Typography sx={{ margin: 3 }}>Peternakan</Typography>
          <NavItem
            item={{
              title: "peternakan",
              path: "/admin/peternakan",
              icon: faHouseChimney,
            }}
          />
        </>
      )}
      <Typography sx={{ margin: 3 }}>Ternak</Typography>
      {ternak.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Aset</Typography>
      {aset.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Pakan</Typography>
      {pakan.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Pupuk</Typography>
      {pupuk.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Pengelolaan Karyawan</Typography>
      {karyawan.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Penjualan</Typography>
      {penjualan.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Laporan Keuangan</Typography>
      {laporan.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
      <Typography sx={{ margin: 3 }}>Akun</Typography>
      {auth.role.find((role) => role === "Pemilik") && (
        <>
          <NavItem
            item={{
              title: "akun terdaftar",
              path: "/admin/akun",
              icon: faUserGroup,
            }}
          />
          <NavItem
            item={{
              title: "buat akun",
              path: "/admin/akun/tambah",
              icon: faUserPlus,
            }}
          />
        </>
      )}
      <NavItem
        item={{
          title: "ganti password",
          path: "/admin/akun/ganti-password",
          icon: faUnlock,
        }}
      />
      <Button
        sx={{
          display: "flex",
          justifyContent: "start",
          ml: -4,
        }}
        onClick={logout}
      >
        <FontAwesomeIcon icon={faRightFromBracket} style={{ color: "red" }} />{" "}
        &emsp;
        <Box component="span" sx={{ color: "red" }}>
          Keluar
        </Box>
      </Button>
    </Stack>
  );

  const renderContent = (
    <Scrollbar
      sx={{
        height: 1,
        "& .simplebar-content": {
          height: 1,
          display: "flex",
          flexDirection: "column",
        },
      }}
    >
      <Logo sx={{ ml: 4, my: 3 }} />

      {renderMenu}

      <Box sx={{ flexGrow: 1 }} />
    </Scrollbar>
  );

  return (
    <Box
      sx={{
        flexShrink: { lg: 0 },
        width: { lg: NAV.WIDTH },
      }}
    >
      {upLg ? (
        <Box
          sx={{
            height: 1,
            position: "fixed",
            width: NAV.WIDTH,
            borderRight: (theme) => `dashed 1px ${theme.palette.divider}`,
          }}
        >
          {renderContent}
        </Box>
      ) : (
        <Drawer
          open={openNav}
          onClose={onCloseNav}
          PaperProps={{
            sx: {
              width: NAV.WIDTH,
            },
          }}
          aria-label="Drawer"
        >
          {renderContent}
        </Drawer>
      )}
    </Box>
  );
}

Nav.propTypes = {
  openNav: PropTypes.bool,
  onCloseNav: PropTypes.func,
  logout: PropTypes.func,
};

// ----------------------------------------------------------------------

function NavItem({ item }) {
  const pathname = usePathname();

  const active = item.path === pathname;

  return (
    <ListItemButton
      href={item.path}
      sx={{
        minHeight: 44,
        borderRadius: 0.75,
        typography: "body2",
        color: "text.secondary",
        textTransform: "capitalize",
        fontWeight: "fontWeightMedium",
        ...(active && {
          color: "primary.main",
          fontWeight: "fontWeightSemiBold",
          bgcolor: (theme) => alpha(theme.palette.primary.main, 0.08),
          "&:hover": {
            bgcolor: (theme) => alpha(theme.palette.primary.main, 0.16),
          },
        }),
      }}
    >
      <FontAwesomeIcon icon={item.icon} /> &emsp;
      <Box component="span">{item.title} </Box>
    </ListItemButton>
  );
}

NavItem.propTypes = {
  item: PropTypes.object,
};
