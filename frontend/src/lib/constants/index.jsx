import {
  faCarrot,
  faCoins,
  faCow,
  faFileInvoice,
  faGear,
  faDashboard,
  faHome,
  faPeopleGroup,
  faRecycle,
  faRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";

export const sidebar = [
  {
    key: "dashboard",
    label: "Dashboard",
    path: "/admin",
    icon: faDashboard,
  },
  {
    key: "peternakan",
    label: "Peternakan",
    path: "/admin/peternakan",
    icon: faHome,
  },
  {
    key: "ternak",
    label: "Ternak",
    path: "/admin/ternak",
    icon: faCow,
  },
  {
    key: "aset",
    label: "Aset",
    path: "/admin/aset",
    icon: faCoins,
  },
  {
    key: "pakan",
    label: "Pakan",
    path: "/admin/pakan",
    icon: faCarrot,
  },
  {
    key: "pupuk",
    label: "Pupuk",
    path: "/admin/pupuk",
    icon: faRecycle,
  },
  {
    key: "karyawan",
    label: "Pengelolaan Karyawan",
    path: "/admin/karyawan",
    icon: faPeopleGroup,
  },
  {
    key: "Penjualan",
    label: "Penjualan",
    path: "/admin/penjualan",
    icon: faFileInvoice,
  },
  {
    key: "Transaksi",
    label: "Transaksi",
    path: "/admin/transaksi",
    icon: faFileInvoice,
  },
  {
    key: "setting",
    label: "Akun",
    path: "/admin/pengaturan",
    icon: faGear,
  },
  {
    key: "keluar",
    label: "Keluar",
    path: "/admin/keluar",
    icon: faRightFromBracket,
  },
];

// export const sortTypes = [""]

export const asets = ["Transportasi", "Teknologi", "Alat & Mesin", "Lainnya"];

export const statusKepemilikan = ["Milik Sendiri", "Sewa"];

export const metode_pembayaran = ["Cash", "Kredit"];

export const caraDapatSapi = ["Beli", "Breeding"];

export const jenisSapi = [
  "Brahmana",
  "Simental",
  "Limosin",
  "Ongole",
  "Peranakan Ongole",
  "Bali",
  "Madura",
  "Angus",
  "Brangus",
];

export const jenisKelamin = ["Jantan", "Betina"];

export const caraDapatPakan = ["Beli", "Produksi Mandiri"];

export const roles = ["Manager", "Pemilik", "Karyawan"];
export const hapusTernak = ["mati", "hilang", "diambil maling", "lainnya"];

export const jenisTransaksi = ["Pemasukan", "Pengeluaran"];

export const pakans = ["Rumput", "Konsentrat", "Fermentasi"];

export const ternaks = ["Sapi", "Ayam", "Kambing"];

export const penjualan = ["Ternak", "Aset", "Pakan", "Pupuk"];

export const jenisKambing = [
  "Kacang",
  "Etawa",
  "Jawarandu",
  "Peranakan Etawa",
  "Boer",
  "Saanen",
  "Gembrong",
  "Boerawa",
  "Boerka",
  "Muara",
  "Kosta",
  "Marica",
  "Samosir",
];

export const jenisAyam = [
  "Kampung",
  "Broiler",
  "Hybrid",
  "Arab",
  "Cemani",
  "Pelung",
  "Bangkok",
  "Brahma",
];

export const bulans = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];
