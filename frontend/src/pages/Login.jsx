import { Helmet } from "react-helmet-async";
import LoginView from "../views/login/login"

// Konfigurasi metadata halaman login

const LoginPage = () => {
  return (
    <>
      <Helmet>
        <title> Login </title>
      </Helmet>

      <LoginView />
    </>
  );
};

export default LoginPage;
