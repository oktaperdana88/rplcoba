import { Helmet } from "react-helmet-async";

import { AppView } from "../views/dashboard/windows";

// Konfigurasi metadata halaman dashboard

export default function AppPage() {
  return (
    <>
      <Helmet>
        <title> Dashboard </title>
      </Helmet>

        <AppView />
    </>
  );
}
