import { Helmet } from "react-helmet-async";
import ChangePasswordView from "../views/login/ganti-password";

// Konfigurasi metadata halaman lupa password

const ChangePasswordPage = () => {
  return (
    <>
      <Helmet>
        <title> Lupa Password </title>
      </Helmet>

      <ChangePasswordView />
    </>
  );
};

export default ChangePasswordPage;
