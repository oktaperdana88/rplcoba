import { Helmet } from "react-helmet-async";
import { DaftarAkunView } from "../../views/akun/windows";

//Konfigurasi metadata halaman list akun terdaftar

const DaftarAkunPage = () => {
  return (
    <>
      <Helmet>
        <title>Akun Terdaftar</title>
      </Helmet>

      <DaftarAkunView />
    </>
  );
};

export default DaftarAkunPage;
