import { Helmet } from "react-helmet-async";
import AsetView from "../../views/aset/windows/daftar-aset";

//Konfigurasi metadata halaman list aset terdaftar

const DaftarAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Aset</title>
      </Helmet>

      <AsetView />
    </>
  );
};

export default DaftarAsetPage;
