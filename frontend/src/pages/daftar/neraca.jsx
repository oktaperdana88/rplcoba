import { Helmet } from "react-helmet-async";
import Neraca from "../../views/laporan/windows/neraca";

//Konfigurasi metadata halaman neraca

const NeracaPage = () => {
  return (
    <>
      <Helmet>
        <title>Neraca Keuangan</title>
      </Helmet>

      <Neraca />
    </>
  );
};

export default NeracaPage;
