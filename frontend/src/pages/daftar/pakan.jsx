import { Helmet } from "react-helmet-async";
import { RiwayatOperasionalPakanView } from "../../views/pakan/windows";

//Konfigurasi metadata halaman list riwayat operasional pakan

const OperasionalPakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Riwayat Operasional Pakan</title>
      </Helmet>

      <RiwayatOperasionalPakanView />
    </>
  );
};

export default OperasionalPakanPage;
