import { Helmet } from "react-helmet-async";
import { DaftarPegawaiView } from "../../views/karyawan/windows";

//Konfigurasi metadata halaman list pegawai tercatat

const DaftarPegawaiPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Pegawai</title>
      </Helmet>

      <DaftarPegawaiView />
    </>
  );
};

export default DaftarPegawaiPage;
