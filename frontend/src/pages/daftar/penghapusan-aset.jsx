import { Helmet } from "react-helmet-async";
import { HapusAsetView } from "../../views/penghapusan/windows";

//Konfigurasi metadata halaman list riwayat penghapusan aset

const PenghapusanAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Riwayat Penghapusan Aset</title>
      </Helmet>

      <HapusAsetView />
    </>
  );
};

export default PenghapusanAsetPage;