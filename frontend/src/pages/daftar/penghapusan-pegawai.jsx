import { Helmet } from "react-helmet-async";
import { HapusPegawaiView } from "../../views/penghapusan/windows";

//Konfigurasi metadata halaman list riwayat penghapusan pegawai

const PenghapusanPegawaiPage = () => {
  return (
    <>
      <Helmet>
        <title>Riwayat Penghapusan Pegawai</title>
      </Helmet>

      <HapusPegawaiView />
    </>
  );
};

export default PenghapusanPegawaiPage;