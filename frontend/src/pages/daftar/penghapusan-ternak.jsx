import { Helmet } from "react-helmet-async";
import { HapusTernakView } from "../../views/penghapusan/windows";

//Konfigurasi metadata halaman list riwayat penghapusan ternak

const PenghapusanTernakPage = () => {
  return (
    <>
      <Helmet>
        <title>Riwayat Penghapusan Ternak</title>
      </Helmet>

      <HapusTernakView />
    </>
  );
};

export default PenghapusanTernakPage;
