import { Helmet } from "react-helmet-async";
import { RiwayatPenjualanView } from "../../views/penjualan/windows";

//Konfigurasi metadata halaman penjualan

const DaftarPenjualanPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Riwayat Penjualan</title>
      </Helmet>

      <RiwayatPenjualanView />
    </>
  );
};

export default DaftarPenjualanPage;
