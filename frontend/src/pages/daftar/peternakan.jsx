import { Helmet } from "react-helmet-async";
import { DaftarPeternakanView } from "../../views/peternakan/windows";

//Konfigurasi metadata halaman list peternakan terdaftar

const DaftarPeternakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Peternakan</title>
      </Helmet>

      <DaftarPeternakanView />
    </>
  );
};

export default DaftarPeternakanPage;
