import { Helmet } from "react-helmet-async";
import { RiwayatOperasionalPupukView } from "../../views/pupuk/windows";

//Konfigurasi metadata halaman riwayat operasional pupuk

const OperasionalPupukPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Riwayat Operasional Pupuk</title>
      </Helmet>

      <RiwayatOperasionalPupukView />
    </>
  );
};

export default OperasionalPupukPage;
