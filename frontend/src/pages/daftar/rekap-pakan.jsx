import { Helmet } from "react-helmet-async";
import TabelPakan from "../../views/pakan/rekap/daftar-rekap-pakan";

//Konfigurasi metadata halaman rekap operasional pakan

const RekapPakan = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Rekap Pakan</title>
      </Helmet>

      <TabelPakan />
    </>
  );
};

export default RekapPakan;
