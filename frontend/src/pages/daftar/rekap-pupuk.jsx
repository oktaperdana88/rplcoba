import { Helmet } from "react-helmet-async";
import RekapPupuk from "../../views/pupuk/rekap/daftar-rekap-pupuk";

//Konfigurasi metadata halaman rekap operasional pupuk

const RekapPakan = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Rekap Pupuk</title>
      </Helmet>

      <RekapPupuk />
    </>
  );
};

export default RekapPakan;
