import { Helmet } from "react-helmet-async"
import { DaftarTernakPage } from "../../views/ternak/windows"

//Konfigurasi metadata halaman list ternak terdaftar

const TernakPage = () => {
  return (
    <>
    <Helmet>
        <title>Daftar Ternak</title>
    </Helmet>

    <DaftarTernakPage />
    </>
  )
}

export default TernakPage