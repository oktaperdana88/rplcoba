import { Helmet } from "react-helmet-async";
import { RiwayatTransaksiView } from "../../views/laporan/windows";
import { Container } from "@mui/material";

//Konfigurasi metadata halaman list transaksi tercatat

const TransaksiPage = () => {
  return (
    <>
      <Helmet>
        <title>Daftar Riwayat Transaksi</title>
      </Helmet>

      <Container>
        <RiwayatTransaksiView />
      </Container>
    </>
  );
};

export default TransaksiPage;
