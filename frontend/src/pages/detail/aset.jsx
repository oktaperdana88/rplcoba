import { Helmet } from "react-helmet-async";
import AsetDetailCard from "../../views/aset/windows/detail-aset-card";

//Konfigurasi metadata halaman detail aset

const DetailAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Detail Aset</title>
      </Helmet>

      <AsetDetailCard />
    </>
  );
};

export default DetailAsetPage;
