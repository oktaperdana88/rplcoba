import { Helmet } from "react-helmet-async";
import PegawaiDetailCard from "../../views/karyawan/summary/karyawan-card";

//Konfigurasi metadata halaman detail pegawai

const DetailPegawaiPage = () => {
  return (
    <>
      <Helmet>
        <title>Detail Pegawai</title>
      </Helmet>

      <PegawaiDetailCard />
    </>
  );
};

export default DetailPegawaiPage;
