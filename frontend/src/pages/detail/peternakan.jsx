import { Helmet } from "react-helmet-async";
import PeternakanDetailCard from "../../views/peternakan/windows/detail-peternakan";

//Konfigurasi metadata halaman detail peternakan

const DetailPeternakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Detail Peternakan</title>
      </Helmet>

      <PeternakanDetailCard />
    </>
  );
};

export default DetailPeternakanPage;
