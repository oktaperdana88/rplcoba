import { Helmet } from "react-helmet-async";
import { DetailTernakView } from "../../views/ternak/windows";

//Konfigurasi metadata halaman detail ternak

const DetailTernakPage = () => {
  return (
    <>
      <Helmet>
        <title>Detail Ternak</title>
      </Helmet>

      <DetailTernakView />
    </>
  );
};

export default DetailTernakPage;
