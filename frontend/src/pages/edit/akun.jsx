import { Helmet } from "react-helmet-async";
import EditRoleAkunView from "../../views/akun/windows/form-edit-akun";

//Konfigurasi metadata halaman edit akun

const EditAkunPage = () => {
  return (
    <>
      <Helmet>
        <title>Edit Akun</title>
      </Helmet>

      <EditRoleAkunView />
    </>
  );
};

export default EditAkunPage;
