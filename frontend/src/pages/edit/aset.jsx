import { Helmet } from "react-helmet-async";
import { FormAsetView } from "../../views/aset/windows";

//Konfigurasi metadata halaman edit aset

const EditAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Edit Aset</title>
      </Helmet>

      <FormAsetView />
    </>
  );
};

export default EditAsetPage;
