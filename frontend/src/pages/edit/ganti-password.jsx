import { Helmet } from "react-helmet-async";
import { GantiPasswordView } from "../../views/akun/windows";

//Konfigurasi metadata halaman ganti password akun

const GantiPasswordPage = () => {
  return (
    <>
      <Helmet>
        <title>Ganti Password</title>
      </Helmet>

      <GantiPasswordView />
    </>
  );
};

export default GantiPasswordPage;
