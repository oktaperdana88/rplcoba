import { Helmet } from "react-helmet-async";
import { FormPegawaiView } from "../../views/karyawan/windows";

//Konfigurasi metadata halaman edit pegawai

const EditPegawaiPage = () => {
  return (
    <>
      <Helmet>
        <title>Edit Pegawai</title>
      </Helmet>

      <FormPegawaiView />
    </>
  );
};

export default EditPegawaiPage;
