import { Helmet } from "react-helmet-async";
import { FormPeternakanView } from "../../views/peternakan/windows";

//Konfigurasi metadata halaman edit peternakan

const EditPeternakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Edit Peternakan</title>
      </Helmet>

      <FormPeternakanView />
    </>
  );
};

export default EditPeternakanPage;
