import { Helmet } from "react-helmet-async";
import { FormTernakView } from "../../views/ternak/windows";

//Konfigurasi metadata halaman edit ternak

const EditTernakPage = () => {
  return (
    <>
      <Helmet>
        <title>Edit Ternak</title>
      </Helmet>

      <FormTernakView />
    </>
  );
};

export default EditTernakPage;
