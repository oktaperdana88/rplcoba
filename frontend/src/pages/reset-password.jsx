import { Helmet } from "react-helmet-async";
import ForgetPasswordView from "../views/login/reset-password";

// Konfigurasi metadata halaman reset password

const ForgetPasswordPage = () => {
  return (
    <>
      <Helmet>
        <title> Lupa Password </title>
      </Helmet>

      <ForgetPasswordView />
    </>
  );
};

export default ForgetPasswordPage;
