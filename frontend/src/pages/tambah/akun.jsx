import { Helmet } from "react-helmet-async";
import { RegisterAkunView } from "../../views/akun/windows";

//Konfigurasi metadata halaman tambah akun

const TambahAkunPage = () => {
  return (
    <>
      <Helmet>
        <title>Buat Akun Baru</title>
      </Helmet>

      <RegisterAkunView />
    </>
  );
};

export default TambahAkunPage;
