import { Helmet } from "react-helmet-async";
import { FormAsetView } from "../../views/aset/windows";

//Konfigurasi metadata halaman tambah aset

const TambahAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Aset</title>
      </Helmet>

      <FormAsetView />
    </>
  );
};

export default TambahAsetPage;
