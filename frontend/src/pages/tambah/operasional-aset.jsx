import { Helmet } from "react-helmet-async";
import FormOperasionalView from "../../views/aset/operasional/operasional-form";

//Konfigurasi metadata halaman tambah operasional aset

const TambahOperasionalAsetPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Operasional Aset</title>
      </Helmet>

      <FormOperasionalView />
    </>
  );
};

export default TambahOperasionalAsetPage;
