import { Helmet } from "react-helmet-async";
import { FormOperasionalPakanView } from "../../views/pakan/windows";

//Konfigurasi metadata halaman tambah riwayat operasional pakan

const TambahOperasionalPakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Operasional Pakan</title>
      </Helmet>

      <FormOperasionalPakanView />
    </>
  );
};

export default TambahOperasionalPakanPage;
