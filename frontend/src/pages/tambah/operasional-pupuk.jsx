import { Helmet } from "react-helmet-async";
import { FormOperasionalPupukView } from "../../views/pupuk/windows";

//Konfigurasi metadata halaman tambah riwayat operasional pupuk

const TambahOperasionalPupukPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Operasional Pupuk</title>
      </Helmet>

      <FormOperasionalPupukView />
    </>
  );
};

export default TambahOperasionalPupukPage;