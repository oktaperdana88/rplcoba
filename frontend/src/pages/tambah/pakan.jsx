import { Helmet } from "react-helmet-async"
import { FormPakanView } from "../../views/pakan/windows"

//Konfigurasi metadata halaman tambah pakan

const TambahPakanPage = () => {
  return (
    <>
    <Helmet>
        <title>Tambah Pakan</title>
    </Helmet>

    <FormPakanView />
    </>
  )
}

export default TambahPakanPage