import { Helmet } from "react-helmet-async";
import { FormPegawaiView } from "../../views/karyawan/windows";

//Konfigurasi metadata halaman tambah pegawai

const TambahPegawaiPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Pegawai</title>
      </Helmet>

      <FormPegawaiView />
    </>
  );
};

export default TambahPegawaiPage;
