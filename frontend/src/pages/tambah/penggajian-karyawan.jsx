import { Helmet } from "react-helmet-async";
import FormPenggajianView from "../../views/karyawan/gaji/penggajian-form";

//Konfigurasi metadata halaman tambah riwayat penggajian pegawai

const PenggajianKaryawanPage = () => {
  return (
    <>
      <Helmet>
        <title>Penggajian</title>
      </Helmet>

      <FormPenggajianView />
    </>
  );
};

export default PenggajianKaryawanPage;
