import { Helmet } from "react-helmet-async";
import FormPengobatanView from "../../views/ternak/pengobatan/pengobatan-form";

//Konfigurasi metadata halaman tambah riwayat pengobatan pegawai

const TambahPengobatanPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Pengobatan ternak</title>
      </Helmet>

      <FormPengobatanView />
    </>
  );
};

export default TambahPengobatanPage;
