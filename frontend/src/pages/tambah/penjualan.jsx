import { Helmet } from "react-helmet-async";
import PenjualanFormView from "../../views/penjualan/windows/penjualan-form-remake";

//Konfigurasi metadata halaman tambah riwayat penjualan

const TambahPenjualanPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Riwayat Penjualan</title>
      </Helmet>

      <PenjualanFormView />
    </>
  );
};

export default TambahPenjualanPage;
