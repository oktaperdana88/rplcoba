import { Helmet } from "react-helmet-async";
import { FormPeternakanView } from "../../views/peternakan/windows";

//Konfigurasi metadata halaman tambah peternakan

const TambahPeternakanPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Peternakan</title>
      </Helmet>

      <FormPeternakanView />
    </>
  );
};

export default TambahPeternakanPage;
