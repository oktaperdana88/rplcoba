import { Helmet } from "react-helmet-async";
import { FormPupukView } from "../../views/pupuk/windows";

//Konfigurasi metadata halaman tambah pupuk

const TambahPupukPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Pupuk</title>
      </Helmet>

      <FormPupukView />
    </>
  );
};

export default TambahPupukPage;
