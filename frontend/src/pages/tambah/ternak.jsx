import { Helmet } from "react-helmet-async"
import { FormTernakView } from "../../views/ternak/windows"

//Konfigurasi metadata halaman tambah ternak

const TambahTernakPage = () => {
  return (
    <>
    <Helmet>
        <title>Tambah Ternak</title>
    </Helmet>

    <FormTernakView />
    </>
  )
}

export default TambahTernakPage