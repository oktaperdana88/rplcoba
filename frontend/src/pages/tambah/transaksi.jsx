import { Helmet } from "react-helmet-async";
import { FormTransaksiView } from "../../views/laporan/windows";

//Konfigurasi metadata halaman tambah transaksi

const TambahTransaksiPage = () => {
  return (
    <>
      <Helmet>
        <title>Tambah Riwayat Transaksi</title>
      </Helmet>

      <FormTransaksiView />
    </>
  );
};

export default TambahTransaksiPage;
