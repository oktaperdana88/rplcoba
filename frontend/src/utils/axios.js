import axios from "axios";

//Konfigurasi Axios

export const baseURL = "https://rplcoba-hn93.vercel.app/api";

export const api = axios.create({
  baseURL,
  withCredentials: true,
});

export const apiPrivate = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
  },
});
