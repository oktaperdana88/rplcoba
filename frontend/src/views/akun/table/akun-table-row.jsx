import { useState } from "react";
import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import Popover from "@mui/material/Popover";
import TableRow from "@mui/material/TableRow";
import MenuItem from "@mui/material/MenuItem";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import Iconify from "../../../components/iconify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useNavigate } from "react-router-dom";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

/**
 * View untuk baris dalam tabel akun
 *
 * @param {number} id => id akun
 * @param {String} nama => nama pemilik akun
 * @param {String} email => email akun
 * @param {String} role => pern akun
 * @param {number} no => nomor di tabel
 * @param {func} setLoading => loading penghapusan akun
 * @returns {component}
 */

const AkunTableRow = ({ id, email, nama, role, no, setLoading }) => {
  //status fitur menu detail
  const [open, setOpen] = useState(null);
  const { auth } = useAuth();
  const navigate = useNavigate();
  const apiPrivate = useAxiosPrivate();

  //Fungsi membuka popover menu detail
  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  //Fungsi menutup popover menu detail
  const handleCloseMenu = () => {
    setOpen(null);
  };

  //Fungsi menekan tombol edit
  const handleEditButton = () => {
    navigate(`/admin/akun/${id}`);
  };

  //Penghapusan akun
  const confirmAlert = async () => {
    setOpen(null);
    const result = await Swal.fire({
      title: "Apakah Anda Yakin?",
      text: "Akun akan dihapus secara permanen",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#C70039",
      confirmButtonText: "Hapus",
    });

    //Algoritma penghapusan
    if (result.value) {
      setLoading(true);
      try {
        await apiPrivate.delete(`users/${id}`);

        toast.fire({
          icon: "success",
          text: "Hapus akun berhasil",
        });

        navigate(0);
      } catch (error) {
        setLoading(false);
        toast.fire({
          icon: "error",
          text: error.message,
        });
      }
    }
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{nama}</TableCell>

        <TableCell>{email}</TableCell>

        <TableCell>{role}</TableCell>

        <TableCell>
          <IconButton
            onClick={handleOpenMenu}
            aria-label="Detail"
            title="Detail"
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>

    {/* Popover menu detail */}
      {auth.user != id && (
        <Popover
          open={!!open}
          anchorEl={open}
          onClose={handleCloseMenu}
          anchorOrigin={{ vertical: "top", horizontal: "left" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          PaperProps={{
            sx: { width: 140 },
          }}
        >
          <MenuItem onClick={handleEditButton}>
            <Iconify icon="eva:edit-fill" sx={{ mr: 2 }} />
            Edit
          </MenuItem>

          <MenuItem onClick={confirmAlert} sx={{ color: "error.main" }}>
            <Iconify icon="eva:trash-2-outline" sx={{ mr: 2 }} />
            Delete
          </MenuItem>
        </Popover>
      )}
    </>
  );
};

AkunTableRow.propTypes = {
  id: PropTypes.number,
  email: PropTypes.any,
  nama: PropTypes.any,
  role: PropTypes.any,
  no: PropTypes.number,
  setLoading: PropTypes.func,
};

export default AkunTableRow;
