import {
  Box,
  FormControl,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Select,
  Toolbar,
} from "@mui/material";
import { any, func } from "prop-types";
import Iconify from "../../../components/iconify";

/**
 * Komponen tools pilihan filter tabel akun
 * 
 * @param {String} role => role terfilter
 * @param {String} keyword => kata kunci pencarian
 * @param {func} onRoleChange => mengganti filter berdasarkan role
 * @param {func} onKeywordChange => mengganti kata pencarian berdasarkan nama dan email
 * @returns {component}
 */

const AkunTableToolbar = ({ role, onRoleChange, keyword, onKeywordChange }) => {
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        padding: 2,
        alignItems: { xs: "start", sm: "center" },
        rowGap: 1,
        width: "100%",
      }}
    >
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          gap: 2,
          width: "100%",
          justifyContent: "space-between",
        }}
      >
        {/* Searchbar */}
        <OutlinedInput
          value={keyword}
          onChange={(e) => onKeywordChange(e.target.value)}
          placeholder="Cari Akun..."
          startAdornment={
            <InputAdornment position="start">
              <Iconify
                icon="eva:search-fill"
                sx={{ color: "text.disabled", width: 20, height: 20 }}
              />
            </InputAdornment>
          }
          size="small"
        />

        {/* Filter role */}
        <FormControl
          variant="standard"
          sx={{ width: "max", textAlign: "right" }}
        >
          <Select
            id="metode"
            value={role}
            onChange={(e) => {
              onRoleChange(e.target.value);
            }}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            size="small"
            aria-label="Filter Role"
          >
            <MenuItem value="Semua Role">Semua Role</MenuItem>
            <MenuItem value="Pemilik">Pemilik</MenuItem>
            <MenuItem value="Manager">Manager</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </Toolbar>
  );
};

AkunTableToolbar.propTypes = {
  onRoleChange: func,
  onKeywordChange: func,
  role: any,
  keyword: any,
};

export default AkunTableToolbar;
