/**
 *
 * @param {Array} inputData => data akun
 * @param {func} comparator => fungsi pembanding tiap akun
 * @param {String} filterRole => role terfilter
 * @param {keyword} keyword => kata kunci pencarian
 * @returns
 */

export const applyFilter = ({ inputData, comparator, filterRole, keyword }) => {
  const stabilizedThis = inputData.map((el, index) => [el, index]);

  // Mengurutkan akun
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  inputData = stabilizedThis.map((el) => el[0]);

  // Filter berdasarkan role
  if (filterRole !== "Semua Role") {
    inputData = inputData.filter(
      (user) => user.role.toLowerCase().indexOf(filterRole.toLowerCase()) !== -1
    );
  }

  // Pencarian
  if (keyword) {
    inputData = inputData.filter(
      (user) =>
        (user.nama.toLowerCase().indexOf(keyword.toLowerCase()) &&
          user.email.toLowerCase().indexOf(keyword.toLowerCase())) !== -1
    );
  }

  return inputData;
};
