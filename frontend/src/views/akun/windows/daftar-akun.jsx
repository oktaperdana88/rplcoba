import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows, getComparator } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import AkunTableToolbar from "../table/akun-table-toolbar";
import AkunTableRow from "../table/akun-table-row";
import { applyFilter } from "../table/utils";
import { Box, Button, CircularProgress, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import Iconify from "../../../components/iconify";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import { toast } from "../../../components/alert";

/**
 * View tabel akun secara utuh
 *
 * @returns
 */

const AkunPakanView = () => {
  const apiPrivate = useAxiosPrivate();

  //Status loading
  const [loading, setLoading] = useState(true);

  //halaman tabel
  const [page, setPage] = useState(0);

  //Urutan
  const [order, setOrder] = useState("asc");

  //Data akun keseluruhan
  const [data, setData] = useState([]);

  //Urutan berdasarkan kolom
  const [orderBy, setOrderBy] = useState("name");

  //keyword pencarian
  const [keyword, setKeyword] = useState("");

  //filter role
  const [filterRole, setFilterRole] = useState("Semua Role");

  //isi perhalaman
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const navigate = useNavigate();

  //ke tambah akun
  const handleClick = () => {
    navigate("/admin/akun/tambah");
  };

  //Sorting
  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  //Pindah halaman
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  //Mengganti isi tabel perhalaman
  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  //Head tabel
  const headLabel = [
    { id: "no", label: "No" },
    { id: "nama", label: "Nama" },
    { id: "email", label: "Email" },
    { id: "role", label: "Role" },
    { id: "", label: "Aksi" },
  ];

  //meminta data akun ke API
  const fetcher = async () => {
    try {
      setLoading(true);
      const response = await apiPrivate.get("users");
      setData(response.data.data);
      setLoading(false);
    } catch (error) {
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  useEffect(() => {
    fetcher();
  }, []);

  //data terfilter
  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    filterRole,
    keyword,
  });

  //Akun tidak ditemukan
  const notFound = !dataFiltered.length && !!filterRole && !!keyword;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      {/* Judul dan navigasi tambah akun */}
      <Box
        component={"div"}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          rowGap: 1,
          width: "100%",
          mb: 2,
        }}
      >
        <Typography variant="h4">Akun Terdaftar</Typography>
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          startIcon={<Iconify icon="eva:plus-fill" />}
          size="small"
        >
          Tambah Akun
        </Button>
      </Box>
      <Card>
        {/* Toolbar akun */}
        <AkunTableToolbar
          onRoleChange={setFilterRole}
          role={filterRole}
          keyword={keyword}
          onKeywordChange={setKeyword}
        />

        {/* Tabel akun body */}
        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <AkunTableRow
                      key={row.id}
                      id={row.id}
                      email={row.email}
                      role={row.role}
                      nama={row.nama}
                      no={page * rowsPerPage + index + 1}
                      setLoading={setLoading}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={filterRole} col={5} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        {/* Paginasi */}
        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default AkunPakanView;
