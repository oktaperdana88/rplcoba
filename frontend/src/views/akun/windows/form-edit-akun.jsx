import {
  Box,
  Card,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { object, string } from "yup";
import { useFormik } from "formik";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useNavigate, useParams } from "react-router-dom";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";

/**
 * Form tambah dan edit akun
 */

//Validasi form
const validateSchema = object({
  nama: string()
    .max(100, "Nama maksimal 100 karakter")
    .matches(/^[a-zA-Z\s]{2,50}$/, "Nama tidak valid")
    .required("Nama tidak boleh kosong"),
  email: string()
    .email("Email tidak valid")
    .max(50, "Email maksimal karakter 50")
    .required("Email tidak boleh kosong"),
  role: string().required("Role tidak boleh kosong"),
});

const EditRoleAkunView = () => {
  const apiPrivate = useAxiosPrivate();
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [btnLoading, setBtnLoading] = useState(false);
  const [akun, setAkun] = useState({});
  const navigate = useNavigate();

  //Penyimpanan isi form
  const formik = useFormik({
    initialValues: {
      nama: akun?.nama || "",
      email: akun?.email || "",
      role: akun?.role || "",
    },
    validationSchema: validateSchema,
    enableReinitialize: true,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Role akun akan dirubah!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      // Pengeditan akun
      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.put(`users/${id}/changerole`, {
            role: value.role,
          });
          toast.fire({
            icon: "success",
            text: "Tambah akun berhasil",
          });
          navigate("/admin/akun");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  //Mendapatkan detail akun dari API
  const fetcher = async () => {
    setLoading(true);
    const res = await apiPrivate.get(`users/${id}`);
    setAkun(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher();
  }, [id]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Edit Role Akun</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {/* Nama pemilik akun */}
        <TextField
          label="Nama"
          name="nama"
          placeholder="Masukkan nama pemilik akun baru"
          variant="outlined"
          fullWidth
          required
          value={formik.values.nama}
          onBlur={formik.handleBlur}
          error={formik.touched.nama && Boolean(formik.errors.nama)}
          helperText={formik.touched.nama && formik.errors.nama}
          sx={{
            my: 2,
          }}
        />
        {/* Email akun */}
        <TextField
          type="email"
          label="Email"
          name="email"
          placeholder="example@gmail.com"
          variant="outlined"
          value={formik.values.email}
          onBlur={formik.handleBlur}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        {/* Pilihan Role */}
        <FormControl fullWidth>
          <InputLabel id="role" sx={{ my: 2 }}>
            Role *
          </InputLabel>
          <Select
            id="role"
            label="Role"
            name="role"
            variant="outlined"
            value={formik.values.role}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.role && Boolean(formik.errors.role)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            <MenuItem value={"Manager"}>Manager</MenuItem>
            <MenuItem value={"Pemilik"}>Pemilik</MenuItem>
          </Select>
          <span color="red">{formik.touched.role && formik.errors.role}</span>
        </FormControl>
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default EditRoleAkunView;
