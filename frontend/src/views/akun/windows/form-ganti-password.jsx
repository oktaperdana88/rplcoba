import {
  Box,
  Card,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useNavigate } from "react-router-dom";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";

/**
 * Form ganti password
 * 
 * (old => password lama, new => password baru, confirm => konfirmasi password baru)
 */

//Validsi isian
const validationSchema = yup.object({
  old: yup
    .string()
    .min(8, "Password minimal 8 karakter")
    .max(24, "Password maksimal 24 karakter")
    .required("Password wajib diisi"),
  new: yup
    .string()
    .min(8, "Password minimal 8 karakter")
    .max(24, "Password maksimal 24 karakter")
    .matches(/[0-9]/, "Password harus memiliki minimal 1 angka")
    .matches(/[a-z]/, "Password harus memiliki minimal 1 huruf kecil")
    .matches(/[A-Z]/, "Password harus memiliki minimal 1 huruf kapital")
    .matches(/[#$@!^%&*?]/, "Password minimal terdiri dari 1 karakter khusus")
    .required("Password wajib diisi"),
  confirm: yup
    .string()
    .required("Konfirmasi password harus terisi")
    .oneOf(
      [yup.ref("new")],
      "Konfirmasi password harus sesuai dengan password baru"
    ),
});

const FormGantiPassword = () => {
  const { auth } = useAuth();
  const apiPrivate = useAxiosPrivate();

  //button loading
  const [btnLoading, setBtnLoading] = useState(false);
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      old: "",
      new: "",
      confirm: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Akun password akan diganti!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      //Mengganti password submit
      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.put(`users/${auth.user}/changepassword`, {
            password: value.old,
            baru: value.new,
            confirm: value.confirm,
          });
          toast.fire({
            icon: "success",
            text: "Ganti password berhasil",
          });
          navigate("/admin/akun");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  //Password terlihat
  const [passwordVisible, setPasswordVisible] = useState({
    old: false,
    new: false,
    confirm: false,
  });
  return (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Ganti Password</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>

        {/* Input password lama */}
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="old">Password Lama *</InputLabel>
          <OutlinedInput
            id="old"
            label="Password Lama"
            name="old"
            type={passwordVisible.old ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      old: !passwordVisible.old,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.old ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.old}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.old && Boolean(formik.errors.old)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.old && formik.errors.old}
          </Box>
        </FormControl>

        {/* Input password baru */}
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="new">Password Baru *</InputLabel>
          <OutlinedInput
            id="new"
            label="Password Baru *"
            name="new"
            type={passwordVisible.new ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      new: !passwordVisible.new,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.new ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.new}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.new && Boolean(formik.errors.new)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.new && formik.errors.new}
          </Box>
        </FormControl>

        {/* Konfirmasi password baru */}
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="confirm">Konfirmasi Password *</InputLabel>
          <OutlinedInput
            id="confirm"
            label="Konfirmasi Password *"
            name="confirm"
            type={passwordVisible.confirm ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      confirm: !passwordVisible.confirm,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.confirm ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.confirm}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.confirm && Boolean(formik.errors.confirm)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.confirm && formik.errors.confirm}
          </Box>
        </FormControl>
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormGantiPassword;
