import {
  Box,
  Card,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { object, ref, string } from "yup";
import { useFormik } from "formik";
import axios from "axios";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useNavigate } from "react-router-dom";
import { baseURL } from "../../../utils/axios";
import LoadingButton from "@mui/lab/LoadingButton";

/**
 * Form Registrasi Akun
 */

//Validasi isian form
const validateSchema = object({
  nama: string()
    .max(100, "Nama maksimal 100 karakter")
    .matches(/^[a-zA-Z\s]{2,50}$/, "Nama tidak valid")
    .required("Nama tidak boleh kosong"),
  email: string()
    .email("Email tidak valid")
    .max(50, "Email maksimal karakter 50")
    .required("Email tidak boleh kosong"),
  role: string().required("Role tidak boleh kosong"),
  password: string()
    .min(8, "Password minimal 8 karakter")
    .max(24, "Password maksimal 24 karakter")
    .matches(/[0-9]/, "Password harus memiliki minimal 1 angka")
    .matches(/[a-z]/, "Password harus memiliki minimal 1 huruf kecil")
    .matches(/[A-Z]/, "Password harus memiliki minimal 1 huruf kapital")
    .matches(/[#$@!%^&*?]/, "Password minimal terdiri dari 1 karakter khusus")
    .required("Password wajib diisi"),
  confirm: string()
    .required("Konfirmasi password harus terisi")
    .oneOf(
      [ref("password")],
      "Konfirmasi password harus sesuai dengan password baru"
    ),
});

const RegisterAkunView = () => {
  //Loading pada button
  const [btnLoading, setBtnLoading] = useState(false)
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      nama: "",
      email: "",
      role: "",
      password: "",
      confirm: "",
    },
    validationSchema: validateSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Akun user akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      //Proses registrasi
      if (result.value) {
        setBtnLoading(true)
        try {
          await axios.post(`${baseURL}/register`, {
            name: value.nama,
            email: value.email,
            password: value.password,
            confirm: value.confirm,
            role: value.role,
          });
          toast.fire({
            icon: "success",
            text: "Tambah akun berhasil",
          });
          navigate("/admin/akun");
        } catch (error) {
          setBtnLoading(false)
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  //Password terlihat
  const [passwordVisible, setPasswordVisible] = useState({
    new: false,
    confirm: false,
  });

  return (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Buat Akun Baru</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>

        {/* Isian nama */}
        <TextField
          label="Nama"
          name="nama"
          placeholder="Masukkan nama pemilik akun baru"
          variant="outlined"
          fullWidth
          required
          value={formik.values.nama}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.nama && Boolean(formik.errors.nama)}
          helperText={formik.touched.nama && formik.errors.nama}
          sx={{
            my: 2,
          }}
        />

        {/* Isian email */}
        <TextField
          type="email"
          label="Email"
          name="email"
          placeholder="example@gmail.com"
          variant="outlined"
          value={formik.values.email}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />

        {/* Isian role */}
        <FormControl fullWidth>
          <InputLabel id="role" sx={{ my: 2 }}>
            Role *
          </InputLabel>
          <Select
            id="role"
            label="Role"
            name="role"
            variant="outlined"
            value={formik.values.role}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.role && Boolean(formik.errors.role)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            <MenuItem value={"Manager"}>Manager</MenuItem>
            <MenuItem value={"Pemilik"}>Pemilik</MenuItem>
          </Select>
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2, mt: -2 }}>
            {formik.touched.role && formik.errors.role}
          </Box>
        </FormControl>

        {/* Isian password */}
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="new">Password *</InputLabel>
          <OutlinedInput
            id="new"
            label="Password *"
            name="password"
            type={passwordVisible.new ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      new: !passwordVisible.new,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.new ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.password && Boolean(formik.errors.password)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.password && formik.errors.password}
          </Box>
        </FormControl>

        {/* Isian konfirmasi password */}
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="confirm">Konfirmasi Password *</InputLabel>
          <OutlinedInput
            id="confirm"
            label="Konfirmasi Password *"
            name="confirm"
            type={passwordVisible.confirm ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      confirm: !passwordVisible.confirm,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.confirm ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.confirm}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.confirm && Boolean(formik.errors.confirm)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.confirm && formik.errors.confirm}
          </Box>
        </FormControl>
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default RegisterAkunView;
