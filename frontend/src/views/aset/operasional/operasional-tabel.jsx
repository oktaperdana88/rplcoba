import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import {
  applyFilter,
  emptyRows,
  getComparator,
} from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { useParams } from "react-router-dom";
import TabelPenggajianToolbar from "./operasional-table-toolbar";
import TabelPenggajianRow from "./operasional-table-row";
import { Box, CircularProgress, Typography } from "@mui/material";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// ----------------------------------------------------------------------

const TabelOperasional = () => {
  const [page, setPage] = useState(0);

  const apiPrivate = useAxiosPrivate();

  const { id } = useParams();

  const [order, setOrder] = useState("asc");

  const [data, setData] = useState([]);

  const [sum, setSum] = useState(0);

  const [orderBy, setOrderBy] = useState("id");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [loading, setLoading] = useState(true);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const fetcher = async (id) => {
    setLoading(true);
    const response = await apiPrivate.get(`aset/${id}/operasional`);
    const data = response.data.data.map((item) => {
      const formattedHarga = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(item.transaksi.jumlah);
      return {
        ...item,
        jumlah: formattedHarga,
      };
    });
    setData(data);
    setSum(response.data.sum);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id);
  }, [id]);

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal_transaksi", label: "Tanggal Transaksi" },
    { id: "pihak_terkait", label: "Pihak Terkait" },
    { id: "deskripsi", label: "Deskripsi" },
    { id: "jumlah", label: "Jumlah (Rp)", align: "center" },
  ];

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
  });

  const notFound = !dataFiltered.length;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <>
      <Card sx={{ padding: 2 }}>
        <TabelPenggajianToolbar id={id} />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <TabelPenggajianRow
                      key={row.id}
                      tanggal={row.transaksi.tanggal_transaksi}
                      pihak_terkait={row.transaksi.pihak_terkait}
                      deskripsi={row.transaksi.deskripsi}
                      jumlah={row.jumlah}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && (
                  <TableNoData query={`Riwayat Operasional`} col={5} />
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <Typography variant="h6" sx={{ margin: 2 }}>
          Total Biaya Operasional:{" "}
          {new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
          }).format(sum)}
        </Typography>

        <TablePagination
          page={page}
          component="div"
          count={data.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </>
  );
};

export default TabelOperasional;
