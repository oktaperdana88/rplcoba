import { Button, Toolbar, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { any } from "prop-types";

const TabelOperasionalToolbar = ({ id }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(`/admin/aset/${id}/tambah-operasional`);
  };
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      <Typography variant="h4">Riwayat Operasional</Typography>
      <Button
        variant="contained"
        color="inherit"
        onClick={handleClick}
        sx={{
          display: "block",
          width: "100%",
          maxWidth: {
            xs: 150,
            sm: 200
          },
          my: 2,
        }}
        size="small"
      >
        + Tambah Operasional
      </Button>
    </Toolbar>
  );
};

TabelOperasionalToolbar.propTypes = {
  id: any,
};

export default TabelOperasionalToolbar;
