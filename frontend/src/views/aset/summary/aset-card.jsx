/* eslint-disable react/prop-types */
import { Box, Button, Card, Grid, Typography } from "@mui/material";
import Iconify from "../../../components/iconify";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useNavigate } from "react-router-dom";
import { any, bool } from "prop-types";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

const AsetCard = ({ aset, tombol }) => {
  const apiPrivate = useAxiosPrivate();
  const tgl = new Date(aset.tanggal_dapat);
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);
  return (
    <Card sx={{ padding: 2, mb: 2 }}>
      <Typography variant="h4">{aset?.nama}</Typography>
      <hr />
      <Grid container spacing={2}>
        <Grid item xs={4}>
          Tanggal Perolehan
        </Grid>
        <Grid item xs={8}>
          {`${tgl.getDate()}-${tgl.getMonth() + 1}-${tgl.getFullYear()}`}
        </Grid>
        <Grid item xs={4}>
          Jenis
        </Grid>
        <Grid item xs={8}>
          {aset.jenis}
        </Grid>
        <Grid item xs={4}>
          lokasi
        </Grid>
        <Grid item xs={8}>
          {aset.peternakan?.nama}
        </Grid>
        <Grid item xs={4}>
          Status Kepemilikan
        </Grid>
        <Grid item xs={8}>
          {aset.status_kepemilikan}
        </Grid>
        <Grid item xs={4}>
          Metode Pembayaran
        </Grid>
        <Grid item xs={8}>
          {aset.metode_pembayaran}
        </Grid>
        <Grid item xs={4}>
          Deskripsi
        </Grid>
        <Grid item xs={8}>
          {aset.deskripsi || "-"}
        </Grid>
        <Grid item xs={4}>
          Harga Beli
        </Grid>
        <Grid item xs={8}>
          {aset.status_kepemilikan !== "Milik Sendiri"
            ? "Aset bukan milik sendiri"
            : aset.metode_pembayaran !== "Cash"
            ? "Aset tidak dibeli dengan cash"
            : aset.harga_beli !== 0
            ? new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR",
              }).format(aset.harga_beli)
            : `Rp 0,00`}
        </Grid>
      </Grid>
      {tombol && (
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            gap: 1,
            justifyContent: "center",
            margin: 2,
          }}
        >
          <Button
            variant="contained"
            size="medium"
            color="success"
            onClick={() => {
              navigate(`/admin/aset/${aset.id}/edit`);
            }}
            startIcon={<Iconify icon="eva:edit-fill" />}
            sx={{ maxWidth: "6rem", fontSize: "12px" }}
          >
            Edit
          </Button>
          <Button
            variant="contained"
            size="medium"
            color="secondary"
            onClick={() => {
              navigate(
                `/admin/penjualan/tambah?cat=Aset&subcat=${aset.jenis}&id=${aset.id}`
              );
            }}
            startIcon={<Iconify icon="eva:shopping-cart-fill" />}
            sx={{ maxWidth: "6rem", fontSize: "12px" }}
          >
            Jual
          </Button>
          <LoadingButton
            loadingPosition="start"
            loading={btnLoading}
            variant="contained"
            size="medium"
            color="error"
            onClick={async () => {
              const result = await Swal.fire({
                title: "Apakah Anda Yakin?",
                text: "Anda akan menghapus aset ini. Data aset akan dihapus secara permanen.",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#C70039",
                confirmButtonText: "Hapus",
              });

              if (result.value) {
                const input = await Swal.fire({
                  title: "Masukkan catatan penghapusan pegawai!",
                  html:
                    `<div style="display: flex; flex-direction: column; width: 100%; justify-content: start; gap: 1rem;">` +
                    `<div style="display: flex; flex-direction: column; width: 100%;">` +
                    `<label for="swal-date">Tanggal Penghapusan</label>` +
                    `<input type='date' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-date' placeholder='Masukkan tanggal penghapusan' required />` +
                    `</div>` +
                    `<div style="display: flex; flex-direction: column; width: 100%;">` +
                    `<label for="swal-text" style="display: block;">Catatan Penghapusan</label>` +
                    `<input type='text' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-text' placeholder='Masukkan catatan penghapusan' required />` +
                    `</div>` +
                    `</div>`,
                  icon: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#C70039",
                  confirmButtonText: "Hapus",
                  preConfirm: () => {
                    const textValue =
                      document.getElementById("swal-text").value;
                    const dateValue =
                      document.getElementById("swal-date").value;

                    if (!textValue) {
                      Swal.showValidationMessage("Catatn tidak boleh kosong");
                      return false;
                    }

                    // Validasi input tanggal (gunakan aturan validasi sesuai kebutuhan)
                    if (!dateValue) {
                      Swal.showValidationMessage(
                        "Tanggal penghapusan tidak boleh kosong"
                      );
                      return false;
                    }

                    if (new Date(dateValue).getTime() > new Date().getTime()) {
                      Swal.showValidationMessage(
                        "Tanggal penghapusan tidak boleh melebihi hari ini"
                      );
                      return false;
                    }

                    if (
                      new Date(dateValue).getTime() <
                      new Date(aset.tanggal_dapat).getTime()
                    ) {
                      Swal.showValidationMessage(
                        "Tanggal penghapusan tidak boleh sebelum Aset diperoleh"
                      );
                      return false;
                    }

                    // Mengembalikan objek dengan nilai dari kedua input
                    return { text: textValue, date: dateValue };
                  },
                });

                if (input.isConfirmed) {
                  setBtnLoading(true);
                  try {
                    await apiPrivate.post(`aset/hapus`, {
                      id: aset.id, // Ganti dengan properti yang sesuai untuk mengidentifikasi aset yang akan dihapus
                      deskripsi: input.value.text,
                      tanggal: input.value.date,
                      jenis: aset.nama,
                      kategori: aset.jenis,
                      id_peternakan: aset.id_peternakan,
                    });

                    toast.fire({
                      icon: "success",
                      text: "Aset berhasil dihapus",
                    });

                    navigate("/admin/aset");
                  } catch (error) {
                    setBtnLoading(false);
                    toast.fire({
                      icon: "error",
                      text: error.message,
                    });
                  }
                }
              }
            }}
            startIcon={<Iconify icon="eva:trash-2-outline" />}
            sx={{ maxWidth: "6rem", fontSize: "12px" }}
          >
            Hapus
          </LoadingButton>
        </Box>
      )}
    </Card>
  );
};

AsetCard.propType = {
  aset: any.isRequired,
  tombol: bool.isRequired,
};

export default AsetCard;
