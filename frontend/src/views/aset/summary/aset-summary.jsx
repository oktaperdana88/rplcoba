import { Grid } from "@mui/material";
import AppWidgetSummary from "../../../components/card";
import { object } from "prop-types";

const AsetSummary = ({ peternakan }) => {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={3}>
        <AppWidgetSummary
          title="Transportasi"
          total={peternakan.jumlah_transportasi}
          route="/admin/aset/tambah?jenis=Transportasi"
        />
      </Grid>

      <Grid item xs={12} md={3}>
        <AppWidgetSummary
          title="Teknologi"
          total={peternakan.jumlah_teknologi}
          route="/admin/aset/tambah?jenis=Teknologi"
        />
      </Grid>

      <Grid item xs={12} md={3}>
        <AppWidgetSummary
          title="Alat & Mesin"
          total={peternakan.jumlah_alat}
          route="/admin/aset/tambah?jenis=Alat%20%26%20Mesin"
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <AppWidgetSummary
          title="Lainnya"
          total={peternakan.jumlah_lainnya}
          route="/admin/aset/tambah?jenis=Lainnya"
        />
      </Grid>
    </Grid>
  );
};

AsetSummary.propTypes = {
  peternakan: object,
};

export default AsetSummary;
