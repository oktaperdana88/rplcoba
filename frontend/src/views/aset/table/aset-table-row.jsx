import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

// ----------------------------------------------------------------------

const AsetTableRow = ({
  id,
  kode,
  jenis,
  tanggal_dapat,
  status_kepemilikan,
  metode_pembayaran,
  no,
}) => {
  const navigate = useNavigate()

  const handleOpenMenu = () => {
    navigate(`/admin/aset/${id}`)
  };

  const tgl = new Date(tanggal_dapat)

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${tgl.getMonth()+1}-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{kode}</TableCell>

        <TableCell>{jenis}</TableCell>

        <TableCell align="center">{Math.floor((new Date().getTime() - new Date(tanggal_dapat).getTime()) / (1000 * 3600 * 24))}</TableCell>

        <TableCell align="center">{status_kepemilikan}</TableCell>
        <TableCell align="center">{metode_pembayaran}</TableCell>

        <TableCell>
          <IconButton
            onClick={handleOpenMenu}
            aria-label="Detail"
            title="Detail"
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>
    </>
  );
};

AsetTableRow.propTypes = {
  tanggal_dapat: PropTypes.any,
  id: PropTypes.any,
  kode: PropTypes.any,
  jenis: PropTypes.any,
  lokasi: PropTypes.any,
  status_kepemilikan: PropTypes.any,
  metode_pembayaran: PropTypes.any,
  no: PropTypes.number,
};

export default AsetTableRow;
