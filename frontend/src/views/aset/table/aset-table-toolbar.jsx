import {
  Box,
  FormControl,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Select,
  Toolbar,
  Typography,
} from "@mui/material";
import { any, func } from "prop-types";
import { asets, metode_pembayaran, statusKepemilikan } from "../../../constant";
import Iconify from "../../../components/iconify";

const AsetTableToolbar = ({
  onFilterStatus,
  onFilterJenis,
  onFilterMetode,
  onKeywordChange,
  jenis,
  status,
  metode,
  keyword,
}) => {
  return (
    <Toolbar
      sx={{
        height: "max",
        display: "flex",
        padding: 2,
        flexDirection: "column",
        alignItems: "start",
        justifyContent: { xs: "normal", md: "space-between" },
      }}
    >
      <Box
        component={"div"}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          rowGap: 1,
          width: "100%",
          mb: 2,
        }}
      >
        <Typography variant="h4">Daftar Aset</Typography>
        <OutlinedInput
          value={keyword}
          onChange={(e) => onKeywordChange(e.target.value)}
          placeholder="Cari Aset..."
          startAdornment={
            <InputAdornment position="start">
              <Iconify
                icon="eva:search-fill"
                sx={{ color: "text.disabled", width: 20, height: 20 }}
              />
            </InputAdornment>
          }
          size="small"
        />
      </Box>
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          justifyContent: "end",
          alignItems: "start",
          gap: 2,
          flexDirection: { xs: "column", sm: "row" },
          width: "100%",
        }}
      >
        <FormControl variant="standard">
          <Select
            id="jenis"
            value={jenis}
            onChange={(e) => onFilterJenis(e.target.value)}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            aria-label="Filter Jenis"
          >
            <MenuItem value="Semua Jenis">Semua Jenis</MenuItem>
            {asets.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl variant="standard">
          <Select
            id="status"
            value={status}
            onChange={(e) => {
              onFilterStatus(e.target.value);
            }}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            aria-label="Filter Status Kepemilikan"
          >
            <MenuItem value="Semua Status Kepemilikan">
              Semua Status Kepemilikan
            </MenuItem>
            {statusKepemilikan.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl variant="standard">
          <Select
            id="metode"
            value={metode}
            onChange={(e) => {
              onFilterMetode(e.target.value);
            }}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            aria-label="Filter metode pembayaran"
          >
            <MenuItem value="Semua Metode Pembayaran">
              Semua Metode Pembayaran
            </MenuItem>
            {metode_pembayaran.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
    </Toolbar>
  );
};

AsetTableToolbar.propTypes = {
  onFilterStatus: func,
  onFilterJenis: func,
  onFilterMetode: func,
  onKeywordChange: func,
  jenis: any,
  status: any,
  metode: any,
  keyword: any,
};

export default AsetTableToolbar;
