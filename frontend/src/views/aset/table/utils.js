export const visuallyHidden = {
  border: 0,
  margin: -1,
  padding: 0,
  width: "1px",
  height: "1px",
  overflow: "hidden",
  position: "absolute",
  whiteSpace: "nowrap",
  clip: "rect(0 0 0 0)",
};

export const emptyRows = (page, rowsPerPage, arrayLength) => {
  return page ? Math.max(0, (1 + page) * rowsPerPage - arrayLength) : 0;
};

export const descendingComparator = (a, b, orderBy) => {
  if (a[orderBy] === null) {
    return 1;
  }
  if (b[orderBy] === null) {
    return -1;
  }
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};
export const getComparator = (order, orderBy) => {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
};

export const filterAset = ({
  inputData,
  comparator,
  filterJenis,
  filterStatus,
  filterMetode,
  keyword,
}) => {
  const stabilizedThis = inputData.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  inputData = stabilizedThis.map((el) => el[0]);

  if (filterJenis !== "Semua Jenis") {
    inputData = inputData.filter(
      (aset) =>
        aset.jenis.toLowerCase().indexOf(filterJenis.toLowerCase()) !== -1
    );
  }

  if (filterStatus !== "Semua Status Kepemilikan") {
    inputData = inputData.filter(
      (aset) =>
        aset.status_kepemilikan
          .toLowerCase()
          .indexOf(filterStatus.toLowerCase()) !== -1
    );
  }

  if (filterMetode !== "Semua Metode Pembayaran") {
    inputData = inputData.filter(
      (aset) =>
        aset.metode_pembayaran
          .toLowerCase()
          .indexOf(filterMetode.toLowerCase()) !== -1
    );
  }

  if (keyword) {
    inputData = inputData.filter(
      (aset) =>
        (aset.nama.toLowerCase().indexOf(keyword.toLowerCase()) &&
          aset.jenis.toLowerCase().indexOf(keyword.toLowerCase()) &&
          aset.deskripsi.toLowerCase().indexOf(keyword.toLowerCase())) !== -1
    );
  }

  return inputData;
};
