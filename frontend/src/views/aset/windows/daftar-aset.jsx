import { useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { Box, CircularProgress } from "@mui/material";
import AsetSummary from "../summary/aset-summary";
import AsetTableRow from "../table/aset-table-row";
import AsetTableToolbar from "../table/aset-table-toolbar";
import { useEffect } from "react";
import { filterAset, getComparator } from "../table/utils";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// ----------------------------------------------------------------------

const AsetView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const [peternakan, setPeternakan] = useState({});

  const [data, setData] = useState([]);

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [keyword, setKeyword] = useState("");

  const [orderBy, setOrderBy] = useState("kode");

  const [filterStatus, setFilterStatus] = useState("Semua Status Kepemilikan");

  const [filterJenis, setFilterJenis] = useState("Semua Jenis");

  const [filterMetode, setFilterMetode] = useState("Semua Metode Pembayaran");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [loading, setLoading] = useState(true);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal_dapat", label: "Tanggal Perolehan" },
    { id: "nama", label: "Nama" },
    { id: "jenis", label: "Jenis" },
    { id: "umur", label: "Umur Penggunaan", align: "center" },
    { id: "status_kepemilikan", label: "Status Kepemilikan", align: "center" },
    { id: "metode_pembayaran", label: "Metode Pembayaran", align: "center" },
    { id: "", label: "Action" },
  ];

  const fetcher = async () => {
    setLoading(true);
    const res = await apiPrivate.get(`aset?peternakan=${id_p}`);
    setData(res.data.data);
    const response = await apiPrivate.get(`peternakan/${id_p}`);
    setPeternakan(response.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id_p);
  }, [id_p]);

  const dataFiltered = filterAset({
    inputData: data,
    comparator: getComparator(order, orderBy),
    filterJenis,
    filterStatus,
    filterMetode,
    keyword,
  });

  const notFound =
    !dataFiltered.length && !!filterJenis && !!filterMetode && !!filterStatus;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container maxWidth={"lg"}>
      <Box sx={{ mb: 3 }}>
        <AsetSummary peternakan={peternakan} />
      </Box>

      <Card>
        <AsetTableToolbar
          onFilterStatus={setFilterStatus}
          status={filterStatus}
          onFilterMetode={setFilterMetode}
          metode={filterMetode}
          onFilterJenis={setFilterJenis}
          jenis={filterJenis}
          keyword={keyword}
          onKeywordChange={setKeyword}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <AsetTableRow
                      key={row.id}
                      id={row.id}
                      tanggal_dapat={row.tanggal_dapat}
                      kode={row.nama}
                      jenis={row.jenis}
                      status_kepemilikan={row.status_kepemilikan}
                      metode_pembayaran={row.metode_pembayaran}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && (
                  <TableNoData
                    query={`${filterJenis}; ${filterMetode}; ${filterStatus}`}
                    col={9}
                  />
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default AsetView;
