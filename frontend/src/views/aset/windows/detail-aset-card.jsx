import { Box, CircularProgress, Container, Grid } from "@mui/material";
import { func } from "prop-types";
import { useEffect, useState } from "react";
import TabelOperasional from "../operasional/operasional-tabel";
import AsetCard from "../summary/aset-card";
import { Navigate, useParams } from "react-router-dom";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";

const AsetDetailCard = () => {
  const { id } = useParams();
  const { auth } = useAuth();
  const apiPrivate = useAxiosPrivate();
  const [aset, setAset] = useState({});
  const [loading, setLoading] = useState(true);
  const [forbidden, setForbidden] = useState(false);

  const fetcher = async (id, apiPrivate, peternakan) => {
    setLoading(true);
    const response = await apiPrivate.get(`aset/${id}`);
    if (response.data.data.id_peternakan != peternakan) {
      setForbidden(true);
    }
    setAset(response.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/aset"} />
  ) : (
    <Container>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <AsetCard aset={aset} tombol={true} />
        </Grid>
        <Grid item xs={12}>
          <TabelOperasional />
        </Grid>
      </Grid>
    </Container>
  );
};

AsetDetailCard.propTypes = {
  setTanggal: func,
  setHarga: func,
};

export default AsetDetailCard;
