import {
  Box,
  Card,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { asets, metode_pembayaran, statusKepemilikan } from "../../../constant";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import {
  Navigate,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import { toast } from "../../../components/alert";
import Swal from "sweetalert2";
import { useEffect, useState } from "react";
import NumericFormatCustom from "../../../components/currency_field";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";

const FormAsetView = () => {
  const [data, setData] = useState({});
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const { id } = useParams();
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const [forbidden, setForbidden] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);

  const validateSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal masuk tidak bisa sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal masuk maksimal hari ini")
      .required("Tanggal tidak boleh kosong"),
    kode: string()
      .max(50, "Nama maksimal terdiri dari 50 karakter")
      .required("Nama tidak boleh kosong"),
    jenis: string().required("Jenis tidak boleh kosong"),
    status: string().required("Status kepemilikan tidak boleh kosong"),
    metode: string().required("Metode Pembayaran tidak boleh kosong"),
    harga: number()
      .min(0, "Harga bei tidak boleh negatif")
      .required("Harga Beli harus diisi"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: data?.tanggal_dapat || "",
      kode: data?.nama || "",
      jenis: data?.jenis || searchParams.get("jenis") || "",
      status: data?.status_kepemilikan || "",
      metode: data?.metode_pembayaran || "",
      harga: data?.harga_beli || 0,
      deskripsi: data?.deskripsi || "",
      pihak_terkait: "",
      peternakan: data?.id_peternakan || 1,
    },
    validationSchema: validateSchema,
    enableReinitialize: true,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Aset akan ${id ? "diperbaharui" : "ditambahkan"}!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          if (!id) {
            await apiPrivate.post(`aset/create`, {
              nama: value.kode,
              jenis: value.jenis,
              harga: value.harga,
              status_kepemilikan: value.status,
              metode_pembayaran: value.metode,
              harga_beli: value.harga,
              tanggal_dapat: value.tanggal,
              pihak_terkait: value.pihak_terkait,
              deskripsi: value.deskripsi,
              id_peternakan: id_p,
            });
          } else {
            await apiPrivate.put(`aset/${id}`, {
              nama: value.kode,
              jenis: value.jenis,
              harga: value.harga,
              status_kepemilikan: value.status,
              metode_pembayaran: value.metode,
              harga_beli: value.harga,
              tanggal_dapat: value.tanggal,
              deskripsi: value.deskripsi,
              id_peternakan: value.peternakan,
            });
          }

          toast.fire({
            icon: "success",
            text: `Aset berhasil ${id ? "diperbaharui" : "ditambahkan"}`,
          });

          navigate("/admin/aset");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  const [loading, setLoading] = useState(true);

  const fetcher = async (id, apiPrivate, peternakan) => {
    if (id) {
      setLoading(true);
      const response = await apiPrivate.get(`aset/${id}`);
      if (response.data.data.id_peternakan != peternakan) {
        setForbidden(true);
      }
      setData(response.data.data);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  const [options, setOptions] = useState([]);

  const fetcherOpt = async (apiPrivate) => {
    const res = await apiPrivate.get(
      `peternakan?size=20&current=1&role=Pemilik`
    );
    setOptions(res.data.data);
  };

  useEffect(() => {
    fetcherOpt(apiPrivate);
  }, [apiPrivate]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/aset"} />
  ) : (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h3">{id ? "Edit" : "Tambah"} Aset</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {!id && (
          <TextField
            label="Tanggal Perolehan"
            name="tanggal"
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="dd/mm/yyyy"
            variant="outlined"
            fullWidth
            required
            value={formik.values.tanggal}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
            helperText={formik.touched.tanggal && formik.errors.tanggal}
            sx={{
              my: 2,
            }}
          />
        )}
        <TextField
          label="Nama"
          name="kode"
          placeholder="Masukkan nama aset"
          variant="outlined"
          fullWidth
          required
          value={formik.values.kode}
          onChange={id ? null : formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.kode && Boolean(formik.errors.kode)}
          helperText={formik.touched.kode && formik.errors.kode}
          sx={{
            my: 2,
          }}
        />
        <FormControl fullWidth>
          <InputLabel id="jenis" sx={{ my: 2 }}>
            Jenis *
          </InputLabel>
          <Select
            id="jenis"
            label="Jenis"
            name="jenis"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            value={formik.values.jenis}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.jenis && Boolean(formik.errors.jenis)}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {asets.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <span color="red">{formik.touched.jenis && formik.errors.jenis}</span>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel id="status" sx={{ my: 2 }}>
            Status Kepemilikan *
          </InputLabel>
          <Select
            id="status"
            label="Status Kepemilikan *"
            name="status"
            variant="outlined"
            value={formik.values.status}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.status && Boolean(formik.errors.status)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {statusKepemilikan.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.status && formik.errors.status}
          </Box>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel id="metode" sx={{ my: 2 }}>
            Metode Pembayaran *
          </InputLabel>
          <Select
            id="metode"
            label="Metode Pembayaran"
            name="metode"
            variant="outlined"
            value={formik.values.metode}
            onChange={
              id
                ? null
                : (e) => {
                    formik.values.harga = 0;
                    formik.handleChange(e);
                  }
            }
            onBlur={formik.handleBlur}
            error={formik.touched.metode && Boolean(formik.errors.metode)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {metode_pembayaran.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.metode && formik.errors.metode}
          </Box>
        </FormControl>
        {!id && formik.values.metode === "Cash" && (
          <>
            <TextField
              label="Penjual"
              name="pihak_terkait"
              placeholder="Adib"
              variant="outlined"
              value={formik.values.pihak_terkait}
              onChange={id ? null : formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.pihak_terkait &&
                Boolean(formik.errors.pihak_terkait)
              }
              helperText={
                formik.touched.pihak_terkait && formik.errors.pihak_terkait
              }
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
            <TextField
              label="Harga"
              name="harga"
              placeholder="Rp10.000.000"
              variant="outlined"
              fullWidth
              required
              value={formik.values.harga}
              onChange={id ? null : formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.harga && Boolean(formik.errors.harga)}
              helperText={formik.touched.harga && formik.errors.harga}
              InputProps={{
                inputComponent: NumericFormatCustom,
              }}
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          rows={4}
          fullWidth
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          sx={{
            my: 2,
          }}
        />
        {id && (
          <FormControl fullWidth>
            <InputLabel id="peternakan" sx={{ my: 2 }}>
              Lokasi Peternakan
            </InputLabel>
            <Select
              id="peternakan"
              label="Lokasi Peternakan"
              name="peternakan"
              variant="outlined"
              value={formik.values.peternakan}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.peternakan && Boolean(formik.errors.peternakan)
              }
              sx={{
                borderBottom: "none",
                my: 2,
              }}
              required
            >
              <MenuItem selected disabled>
                Pilih Salah Satu
              </MenuItem>
              {options.map((item) => (
                <MenuItem key={item.id} value={item.id}>
                  {item.nama}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormAsetView;
