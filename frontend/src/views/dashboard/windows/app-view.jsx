import Container from "@mui/material/Container";
import Grid from "@mui/material/Unstable_Grid2";
import Typography from "@mui/material/Typography";

import AppIncomeExpenseOverview from "../app-income-expense-overview";
import AppWidgetSummary from "../app-widget-summary";
import { RiwayatTransaksiView } from "../../laporan/windows";
import { useEffect, useState } from "react";
import { Box, CircularProgress } from "@mui/material";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import { bulans } from "../../../constant";

// ----------------------------------------------------------------------

export default function AppView() {
  const formatDate = (date) => {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  const date = new Date();

  const [loading, setLoading] = useState(true);
  const [chartLoading, setChartLoading] = useState(true);
  const [chartData, setChartData] = useState({
    labels: [],
    pemasukan: [],
    pengeluaran: [],
  });
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const [data, setData] = useState({});
  const fetcher = async (id) => {
    setLoading(true);
    const response = await apiPrivate.get(`transaksi/summary?peternakan=${id}`);
    setData(response.data);
    setLoading(false);
  };

  const fetchChartData = async () => {
    try {
      setChartLoading(true);

      // Hitung tanggal 12 bulan sebelumnya dari tanggal sekarang
      const currentDate = new Date();
      const dateStart = new Date(
        currentDate.getFullYear() - 1,
        currentDate.getMonth() + 1
      );
      const formattedDateStart = formatDate(dateStart);

      // Hitung tanggal akhir bulan ini
      const dateEnd = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth() + 1,
        0
      );
      const formattedDateEnd = formatDate(dateEnd);

      const response = await apiPrivate.get(`transaksi/summaryByYear`, {
        params: {
          dateStart: formattedDateStart,
          dateEnd: formattedDateEnd,
          peternakan: id_p,
        },
      });
      setChartData(response.data);
      setChartLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetcher(id_p);
    fetchChartData(); // Ganti dengan range tanggal yang sesuai
  }, [id_p]);

  const seriesData = [
    {
      name: "Pemasukan",
      type: "area",
      fill: "gradient",
      data: chartData.pemasukan,
    },
  ];

  seriesData.push({
    name: "Pengeluaran",
    type: "line",
    fill: "solid",
    data: chartData.pengeluaran,
  });

  return !loading && !chartLoading ? (
    <Container maxWidth="xl">
      <Box
        component={"div"}
        sx={{ display: "flex", justifyContent: "space-between" }}
      >
        <Typography variant="h4" sx={{ mb: 3 }}>
          Dashboard
        </Typography>

        <Typography sx={{ mb: 3 }}>
          {`${date.getDate()} ${bulans[date.getMonth()]} ${date.getFullYear()}`}
        </Typography>
      </Box>

      <Typography variant="h6" sx={{ mb: 2 }}>
        Ringkasan Transaksi Bulan ini
      </Typography>

      <Grid container spacing={3}>
        <Grid xs={12} md={4}>
          <AppWidgetSummary
            title={`Saldo ${data.nama}`}
            total={data.kas}
            color="success"
          />
        </Grid>

        <Grid xs={12} md={4}>
          <AppWidgetSummary
            title="Pemasukan"
            total={data.pemasukan}
            color="info"
          />
        </Grid>

        <Grid xs={12} md={4}>
          <AppWidgetSummary
            title="Pengeluaran"
            total={data.pengeluaran}
            color="warning"
          />
        </Grid>

        <Grid xs={12}>
          <AppIncomeExpenseOverview
            title="Grafik Transaksi"
            subheader="Ringkasan Transaksi Setiap Bulan"
            chart={{
              labels: chartData.labels,
              series: seriesData,
            }}
          />
        </Grid>

        <Grid xs={12} md={12} lg={12}>
          <RiwayatTransaksiView />
        </Grid>
      </Grid>
    </Container>
  ) : (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  );
}
