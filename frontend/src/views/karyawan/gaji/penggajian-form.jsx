import { Card, TextField, Typography } from "@mui/material";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "../../../components/alert";
import { useEffect, useState } from "react";
import NumericFormatCustom from "../../../components/currency_field";
import LoadingButton from "@mui/lab/LoadingButton";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

const FormPenggajianView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const { id } = useParams();
  const [pegawai, setPegawai] = useState({});
  const [btnLoading, setBtnLoading] = useState(false);

  const validationSchema = object({
    tanggal: date()
      .max(new Date(), "Tanggal penggajian tidak boleh lebih dari hari ini")
      .required("Tanggal tidak boleh kosing"),
    pihak_terkait: string()
      .max(50, "Pihak terlalu panjang")
      .required("Pihak terkait tidak boleh kosong"),
    jumlah: number()
      .min(1, "jumlah tidak 0 atau kurang")
      .required("Jumlah harus diisi"),
    deskripsi: string()
      .max(512, "Deskripsi terlalu panjang")
      .required("Deskripsi tidak boleh kosong"),
  });

  const fetcher = async (id) => {
    const response = await apiPrivate.get(`pegawai/${id}`);
    setPegawai(response.data.data);
  };

  useEffect(() => {
    fetcher(id);
  }, [id]);

  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      tanggal: "",
      pihak_terkait: pegawai.nama || "",
      jumlah: 0,
      deskripsi: "",
    },
    validate: (value) => {
      const error = {};
      if (
        new Date(value.tanggal).getTime() <
        new Date(pegawai.tanggal_masuk).getTime()
      ) {
        error.tanggal = "Tanggal penggajian tidak bisa sebelum pegawai masuk";
      }
      return error;
    },
    validationSchema: validationSchema,
    enableReinitialize: true,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Transaksi akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.post(`pegawai/${id}/operasional`, {
            tanggal_transaksi: value.tanggal,
            jumlah: value.jumlah,
            deskripsi: value.deskripsi,
            pihak_terkait: value.pihak_terkait,
            id_peternakan: id_p,
          });

          toast.fire({
            icon: "success",
            text: `Transaksi berhasil ditambahkan`,
          });

          navigate(`/admin/pegawai/${id}`);
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  return (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Penggajian</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        <TextField
          label="Tanggal Transaksi"
          name="tanggal"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="dd/mm/yyyy"
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Pihak Terkait"
          name="pihak_terkait"
          placeholder="Jatinegara"
          variant="outlined"
          value={formik.values.pihak_terkait}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.pihak_terkait && Boolean(formik.errors.pihak_terkait)
          }
          helperText={
            formik.touched.pihak_terkait && formik.errors.pihak_terkait
          }
          fullWidth
          aria-readonly
          sx={{
            my: 2,
          }}
        />
        <TextField
          label="Jumlah (Rp)"
          name="jumlah"
          placeholder="100000000"
          variant="outlined"
          value={formik.values.jumlah}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.jumlah && Boolean(formik.errors.jumlah)}
          helperText={formik.touched.jumlah && formik.errors.jumlah}
          InputProps={{
            inputComponent: NumericFormatCustom,
          }}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          rows={4}
          required
          fullWidth
          sx={{
            my: 2,
          }}
        />
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          variant="contained"
          color="inherit"
          type="submit"
          sx={{
            display: "block",
            mx: "auto",
            width: 120,
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPenggajianView;
