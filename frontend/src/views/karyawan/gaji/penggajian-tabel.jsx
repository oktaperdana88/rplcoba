import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import {
  applyFilter,
  emptyRows,
  getComparator,
} from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { useParams } from "react-router-dom";
import { any, func } from "prop-types";
import TabelPenggajianToolbar from "./penggajian-table-toolbar";
import TabelPenggajianRow from "./penggajian-table-row";
import { Box, CircularProgress } from "@mui/material";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// ----------------------------------------------------------------------

const TabelPenggajian = () => {
  const apiPrivate = useAxiosPrivate();
  const [page, setPage] = useState(0);

  const { id } = useParams();

  const [order, setOrder] = useState("asc");

  const [data, setData] = useState([]);

  const [orderBy, setOrderBy] = useState("id");

  const [loading, setLoading] = useState(true);

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const fetcher = async (currentPage, id) => {
    setLoading(true);
    const response = await apiPrivate.get(`pegawai/${id}/operasional`);
    const data = response.data.data.map((item) => {
      const formattedHarga = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(item.transaksi.jumlah);
      return {
        ...item,
        jumlah: formattedHarga,
      };
    });
    setData(data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(page + 1, id);
  }, [id, page, rowsPerPage]);

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal_transaksi", label: "Tanggal Pemberian Gaji" },
    { id: "pihak_terkait", label: "Pihak Terkait" },
    { id: "jumlah", label: "Jumlah (Rp)", align: "center" },
  ];

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
  });

  const notFound = !dataFiltered.length;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Card sx={{ padding: 2 }}>
      <TabelPenggajianToolbar id={id} />

      <Scrollbar>
        <TableContainer sx={{ overflow: "unset" }}>
          <Table sx={{ minWidth: 800 }}>
            <TableHeadCustom
              order={order}
              orderBy={orderBy}
              rowCount={data.length}
              numSelected={data.length}
              onRequestSort={handleSort}
              headLabel={headLabel}
            />
            <TableBody>
              {dataFiltered
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => (
                  <TabelPenggajianRow
                    key={row.id}
                    tanggal={row.transaksi.tanggal_transaksi}
                    pihak_terkait={row.transaksi.pihak_terkait}
                    jumlah={row.jumlah}
                    no={page * rowsPerPage + index + 1}
                  />
                ))}

              <TableEmptyRow
                height={77}
                emptyRows={emptyRows(0, rowsPerPage, data.length)}
              />

              {notFound && <TableNoData query={`Riwayat Penggajian`} col={4} />}
            </TableBody>
          </Table>
        </TableContainer>
      </Scrollbar>

      <TablePagination
        page={page}
        component="div"
        count={data.length}
        labelRowsPerPage={"Baris per Halaman"}
        rowsPerPage={rowsPerPage}
        onPageChange={handleChangePage}
        rowsPerPageOptions={[5, 10, 25]}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Card>
  );
};

TabelPenggajian.propTypes = {
  sum: any,
  setSum: func,
};

export default TabelPenggajian;
