import { Button, Toolbar, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import Iconify from "../../../components/iconify";
import { any } from "prop-types";

const TabelPenggajianToolbar = ({ id}) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(`/admin/pegawai/${id}/tambah-penggajian`);
  };
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2
      }}
    >
      <Typography variant="h4">Riwayat Penggajian</Typography>
      <Button
        variant="contained"
        color="inherit"
        onClick={handleClick}
        startIcon={<Iconify icon="eva:plus-fill" />}
        sx={{
          maxWidth: 200
        }}
        size="small"
      >
        Tambah Penggajian
      </Button>
    </Toolbar>
  );
};

TabelPenggajianToolbar.propTypes = {
  id: any,
};

export default TabelPenggajianToolbar;
