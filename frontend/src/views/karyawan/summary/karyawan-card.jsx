import {
  Box,
  Button,
  Card,
  CircularProgress,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import { func } from "prop-types";
import { useEffect, useState } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import Iconify from "../../../components/iconify";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import TabelPenggajian from "../gaji/penggajian-tabel";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAddressCard,
  faCalendarDays,
  faLocationDot,
  faPhone,
  faUser,
  faWarehouse,
} from "@fortawesome/free-solid-svg-icons";
import useAuth from "../../../hooks/use-auth";
import LoadingButton from "@mui/lab/LoadingButton";

const PegawaiDetailCard = () => {
  const { id } = useParams();
  const { auth } = useAuth();
  const apiPrivate = useAxiosPrivate();
  const [loading, setLoading] = useState(true);
  const id_p = auth.peternakan;
  const [pegawai, setPegawai] = useState({});
  const [forbidden, setForbidden] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const navigate = useNavigate();
  const fetcher = async (id, apiPrivate, peternakan) => {
    setLoading(true);
    const response = await apiPrivate.get(`pegawai/${id}`);
    if (response.data.data.peternakan.id != peternakan) {
      setForbidden(true);
    }
    setPegawai(response.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/pegawai"} />
  ) : (
    <Container>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card sx={{ padding: 2, mb: 2 }}>
            <Typography variant="h4">{pegawai.nama}</Typography>
            <hr />
            <Grid container spacing={2}>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faAddressCard} /> NIK
              </Grid>
              <Grid item xs={7} sm={9}>
                {pegawai.nik}
              </Grid>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faCalendarDays} /> Tanggal Masuk
              </Grid>
              <Grid item xs={7} sm={9}>
                {`${new Date(pegawai.tanggal_masuk).getDate()}-${
                  new Date(pegawai.tanggal_masuk).getMonth() + 1
                }-${new Date(pegawai.tanggal_masuk).getFullYear()}`}
              </Grid>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faPhone} /> Nomor Telepon
              </Grid>
              <Grid item xs={7} sm={9}>
                +{pegawai.no_telpon}
              </Grid>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faUser} /> Role
              </Grid>
              <Grid item xs={7} sm={9}>
                {pegawai.role}
              </Grid>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faLocationDot} /> Alamat
              </Grid>
              <Grid item xs={7} sm={9}>
                {pegawai.alamat}
              </Grid>
              <Grid item xs={5} sm={3}>
                <FontAwesomeIcon icon={faWarehouse} /> Lokasi Kerja
              </Grid>
              <Grid item xs={7} sm={9}>
                {pegawai.peternakan?.nama}
              </Grid>
            </Grid>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                gap: 1,
                justifyContent: "center",
                margin: 2,
              }}
            >
              <Button
                variant="contained"
                size="medium"
                color="success"
                onClick={() => {
                  navigate(`/admin/pegawai/${id}/edit`);
                }}
                startIcon={<Iconify icon="eva:edit-fill" />}
                sx={{ maxWidth: "6rem", fontSize: "12px" }}
              >
                Edit
              </Button>
              <LoadingButton
                loading={btnLoading}
                loadingPosition="start"
                variant="contained"
                size="medium"
                color="error"
                onClick={async () => {
                  const result = await Swal.fire({
                    title: "Apakah Anda Yakin?",
                    text: "Anda akan menghapus pegawai ini. Data pegawai akan dihapus secara permanen.",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#C70039",
                    confirmButtonText: "Hapus",
                  });

                  if (result.value) {
                    const input = await Swal.fire({
                      title: "Masukkan catatan penghapusan pegawai!",
                      html:
                        `<div style="display: flex; flex-direction: column; width: 100%; justify-content: start; gap: 1rem;">` +
                        `<div style="display: flex; flex-direction: column; width: 100%;">` +
                        `<label for="swal-date">Tanggal Penghapusan</label>` +
                        `<input type='date' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-date' placeholder='Masukkan tanggal penghapusan' required />` +
                        `</div>` +
                        `<div style="display: flex; flex-direction: column; width: 100%;">` +
                        `<label for="swal-text" style="display: block;">Catatan Penghapusan</label>` +
                        `<input type='text' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-text' placeholder='Masukkan catatan penghapusan' required />` +
                        `</div>` +
                        `</div>`,
                      icon: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#C70039",
                      confirmButtonText: "Hapus",
                      preConfirm: () => {
                        const textValue =
                          document.getElementById("swal-text").value;
                        const dateValue =
                          document.getElementById("swal-date").value;

                        if (!textValue) {
                          Swal.showValidationMessage(
                            "Catatn tidak boleh kosong"
                          );
                          return false;
                        }

                        // Validasi input tanggal (gunakan aturan validasi sesuai kebutuhan)
                        if (!dateValue) {
                          Swal.showValidationMessage(
                            "Tanggal penghapusan tidak boleh kosong"
                          );
                          return false;
                        }

                        if (
                          new Date(dateValue).getTime() > new Date().getTime()
                        ) {
                          Swal.showValidationMessage(
                            "Tanggal penghapusan tidak boleh melebihi hari ini"
                          );
                          return false;
                        }

                        if (
                          new Date(dateValue).getTime() <
                          new Date(pegawai.tanggal_masuk).getTime()
                        ) {
                          Swal.showValidationMessage(
                            "Tanggal penghapusan tidak boleh sebelum pegawai masuk"
                          );
                          return false;
                        }

                        // Mengembalikan objek dengan nilai dari kedua input
                        return { text: textValue, date: dateValue };
                      },
                    });

                    if (input.isConfirmed) {
                      setBtnLoading(true);
                      try {
                        await apiPrivate.post(`pegawai/hapus`, {
                          id: id, // Ganti dengan properti yang sesuai untuk mengidentifikasi ternak yang akan dihapus
                          deskripsi: input.value.text,
                          tanggal: input.value.date,
                          jenis: pegawai.nama,
                          kategori: pegawai.nik,
                          id_peternakan: id_p,
                        });

                        toast.fire({
                          icon: "success",
                          text: "Hapus pegawai berhasil",
                        });

                        navigate("/admin/pegawai");
                      } catch (error) {
                        setBtnLoading(false);
                        toast.fire({
                          icon: "error",
                          text: error.message,
                        });
                      }
                    }
                  }
                }}
                startIcon={<Iconify icon="eva:trash-2-outline" />}
                sx={{ maxWidth: "6rem", fontSize: "12px" }}
              >
                Hapus
              </LoadingButton>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <TabelPenggajian />
        </Grid>
      </Grid>
    </Container>
  );
};

PegawaiDetailCard.propTypes = {
  setTanggal: func,
  setHarga: func,
};

export default PegawaiDetailCard;
