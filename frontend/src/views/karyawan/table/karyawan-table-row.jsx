import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
// ----------------------------------------------------------------------

const PegawaiTableRow = ({ nik, nama, no_telp, alamat, role, no, id }) => {
  const navigate = useNavigate()

  const handleOpenMenu = () => {
    navigate(`/admin/pegawai/${id}`)
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{nik}</TableCell>

        <TableCell>{nama}</TableCell>

        <TableCell align="center">+{no_telp}</TableCell>

        <TableCell>{alamat}</TableCell>

        <TableCell>{role}</TableCell>

        <TableCell>
          <IconButton onClick={handleOpenMenu} aria-label="Detail" title="Detail">
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>
    </>
  );
};

PegawaiTableRow.propTypes = {
  id: PropTypes.any,
  nik: PropTypes.any,
  nama: PropTypes.any,
  role: PropTypes.any,
  alamat: PropTypes.any,
  no_telp: PropTypes.any,
  no: PropTypes.number,
};

export default PegawaiTableRow;
