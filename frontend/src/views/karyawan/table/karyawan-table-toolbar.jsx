import {
  Box,
  FormControl,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Select,
  Toolbar,
} from "@mui/material";
import { any, func } from "prop-types";
import Iconify from "../../../components/iconify";

const PegawaiTableToolbar = ({
  role,
  onRoleChange,
  keyword,
  onKeywordChange,
}) => {
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: { xs: "column", sm: "row" },
        padding: 2,
        alignItems: { xs: "start", sm: "center" },
        rowGap: 1,
      }}
    >
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: { xs: "100%", sm: "max" },
          gap: 2,
          justifyContent: "space-between",
        }}
      >
        <OutlinedInput
          value={keyword}
          onChange={(e) => onKeywordChange(e.target.value)}
          placeholder="Cari Karyawan..."
          startAdornment={
            <InputAdornment position="start">
              <Iconify
                icon="eva:search-fill"
                sx={{ color: "text.disabled", width: 20, height: 20 }}
              />
            </InputAdornment>
          }
          size="small"
        />
        <FormControl
          variant="standard"
          sx={{ width: "max", textAlign: "right" }}
        >
          <Select
            id="metode"
            value={role}
            onChange={(e) => {
              onRoleChange(e.target.value);
            }}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            size="small"
          >
            <MenuItem value="Semua Role">Semua Role</MenuItem>
            <MenuItem value="Manager">Manager</MenuItem>
            <MenuItem value="Karyawan">Karyawan</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </Toolbar>
  );
};

PegawaiTableToolbar.propTypes = {
  onRoleChange: func,
  onKeywordChange: func,
  role: any,
  keyword: any,
};

export default PegawaiTableToolbar;
