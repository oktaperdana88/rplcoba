import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import PegawaiTableToolbar from "../table/karyawan-table-toolbar";
import PegawaiTableRow from "../table/karyawan-table-row";
import { applyFilter, getComparator } from "../table/utils";
import { Box, Button, CircularProgress, Typography } from "@mui/material";
import Iconify from "../../../components/iconify";
import { useNavigate } from "react-router-dom";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";

// ----------------------------------------------------------------------

const PegawaiPakanView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [orderBy, setOrderBy] = useState("id");

  const [keyword, setKeyword] = useState("");

  const [filterRole, setFilterRole] = useState("Semua Role");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "NIK" },
    { id: "nama", label: "Nama" },
    { id: "no_telp", label: "No Telepon", align: "center" },
    { id: "alamat", label: "Alamat" },
    { id: "role", label: "Role" },
    { id: "", label: "Aksi" },
  ];

  const fetcher = async () => {
    setLoading(true);
    const response = await apiPrivate.get(`pegawai?peternakan=${id_p}`);
    setData(response.data.data);
    setLoading(false);
  };

  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/admin/pegawai/tambah");
  };

  useEffect(() => {
    fetcher();
  }, [id_p]);

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    filterRole,
    keyword,
  });

  const notFound = !dataFiltered.length && !!filterRole;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Box
        component={"div"}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          rowGap: 1,
          width: "100%",
          padding: 2,
        }}
      >
        <Typography variant="h4">Daftar Pegawai</Typography>
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          startIcon={<Iconify icon="eva:plus-fill" />}
          sx={{
            maxWidth: 200,
          }}
          size="small"
        >
          Tambah Pegawai
        </Button>
      </Box>
      <Card>
        <PegawaiTableToolbar
          onRoleChange={setFilterRole}
          role={filterRole}
          keyword={keyword}
          onKeywordChange={setKeyword}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <PegawaiTableRow
                      key={row.id}
                      id={row.id}
                      nik={row.nik}
                      tanggal={row.tanggal_masuk}
                      role={row.role}
                      nama={row.nama}
                      no_telp={row.no_telpon}
                      alamat={row.alamat}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={filterRole} col={7} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          rowsPerPage={rowsPerPage}
          labelRowsPerPage={"Baris tiap Halaman"}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default PegawaiPakanView;
