import {
  Box,
  Card,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { date, object, string } from "yup";
import { useFormik } from "formik";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { toast } from "../../../components/alert";
import Swal from "sweetalert2";
import { useEffect, useState } from "react";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";
import LoadingButton from "@mui/lab/LoadingButton";

const validationSchema = object({
  tanggal: date()
    .max(new Date(), "Tanggal masuk maksimal hari ini")
    .required("Tanggal masuk wajib diisi"),
  nik: string()
    .matches(/^[0-9]{16}$/, "NIK tidak valid")
    .required("NIK tidak boleh kosong"),
  nama: string()
    .max(100, "Nama maksimal 100 karakter")
    .matches(/^[a-zA-Z\s]{2,50}$/, "Nama tidak valid")
    .required("Nama tidak boleh kosong"),
  alamat: string()
    .max(512, "Alamat terlalu panjang")
    .required("Alamat wajib diisi"),
  nomor: string()
    .matches(/^(62)\d{10,14}$/, "Nomor telepon tidak valid")
    .required("Nomor telepon harus diisi"),
  role: string().required("Role harus diisi"),
});

const FormPegawaiView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const [data, setData] = useState();
  const { id } = useParams();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [forbidden, setForbidden] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const formik = useFormik({
    initialValues: {
      tanggal: data?.tanggal_masuk || "",
      nama: data?.nama || "",
      nik: data?.nik || "",
      alamat: data?.alamat || "",
      nomor: data?.no_telpon || "",
      role: data?.role || "",
      peternakan: data?.peternakan?.id || "",
    },
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Karyawan akan ${id ? "diperbaharui" : "ditambahkan"}!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true)
        try {
          if (!id) {
            await apiPrivate.post(`pegawai/tambah`, {
              nama: value.nama,
              nik: value.nik,
              alamat: value.alamat,
              no_telpon: value.nomor,
              role: value.role,
              tanggal_masuk: value.tanggal,
              id_peternakan: id_p,
            });
          } else {
            await apiPrivate.put(`pegawai/${id}`, {
              nama: value.nama,
              nik: value.nik,
              alamat: value.alamat,
              no_telpon: value.nomor,
              role: value.role,
              tanggal_masuk: value.tanggal,
              id_peternakan: value.peternakan,
            });
          }

          toast.fire({
            icon: "success",
            text: `Karyawan berhasil ${id ? "diperbaharui" : "ditambahkan"}`,
          });

          navigate("/admin/pegawai");
        } catch (error) {
          setBtnLoading(false)
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  const fetcher = async (id, apiPrivate, peternakan) => {
    if (id) {
      setLoading(true);
      const response = await apiPrivate.get(`pegawai/${id}`);
      if (response.data.data.peternakan.id != peternakan) {
        setForbidden(true);
      }
      setData(response.data.data);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  const [options, setOptions] = useState([]);

  const fetcherOpt = async () => {
    setLoading(true);
    const res = await apiPrivate.get(
      `peternakan?size=20&current=1&role=Pemilik`
    );
    setOptions(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcherOpt();
  }, []);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/pegawai"} />
  ) : (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h3" sx={{ textAlign: "center" }}>
        {id ? "Edit" : "Tambah"} Pegawai
      </Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {!id && (
          <TextField
            label="Tanggal Masuk"
            name="tanggal"
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="dd/mm/yyyy"
            variant="outlined"
            value={formik.values.tanggal}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
            helperText={formik.touched.tanggal && formik.errors.tanggal}
            fullWidth
            required
            sx={{
              my: 2,
            }}
          />
        )}
        <TextField
          type="number"
          label="NIK"
          name="nik"
          placeholder="Masukkan NIK pegawai"
          variant="outlined"
          value={formik.values.nik}
          onChange={id ? null : formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.nik && Boolean(formik.errors.nik)}
          helperText={formik.touched.nik && formik.errors.nik}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          label="Nama"
          name="nama"
          placeholder="Masukkan nama pegawai"
          variant="outlined"
          value={formik.values.nama}
          onChange={id ? null : formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.nama && Boolean(formik.errors.nama)}
          helperText={formik.touched.nama && formik.errors.nama}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Alamat"
          name="alamat"
          placeholder="Jatinegara"
          variant="outlined"
          value={formik.values.alamat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.alamat && Boolean(formik.errors.alamat)}
          helperText={formik.touched.alamat && formik.errors.alamat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="number"
          label="Nomor Telepon"
          name="nomor"
          placeholder="ex: 6285845631234"
          variant="outlined"
          value={formik.values.nomor}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.nomor && Boolean(formik.errors.nomor)}
          helperText={formik.touched.nomor && formik.errors.nomor}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <FormControl fullWidth>
          <InputLabel id="role" sx={{ my: 2 }}>
            Role *
          </InputLabel>
          <Select
            id="role"
            label="Role"
            name="role"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
            value={formik.values.role}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.role && Boolean(formik.errors.role)}
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            <MenuItem value="Manager">Manager</MenuItem>
            <MenuItem value="Karyawan">Karyawan</MenuItem>
          </Select>

          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -2, ml: 2 }}
          >
            {formik.touched.role && formik.errors.role}
          </Box>
        </FormControl>
        {id && (
          <FormControl fullWidth>
            <InputLabel id="peternakan" sx={{ my: 2 }}>
              Lokasi Peternakan
            </InputLabel>
            <Select
              id="peternakan"
              label="Lokasi Peternakan"
              name="peternakan"
              variant="outlined"
              value={formik.values.peternakan}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.peternakan && Boolean(formik.errors.peternakan)
              }
              sx={{
                borderBottom: "none",
                my: 2,
              }}
              required
            >
              <MenuItem selected disabled>
                Pilih Salah Satu
              </MenuItem>
              {options.map((item) => (
                <MenuItem key={item.id} value={item.id}>
                  {item.nama}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPegawaiView;
