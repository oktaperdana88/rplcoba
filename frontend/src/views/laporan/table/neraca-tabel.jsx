import { useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import {
  applyFilter,
  emptyRows,
  getComparator,
} from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { array } from "prop-types";
import { TableCell, TableRow } from "@mui/material";

/**
 * Tabel neraca
 * 
 * @param {Array} data => Data transaksi menurut jenis transaksi (pemasukan/pengeluaran)
 * @returns 
 */

const TabelNeraca = ({ data }) => {

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [orderBy, setOrderBy] = useState("id");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal_transaksi", label: "Tanggal" },
    { id: "deskripsi", label: "Deskripsi" },
    { id: "pihak_terkait", label: "Pihak Terkait" },
    { id: "jumlah", label: "Jumlah", align: "center" },
  ];

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
  });

  const notFound = !dataFiltered.length;

  return (
    <Card>
      <Scrollbar>
        <TableContainer sx={{ overflow: "unset" }}>
          <Table sx={{ minWidth: 800 }}>
            <TableHeadCustom
              order={order}
              orderBy={orderBy}
              rowCount={data.length}
              numSelected={data.length}
              onRequestSort={handleSort}
              headLabel={headLabel}
            />
            <TableBody>
              {dataFiltered
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => (
                  <TableRow key={index}>
                    <TableCell>{page * rowsPerPage + index + 1}</TableCell>
                    <TableCell>
                      {`${new Date(row.tanggal_transaksi).getDate()}-${
                        new Date(row.tanggal_transaksi).getMonth() + 1
                      }-${new Date(row.tanggal_transaksi).getFullYear()}`}
                    </TableCell>
                    <TableCell>{row.deskripsi}</TableCell>
                    <TableCell>{row.pihak_terkait}</TableCell>
                    <TableCell align="center">{row.jumlah}</TableCell>
                  </TableRow>
                ))}

              <TableEmptyRow
                height={77}
                emptyRows={emptyRows(0, rowsPerPage, data.length)}
              />

              {notFound && <TableNoData query={"Riwayat Transaksi"} col={5} />}
            </TableBody>
          </Table>
        </TableContainer>
      </Scrollbar>

      <TablePagination
        page={page}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        labelRowsPerPage={"Baris tiap Halaman"}
        onPageChange={handleChangePage}
        rowsPerPageOptions={[5, 10, 25]}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Card>
  );
};

TabelNeraca.propTypes = {
  data: array,
};

export default TabelNeraca;
