import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";

/**
 * 
 * @param {Date} tanggal => tanggal transaksi
 * @param {String} jenis => jenis transaksi
 * @param {String} deskripsi => deskripsi transaksi
 * @param {String} pihak => pihak terkait dalam transaksi
 * @param {Number} no => nomor baris transaksi
 * @param {Number} jumlah => jumlah nominal transaksi
 * @returns 
 */

const TransaksiTableRow = ({ tanggal, jenis, deskripsi, jumlah, no, pihak }) => {
  const tgl = new Date(tanggal)
  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${tgl.getMonth()+1}-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{jenis}</TableCell>
        <TableCell>{deskripsi}</TableCell>
        <TableCell>{pihak}</TableCell>

        <TableCell>{jumlah}</TableCell>
      </TableRow>
    </>
  );
};

TransaksiTableRow.propTypes = {
  tanggal: PropTypes.any,
  deskripsi: PropTypes.any,
  jenis: PropTypes.any,
  jumlah: PropTypes.any,
  pihak: PropTypes.any,
  no: PropTypes.number,
};

export default TransaksiTableRow;
