import {
  Box,
  FormControl,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Select,
  Toolbar,
} from "@mui/material";
import Iconify from "../../../components/iconify";
import { any, func } from "prop-types";

/**
 * 
 * @param {String} keyword
 * @param {String} jenis
 * @param {func} onKeywordChange => search bar
 * @param {func} onJenisChange => filter jenis
 * @returns 
 */

const TransaksiTableToolbar = ({
  keyword,
  onKeywordChange,
  jenis,
  onJenisChange,
}) => {
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2,
      }}
    >
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: {xs: "column",sm: "row"},
          alignItems: {sm: "center"},
          gap: 2,
          width: "100%",
          justifyContent: "space-between",
        }}
      >
        <OutlinedInput
          value={keyword}
          onChange={(e) => onKeywordChange(e.target.value)}
          placeholder="Cari Riwayat Transaksi..."
          startAdornment={
            <InputAdornment position="start">
              <Iconify
                icon="eva:search-fill"
                sx={{ color: "text.disabled", width: 20, height: 20 }}
              />
            </InputAdornment>
          }
          size="small"
        />
        <FormControl
          variant="standard"
          sx={{ width: "max", textAlign: "right" }}
        >
          <Select
            id="metode"
            value={jenis}
            onChange={(e) => {
              onJenisChange(e.target.value);
            }}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
            size="small"
            aria-label="Filter Jenis"
          >
            <MenuItem value="Semua Jenis">Semua Jenis</MenuItem>
            <MenuItem value="Pemasukan">Pemasukan</MenuItem>
            <MenuItem value="Pengeluaran">Pengeluaran</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </Toolbar>
  );
};

TransaksiTableToolbar.propTypes = {
  keyword: any,
  jenis: any,
  onJenisChange: func,
  onKeywordChange: func,
};

export default TransaksiTableToolbar;
