/**
 * Fungsi filter untuk dataFiltered
 * 
 * @param {Array} inputData
 * @param {func} comparator
 * @param {String} filterJenis
 * @param {String} keyword
 * @returns 
 */

export const applyFilter = ({
  inputData,
  comparator,
  filterJenis,
  keyword,
}) => {
  const stabilizedThis = inputData.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  inputData = stabilizedThis.map((el) => el[0]);

  if (filterJenis !== "Semua Jenis") {
    inputData = inputData.filter(
      (transaksi) =>
        transaksi.jenis_transaksi
          .toLowerCase()
          .indexOf(filterJenis.toLowerCase()) !== -1
    );
  }

  if (keyword) {
    inputData = inputData.filter(
      (transaksi) =>
        transaksi.deskripsi.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
    );
  }

  return inputData;
};
