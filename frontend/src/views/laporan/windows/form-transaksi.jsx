import {
  Box,
  Card,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { jenisTransaksi } from "../../../constant";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import Swal from "sweetalert2";
import { useNavigate, useSearchParams } from "react-router-dom";
import { toast } from "../../../components/alert";
import NumericFormatCustom from "../../../components/currency_field";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

/**
 * Form penambahan transaksi
 * 
 * @returns 
 */

const FormTransaksiView = () => {
  const [searchParams] = useSearchParams();
  const cat = searchParams.get("cat");
  const pihak = searchParams.get("pihak");
  const desc = searchParams.get("desc");
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);

  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal transaksi tidak boleh lebih sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal transaksi tidak boleh lebih dari hari ini")
      .required("Tanggal tidak boleh kosing"),
    jenis: string().required("Jenis tidak boleh kosong"),
    pihak_terkait: string()
      .max(50, "Pihak terlalu panjang")
      .required("Pihak terkait tidak boleh kosong"),
    jumlah: number()
      .min(0, "jumlah tidak boleh negatif")
      .required("Jumlah harus diisi"),
    deskripsi: string()
      .max(512, "Deskripsi terlalu panjang")
      .required("Deskrpisi tidak boleh kosong"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: "",
      jenis: cat || "",
      pihak_terkait: pihak || "",
      jumlah: 0,
      deskripsi: desc || "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Transaksi akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.post(`transaksi`, {
            tanggal_transaksi: value.tanggal,
            harga: value.jumlah,
            jenis: value.jenis,
            deskripsi: value.deskripsi,
            pihak_terkait: value.pihak_terkait,
            id_peternakan: id_p,
          });

          toast.fire({
            icon: "success",
            text: `Transaksi berhasil ditambahkan`,
          });

          navigate("/admin/transaksi");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  return (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Transaksi</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        <TextField
          label="Tanggal Transaksi"
          name="tanggal"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="dd/mm/yyyy"
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <FormControl fullWidth>
          <InputLabel id="jenis" sx={{ my: 2 }}>
            Jenis Transaksi *
          </InputLabel>
          <Select
            id="jenis"
            label="Jenis Transaksi *"
            name="jenis"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
            value={formik.values.jenis}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.jenis && Boolean(formik.errors.jenis)}
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {jenisTransaksi.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.jenis && formik.errors.jenis}
          </Box>
        </FormControl>
        <TextField
          type="text"
          label="Pihak Terkait"
          name="pihak_terkait"
          placeholder="PT Abadi"
          variant="outlined"
          value={formik.values.pihak_terkait}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.pihak_terkait && Boolean(formik.errors.pihak_terkait)
          }
          helperText={
            formik.touched.pihak_terkait && formik.errors.pihak_terkait
          }
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          label="Jumlah (Rp)"
          name="jumlah"
          placeholder="100000000"
          variant="outlined"
          value={formik.values.jumlah}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.jumlah && Boolean(formik.errors.jumlah)}
          helperText={formik.touched.jumlah && formik.errors.jumlah}
          InputProps={{
            inputComponent: NumericFormatCustom,
          }}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          rows={4}
          required
          fullWidth
          sx={{
            my: 2,
          }}
        />
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormTransaksiView;
