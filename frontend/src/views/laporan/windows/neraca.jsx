import {
  Box,
  Card,
  CircularProgress,
  Container,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import TabelNeraca from "../table/neraca-tabel";
import { bulans } from "../../../constant";
import { useEffect, useState } from "react";
import ContentNotFoundView from "../../error/content-not-found-view";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { toast } from "../../../components/alert";

const Neraca = () => {
  const { auth } = useAuth();

  //id peternakan dikunjungi
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  //bulan terpilih
  const [bulan, setBulan] = useState(new Date().getMonth() + 1);

  //tahun terpilih
  const [tahun, setTahun] = useState(new Date().getFullYear());

  //total transaksi keluar
  const [totalKeluar, setTotalKeluar] = useState(0);

  //Total transaksi masuk
  const [totalMasuk, setTotalMasuk] = useState(0);

  //kas saat ini
  const [kasNow, setKasNow] = useState(0);

  //kas bulan sebelumnya
  const [kasBefore, setKasBefore] = useState(0);

  //status loading
  const [loading, setLoading] = useState(true);

  //download pdf loading
  const [pdfLoading, setPdfLoading] = useState(false);

  //download excel loading
  const [excelLoading, setExcelLoading] = useState(false);

  //daftar pemasukan
  const [pemasukan, setPemasukan] = useState([]);

  //daftar pengeluaran
  const [pengeluaran, setPengeluaran] = useState([]);

  //fungsi download pdf
  const handleClickPDF = async () => {
    setPdfLoading(true);
    try {
      const response = await apiPrivate.get(
        `transaksi/download/pdf?peternakan=${id_p}&bulan=${bulan}&tahun=${tahun}`,
        {
          responseType: "blob",
        }
      );
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        `laporan_pemasukan_${bulans[bulan - 1]}_${tahun}.pdf`
      );
      document.body.appendChild(link);
      link.click();
      setPdfLoading(false);
    } catch (error) {
      setPdfLoading(false);
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  // Fungsi download excel
  const handleClickExcel = async () => {
    setExcelLoading(true);
    try {
      const response = await apiPrivate.get(
        `transaksi/download/excel?peternakan=${id_p}&bulan=${bulan}&tahun=${tahun}`,
        {
          responseType: "blob",
        }
      );
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        `laporan_pemasukan_${bulans[bulan - 1]}_${tahun}.xlsx`
      );
      document.body.appendChild(link);
      link.click();
      setExcelLoading(false);
    } catch (error) {
      setExcelLoading(false);
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  // Semua tahun sejak peternakan berdiri
  const getAllYear = () => {
    const element = [];
    for (
      let i = new Date(auth.tanggal).getFullYear();
      i <= new Date().getFullYear();
      i++
    ) {
      element.push(i);
    }
    return element;
  };

  const allYear = getAllYear();

  // mengambil data transaksi

  const fetch = async (id_p, month, year) => {
    try {
      setLoading(true);
      const pemasukan = await apiPrivate.get(
        `transaksi?&jenis=Pemasukan&month=${month}&year=${year}&peternakan=${id_p}`
      );
      const data = pemasukan.data.data.map((item) => {
        const formattedHarga = new Intl.NumberFormat("id-ID", {
          style: "currency",
          currency: "IDR",
        }).format(item.jumlah);
        return {
          ...item,
          jumlah: formattedHarga,
        };
      });

      setPemasukan(data);
      setTotalMasuk(pemasukan.data.sum);
      setKasBefore(pemasukan.data.kas);
      const pengeluaran = await apiPrivate.get(
        `transaksi?&jenis=Pengeluaran&month=${month}&year=${year}&peternakan=${id_p}`
      );
      const data2 = pengeluaran.data.data.map((item) => {
        const formattedHarga = new Intl.NumberFormat("id-ID", {
          style: "currency",
          currency: "IDR",
        }).format(item.jumlah);
        return {
          ...item,
          jumlah: formattedHarga,
        };
      });

      setPengeluaran(data2);
      setTotalKeluar(pengeluaran.data.sum);
      setKasNow(pengeluaran.data.kas);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching data:", error.response.data);
    }
  };

  useEffect(() => {
    fetch(auth.peternakan, bulan, tahun);
  }, [bulan, tahun, auth.peternakan]);

  return (
    <Container>
      <Card sx={{ padding: 2 }}>
        <Typography variant="h3">Neraca Keuangan</Typography>
        <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
          {pemasukan.length === 0 && pengeluaran.length === 0 ? (
            <Box></Box>
          ) : (
            <Box sx={{ display: "flex", flexDirection: "row", gap: 1 }}>
              <LoadingButton
                loading={pdfLoading}
                loadingPosition="start"
                variant="contained"
                size="small"
                color="error"
                onClick={handleClickPDF}
                sx={{ maxWidth: "6rem", fontSize: "12px" }}
              >
                PDF
              </LoadingButton>
              <LoadingButton
                loading={excelLoading}
                loadingPosition="start"
                variant="contained"
                size="small"
                color="success"
                onClick={handleClickExcel}
                sx={{ maxWidth: "6rem", fontSize: "12px" }}
              >
                Excel
              </LoadingButton>
            </Box>
          )}
          <Box sx={{ display: "flex", flexDirection: "row", gap: 1 }}>
            <FormControl>
              <Select
                id="bulan"
                variant="standard"
                value={bulan}
                onChange={(e) => {
                  setBulan(e.target.value);
                }}
                disableUnderline={true}
                sx={{
                  borderBottom: "none",
                }}
              >
                {bulans.map((item, index) => (
                  <MenuItem key={item} value={index + 1}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl>
              <Select
                id="tahun"
                variant="standard"
                value={tahun}
                onChange={(e) => {
                  setTahun(e.target.value);
                }}
                disableUnderline={true}
                sx={{
                  borderBottom: "none",
                }}
              >
                {allYear.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </Box>
        {loading ? (
          <Box
            component="div"
            sx={{
              height: "100%",
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CircularProgress />
          </Box>
        ) : pemasukan.length === 0 && pengeluaran.length === 0 ? (
          <ContentNotFoundView
            message={`Maaf, tidak terdapat transaksi pada ${
              bulans[bulan - 1]
            } ${tahun}`}
          />
        ) : (
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h3>Pemasukan</h3>
              <TabelNeraca data={pemasukan} />
              <Typography variant="h6" sx={{ margin: 2 }}>
                Kas{" "}
                {bulan === 1
                  ? ` ${bulans[11]} ${tahun - 1}`
                  : `${bulans[bulan - 2]} ${tahun}`}
                :
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(
                  kasBefore === 0
                    ? totalKeluar + kasNow - totalMasuk
                    : kasBefore
                )}
              </Typography>
              <Typography variant="h6" sx={{ margin: 2 }}>
                Total:
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(
                  kasBefore === 0
                    ? totalKeluar + kasNow
                    : kasBefore + totalMasuk
                )}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <h3>Pengeluaran</h3>
              <TabelNeraca data={pengeluaran} />
              <Typography variant="h6" sx={{ margin: 2 }}>
                Kas {bulans[bulan - 1]} {tahun}:{" "}
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(kasNow)}
              </Typography>
              <Typography variant="h6" sx={{ margin: 2 }}>
                Total:{" "}
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(kasNow + totalKeluar)}
              </Typography>
            </Grid>
          </Grid>
        )}
      </Card>
    </Container>
  );
};

export default Neraca;
