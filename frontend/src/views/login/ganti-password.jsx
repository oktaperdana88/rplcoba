import { useTheme } from "@emotion/react";
import { useState } from "react";
import {
  Box,
  Card,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  Typography,
  alpha,
} from "@mui/material";
import { useNavigate, useSearchParams } from "react-router-dom";
import LoadingButton from "@mui/lab/LoadingButton";
import { bgGradient } from "../../theme/css";
import { api } from "../../utils/axios";
import { toast } from "../../components/alert";
import { useFormik } from "formik";
import { object, ref, string } from "yup";
import { Visibility, VisibilityOff } from "@mui/icons-material";

/**
 * Halaman mengganti password diluar login
 * 
 * @returns 
 */

const ChangePasswordView = () => {
  const theme = useTheme();

  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const key = searchParams.get("key");

  const [btnLoading, setBtnLoading] = useState(false);

  const [passwordVisible, setPasswordVisible] = useState({
    new: false,
    confirm: false,
  });

  const formik = useFormik({
    initialValues: {
      password: "",
      confirm: "",
    },
    validationSchema: object({
      password: string()
        .min(8, "Password minimal 8 karakter")
        .max(24, "Password maksimal 24 karakter")
        .matches(/[0-9]/, "Password harus memiliki minimal 1 angka")
        .matches(/[a-z]/, "Password harus memiliki minimal 1 huruf kecil")
        .matches(/[A-Z]/, "Password harus memiliki minimal 1 huruf kapital")
        .matches(
          /[#$@!^%&*?]/,
          "Password minimal terdiri dari 1 karakter khusus"
        )
        .required("Password wajib diisi"),
      confirm: string()
        .required("Konfirmasi password harus terisi")
        .oneOf(
          [ref("password")],
          "Konfirmasi password harus sesuai dengan password baru"
        ),
    }),
    onSubmit: async (value) => {
      setBtnLoading(true);
      try {
        await api.post(
          `change-password`,
          {
            password: value.password,
            confirm: value.confirm,
          },
          {
            headers: {
              Authorization: `Bearer ${key}`,
            },
          }
        );

        toast.fire({
          icon: "success",
          text: "Ganti Password berhasil",
        });

        navigate("/");
      } catch (error) {
        setBtnLoading(false);
        toast.fire({
          icon: "error",
          text: error.response.data.message,
        });
      }
    },
  });

  const renderForm = (
    <>
      <form noValidate onSubmit={formik.handleSubmit}>
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="password">Password Baru *</InputLabel>
          <OutlinedInput
            id="password"
            label="Password Baru *"
            name="password"
            type={passwordVisible.password ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      password: !passwordVisible.password,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.password ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.password && Boolean(formik.errors.password)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.password && formik.errors.password}
          </Box>
        </FormControl>
        <FormControl sx={{ my: 2 }} fullWidth variant="outlined">
          <InputLabel id="confirm">Konfirmasi Password *</InputLabel>
          <OutlinedInput
            id="confirm"
            label="Konfirmasi Password *"
            name="confirm"
            type={passwordVisible.confirm ? "text" : "password"}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setPasswordVisible({
                      ...passwordVisible,
                      confirm: !passwordVisible.confirm,
                    });
                  }}
                  edge="end"
                >
                  {!passwordVisible.confirm ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            }
            value={formik.values.confirm}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.confirm && Boolean(formik.errors.confirm)}
          />
          <Box component={"div"} sx={{ fontSize: 12, color: "red", ml: 2 }}>
            {formik.touched.confirm && formik.errors.confirm}
          </Box>
        </FormControl>

        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          fullWidth
          size="large"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{ mt: 4 }}
        >
          Submit
        </LoadingButton>
      </form>
    </>
  );

  return (
    <Box
      sx={{
        ...bgGradient({
          color: alpha(theme.palette.background.default, 0.9),
          imgUrl: "background/overlay_4.jpg",
        }),
        height: 1,
      }}
    >
      <Stack alignItems="center" justifyContent="center" sx={{ height: 1 }}>
        <Card
          sx={{
            p: 5,
            width: "100%",
            maxWidth: 500,
            mx: 2,
          }}
        >
          <img
            src="/img/DB-Mulyo Farm_Logo.svg"
            alt="Mulyo Farm"
            width={150}
            className="logo"
          />
          <Typography variant="h3" sx={{ mb: 2, textAlign: "center" }}>
            Ganti Password
          </Typography>
          <Typography sx={{ mb: 5, textAlign: "center" }}>
            Masukkan password baru Anda
          </Typography>

          {renderForm}
        </Card>
      </Stack>
    </Box>
  );
};

export default ChangePasswordView;
