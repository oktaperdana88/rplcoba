import { useTheme } from "@emotion/react";
import { useState } from "react";
import {
  Box,
  Card,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
  alpha,
} from "@mui/material";
import Iconify from "../../components/iconify";
import { Link, useNavigate } from "react-router-dom";
import LoadingButton from "@mui/lab/LoadingButton";
import { bgGradient } from "../../theme/css";
import { api } from "../../utils/axios";
import { toast } from "../../components/alert";
import useAuth from "../../hooks/use-auth";
import ForbiddenView from "../error/forbidden-view";

const LoginView = () => {
  const theme = useTheme();

  // Statis loading button
  const [btnLoading, setBtnLoading] = useState(false);

  const { setAuth } = useAuth();

  // ketika manager yang tidak mengurus peternakan mencoba login
  const [forbidden, setForbidden] = useState(false);

  //state user
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  //Tampilkan password
  const [showPassword, setShowPassword] = useState(false);

  //Login submit
  const handleClick = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    try {
      let res;
      res = await api.post(`login`, {
        email: user.email,
        password: user.password,
      });

      // menyimpan data user dan role
      const userLoggedIn = res.data?.id;
      const role = [res.data?.role];
      const accessToken = res.data?.accessToken;

      sessionStorage.setItem("_mulyo_farm_u_", res.data?.id);
      sessionStorage.setItem("_mulyo_farm_r_", res.data?.role);

      // Mengambil daftar peternakan
      if (res.data?.role === "Pemilik") {
        res = await api.get(`peternakan`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
      } else {
        res = await api.get(`peternakan?user=${res.data?.id}`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
      }

      // ketika tidak ada peternakan
      if (res.data.data.length === 0) {
        await api.post(`logout`, {
          id: userLoggedIn,
        });
        setAuth({ user: "", role: "" });
        sessionStorage.clear();
        setForbidden(true);
      }

      // Mengatur state global
      setAuth({
        user: userLoggedIn,
        role,
        token: accessToken,
        peternakan: res.data.data[0].id,
        tanggal: res.data.data[0].tanggal_berdiri,
      });
      sessionStorage.setItem("_mulyo_farm_p_", res.data.data[0].id);
      localStorage.setItem("_mulyo_farm_sess_id_", accessToken);
      sessionStorage.setItem(
        "_mulyo_farm_p_t_",
        res.data.data[0].tanggal_berdiri
      );

      navigate("/admin");
      setBtnLoading(false);
    } catch (error) {
      setBtnLoading(false);
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  const renderForm = (
    <>
      {/* Input email */}
      <form noValidate autoComplete="off" onSubmit={handleClick}>
        <Stack spacing={3}>
          <TextField
            name="email"
            label="Email address"
            value={user.email}
            autoComplete="off"
            onChange={(e) =>
              setUser({
                ...user,
                email: e.target.value,
              })
            }
            required
          />

          {/* Form Password */}
          <TextField
            name="password"
            label="Password"
            autoComplete="new-password"
            value={user.password}
            type={showPassword ? "text" : "password"}
            onChange={(e) =>
              setUser({
                ...user,
                password: e.target.value,
              })
            }
            required
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={() => setShowPassword(!showPassword)}
                    edge="end"
                  >
                    <Iconify
                      icon={showPassword ? "eva:eye-fill" : "eva:eye-off-fill"}
                    />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Stack>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
          sx={{ my: 3 }}
        >
          <Link
            to={"/forget-password"}
            variant="subtitle2"
            style={{ textDecoration: "none", color: "black" }}
          >
            Forgot password?
          </Link>
        </Stack>

        <LoadingButton
          fullWidth
          loadingPosition="start"
          loading={btnLoading}
          size="large"
          type="submit"
          variant="contained"
          color="inherit"
        >
          Login
        </LoadingButton>
      </form>
    </>
  );

  return forbidden ? (
    <ForbiddenView
      message={
        "Anda tidak sedang mengurusi peternakan apapun. Mohon login dengan akun pemilik atau manager yang sedang mengurusi peternakan."
      }
    />
  ) : (
    <Box
      sx={{
        ...bgGradient({
          color: alpha(theme.palette.background.default, 0.9),
          imgUrl: "/background/overlay_4.jpg",
        }),
        height: 1,
      }}
    >
      <Stack alignItems="center" justifyContent="center" sx={{ height: 1 }}>
        <Card
          sx={{
            p: 5,
            width: "100%",
            maxWidth: 500,
            mx: 2,
          }}
        >
          <img
            src="/img/DB-Mulyo Farm_Logo.svg"
            alt="Mulyo Farm"
            width={150}
            className="logo"
          />
          <Typography variant="h3" sx={{ mb: 2, textAlign: "center" }}>
            Selamat Datang
          </Typography>
          <Typography sx={{ mb: 5, textAlign: "center" }}>
            Masukkan email dan password Anda
          </Typography>

          {renderForm}
        </Card>
      </Stack>
    </Box>
  );
};

export default LoginView;
