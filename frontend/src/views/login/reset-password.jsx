import { useTheme } from "@emotion/react";
import { useState } from "react";
import { Box, Card, Stack, TextField, Typography, alpha } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import LoadingButton from "@mui/lab/LoadingButton";
import { bgGradient } from "../../theme/css";
import { api } from "../../utils/axios";
import { toast } from "../../components/alert";

/**
 * Halaman reset password yang memerlukan email kemudian link pergantian password akan dikirim ke email
 *
 * @returns
 */

const ForgetPasswordView = () => {
  //custom tema
  const theme = useTheme();

  const navigate = useNavigate();

  //status loading pada button
  const [btnLoading, setBtnLoading] = useState(false);

  //penyimpanan email
  const [user, setUser] = useState({
    email: "",
  });

  //submit
  const handleClick = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    //mengirimkan email ke tertulis
    try {
      await api.post(`reset`, {
        email: user.email,
      });

      toast.fire({
        icon: "success",
        text: "Mohon cek Email Anda!",
      });

      navigate("/");
    } catch (error) {
      setBtnLoading(false);
      toast.fire({
        icon: "error",
        text: error.response.data.message,
      });
    }
  };

  const renderForm = (
    <>
      <Stack spacing={3}>
        {/* Input email */}
        <TextField
          name="email"
          label="Email address"
          onChange={(e) =>
            setUser({
              ...user,
              email: e.target.value,
            })
          }
        />
      </Stack>

      {/* kembali ke lodin */}
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="flex-end"
        sx={{ my: 3 }}
      >
        <Link
          to={"/"}
          variant="subtitle2"
          style={{ textDecoration: "none", color: "black" }}
        >
          Sudah ingat?
        </Link>
      </Stack>

      <LoadingButton
        fullWidth
        loadingPosition="start"
        loading={btnLoading}
        size="large"
        type="submit"
        variant="contained"
        color="inherit"
        onClick={handleClick}
      >
        Submit
      </LoadingButton>
    </>
  );

  return (
    <Box
      sx={{
        ...bgGradient({
          color: alpha(theme.palette.background.default, 0.9),
          imgUrl: "/background/overlay_4.jpg",
        }),
        height: 1,
      }}
    >
      <Stack alignItems="center" justifyContent="center" sx={{ height: 1 }}>
        <Card
          sx={{
            p: 5,
            width: "100%",
            maxWidth: 500,
            mx: 2,
          }}
        >
          <img
            src="/img/DB-Mulyo Farm_Logo.svg"
            alt="Mulyo Farm"
            width={150}
            className="logo"
          />
          <Typography variant="h3" sx={{ mb: 2, textAlign: "center" }}>
            Reset Password
          </Typography>
          <Typography sx={{ mb: 5, textAlign: "center" }}>
            Masukkan email Anda untuk reset password
          </Typography>

          {renderForm}
        </Card>
      </Stack>
    </Box>
  );
};

export default ForgetPasswordView;
