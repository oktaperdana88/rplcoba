import { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import { Box, Card, CircularProgress, Container, Grid } from "@mui/material";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// Tabel rekap operasional pakan 1 minggu dan bulanan

const TabelPakan = () => {
  // Data agregat operasional
  const [dataWeek, setDataWeek] = useState({
    week: [],
    month: [],
  });

  const [loading, setLoading] = useState(true);

  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  // Mendapatkan data operasional
  const fetcher = async (id_p) => {
    try {
      setLoading(true)
      const res = await apiPrivate.get(
        `inventoris/summary?peternakan=${id_p}`
      );
      setDataWeek(res.data);
      setLoading(false)
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetcher(id_p);
  }, [id_p]);

  const month = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];

  // Mendapatkan tanggal minggu sebelumnya
  const now = new Date();
  const lastWeekStart = new Date(now);
  lastWeekStart.setDate(now.getDate() - 7);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>

      {/* 1 minggu terakhir */}
      <Card sx={{padding: 2}}>
        <h2>
          {lastWeekStart.getDate()} {month[lastWeekStart.getMonth()]}{" "}
          {lastWeekStart.getFullYear()} - {now.getDate()}{" "}
          {month[now.getMonth()]} {now.getFullYear()}
        </h2>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <h3>Pengeluaran</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataWeek.week
                    .filter(
                      (item) =>
                        item.riwayat === "Pengeluaran" && item.jenis !== "Pupuk"
                    )
                    .map((pakan) => (
                      <TableRow key={pakan.jenis}>
                        <TableCell>{pakan.jenis}</TableCell>
                        <TableCell>{pakan._sum?.berat || 0} Kg</TableCell>
                      </TableRow>
                    ))}
                  <TableRow>
                    <TableCell>Total</TableCell>
                    <TableCell>
                      {dataWeek.week
                        .filter(
                          (item) =>
                            item.riwayat === "Pengeluaran" &&
                            item.jenis !== "Pupuk"
                        )
                        .reduce(
                          (acc, cur) => acc + (cur._sum?.berat || 0),
                          0
                        )}{" "}
                      Kg
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12} md={6}>
            <h3>Pemasukan</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataWeek.week
                    .filter(
                      (item) =>
                        item.riwayat === "Penambahan" && item.jenis !== "Pupuk"
                    )
                    .map((pakan) => (
                      <TableRow key={pakan.jenis}>
                        <TableCell>{pakan.jenis}</TableCell>
                        <TableCell>{pakan._sum?.berat || 0} Kg</TableCell>
                      </TableRow>
                    ))}
                  <TableRow>
                    <TableCell>Total</TableCell>
                    <TableCell>
                      {dataWeek.week
                        .filter(
                          (item) =>
                            item.riwayat === "Penambahan" &&
                            item.jenis !== "Pupuk"
                        )
                        .reduce(
                          (acc, cur) => acc + (cur._sum?.berat || 0),
                          0
                        )}{" "}
                      Kg
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>

        {/* bulan ini */}
        <h2>
          {month[now.getMonth()]}&ensp;{now.getFullYear()}
        </h2>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <h3>Pengeluaran</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataWeek.month
                    .filter(
                      (item) =>
                        item.riwayat === "Pengeluaran" && item.jenis !== "Pupuk"
                    )
                    .map((pakan) => (
                      <TableRow key={pakan.jenis}>
                        <TableCell>{pakan.jenis}</TableCell>
                        <TableCell>{pakan._sum?.berat || 0} Kg</TableCell>
                      </TableRow>
                    ))}
                  <TableRow>
                    <TableCell>Total</TableCell>
                    <TableCell>
                      {dataWeek.month
                        .filter(
                          (item) =>
                            item.riwayat === "Pengeluaran" &&
                            item.jenis !== "Pupuk"
                        )
                        .reduce(
                          (acc, cur) => acc + (cur._sum?.berat || 0),
                          0
                        )}{" "}
                      Kg
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12} md={6}>
            <h3>Pemasukan</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataWeek.month
                    .filter(
                      (item) =>
                        item.riwayat === "Penambahan" && item.jenis !== "Pupuk"
                    )
                    .map((pakan) => (
                      <TableRow key={pakan.jenis}>
                        <TableCell>{pakan.jenis}</TableCell>
                        <TableCell>{pakan._sum?.berat || 0} Kg</TableCell>
                      </TableRow>
                    ))}
                  <TableRow>
                    <TableCell>Total</TableCell>
                    <TableCell>
                      {dataWeek.month
                        .filter(
                          (item) =>
                            item.riwayat === "Penambahan" &&
                            item.jenis !== "Pupuk"
                        )
                        .reduce(
                          (acc, cur) => acc + (cur._sum?.berat || 0),
                          0
                        )}{" "}
                      Kg
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
      </Card>
    </Container>
  );
};

export default TabelPakan;
