import { Box, CircularProgress, Grid } from "@mui/material";
import AppWidgetSummary from "../../../components/card";
import { useEffect, useState } from "react";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// Ringkasan stok pakan

const PakanSummary = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const [peternakan, setPeternakan] = useState({});

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getPeternakan = async () => {
      setLoading(true)
      const res = await apiPrivate.get(`peternakan/${id_p}`); // TODO: ganti 1 menjadi id peternakan dari user yg login
      setPeternakan(res.data.peternakan);
      setLoading(false)
    };
    getPeternakan();
  }, [id_p]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Grid container spacing={3}>
      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          title="Rumput"
          total={`${peternakan.stok_rumput || 0} kg`}
          route="/admin/pakan/tambah?jenis=Rumput"
        />
      </Grid>

      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          title="Fermentasi"
          total={`${peternakan.stok_fermentasi || 0} kg`}
          route="/admin/pakan/tambah?jenis=Fermentasi"
        />
      </Grid>

      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          title="Konsentrat"
          total={`${peternakan.stok_konsentrat || 0} kg`}
          route="/admin/pakan/tambah?jenis=Konsentrat"
        />
      </Grid>
    </Grid>
  );
};

export default PakanSummary;
