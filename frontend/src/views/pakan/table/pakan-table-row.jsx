import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";

// Baris tabel operasional pakan

const PakanTableRow = ({ tanggal, jenis, ternak, deskripsi, berat, no }) => {
  const tgl = new Date(tanggal);

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${
          tgl.getMonth() + 1
        }-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{jenis}</TableCell>

        <TableCell>{ternak}</TableCell>

        <TableCell>{deskripsi}</TableCell>

        <TableCell>{berat}kg</TableCell>
      </TableRow>
    </>
  );
};

PakanTableRow.propTypes = {
  tanggal: PropTypes.any,
  deskripsi: PropTypes.any,
  jenis: PropTypes.any,
  berat: PropTypes.any,
  ternak: PropTypes.any,
  no: PropTypes.number,
};

export default PakanTableRow;
