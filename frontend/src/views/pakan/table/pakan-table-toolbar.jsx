import {
  Box,
  Button,
  FormControl,
  MenuItem,
  Select,
  Toolbar,
  Typography,
} from "@mui/material";
import { any, func } from "prop-types";
import { kategoriTernak, pakans } from "../../../constant";
import Iconify from "../../../components/iconify";
import { useNavigate } from "react-router-dom";

// Baris tabel operasional 

const PakanTableToolbar = ({ onFilterJenis, jenis, onFilterTernak, ternak }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/admin/pakan/tambah-operasional");
  };
  return (
    <Toolbar
      sx={{
        display: "flex",
        flexDirection: { xs: "column", md: "row" },
        alignItems: {xs: "start", sm: "center"},
        justifyContent: "space-between",
        padding: 2,
        gap: 1,
      }}
    >
      <Typography variant="h4">Riwayat Operasional Pakan</Typography>
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          justifyContent: {xs: "space-between", md: "right"},
          gap: 2,
          width: "100%",
          flexDirection: { xs: "column", sm: "row" },
          alignItems: "end",
        }}
      >
        <FormControl variant="standard" sx={{ textAlign: "right" }}>
          <Select
            id="jenis"
            value={jenis}
            onChange={(e) => onFilterJenis(e.target.value)}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
          >
            <MenuItem value="Semua Jenis">Semua Jenis</MenuItem>
            {pakans.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl variant="standard" sx={{ textAlign: "right" }}>
          <Select
            id="jenis"
            value={ternak}
            onChange={(e) => onFilterTernak(e.target.value)}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
          >
            <MenuItem value="Semua Jenis">Semua Jenis</MenuItem>
            {kategoriTernak.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          startIcon={<Iconify icon="eva:plus-fill" />}
          sx={{
            fontSize: 12,
            width: {xs: "100%", sm: "max"},
            maxWidth: {sm: 200}
          }}
          size="small"
        >
          Tambah Operasional
        </Button>
      </Box>
    </Toolbar>
  );
};

PakanTableToolbar.propTypes = {
  onFilterTernak: func,
  onFilterJenis: func,
  jenis: any,
  ternak: any,
};

export default PakanTableToolbar;
