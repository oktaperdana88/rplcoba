import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { Box, CircularProgress } from "@mui/material";
import PakanSummary from "../summary/pakan-summary";
import PakanTableToolbar from "../table/pakan-table-toolbar";
import PakanTableRow from "../table/pakan-table-row";
import { applyFilter, getComparator } from "../table/utils";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

//Tabel riwayat operasional pakan

const OperasionalPakanView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [data, setData] = useState([]);

  const [orderBy, setOrderBy] = useState("id");

  const [filterJenis, setFilterJenis] = useState("Semua Jenis");

  const [filterTernak, setFilterTernak] = useState("Semua Jenis");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [loading, setLoading] = useState(true);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "Tanggal" },
    { id: "jenis", label: "Jenis" },
    { id: "ternak", label: "Ternak" },
    { id: "deskripsi", label: "Deskripsi" },
    { id: "berat", label: "Berat (kg)" },
  ];

  const fetcher = async (id_p) => {
    setLoading(true);
    const response = await apiPrivate.get(
      `inventoris?peternakan=${id_p}&riwayat=Pengeluaran&jenis=Semua%20Jenis`
    );
    setData(response.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id_p);
  }, [id_p]);

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    filterJenis,
    filterTernak,
  });

  const notFound = !dataFiltered.length;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container maxWidth={"lg"}>
      <Box sx={{ mb: 3 }}>
        <PakanSummary />
      </Box>

      <Card>
        <PakanTableToolbar
          onFilterJenis={setFilterJenis}
          jenis={filterJenis}
          ternak={filterTernak}
          onFilterTernak={setFilterTernak}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <PakanTableRow
                      key={row.id}
                      tanggal={row.tanggal}
                      jenis={row.jenis}
                      ternak={row.ternak}
                      deskripsi={row.deskripsi}
                      berat={row.berat}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={filterJenis} col={7} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default OperasionalPakanView;
