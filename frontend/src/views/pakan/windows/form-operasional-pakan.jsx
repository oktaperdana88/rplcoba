import {
  Box,
  Card,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { kategoriTernak, pakans } from "../../../constant";
import { useFormik } from "formik";
import { date, number, object, string } from "yup";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

//Form pencatatan riwayat operasional pakan

const FormOperasionalPakanView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);

  // Validasi form
  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal operasional tidak bisa sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal operasional maksimal hari ini")
      .required("Tanggal operasional wajib diisi"),
    jenis: string().required("Jenis pakan wajib diisi"),
    ternak: string().required("Ternak wajib diisi"),
    berat: number()
      .min(0.0000001, "Berat tidak boleh kurang dari sama dengan 0")
      .required("Berat wajib diisi"),
    deskripsi: string()
      .max(512, "Deskripsi terlalu panjang")
      .required("Deskripsi harus diisi"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: "",
      jenis: "",
      ternak: "",
      berat: "",
      nomor: "",
      deskripsi: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: "Riwayat Operasional Pakan akan ditambahkan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      // Penambahan inventoris
      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.post(`inventoris/tambah`, {
            tanggal: value.tanggal,
            ternak: value.ternak,
            jenis: value.jenis,
            berat: value.berat,
            riwayat: "Pengeluaran",
            deskripsi: value.deskripsi,
            id_peternakan: id_p,
          });
          toast.fire({
            icon: "success",
            text: "Tambah operasional pakan berhasil",
          });
          navigate("/admin/pakan");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message, // Menampilkan pesan error dari server
          });
        }
      }
    },
  });

  return (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Operasional Pakan</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {/* Tanggal operasional */}
        <TextField
          label="Tanggal Operasional"
          name="tanggal"
          type="date"
          placeholder="dd/mm/yyyy"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />

        {/* Jenis Pakan */}
        <FormControl fullWidth>
          <InputLabel id="jenis" sx={{ my: 2 }}>
            Jenis *
          </InputLabel>
          <Select
            id="jenis"
            label="Jenis"
            name="jenis"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
            value={formik.values.jenis}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.jenis && Boolean(formik.errors.jenis)}
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {pakans.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.jenis && formik.errors.jenis}
          </Box>
        </FormControl>

        {/* Jenis ternak */}
        <FormControl fullWidth>
          <InputLabel id="ternak" sx={{ my: 2 }}>
            Ternak *
          </InputLabel>
          <Select
            id="ternak"
            label="Ternak *"
            name="ternak"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
            value={formik.values.ternak}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.ternak && Boolean(formik.errors.ternak)}
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {kategoriTernak.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.ternak && formik.errors.status}
          </Box>
        </FormControl>

        {/* Berat pakan yang digunakan */}
        <TextField
          type="number"
          label="Berat (kg)"
          name="berat"
          placeholder="30"
          variant="outlined"
          value={formik.values.berat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.berat && Boolean(formik.errors.berat)}
          helperText={formik.touched.berat && formik.errors.berat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />

        {/* Deskripsi penggunaan */}
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          rows={4}
          fullWidth
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          required
          sx={{
            my: 2,
          }}
        />
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormOperasionalPakanView;
