import {
  Box,
  Card,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { caraDapatPakan, pakans } from "../../../constant";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import { useNavigate, useSearchParams } from "react-router-dom";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import NumericFormatCustom from "../../../components/currency_field";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

// Form penambahan pakan

const FormPakanView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);

  //Validasi
  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal penambahan pakan tidak bisa sebelum peternakan dibuat"
      )
      .max(new Date(), "Tanggal penambahan pakan maksimal hari ini")
      .required("Tanggal penambahan pakan wajib diisi"),
    jenis: string().required("Jenis pakan wajib diisi"),
    cara_peroleh: string().required("Cara mendapatkan pakan wajib diisi"),
    berat: number()
      .min(0.000001, "Berat tidak boleh negatif")
      .required("Berat wajib diisi"),
    pihak_terkait: string()
      .max(50, "Pihak terlalu panjang")
      .required("Pihak terkait tidak boleh kosong"),
    harga: number()
      .min(0, "Harga beli tidak boleh negatif")
      .required("Harga beli harus diisi"),
    deskripsi: string().max(512, "Deskripsi terlalu panjang"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: "",
      jenis: searchParams.get("jenis"),
      cara_peroleh: "",
      berat: 0,
      pihak_terkait: "-",
      harga: 0,
      deskripsi: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      //Penambahan
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Pakan akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.post(`inventoris/tambah`, {
            tanggal: value.tanggal,
            jenis: value.jenis,
            riwayat: "Penambahan",
            cara_peroleh: value.cara_peroleh,
            pihak_terkait: value.pihak_terkait,
            berat: value.berat,
            harga: value.harga,
            deskripsi: value.deskripsi,
            id_peternakan: id_p,
          });
          toast.fire({
            icon: "success",
            text: "Tambah pakan berhasil",
          });
          navigate("/admin/pakan");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });
  return (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Pakan</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {/* Tanggal perolehan pakan */}
        <TextField
          label="Tanggal Perolehan"
          name="tanggal"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="dd/mm/yyyy"
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />

        {/* Jenis Pakan */}
        <FormControl fullWidth>
          <InputLabel id="jenis" sx={{ my: 2 }}>
            Jenis *
          </InputLabel>
          <Select
            id="jenis"
            label="Jenis"
            name="jenis"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            value={formik.values.jenis}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.jenis && Boolean(formik.errors.jenis)}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {pakans.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <span>{formik.touched.jenis && formik.errors.jenis}</span>
        </FormControl>

        {/* Cara mendapatkan pakan */}
        <FormControl fullWidth>
          <InputLabel id="cara_peroleh" sx={{ my: 2 }}>
            Cara Mendapatkan *
          </InputLabel>
          <Select
            id="cara_peroleh"
            label="Cara Mendapatkan *"
            name="cara_peroleh"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            value={formik.values.cara_peroleh}
            onChange={(e) => {
              formik.values.pihak_terkait = "-";
              formik.values.harga = 0;
              formik.handleChange(e);
            }}
            onBlur={formik.handleBlur}
            error={
              formik.touched.cara_peroleh && Boolean(formik.errors.cara_peroleh)
            }
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {caraDapatPakan.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.cara_peroleh && formik.errors.cara_peroleh}
          </Box>
        </FormControl>

        {/* Berat pakan diperoleh */}
        <TextField
          label="Berat (kg)"
          name="berat"
          placeholder="30"
          variant="outlined"
          value={formik.values.berat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.berat && Boolean(formik.errors.berat)}
          helperText={formik.touched.berat && formik.errors.berat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />

        {/* Pembelian Pakan */}
        {formik.values.cara_peroleh === "Beli" && (
          <>
            {/* Pihak terkait dalam transaksi pembelian */}
            <TextField
              type="text"
              label="Penjual/Pihak Terkait"
              name="pihak_terkait"
              placeholder="Jatinegara"
              variant="outlined"
              fullWidth
              required
              value={formik.values.pihak_terkait}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.pihak_terkait &&
                Boolean(formik.errors.pihak_terkait)
              }
              helperText={
                formik.touched.pihak_terkait && formik.errors.pihak_terkait
              }
              sx={{
                my: 2,
              }}
            />

            {/* Harga pembelian */}
            <TextField
              label="Harga Beli (Rp)"
              name="harga"
              placeholder="Rp100.000.000"
              variant="outlined"
              fullWidth
              required
              InputProps={{
                inputComponent: NumericFormatCustom,
              }}
              value={formik.values.harga}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.harga && Boolean(formik.errors.harga)}
              helperText={formik.touched.harga && formik.errors.harga}
              sx={{
                my: 2,
              }}
            />
            <TextField
              type="text"
              label="Deskripsi"
              name="deskripsi"
              variant="outlined"
              multiline
              rows={4}
              fullWidth
              value={formik.values.deskripsi}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.deskripsi && Boolean(formik.errors.deskripsi)
              }
              helperText={formik.touched.deskripsi && formik.errors.deskripsi}
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPakanView;
