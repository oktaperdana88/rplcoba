import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";


/**
 * 
 * @param {Date} tanggal => tanggal penghapusan
 * @param {String} kode => nama aset
 * @param {String} jenis => jenis aset
 * @param {String} lokasi => lokasi peternakan
 * @param {String} deskripsi => catatan penghapusan
 * @param {Number} no => nomor baris
 * @returns 
 */

const HapusAsetTableRow = ({ tanggal, kode, jenis, lokasi, deskripsi, no }) => {
const tgl = new Date(tanggal)
  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${tgl.getMonth()+1}-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{kode}</TableCell>

        <TableCell>{jenis}</TableCell>

        <TableCell>{lokasi}</TableCell>

        <TableCell>{deskripsi || "-"}</TableCell>
      </TableRow>
    </>
  );
};

HapusAsetTableRow.propTypes = {
  tanggal: PropTypes.any,
  deskripsi: PropTypes.any,
  jenis: PropTypes.any,
  kode: PropTypes.any,
  lokasi: PropTypes.any,
  no: PropTypes.number,
};

export default HapusAsetTableRow;
