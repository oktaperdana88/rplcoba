import { useState } from "react";
import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import Popover from "@mui/material/Popover";
import TableRow from "@mui/material/TableRow";
import MenuItem from "@mui/material/MenuItem";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import Iconify from "../../../components/iconify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

/**
 * 
 * @param {Date} tanggal => tanggal penghapusan
 * @param {String} nik => nik pegawai
 * @param {String} nama => nama pegawai
 * @param {String} lokasi => lokasi peternakan
 * @param {String} deskripsi => catatan penghapusan
 * @param {Number} no => nomor baris
 * @returns 
 */

const HapusPegawaiTableRow = ({
  tanggal,
  nik,
  nama,
  lokasi,
  deskripsi,
  no,
}) => {
  const [open, setOpen] = useState(null);

  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{tanggal}</TableCell>

        <TableCell>{nik}</TableCell>

        <TableCell>{nama}</TableCell>

        <TableCell>{lokasi}</TableCell>

        <TableCell>{deskripsi || "-"}</TableCell>

        <TableCell>
          <IconButton
            onClick={handleOpenMenu}
            aria-label="Detail"
            title="Detail"
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>

      <Popover
        open={!!open}
        anchorEl={open}
        onClose={handleCloseMenu}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        PaperProps={{
          sx: { width: 140 },
        }}
      >
        <MenuItem onClick={handleCloseMenu}>
          <Iconify icon="eva:edit-fill" sx={{ mr: 2 }} />
          Edit
        </MenuItem>

        <MenuItem onClick={handleCloseMenu} sx={{ color: "error.main" }}>
          <Iconify icon="eva:trash-2-outline" sx={{ mr: 2 }} />
          Delete
        </MenuItem>
      </Popover>
    </>
  );
};

HapusPegawaiTableRow.propTypes = {
  tanggal: PropTypes.any,
  deskripsi: PropTypes.any,
  nama: PropTypes.any,
  nik: PropTypes.any,
  lokasi: PropTypes.any,
  no: PropTypes.number,
};

export default HapusPegawaiTableRow;
