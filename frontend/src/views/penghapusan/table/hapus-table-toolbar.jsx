import {
  InputAdornment,
  OutlinedInput,
  Toolbar,
  Typography,
} from "@mui/material";
import { any, func } from "prop-types";
import Iconify from "../../../components/iconify";

/**
 * 
 * @param {String}jenis => jenis penghapusan
 * @param {String}keyword => keyword pencarian
 * @param {func}onKeywordChange => fungsi perubah state keyword
 * 
 * @returns 
 */

const HapusTableToolbar = ({ jenis, keyword, onKeywordChange }) => {
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2,
        width: "100%",
      }}
    >
      <Typography variant="h4">
        Riwayat Penghapusan{" "}
        {jenis === "Ternak"
          ? "Ternak"
          : jenis === "Karyawan"
          ? "Karyawan"
          : "Aset"}
      </Typography>
      <OutlinedInput
        value={keyword}
        onChange={(e) => onKeywordChange(e.target.value)}
        placeholder={`Cari ${jenis}...`}
        startAdornment={
          <InputAdornment position="start">
            <Iconify
              icon="eva:search-fill"
              sx={{ color: "text.disabled", width: 20, height: 20 }}
            />
          </InputAdornment>
        }
        size="small"
      />
    </Toolbar>
  );
};

HapusTableToolbar.propTypes = {
  jenis: any,
  keyword: any,
  onKeywordChange: func,
};

export default HapusTableToolbar;
