/**
 * Filter data penghapusan
 *
 * @param {String} keyword => keyword pencarian
 * @param {Array} inputData => data awal
 * @param {func} comparator => fungsi mengurutkan data
 */

export const applyFilter = ({ inputData, comparator, keyword }) => {
  const stabilizedThis = inputData.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  inputData = stabilizedThis.map((el) => el[0]);

  //filter data berdasarkan keyword
  if (keyword) {
    inputData = inputData.filter(
      (ternak) =>
        (ternak.jenis.toLowerCase().indexOf(keyword.toLowerCase()) &&
          ternak.kategori.toLowerCase().indexOf(keyword.toLowerCase()) &&
          ternak.peternakan?.nama
            .toLowerCase()
            .indexOf(keyword.toLowerCase())) !== -1
    );
  }

  return inputData;
};
