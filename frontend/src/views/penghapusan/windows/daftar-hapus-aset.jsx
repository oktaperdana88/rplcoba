import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows, getComparator } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";

import HapusTableToolbar from "../table/hapus-table-toolbar";
import HapusAsetTableRow from "../table/hapus-aset-table-row";
import { Box, CircularProgress } from "@mui/material";
import { applyFilter } from "../table/utils";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

/**
 * View riwayat penghapusan aset
 *
 * @returns
 */

const HapusAsetView = () => {
  const { auth } = useAuth();

  //id peternakan dikunjungi
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  //halaman saat ini
  const [page, setPage] = useState(0);

  //Urutan
  const [order, setOrder] = useState("asc");

  //keyword pencarian
  const [keyword, setKeyword] = useState("");

  //semua data penghapusan aset
  const [data, setData] = useState([]);

  //urutan berdasarkan kolom
  const [orderBy, setOrderBy] = useState("name");

  //baris data per halaman
  const [rowsPerPage, setRowsPerPage] = useState(5);

  //status loading
  const [loading, setLoading] = useState(true);

  //mengambil data dari API
  const fetcher = async () => {
    try {
      setLoading(true);
      const res = await apiPrivate.get(
        `ternak/penghapusan?peternakan=${id_p}&tipe=Aset`
      );
      setData(res.data.data);
      setLoading(false);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    fetcher();
  }, [id_p, rowsPerPage, page]);

  //pengurutan
  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  //ganti halaman
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  //ganti jumlah baris dalam 1 halaman
  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  //filter data
  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    keyword,
  });

  //kondisi data tidak ditemukan
  const notFound = !dataFiltered.length;

  //label tabel head, nama kolom
  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "Tanggal" },
    { id: "kode", label: "Kode" },
    { id: "jenis", label: "Jenis" },
    { id: "lokasi", label: "Lokasi" },
    { id: "deskripsi", label: "Catatan" },
  ];

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Card>
        {/* Toolbar tabel (pencarian) */}
        <HapusTableToolbar keyword={keyword} onKeywordChange={setKeyword} />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              {/* Table head */}
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />

              {/* Table body */}
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <HapusAsetTableRow
                      key={row.id}
                      tanggal={row.tanggal}
                      kode={row.kategori}
                      jenis={row.jenis}
                      lokasi={row.peternakan?.nama}
                      deskripsi={row.deskripsi}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                {/* Baris kosong */}
                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {/* Isi ketika data tidak ditemukan */}
                {notFound && <TableNoData query={`Penghapusan Aset`} col={6} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        {/* Paginasi */}
        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default HapusAsetView;
