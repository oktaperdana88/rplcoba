import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows, getComparator } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";

import HapusTableToolbar from "../table/hapus-table-toolbar";
import HapusAsetTableRow from "../table/hapus-aset-table-row";
import { Box, CircularProgress } from "@mui/material";
import { applyFilter } from "../table/utils";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

/**
 * View riwayat penghapusan pegawai
 *
 * @returns
 */

const HapusPegawaiView = () => {
  const { auth } = useAuth();

  //peternakan yang sedang dikunjungi
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  //halaman saat ini
  const [page, setPage] = useState(0);

  //tipe Urutan
  const [order, setOrder] = useState("asc");

  //data lengkap
  const [data, setData] = useState([]);

  //kata cari
  const [keyword, setKeyword] = useState("");

  //urut berdasarkan nama kolom
  const [orderBy, setOrderBy] = useState("name");

  //baris per halaman
  const [rowsPerPage, setRowsPerPage] = useState(5);

  //status loading
  const [loading, setLoading] = useState(true);

  //pergantian status sorting
  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  //pergantian halaman
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  //pergantian baris per kolom
  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  //label tabel head
  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "Tanggal" },
    { id: "nik", label: "NIK" },
    { id: "nama", label: "Nama" },
    { id: "lokasi", label: "Lokasi" },
    { id: "deskripsi", label: "Catatan" },
  ];

  //mengambil data dari API
  const fetcher = async () => {
    try {
      setLoading(true);
      const res = await apiPrivate.get(
        `ternak/penghapusan?peternakan=${id_p}&tipe=Pegawai`
      );
      console.log(res.data);
      setData(res.data.data);
      setLoading(false);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    fetcher();
  }, [id_p]);

  //data filter
  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    keyword,
  });

  //status data tidak ditemukan
  const notFound = !dataFiltered.length;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Card>
        {/* Toolbar tabel */}
        <HapusTableToolbar
          jenis={"Karyawan"}
          keyword={keyword}
          onKeywordChange={setKeyword}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              {/* Table head */}
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />

              {/* Tabel body */}
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <HapusAsetTableRow
                      key={row.id}
                      tanggal={row.tanggal}
                      kode={row.kategori}
                      jenis={row.jenis}
                      lokasi={row.peternakan?.nama}
                      deskripsi={row.deskripsi}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                {/* Tabel kosong (jumlha baris) */}
                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {/* tabel ketika data hasil filter kosong */}
                {notFound && (
                  <TableNoData query={`Penghapusan Pegawai`} col={6} />
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default HapusPegawaiView;
