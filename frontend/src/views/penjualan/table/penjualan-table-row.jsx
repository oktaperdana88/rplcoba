import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";

//Baris tabel riwayat transaksi penjualan

const PenjualanTableRow = ({ tanggal, deskripsi, jumlah, no, pihak }) => {

  const tgl = new Date(tanggal)

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${tgl.getMonth()+1}-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{deskripsi}</TableCell>
        <TableCell>{pihak}</TableCell>

        <TableCell>{jumlah}</TableCell>

      </TableRow>
    </>
  );
};

PenjualanTableRow.propTypes = {
  tanggal: PropTypes.any,
  deskripsi: PropTypes.any,
  jumlah: PropTypes.any,
  pihak: PropTypes.any,
  no: PropTypes.number,
};

export default PenjualanTableRow;
