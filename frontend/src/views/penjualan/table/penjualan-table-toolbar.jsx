import { InputAdornment, OutlinedInput, Toolbar } from "@mui/material";
import Iconify from "../../../components/iconify";
import { any, func } from "prop-types";

//Toolbar tabel riwayat transaksi penjualan

const PenjualanTableToolbar = ({ keyword, onKeywordChange }) => {
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2,
      }}
    >
      <OutlinedInput
        value={keyword}
        onChange={(e) => onKeywordChange(e.target.value)}
        placeholder="Cari Riwayat Transaksi..."
        startAdornment={
          <InputAdornment position="start">
            <Iconify
              icon="eva:search-fill"
              sx={{ color: "text.disabled", width: 20, height: 20 }}
            />
          </InputAdornment>
        }
        size="small"
      />
    </Toolbar>
  );
};

PenjualanTableToolbar.propTypes = {
  keyword: any,
  onKeywordChange: func,
};

export default PenjualanTableToolbar;
