import {
  Container,
  Card,
  Typography,
  FormControl,
  MenuItem,
  InputLabel,
  Select,
  TextField,
  Box,
  CircularProgress,
  Grid,
} from "@mui/material";
import { useFormik } from "formik";
import { useNavigate, useSearchParams } from "react-router-dom";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { asets, kategoriTernak, pakans, penjualan } from "../../../constant";
import { formatToRupiah } from "../../../utils/format-number";
import NumericFormatCustom from "../../../components/currency_field";
import { useEffect, useState } from "react";
import { applyFilter, getComparator } from "../../ternak/table/utils";
import TernakDetailCard from "../../ternak/summary/ternak-detail-card";
import AsetCard from "../../aset/summary/aset-card";
import { filterAset } from "../../aset/table/utils";
import PerkiraanHarga from "../../ternak/price/ternak-hitung-harga";
import { date, number, object, string } from "yup";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";
import LoadingButton from "@mui/lab/LoadingButton";

/**
 * Form penjualan
 *
 * @returns
 */

const PenjualanFormView = () => {
  const apiPrivate = useAxiosPrivate();
  const navigate = useNavigate();

  //Mendapatkan url query ketika menekan tombol jual dari aset/ternak
  const [searchParams] = useSearchParams();
  const id = searchParams.get("id");
  const cat = searchParams.get("cat");
  const nilai = searchParams.get("nilai");

  const { auth } = useAuth();

  //Data semua aset
  const [aset, setAset] = useState([]);

  //Perkiraan nilai jual
  const [sum, setSum] = useState(0);

  //Data semua ternak
  const [ternak, setTernak] = useState([]);

  //Ternak atau aset terpilih
  const [selected, setSelected] = useState({
    kategori: "",
    jenis: "",
  });

  //status loading
  const [loading, setLoading] = useState(true);
  //button loading
  const [btnLoading, setBtnLoading] = useState(false);

  //validasi
  const validationSchema = object({
    kategori: string().required("Kategori harus diisi"),
    subKategori: string(),
    pembeli: string().required("Pembeli wajib diisi"),
    tanggal_transaksi: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal transaksi tidak boleh sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal transaksi tidak boleh lebih dari hari ini")
      .required("Tanggal transaksi tidak boleh kosong"),
    nominal: number()
      .min(0, "Jumlah tidak boleh negatif")
      .required("Nominal wajib diisi"),
  });

  const formik = useFormik({
    initialValues: {
      id: id || "",
      kategori: cat || "",
      subkategori:
        cat === "Ternak"
          ? selected.kategori
          : cat === "Aset"
          ? selected.jenis
          : "",
      pembeli: "",
      detail: "",
      tanggal_transaksi: "",
      nominal: "",
    },
    validationSchema,
    validate: (value) => {
      const error = {};
      //validasi tanggal jual ternak
      if (value.kategori === "Ternak") {
        if (
          new Date(value.tanggal_transaksi).getTime() <
          new Date(selected.tanggal_masuk).getTime()
        )
          error.tanggal_transaksi =
            "Tanggal transaksi tidak bisa sebelum ternak perolehan ternak";
      }

      //validasi tanggal jual aset
      if (value.kategori === "Aset") {
        if (
          new Date(value.tanggal_transaksi).getTime() <
          new Date(selected.tanggal_dapat).getTime()
        )
          error.tanggal_transaksi =
            "Tanggal transaksi tidak bisa sebelum ternak perolehan aset";
      }
      return error;
    },
    enableReinitialize: true,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: "Riwayat Penjualan akan ditambahkan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });
      if (result.value) {
        setBtnLoading(true);
        try {
          //penjualan ternak
          if (formik.values.kategori === "Ternak") {
            await apiPrivate.post(`ternak/${value.id}/jual`, {
              id: value.id,
              kategori: selected.kategori,
              subkategori: selected.jenis,
              detail: `Umur: ${
                selected.umur +
                Math.ceil(
                  (new Date().getTime() -
                    new Date(selected.tanggal_masuk).getTime()) /
                    (1000 * 3600 * 24)
                )
              } berat: ${selected.berat}`,
              pembeli: value.pembeli,
              tanggal_transaksi: value.tanggal_transaksi,
              nominal: value.nominal,
              id_peternakan: auth.peternakan,
            });
          }

          //Penjualan aset
          if (formik.values.kategori === "Aset") {
            await apiPrivate.post(`aset/${selected.id}/jual`, {
              id: value.id,
              kategori: value.kategori,
              subkategori: value.subkategori,
              detail: `Nama: ${selected.nama}; Deskripsi: ${selected.deskripsi}`,
              pembeli: value.pembeli,
              tanggal_transaksi: value.tanggal_transaksi,
              nominal: value.nominal,
              id_peternakan: auth.peternakan,
            });
          }

          //Penjualan pupuk dan pakan
          if (
            formik.values.kategori === "Pupuk" ||
            formik.values.kategori === "Pakan"
          ) {
            await apiPrivate.post(`inventoris/jual`, {
              id: value.id,
              kategori: value.kategori,
              subkategori: value.subkategori,
              detail: value.detail,
              pembeli: value.pembeli,
              tanggal_transaksi: value.tanggal_transaksi,
              nominal: value.nominal,
              id_peternakan: auth.peternakan,
            });
          }
          toast.fire({
            title: `Penjualan berhasil!`,
            icon: "success",
          });
          navigate(`/admin/penjualan`);
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  //Folter ternak berdasarkan subkategori
  const ternakFiltered = applyFilter({
    inputData: ternak,
    comparator: getComparator("asc", "nomor"),
    filterKategori: formik.values.subkategori,
    filterJenis: "Semua Jenis",
  });

  //Folter aset berdasarkan subkategori
  const asetFiltered = filterAset({
    inputData: aset,
    comparator: getComparator("asc", "id"),
    filterJenis: formik.values.subkategori,
    filterStatus: "Milik Sendiri",
    filterMetode: "Semua Metode Pembayaran",
  });

  //Mengambil semua data ternak dan aset
  const fetcher = async (id, peternakan, apiPrivate) => {
    setLoading(true);
    if (id) {
      if (cat === "Ternak") {
        const resSelected = await apiPrivate.get(`ternak/${id}`);
        setSelected(resSelected.data.data);
      } else {
        const resSelected = await apiPrivate.get(`aset/${id}`);
        setSelected(resSelected.data.data);
      }
    }
    const resTernak = await apiPrivate.get(`ternak?peternakan=${peternakan}`);
    setTernak(resTernak.data.data);
    const resAset = await apiPrivate.get(`aset?peternakan=${peternakan}`);
    setAset(resAset.data.data);
    setLoading(false);
  };

  //Memilih salah satu ternak atau aset
  const handleSelected = (e) => {
    formik.values.id = e.target.value;
    if (formik.values.kategori === "Ternak") {
      setSelected(ternak.find((item) => item.id === e.target.value));
    } else {
      setSelected(aset.find((item) => item.id === e.target.value));
    }
  };

  useEffect(() => {
    fetcher(id, auth.peternakan, apiPrivate);
    formik.values.id = "";
  }, [id, auth.peternakan, apiPrivate]);

  //Mendapatkan total pengobatan ternak
  const fetchPengobatanTernak = async (id, kategori, apiPrivate) => {
    if (kategori === "Ternak") {
      setLoading(true);
      const response = await apiPrivate.get(`ternak/${id}/pemeliharaan`);
      setSum(response.data.sum);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchPengobatanTernak(formik.values.id, formik.values.kategori, apiPrivate);
  }, [formik.values.id]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Grid container spacing={3}>
        <Grid
          item
          xs={12}
          sm={
            formik.values.id &&
            (formik.values.kategori === "Ternak" ||
              formik.values.kategori === "Aset")
              ? 6
              : 12
          }
        >
          <Card sx={{ padding: 2 }}>
            <form
              noValidate
              onSubmit={(e) => {
                console.log(formik.errors);
                formik.handleSubmit(e);
              }}
            >
              <Typography variant="h4">Form Penjualan</Typography>

              {/* Kategori penjualan */}
              <FormControl fullWidth>
                <InputLabel id="kategori" sx={{ my: 2 }}>
                  Kategori
                </InputLabel>
                <Select
                  id="kategori"
                  label="Kategori"
                  name="kategori"
                  variant="outlined"
                  value={formik.values.kategori}
                  onChange={(e) => {
                    formik.values.id = "";
                    formik.values.subkategori = "";
                    formik.values.detail = "";
                    formik.handleChange(e);
                  }}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.kategori && Boolean(formik.errors.kategori)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {penjualan.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              {/* Jenis Ternak atau aset */}
              {(formik.values.kategori === "Aset" ||
                formik.values.kategori === "Ternak") && (
                <FormControl fullWidth>
                  <InputLabel id="subkategori" sx={{ my: 2 }}>
                    {formik.values.kategori === "Ternak"
                      ? "Jenis Ternak"
                      : "Jenis Aset"}
                  </InputLabel>
                  <Select
                    id="subkategori"
                    label={
                      formik.values.kategori === "Ternak"
                        ? "Jenis Ternak"
                        : "Jenis Aset"
                    }
                    name="subkategori"
                    variant="outlined"
                    value={formik.values.subkategori}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.touched.subkategori &&
                      Boolean(formik.errors.subkategori)
                    }
                    sx={{
                      borderBottom: "none",
                      my: 2,
                    }}
                    required
                  >
                    <MenuItem selected disabled>
                      Pilih Salah Satu
                    </MenuItem>
                    {formik.values.kategori === "Ternak"
                      ? kategoriTernak.map((item) => (
                          <MenuItem key={item} value={item}>
                            {item}
                          </MenuItem>
                        ))
                      : asets.map((item) => (
                          <MenuItem key={item} value={item}>
                            {item}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
              )}

              {/* nomor sapi */}
              {formik.values.subkategori === "Sapi" &&
                formik.values.kategori === "Ternak" && (
                  <FormControl fullWidth>
                    <InputLabel id="id" sx={{ my: 2 }}>
                      Nomor Sapi
                    </InputLabel>
                    <Select
                      id="id"
                      label="Nomor Sapi"
                      name="id"
                      variant="outlined"
                      value={formik.values.id}
                      onChange={handleSelected}
                      sx={{
                        borderBottom: "none",
                        my: 2,
                      }}
                      required
                    >
                      <MenuItem selected disabled>
                        Pilih Salah Satu
                      </MenuItem>
                      {ternakFiltered.map((item) => (
                        <MenuItem key={item.id} value={item.id}>
                          {item.nomor}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}

              {/* Pilihan kambing dan ayam */}
              {(formik.values.subkategori === "Kambing" ||
                formik.values.subkategori === "Ayam") && (
                <FormControl fullWidth>
                  <InputLabel id="id" sx={{ my: 2 }}>
                    {formik.values.subkategori}
                  </InputLabel>
                  <Select
                    id="id"
                    label={formik.values.subkategori}
                    name="id"
                    variant="outlined"
                    value={formik.values.id}
                    onChange={handleSelected}
                    sx={{
                      borderBottom: "none",
                      my: 2,
                    }}
                    required
                  >
                    <MenuItem selected disabled>
                      Pilih Salah Satu
                    </MenuItem>
                    {ternakFiltered.map((item) => (
                      <MenuItem key={item.id} value={item.id}>
                        {`Jenis: ${item.jenis}; Berat: ${item.berat}kg; Umur: ${item.umur} hari `}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              )}

              {/* Pilihan aset */}
              {formik.values.kategori === "Aset" &&
                formik.values.subkategori && (
                  <FormControl fullWidth>
                    <InputLabel id="id" sx={{ my: 2 }}>
                      Nama Aset
                    </InputLabel>
                    <Select
                      id="id"
                      label="Nama Aset"
                      name="id"
                      variant="outlined"
                      value={formik.values.id}
                      onChange={handleSelected}
                      sx={{
                        borderBottom: "none",
                        my: 2,
                      }}
                      required
                    >
                      <MenuItem selected disabled>
                        Pilih Salah Satu
                      </MenuItem>
                      {asetFiltered.map((item) => (
                        <MenuItem key={item.id} value={item.id}>
                          {`Nama Aset: ${item.nama}; Deskripsi: ${item.deskripsi}`}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}

              {/* Detail ternak dan aset ketika layar handphone */}
              {formik.values.id &&
                (formik.values.kategori === "Aset" ||
                  formik.values.kategori === "Ternak") && (
                  <Box
                    component={"div"}
                    sx={{ display: { xs: "block", sm: "none" } }}
                  >
                    {selected.kategori === "Sapi" ||
                    selected.kategori === "Kambing" ||
                    selected.kategori === "Ayam" ? (
                      <>
                        <TernakDetailCard ternak={selected} tombol={false} />
                        {nilai ? (
                          <Typography sx={{ ml: 4 }}>
                            Rekomendasi Harga Jual: {formatToRupiah(nilai)}
                          </Typography>
                        ) : (
                          <>
                            <PerkiraanHarga
                              hargaBeli={selected.harga_beli}
                              tanggalMasuk={selected.tanggal_masuk}
                              totalPengobatan={sum}
                            />
                          </>
                        )}
                      </>
                    ) : selected.jenis === "Transportasi" ||
                      selected.jenis === "Teknologi" ||
                      selected.jenis === "Alat & Mesin" ||
                      selected.jenis === "Lainnya" ? (
                      <AsetCard aset={selected} tombol={false} />
                    ) : (
                      ""
                    )}
                  </Box>
                )}

              {/* Kategori dan berat pakan */}
              {formik.values.kategori === "Pakan" && (
                <>
                  <FormControl fullWidth>
                    <InputLabel id="detail" sx={{ my: 2 }}>
                      Jenis Pakan
                    </InputLabel>
                    <Select
                      id="subkategori"
                      label="Jenis Pakan"
                      name="subkategori"
                      variant="outlined"
                      onChange={formik.handleChange}
                      sx={{
                        borderBottom: "none",
                        my: 2,
                      }}
                      required
                    >
                      <MenuItem selected disabled>
                        Pilih Salah Satu
                      </MenuItem>
                      {pakans.map((item) => (
                        <MenuItem key={item} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <TextField
                    label="Berat (kg)"
                    name="detail"
                    type="number"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    value={formik.values.detail}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.touched.detail && Boolean(formik.errors.detail)
                    }
                    helperText={formik.touched.detail && formik.errors.detail}
                    fullWidth
                    required
                    sx={{
                      my: 2,
                    }}
                  />
                </>
              )}

              {/* Berat pupuk */}
              {formik.values.kategori === "Pupuk" && (
                <>
                  <TextField
                    label="Berat (kg)"
                    name="detail"
                    type="number"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    value={formik.values.detail}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.touched.detail && Boolean(formik.errors.detail)
                    }
                    helperText={formik.touched.detail && formik.errors.detail}
                    fullWidth
                    required
                    sx={{
                      my: 2,
                    }}
                  />
                </>
              )}

              {/* Tanggal transaksi */}
              <TextField
                label="Tanggal Transaksi"
                name="tanggal_transaksi"
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                placeholder="dd/mm/yyyy"
                variant="outlined"
                value={formik.values.tanggal_transaksi}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.tanggal_transaksi &&
                  Boolean(formik.errors.tanggal_transaksi)
                }
                helperText={
                  formik.touched.tanggal_transaksi &&
                  formik.errors.tanggal_transaksi
                }
                fullWidth
                required
                sx={{
                  my: 2,
                }}
              />

              {/* Pembeli */}
              <TextField
                type="text"
                label="Pembeli"
                name="pembeli"
                placeholder="Nama Pembeli"
                variant="outlined"
                value={formik.values.pembeli}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.pembeli && Boolean(formik.errors.pembeli)}
                helperText={formik.touched.pembeli && formik.errors.pembeli}
                fullWidth
                required
                sx={{
                  my: 2,
                }}
              />

              {/* Harga jual */}
              <TextField
                label="Nominal (Rp)"
                name="nominal"
                placeholder="RP100.000.000"
                variant="outlined"
                value={formik.values.nominal}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.nominal && Boolean(formik.errors.nominal)}
                helperText={formik.touched.nominal && formik.errors.nominal}
                InputProps={{
                  inputComponent: NumericFormatCustom,
                }}
                fullWidth
                required
                sx={{
                  my: 2,
                }}
              />

              <LoadingButton
                loadingPosition="start"
                loading={btnLoading}
                size="medium"
                type="submit"
                variant="contained"
                color="inherit"
                sx={{
                  width: 120,
                  display: "block",
                  mx: "auto",
                }}
              >
                Submit
              </LoadingButton>
            </form>
          </Card>
        </Grid>
        {formik.values.id &&
          (formik.values.kategori === "Aset" ||
            formik.values.kategori === "Ternak") && (
            <Grid
              item
              xs={0}
              sm={6}
              sx={{ display: { xs: "none", sm: "block" } }}
            >
              {selected.kategori === "Sapi" ||
              selected.kategori === "Kambing" ||
              selected.kategori === "Ayam" ? (
                <>
                  <TernakDetailCard ternak={selected} tombol={false} />
                  {nilai ? (
                    <Typography sx={{ ml: 4 }}>
                      Rekomendasi Harga Jual: {formatToRupiah(nilai)}
                    </Typography>
                  ) : (
                    <>
                      <PerkiraanHarga
                        hargaBeli={selected.harga_beli}
                        tanggalMasuk={selected.tanggal_masuk}
                        totalPengobatan={sum}
                      />
                    </>
                  )}
                </>
              ) : selected.jenis === "Transportasi" ||
                selected.jenis === "Teknologi" ||
                selected.jenis === "Alat & Mesin" ||
                selected.jenis === "Lainnya" ? (
                <AsetCard aset={selected} tombol={false} />
              ) : (
                ""
              )}
            </Grid>
          )}
      </Grid>
    </Container>
  );
};

export default PenjualanFormView;
