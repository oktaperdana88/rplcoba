import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows, getComparator } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import PenjualanTableToolbar from "../table/penjualan-table-toolbar";
import PenjualanTableRow from "../table/penjualan-table-row";
import { Box, Button, CircularProgress, Typography } from "@mui/material";
import Iconify from "../../../components/iconify";
import { useNavigate } from "react-router-dom";
import { applyFilter } from "../table/utils";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// Tabel riwayat transaksi penjualan

const PenjualanPakanView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [data, setData] = useState([]);

  const [orderBy, setOrderBy] = useState("name");

  const [keyword, setKeyword] = useState("");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const [loading, setLoading] = useState(true);

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "Tanggal" },
    { id: "deskripsi", label: "Deskripsi" },
    { id: "pihak_terkait", label: "Pihak Terkait" },
    { id: "jumlah", label: "Jumlah" },
  ];

  const fetch = async (id_p) => {
    try {
      setLoading(true);
      const response = await apiPrivate.get(
        `transaksi?peternakan=${id_p}&deskripsi=Penjualan`
      );
      const formattedData = response.data.data.map((item) => {
        const formattedHarga = new Intl.NumberFormat("id-ID", {
          style: "currency",
          currency: "IDR",
        }).format(item.jumlah);
        return {
          ...item,
          jumlah: formattedHarga,
        };
      });
      setData(formattedData);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/admin/penjualan/tambah");
  };

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    keyword,
  });

  const notFound = !dataFiltered.length;

  useEffect(() => {
    fetch(id_p);
  }, [id_p]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 2,
          width: "100%",
        }}
      >
        <Typography variant="h4">Riwayat Penjualan</Typography>
        <Box
          title="Filter list"
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            maxWidth: 200,
          }}
        >
          <Button
            variant="contained"
            color="inherit"
            onClick={handleClick}
            startIcon={<Iconify icon="eva:plus-fill" />}
            sx={{
              maxWidth: 200,
            }}
            size="small"
          >
            Tambah Penjualan
          </Button>
        </Box>
      </Box>
      <Card>
        <PenjualanTableToolbar keyword={keyword} onKeywordChange={setKeyword} />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <PenjualanTableRow
                      key={row.id}
                      tanggal={new Date().toDateString()}
                      deskripsi={row.deskripsi}
                      pihak={row.pihak_terkait}
                      jumlah={row.jumlah}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={`Penjualan`} col={6} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          labelRowsPerPage={"Baris tiap halaman"}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default PenjualanPakanView;
