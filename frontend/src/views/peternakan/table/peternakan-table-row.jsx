import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

// ----------------------------------------------------------------------

const PeternakanTableRow = ({
  id,
  nama,
  lokasi,
  manager,
  no,
}) => {
  const navigate = useNavigate()

  const handleOpenMenu = () => {
    navigate(`/admin/peternakan/${id}`)
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{nama}</TableCell>

        <TableCell>{lokasi}</TableCell>

        <TableCell>{manager || "Dikelola Pemilik"}</TableCell>

        <TableCell>
          <IconButton
            onClick={handleOpenMenu}
            aria-label="Detail"
            title="Detail"
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>
    </>
  );
};

PeternakanTableRow.propTypes = {
  id: PropTypes.any,
  nama: PropTypes.any,
  lokasi: PropTypes.any,
  manager: PropTypes.any,
  no: PropTypes.number,
};

export default PeternakanTableRow;
