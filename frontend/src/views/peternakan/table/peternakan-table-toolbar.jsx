import { InputAdornment, OutlinedInput, Toolbar } from "@mui/material";
import { any, func } from "prop-types";
import Iconify from "../../../components/iconify";

const PeternakanTableToolbar = ({ keyword, onKeywordChange }) => {
  return (
    <Toolbar
      sx={{
        height: 96,
        display: "flex",
        justifyContent: "space-between",
        p: (theme) => theme.spacing(0, 1, 0, 3),
      }}
    >
      <OutlinedInput
        value={keyword}
        onChange={(e) => onKeywordChange(e.target.value)}
        placeholder={`Cari Peternakan...`}
        startAdornment={
          <InputAdornment position="start">
            <Iconify
              icon="eva:search-fill"
              sx={{ color: "text.disabled", width: 20, height: 20 }}
            />
          </InputAdornment>
        }
        size="small"
      />
    </Toolbar>
  );
};

PeternakanTableToolbar.propTypes = {
  keyword: any,
  onKeywordChange: func,
};

export default PeternakanTableToolbar;
