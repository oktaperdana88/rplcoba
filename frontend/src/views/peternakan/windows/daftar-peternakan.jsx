import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows, getComparator } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import PeternakanTableToolbar from "../table/peternakan-table-toolbar";
import PeternakanTableRow from "../table/peternakan-table-row";
import { Box, Button, CircularProgress, Typography } from "@mui/material";
import Iconify from "../../../components/iconify";
import { useNavigate } from "react-router-dom";
import { applyFilter } from "../table/utils";
import useAxiosPrivate from "../../../hooks/use-axios-private";

// ----------------------------------------------------------------------

const DaftarPeternakanView = () => {
  const apiPrivate = useAxiosPrivate();
  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [orderBy, setOrderBy] = useState("name");

  const [keyword, setKeyword] = useState("");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const headLabel = [
    { id: "no", label: "No" },
    { id: "nama", label: "Nama" },
    { id: "lokasi", label: "Lokasi" },
    { id: "manager", label: "Manager" },
    { id: "", label: "Aksi" },
  ];

  const fetcher = async (apiPrivate) => {
    setLoading(true);
    const response = await apiPrivate.get("peternakan");
    setData(response.data.data);
    setLoading(false);
  };

  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/admin/peternakan/tambah");
  };

  useEffect(() => {
    fetcher(apiPrivate);
  }, [apiPrivate]);

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    keyword,
  });

  const notFound = !dataFiltered.length;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Box
        component={"div"}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          rowGap: 1,
          width: "100%",
          mb: 2,
        }}
      >
        <Typography variant="h4">Daftar Peternakan</Typography>
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          startIcon={<Iconify icon="eva:plus-fill" />}
        >
          Tambah Peternakan
        </Button>
      </Box>
      <Card>
        <PeternakanTableToolbar
          keyword={keyword}
          onKeywordChange={setKeyword}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <PeternakanTableRow
                      key={row.id}
                      id={row.id}
                      nama={row.nama}
                      lokasi={row.lokasi}
                      manager={row.user?.nama}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={`Peternakan`} col={5} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={data.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default DaftarPeternakanView;
