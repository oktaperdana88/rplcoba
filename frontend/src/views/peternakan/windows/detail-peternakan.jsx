import {
  Box,
  Button,
  Card,
  CircularProgress,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import { func } from "prop-types";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Iconify from "../../../components/iconify";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { formatToRupiah } from "../../../utils/format-number";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBoxesStacked,
  faCalendarDays,
  faLocationDot,
  faUser,
  faWallet,
  faWarehouse,
} from "@fortawesome/free-solid-svg-icons";
import LoadingButton from "@mui/lab/LoadingButton";

const PeternakanDetailCard = () => {
  const { auth } = useAuth();
  const apiPrivate = useAxiosPrivate();
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const [peternakan, setPeternakan] = useState({});
  const [kas, setKas] = useState(0);
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);

  const fetcher = async (id, apiPrivate) => {
    setLoading(true);
    const response = await apiPrivate.get(`peternakan/${id}`);
    setPeternakan(response.data.peternakan);
    setKas(response.data.kas);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate);
  }, [id, apiPrivate]);

  const date = new Date(peternakan.tanggal_berdiri);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Card sx={{ padding: 2, mb: 2 }}>
        <Typography variant="h4">{peternakan.nama}</Typography>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faCalendarDays} /> Tanggal Berdiri
          </Grid>
          <Grid item xs={6}>
            {`${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`}
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faLocationDot} /> Alamat
          </Grid>
          <Grid item xs={6}>
            {peternakan.lokasi}
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faWarehouse} /> Kapasitas Sapi
          </Grid>
          <Grid item xs={6}>
            {peternakan.kapasitas_sapi} ekor
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faWarehouse} /> Kapasitas Kambing
          </Grid>
          <Grid item xs={6}>
            {peternakan.kapasitas_kambing} ekor
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faWarehouse} /> Kapasitas Ayam
          </Grid>
          <Grid item xs={6}>
            {peternakan.kapasitas_ayam} ekor
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faBoxesStacked} /> Stok Rumput
          </Grid>
          <Grid item xs={6}>
            {peternakan.stok_rumput} kg
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faBoxesStacked} /> Stok Fermantasi
          </Grid>
          <Grid item xs={6}>
            {peternakan.stok_fermentasi} kg
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faBoxesStacked} /> Stok Konsentrat
          </Grid>
          <Grid item xs={6}>
            {peternakan.stok_konsentrat} kg
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faBoxesStacked} /> Stok Pupuk
          </Grid>
          <Grid item xs={6}>
            {peternakan.stok_pupuk} sak
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faWallet} /> Kas
          </Grid>
          <Grid item xs={6}>
            {formatToRupiah(kas)}
          </Grid>
          <Grid item xs={6}>
            <FontAwesomeIcon icon={faUser} /> Manager
          </Grid>
          <Grid item xs={6}>
            {peternakan.user?.nama ? peternakan.user?.nama : "Dikelola Pemilik"}
          </Grid>
        </Grid>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            gap: 1,
            justifyContent: "center",
            margin: 2,
          }}
        >
          <Button
            variant="contained"
            size="medium"
            color="success"
            onClick={() => {
              navigate(`/admin/peternakan/${id}/edit`);
            }}
            startIcon={<Iconify icon="eva:edit-fill" />}
            sx={{ maxWidth: "6rem", fontSize: "12px" }}
          >
            Edit
          </Button>
          {auth.peternakan == id && (
            <Button
              variant="contained"
              size="medium"
              color="secondary"
              onClick={() => {
                navigate(
                  `/admin/transaksi/tambah?cat=Pemasukan&pihak=Pemilik%20Mulyo%20Farm&desc=Tambah%20Modal`
                );
              }}
              sx={{ maxWidth: "6rem", fontSize: "12px" }}
            >
              Tambah Modal
            </Button>
          )}
          {auth.peternakan != id && (
            <LoadingButton
              loading={btnLoading}
              loadingPosition="start"
              variant="contained"
              size="medium"
              color="error"
              onClick={async () => {
                const result = await Swal.fire({
                  title: "Apakah Anda Yakin?",
                  text: "Anda akan menghapus ternak ini. Data ternak akan dihapus secara permanen.",
                  icon: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#C70039",
                  confirmButtonText: "Hapus",
                });

                if (result.value) {
                  setBtnLoading(true);
                  try {
                    await apiPrivate.delete(`peternakan/${id}`);

                    toast.fire({
                      icon: "success",
                      text: "Hapus peternakan berhasil",
                    });

                    navigate("/admin/peternakan");
                  } catch (error) {
                    setBtnLoading(false);
                    toast.fire({
                      icon: "error",
                      text: error.response.data.message,
                    });
                  }
                }
              }}
              startIcon={<Iconify icon="eva:trash-2-outline" />}
              sx={{ maxWidth: "6rem", fontSize: "12px" }}
            >
              Hapus
            </LoadingButton>
          )}
        </Box>
      </Card>
    </Container>
  );
};

PeternakanDetailCard.propTypes = {
  setTanggal: func,
  setHarga: func,
};

export default PeternakanDetailCard;
