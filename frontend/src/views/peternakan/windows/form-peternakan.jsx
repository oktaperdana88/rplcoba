import {
  Box,
  Card,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "../../../components/alert";
import Swal from "sweetalert2";
import { useEffect, useState } from "react";
import NumericFormatCustom from "../../../components/currency_field";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";

const validationSchema = object({
  nama: string()
    .matches(/^[A-Za-z0-9\s]{0,50}$/, "Nama tidak valid")
    .required("Nama peternakan harus diisi"),
  tanggal: date()
    .max(new Date(), "Tanggal peternakan berdiri maksimal hari ini")
    .required("Tanggal berdiri harus diisi"),
  lokasi: string()
    .max(512, "Lokasi terlalu panjang")
    .required("Lokasi wajib diisi"),
  kapasitasSapi: number()
    .min(0, "Kapasitas maksimal sapi tidak boleh negatif")
    .required("Kapasitas maksimal sapi harus diisi"),
  kapasitasKambing: number()
    .min(0, "Kapasitas maksimal kambing tidak boleh negatif")
    .required("Kapasitas maksimal kambing harus diisi"),
  kapasitasAyam: number()
    .min(0, "Kapasitas maksimal ayam tidak boleh negatif")
    .required("Kapasitas maksimal ayam harus diisi"),
  stokRumput: number()
    .min(0, "Stok rumput saat ini tidak boleh negatif")
    .required("Stok rumput saat ini harus diisi"),
  stokKonsentrat: number()
    .min(0, "Stok konsentrat saat ini tidak boleh negatif")
    .required("Stok konsantrat saat ini harus diisi"),
  stokFermentasi: number()
    .min(0, "Stok fermentasi saat ini tidak boleh negatif")
    .required("Stok fermentasi saat ini harus diisi"),
  stokPupuk: number()
    .min(0, "Stok pupuk saat ini tidak boleh negatif")
    .required("Stok pupuk saat ini harus diisi"),
  kas: number().required("Kas harus diisi"),
});

const FormPeternakanView = () => {
  const apiPrivate = useAxiosPrivate();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});
  const [managerOpt, setManagerOpt] = useState([]);
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);
  const { id } = useParams();
  const formik = useFormik({
    initialValues: {
      nama: data?.nama || "",
      tanggal: data?.tanggal_berdiri || "",
      lokasi: data?.lokasi || "",
      kapasitasSapi: data?.kapasitas_sapi || 0,
      kapasitasKambing: data?.kapasitas_kambing || 0,
      kapasitasAyam: data?.kapasitas_ayam || 0,
      stokRumput: data?.stok_rumput || 0,
      stokFermentasi: data?.stok_fermentasi || 0,
      stokKonsentrat: data?.stok_konsentrat || 0,
      stokPupuk: data?.stok_pupuk || 0,
      kas: data?.kas || 0,
      manager: data?.id_user || "Dikelola Pemilik",
    },
    validationSchema: validationSchema,
    enableReinitialize: true,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Peternakan akan ${id ? "diperbaharui" : "ditambahkan"}!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          if (!id) {
            await apiPrivate.post(`peternakan/create`, {
              name: value.nama,
              tanggal: value.tanggal,
              lokasi: value.lokasi,
              kapasitas_sapi: value.kapasitasSapi,
              kapasitas_kambing: value.kapasitasKambing,
              kapasitas_ayam: value.kapasitasAyam,
              stok_pupuk: value.stokPupuk,
              stok_rumput: value.stokRumput,
              stok_fermentasi: value.stokFermentasi,
              stok_konsentrat: value.stokKonsentrat,
              kas: value.kas,
              id_user: value.manager,
            });
          } else {
            await apiPrivate.put(`peternakan/${id}`, {
              name: value.nama,
              lokasi: value.lokasi,
              kapasitas_sapi: value.kapasitasSapi,
              kapasitas_kambing: value.kapasitasKambing,
              kapasitas_ayam: value.kapasitasAyam,
              stok_pupuk: value.stokPupuk,
              stok_rumput: value.stokRumput,
              stok_fermentasi: value.stokFermentasi,
              stok_konsentrat: value.stokKonsentrat,
              kas: value.kas,
              id_user: value.manager,
            });
          }

          toast.fire({
            icon: "success",
            text: `Peternakan berhasil ${id ? "diperbaharui" : "ditambahkan"}`,
          });

          navigate("/admin/peternakan");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  const fetcher = async (id, apiPrivate) => {
    if (id) {
      setLoading(true);
      const response = await apiPrivate.get(`peternakan/${id}`);
      setData(response.data.peternakan);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetcher(id, apiPrivate);
  }, [id, apiPrivate]);

  const fetchManager = async (apiPrivate) => {
    const res = await apiPrivate.get(`users?role=Manager`);
    setManagerOpt(res.data.data);
  };

  useEffect(() => {
    fetchManager(apiPrivate);
  }, [apiPrivate]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h3" sx={{ textAlign: "center" }}>
        {id ? "Edit" : "Tambah"} Peternakan
      </Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {!id && (
          <TextField
            label="Tanggal Berdiri"
            name="tanggal"
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="dd/mm/yyyy"
            variant="outlined"
            value={formik.values.tanggal}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
            helperText={formik.touched.tanggal && formik.errors.tanggal}
            fullWidth
            required
            sx={{
              my: 2,
            }}
          />
        )}
        <TextField
          label="Nama"
          name="nama"
          placeholder="Masukkan nama peternakan"
          variant="outlined"
          value={formik.values.nama}
          onChange={!id ? formik.handleChange : null}
          onBlur={formik.handleBlur}
          error={formik.touched.nama && Boolean(formik.errors.nama)}
          helperText={formik.touched.nama && formik.errors.nama}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Lokasi"
          name="lokasi"
          pattern="^[A-Za-z0-9\s,.]{0,100}$"
          placeholder="Jatinegara"
          variant="outlined"
          value={formik.values.lokasi}
          onChange={!id ? formik.handleChange : null}
          onBlur={formik.handleBlur}
          error={formik.touched.lokasi && Boolean(formik.errors.lokasi)}
          helperText={formik.touched.lokasi && formik.errors.lokasi}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="number"
          label="Kapasitas Sapi"
          name="kapasitasSapi"
          placeholder="100"
          variant="outlined"
          value={formik.values.kapasitasSapi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.kapasitasSapi && Boolean(formik.errors.kapasitasSapi)
          }
          helperText={
            formik.touched.kapasitasSapi && formik.errors.kapasitasSapi
          }
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="number"
          label="Kapasitas Ayam"
          name="kapasitasAyam"
          placeholder="100"
          variant="outlined"
          value={formik.values.kapasitasAyam}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.kapasitasAyam && Boolean(formik.errors.kapasitasAyam)
          }
          helperText={
            formik.touched.kapasitasAyam && formik.errors.kapasitasAyam
          }
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="number"
          label="Kapasitas Kambing"
          name="kapasitasKambing"
          placeholder="100"
          variant="outlined"
          value={formik.values.kapasitasKambing}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.kapasitasKambing &&
            Boolean(formik.errors.kapasitasKambing)
          }
          helperText={
            formik.touched.kapasitasKambing && formik.errors.kapasitasKambing
          }
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        {!id && (
          <>
            <TextField
              type="number"
              label="Stok Rumput (kg)"
              name="stokRumput"
              placeholder="100"
              variant="outlined"
              value={formik.values.stokRumput}
              onChange={!id ? formik.handleChange : null}
              onBlur={formik.handleBlur}
              error={
                formik.touched.stokRumput && Boolean(formik.errors.stokRumput)
              }
              helperText={formik.touched.stokRumput && formik.errors.stokRumput}
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
            <TextField
              type="number"
              label="Stok Fermentasi (kg)"
              name="stokFermentasi"
              placeholder="100"
              variant="outlined"
              value={formik.values.stokFermentasi}
              onChange={!id ? formik.handleChange : null}
              onBlur={formik.handleBlur}
              error={
                formik.touched.stokFermentasi &&
                Boolean(formik.errors.stokFermentasi)
              }
              helperText={
                formik.touched.stokFermentasi && formik.errors.stokFermentasi
              }
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
            <TextField
              type="number"
              label="Stok Konsentrat (kg)"
              name="stokKonsentrat"
              placeholder="100"
              variant="outlined"
              value={formik.values.stokKonsentrat}
              onChange={!id ? formik.handleChange : null}
              onBlur={formik.handleBlur}
              error={
                formik.touched.stokKonsentrat &&
                Boolean(formik.errors.stokKonsentrat)
              }
              helperText={
                formik.touched.stokKonsentrat && formik.errors.stokKonsentrat
              }
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
            <TextField
              type="number"
              label="Stok Pupuk (sak)"
              name="stokPupuk"
              placeholder="100"
              variant="outlined"
              value={formik.values.stokPupuk}
              onChange={!id ? formik.handleChange : null}
              onBlur={formik.handleBlur}
              error={
                formik.touched.stokPupuk && Boolean(formik.errors.stokPupuk)
              }
              helperText={formik.touched.stokPupuk && formik.errors.stokPupuk}
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
            <TextField
              label="Kas"
              name="kas"
              placeholder="Rp1.000.000"
              variant="outlined"
              value={formik.values.kas}
              onChange={!id ? formik.handleChange : null}
              onBlur={formik.handleBlur}
              error={formik.touched.kas && Boolean(formik.errors.kas)}
              helperText={formik.touched.kas && formik.errors.kas}
              InputProps={{
                inputComponent: NumericFormatCustom,
              }}
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        <FormControl fullWidth>
          <InputLabel id="manager" sx={{ my: 2 }}>
            Manager
          </InputLabel>
          <Select
            id="manager"
            label="Manager"
            name="manager"
            variant="outlined"
            value={formik.values.manager}
            onChange={(e) => {
              formik.handleChange(e);
            }}
            onBlur={formik.handleBlur}
            error={formik.touched.manager && Boolean(formik.errors.manager)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
          >
            <MenuItem selected value={"Dikelola Pemilik"}>
              Dikelola Pemilik
            </MenuItem>
            {managerOpt.map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.nama}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPeternakanView;
