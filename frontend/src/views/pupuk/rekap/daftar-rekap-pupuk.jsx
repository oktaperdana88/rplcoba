import { useState, useEffect } from "react";
// import Container from "@mui/material/Container";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
// import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import { Box, CircularProgress, Container, Grid } from "@mui/material";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";

const RekapPupuk = () => {
  const [dataWeek, setDataWeek] = useState({
    week: [],
    month: [],
  });

  const [loading, setLoading] = useState(true);

  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const fetcher = async (id_p) => {
    try {
      setLoading(true);
      const res = await apiPrivate.get(
        `inventoris/summary?peternakan=${id_p}`
      );
      setDataWeek(res.data);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetcher(id_p);
  }, [id_p]);

  const month = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];

  const now = new Date();
  const lastWeekStart = new Date(now);
  lastWeekStart.setDate(now.getDate() - 7);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <h2>
        {lastWeekStart.getDate()} {month[lastWeekStart.getMonth()]}{" "}
        {lastWeekStart.getFullYear()} - {now.getDate()} {month[now.getMonth()]}{" "}
        {now.getFullYear()}
      </h2>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <h3>Pengeluaran</h3>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Jenis</TableCell>
                  <TableCell>Berat (Kg)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>{"Pupuk"}</TableCell>
                  {dataWeek.week.filter(
                    (item) =>
                      item.riwayat === "Pengeluaran" && item.jenis === "Pupuk"
                  ).length !== 0 ? (
                    dataWeek.week
                      .filter(
                        (item) =>
                          item.riwayat === "Pengeluaran" &&
                          item.jenis === "Pupuk"
                      )
                      .map((pakan) => (
                        <TableCell key={pakan.jenis}>
                          {pakan._sum?.berat || 0} Kg
                        </TableCell>
                      ))
                  ) : (
                    <TableCell>{0} Kg</TableCell>
                  )}
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <Grid item xs={12} md={6}>
          <h3>Pemasukan</h3>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Jenis</TableCell>
                  <TableCell>Berat (Kg)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>{"Pupuk"}</TableCell>
                  {dataWeek.week.filter(
                    (item) =>
                      item.riwayat === "Penambahan" && item.jenis === "Pupuk"
                  ).length !== 0 ? (
                    dataWeek.week
                      .filter(
                        (item) =>
                          item.riwayat === "Penambahan" &&
                          item.jenis === "Pupuk"
                      )
                      .map((pakan) => (
                        <TableCell key={pakan.jenis}>
                          {pakan._sum?.berat || 0} Kg
                        </TableCell>
                      ))
                  ) : (
                    <TableCell>{0} Kg</TableCell>
                  )}
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <div className="flex flex-col gap-2 justify-center mx-4 rounded-lg p-4 bg-white mb-4">
        <h2>
          {month[now.getMonth()]}&ensp;{now.getFullYear()}
        </h2>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <h3>Pengeluaran</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>{"Pupuk"}</TableCell>
                    {dataWeek.month.filter(
                      (item) =>
                        item.riwayat === "Pengeluaran" && item.jenis === "Pupuk"
                    ).length !== 0 ? (
                      dataWeek.month
                        .filter(
                          (item) =>
                            item.riwayat === "Pengeluaran" &&
                            item.jenis === "Pupuk"
                        )
                        .map((pakan) => (
                          <TableCell key={pakan.jenis}>
                            {pakan._sum?.berat || 0} Kg
                          </TableCell>
                        ))
                    ) : (
                      <TableCell>{0} Kg</TableCell>
                    )}
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12} md={6}>
            <h3>Pemasukan</h3>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Jenis</TableCell>
                    <TableCell>Berat (Kg)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>{"Pupuk"}</TableCell>
                    {dataWeek.month.filter(
                      (item) =>
                        item.riwayat === "Penambahan" && item.jenis === "Pupuk"
                    ).length !== 0 ? (
                      dataWeek.month
                        .filter(
                          (item) =>
                            item.riwayat === "Penambahan" &&
                            item.jenis === "Pupuk"
                        )
                        .map((pakan) => (
                          <TableCell key={pakan.jenis}>
                            {pakan._sum?.berat} Kg
                          </TableCell>
                        ))
                    ) : (
                      <TableCell>{0} Kg</TableCell>
                    )}
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default RekapPupuk;
