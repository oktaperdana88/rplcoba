import { Box, CircularProgress, Grid } from "@mui/material";
import AppWidgetSummary from "../../../components/card";
import { useEffect, useState } from "react";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";

const PupukSummary = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();

  const [peternakan, setPeternakan] = useState({});

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getPeternakan = async () => {
      setLoading(true)
      const res = await apiPrivate.get(`peternakan/${id_p}`); // TODO: ganti 1 menjadi id peternakan dari user yg login
      setPeternakan(res.data.peternakan);
      setLoading(false)
    };
    getPeternakan();
  }, [id_p]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Grid container spacing={3} gap={1} width={"100%"}>
      <Grid item xs={12}>
        <AppWidgetSummary
          title="Pupuk"
          total={`${peternakan.stok_pupuk} sak`}
          route="/admin/pupuk/tambah"
        />
      </Grid>
    </Grid>
  );
};

export default PupukSummary;
