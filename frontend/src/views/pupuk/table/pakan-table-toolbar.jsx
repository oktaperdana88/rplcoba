import {
  Box,
  Button,
  Toolbar,
  Typography,
} from "@mui/material";
import { any, func } from "prop-types";
import Iconify from "../../../components/iconify";
import { useNavigate } from "react-router-dom";

const PupukTableToolbar = () => {
  const navigate = useNavigate()
  const handleClick = () => {
    navigate("/admin/pupuk/tambah-operasional")
  }
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2
      }}
    >
      <Typography variant="h4">Riwayat Operasional Pupuk</Typography>
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: "column"
        }}
      >
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          startIcon={<Iconify icon="eva:plus-fill" />}
          size="small"
        >
          Tambah Operasional
        </Button>
      </Box>
    </Toolbar>
  );
};

PupukTableToolbar.propTypes = {
  onFilterStatus: func,
  onFilterJenis: func,
  onFilterMetode: func,
  jenis: any,
  status: any,
  metode: any,
};

export default PupukTableToolbar;
