import { Card, TextField, Typography } from "@mui/material";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import { date, number, object, string } from "yup";
import { toast } from "../../../components/alert";
import Swal from "sweetalert2";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

const FormOperasionalPupukView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false);

  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal operasional pupuk tidak boleh sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal operasional maksimal hari ini")
      .required("Tanggal operasional wajib diisi"),
    berat: number()
      .min(0.000001, "Berat tidak boleh negatif")
      .required("Berat wajib diisi"),
    deskripsi: string()
      .max(512, "Deskripsi terlalu panjang")
      .required("Deskripsi wajib diisi"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: "",
      berat: "",
      deskripsi: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: "Riwayat Operasional Pupuk akan ditambahkan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          const response = await apiPrivate.post(`inventoris/tambah`, {
            tanggal: value.tanggal,
            riwayat: "Pengeluaran",
            jenis: "Pupuk",
            berat: value.berat,
            deskripsi: value.deskripsi,
            id_peternakan: id_p,
          });
          if (response.status === 400) throw new Error(response.data.message); // Menangani error stok tidak mencukupi
          toast.fire({
            icon: "success",
            text: "Tambah operasional pupuk berhasil",
          });
          navigate("/admin/pupuk");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message, // Menampilkan pesan error dari server
          });
        }
      }
    },
  });

  return (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Operasional Pupuk</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        <TextField
          label="Tanggal Operasional"
          name="tanggal"
          type="date"
          placeholder="dd/mm/yyyy"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="number"
          label="Berat (sak)"
          name="berat"
          placeholder="30"
          variant="outlined"
          fullWidth
          required
          value={formik.values.berat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.berat && Boolean(formik.errors.berat)}
          helperText={formik.touched.berat && formik.errors.berat}
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          rows={4}
          fullWidth
          required
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          sx={{
            my: 2,
          }}
        />
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormOperasionalPupukView;
