import {
  Box,
  Card,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { caraDapatPakan } from "../../../constant";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import NumericFormatCustom from "../../../components/currency_field";
import useAuth from "../../../hooks/use-auth";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

const FormPupukView = () => {
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const apiPrivate = useAxiosPrivate();
  const navigate = useNavigate();
  const [btnLoading, setBtnLoading] = useState(false)

  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal penambahan pupuk tidak boleh sebelum peternakan berdiri"
      )
      .max(new Date(), "Tanggal penambahan pupuk maksimal hari ini")
      .required("Tanggal penambahan pupuk wajib diisi"),
    cara_peroleh: string().required("Cara mendapatkan pakan wajib diisi"),
    berat: number()
      .min(0.0000001, "Berat tidak boleh kurang dari sama dengan 0")
      .required("Berat wajib diisi"),
    pihak_terkait: string()
      .max(50, "Pihak terlalu panjang")
      .required("Pihak terkait tidak boleh kosong"),
    harga: number()
      .min(0, "Harga beli tidak boleh negatif")
      .required("Harga beli harus diisi"),
    deskripsi: string().max(512, "Deskripsi terlalu panjang"),
  });

  const formik = useFormik({
    initialValues: {
      tanggal: "",
      cara_peroleh: "",
      berat: 0,
      pihak_terkait: "-",
      harga: 0,
      deskripsi: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Pupuk akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true)
        try {
          await apiPrivate.post(`inventoris/tambah`, {
            jenis: "Pupuk",
            tanggal: value.tanggal,
            berat: value.berat,
            cara_peroleh: value.cara_peroleh,
            riwayat: "Penambahan",
            pihak_terkait: value.pihak_terkait,
            harga: value.harga,
            deskripsi: value.deskripsi,
            id_peternakan: id_p,
          });
          toast.fire({
            icon: "success",
            text: "Tambah pupuk berhasil",
          });
          navigate("/admin/pupuk");
        } catch (error) {
          setBtnLoading(false)
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });
  return (
    <Card sx={{ mx: 2, padding: 2 }}>
      <Typography variant="h4">Tambah Pupuk</Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        <TextField
          label="Tanggal Perolehan"
          name="tanggal"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="dd/mm/yyyy"
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <FormControl fullWidth>
          <InputLabel id="cara_peroleh" sx={{ my: 2 }}>
            Cara Mendapatkan *
          </InputLabel>
          <Select
            id="cara_peroleh"
            label="Cara Mendapatkan *"
            name="cara_peroleh"
            variant="outlined"
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            value={formik.values.cara_peroleh}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={
              formik.touched.cara_peroleh && Boolean(formik.errors.cara_peroleh)
            }
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {caraDapatPakan.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.cara_peroleh && formik.errors.cara_peroleh}
          </Box>
        </FormControl>
        <TextField
          type="number"
          label="Berat (sak)"
          name="berat"
          placeholder="30"
          variant="outlined"
          value={formik.values.berat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.berat && Boolean(formik.errors.berat)}
          helperText={formik.touched.berat && formik.errors.berat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        {formik.values.cara_peroleh === "Beli" && (
          <>
            <TextField
              type="text"
              label="Pihak Terkait"
              name="pihak_terkait"
              placeholder="PT Adiguna"
              variant="outlined"
              fullWidth
              required
              value={formik.values.pihak_terkait}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.pihak_terkait &&
                Boolean(formik.errors.pihak_terkait)
              }
              helperText={
                formik.touched.pihak_terkait && formik.errors.pihak_terkait
              }
              sx={{
                my: 2,
              }}
            />
            <TextField
              label="Harga Beli (Rp)"
              name="harga"
              placeholder="Rp100,000,000"
              variant="outlined"
              InputProps={{
                inputComponent: NumericFormatCustom,
              }}
              fullWidth
              required
              value={formik.values.harga}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.harga && Boolean(formik.errors.harga)}
              helperText={formik.touched.harga && formik.errors.harga}
              sx={{
                my: 2,
              }}
            />
            <TextField
              type="text"
              label="Deskripsi"
              name="deskripsi"
              variant="outlined"
              multiline
              rows={4}
              fullWidth
              value={formik.values.deskripsi}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.deskripsi && Boolean(formik.errors.deskripsi)
              }
              helperText={formik.touched.deskripsi && formik.errors.deskripsi}
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPupukView;
