import {
  Box,
  Card,
  CircularProgress,
  TextField,
  Typography,
} from "@mui/material";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "../../../components/alert";
import NumericFormatCustom from "../../../components/currency_field";
import { useEffect, useState } from "react";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";
import LoadingButton from "@mui/lab/LoadingButton";

const validationSchema = object({
  tanggal: date()
    .max(new Date(), "Tanggal transaksi tidak boleh lebih dari hari ini")
    .required("Tanggal tidak boleh kosong"),
  obat: string()
    .max(50, "Obat terlalu panjang")
    .required("Obat tidak boleh kosong"),
  penyakit: string()
    .max(50, "Penyakit terlalu panjang")
    .required("Penyakit tidak boleh kosong"),
  pihak_terkait: string()
    .max(50, "Pihak terlalu panjang")
    .required("Pihak terkait tidak boleh kosong"),
  jumlah: number()
    .min(0, "jumlah tidak boleh negatif")
    .required("Jumlah harus diisi"),
  deskripsi: string()
    .max(512, "Deskripsi terlalu panjang")
    .required("Deskrpisi tidak boleh kosong"),
});

const FormPengobatanView = () => {
  const apiPrivate = useAxiosPrivate();
  const { auth } = useAuth();
  const id_p = auth.peternakan;
  const { id } = useParams();
  const [data, setData] = useState({});
  const [loading, setLoading] = useState({});
  const [btnLoading, setBtnLoading] = useState(false);
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      tanggal: "",
      obat: "",
      penyakit: "",
      pihak_terkait: "",
      jumlah: 0,
      deskripsi: "",
    },
    validate: (value) => {
      const error = {};
      if (
        new Date(value.tanggal).getTime() <
        new Date(data.tanggal_masuk).getTime()
      ) {
        error.tanggal = "Tanggal pengobatan tidak bisa sebelum ternak masuk";
      }
      return error;
    },
    validationSchema: validationSchema,
    onSubmit: async (value) => {
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Transaksi akan ditambahkan!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          await apiPrivate.post(`ternak/${id}/pemeliharaan`, {
            tanggal_transaksi: value.tanggal,
            jumlah: value.jumlah,
            obat: value.obat,
            penyakit: value.penyakit,
            deskripsi: value.deskripsi,
            pihak_terkait: value.pihak_terkait,
            id_peternakan: id_p,
          });

          toast.fire({
            icon: "success",
            text: `Transaksi berhasil ditambahkan`,
          });

          navigate(`/admin/ternak/${id}`);
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  const fetcher = async (id, apiPrivate) => {
    setLoading(true);
    const response = await apiPrivate.get(`ternak/${id}`);
    setData(response.data.data);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate);
  }, [id, apiPrivate]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h3" sx={{ textAlign: "center" }}>
        Tambah Transaksi
      </Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        <TextField
          label="Tanggal Transaksi"
          name="tanggal"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="dd/mm/yyyy"
          variant="outlined"
          value={formik.values.tanggal}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
          helperText={formik.touched.tanggal && formik.errors.tanggal}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Penyakit"
          name="penyakit"
          placeholder="LSD"
          variant="outlined"
          value={formik.values.penyakit}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.penyakit && Boolean(formik.errors.penyakit)}
          helperText={formik.touched.penyakit && formik.errors.penyakit}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Obat"
          name="obat"
          placeholder="Antibiotik"
          variant="outlined"
          value={formik.values.obat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.obat && Boolean(formik.errors.obat)}
          helperText={formik.touched.obat && formik.errors.obat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Dokter/Pihak Terkait"
          name="pihak_terkait"
          placeholder="Adib Sulthon"
          variant="outlined"
          value={formik.values.pihak_terkait}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.pihak_terkait && Boolean(formik.errors.pihak_terkait)
          }
          helperText={
            formik.touched.pihak_terkait && formik.errors.pihak_terkait
          }
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          label="Jumlah (Rp)"
          name="jumlah"
          placeholder="Rp100.000.000"
          variant="outlined"
          value={formik.values.jumlah}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.jumlah && Boolean(formik.errors.jumlah)}
          helperText={formik.touched.jumlah && formik.errors.jumlah}
          InputProps={{
            inputComponent: NumericFormatCustom,
          }}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <TextField
          type="text"
          label="Deskripsi"
          name="deskripsi"
          variant="outlined"
          multiline
          value={formik.values.deskripsi}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.deskripsi && Boolean(formik.errors.deskripsi)}
          helperText={formik.touched.deskripsi && formik.errors.deskripsi}
          rows={4}
          required
          fullWidth
          sx={{
            my: 2,
          }}
        />
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormPengobatanView;
