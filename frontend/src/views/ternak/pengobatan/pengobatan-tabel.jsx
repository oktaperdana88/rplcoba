import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import {
  applyFilter,
  emptyRows,
  getComparator,
} from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import { Box, CircularProgress, Typography } from "@mui/material";
import TabelPengobatanToolbar from "./pengobatan-table-toolbar";
import TabelPengobatanTernakRow from "./pengobatan-table-row";
import { useParams } from "react-router-dom";
import { any, func } from "prop-types";
import useAxiosPrivate from "../../../hooks/use-axios-private";
// ----------------------------------------------------------------------

const TabelPengobatan = ({ sum, setSum }) => {
  const apiPrivate = useAxiosPrivate()
  const [page, setPage] = useState(0);

  const { id } = useParams();

  const [loading, setLoading] = useState(true);

  const [order, setOrder] = useState("asc");

  const [data, setData] = useState([]);

  const [orderBy, setOrderBy] = useState("id");

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const fetcher = async (id, apiPrivate) => {
    setLoading(true);
    const response = await apiPrivate.get(`ternak/${id}/pemeliharaan`);
    const data = response.data.data.map((item) => {
      const tanggal = item.transaksi.tanggal_transaksi;
      const pihak_terkait = item.transaksi.pihak_terkait;
      const formattedHarga = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(item.transaksi.jumlah);
      return {
        ...item,
        tanggal: tanggal,
        pihak_terkait: pihak_terkait,
        jumlah: formattedHarga,
      };
    });
    setData(data);
    setSum(response.data.sum);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate);
  }, [id, apiPrivate]);

  const headLabel = [
    { id: "no", label: "No" },
    { id: "tanggal", label: "Tanggal Transaksi" },
    { id: "penyakit", label: "Penyakit" },
    { id: "obat", label: "Obat" },
    { id: "pihak_terkait", label: "Dokter" },
    { id: "jumlah", label: "Jumlah (Rp)", align: "center" },
  ];

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
  });

  const notFound = !dataFiltered;

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Card>
        <TabelPengobatanToolbar id={id} />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <TabelPengobatanTernakRow
                      key={row.id}
                      tanggal={row.tanggal}
                      obat={row.obat}
                      penyakit={row.penyakit}
                      pihak_terkait={row.pihak_terkait}
                      jumlah={row.jumlah}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && (
                  <TableNoData query={`Pengobatan Ternak`} col={6} />
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <Typography variant="h6" sx={{ margin: 2 }}>
          Total Biaya Pengobatan:{" "}
          {new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
          }).format(sum)}
        </Typography>

        <TablePagination
          page={page}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          labelRowsPerPage={"Baris tiap Halaman"}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

TabelPengobatan.propTypes = {
  sum: any,
  setSum: func,
};

export default TabelPengobatan;
