import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";

// ----------------------------------------------------------------------

const TabelPengobatanTernakRow = ({
  tanggal,
  obat,
  penyakit,
  pihak_terkait,
  jumlah,
  no,
}) => {
  const tgl = new Date(tanggal);
  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${tgl.getDate()}-${
          tgl.getMonth() + 1
        }-${tgl.getFullYear()}`}</TableCell>

        <TableCell>{penyakit}</TableCell>

        <TableCell>{obat}</TableCell>

        <TableCell>{pihak_terkait}</TableCell>
        <TableCell align="center">{jumlah}</TableCell>
      </TableRow>
    </>
  );
};

TabelPengobatanTernakRow.propTypes = {
  tanggal: PropTypes.any,
  obat: PropTypes.any,
  penyakit: PropTypes.any,
  pihak_terkait: PropTypes.any,
  jumlah: PropTypes.any,
  no: PropTypes.number,
};

export default TabelPengobatanTernakRow;
