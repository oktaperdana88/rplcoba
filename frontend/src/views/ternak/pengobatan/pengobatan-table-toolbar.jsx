import { Button, Toolbar, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import Iconify from "../../../components/iconify";
import { any } from "prop-types";

const TabelPengobatanToolbar = ({ id }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(`/admin/ternak/${id}/tambah-operasional`);
  };
  return (
    <Toolbar
      sx={{
        display: "flex",
        justifyContent: "space-between",
        p: (theme) => theme.spacing(0, 1, 0, 3),
      }}
    >
      <Typography variant="h4">Riwayat Pengobatan Ternak</Typography>
      <Button
        variant="contained"
        color="inherit"
        onClick={handleClick}
        startIcon={<Iconify icon="eva:plus-fill" />}
        sx={{
          display: "block",
          my: 2,
        }}
        size="small"
      >
        Tambah Pengobatan
      </Button>
    </Toolbar>
  );
};

TabelPengobatanToolbar.propTypes = {
  id: any,
};

export default TabelPengobatanToolbar;
