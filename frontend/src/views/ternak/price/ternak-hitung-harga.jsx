import {
  Box,
  Button,
  Card,
  Container,
  TextField,
  Typography,
} from "@mui/material";
import { any, bool, string } from "prop-types";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import NumericFormatCustom from "../../../components/currency_field";

const PerkiraanHarga = ({
  hargaBeli,
  tanggalMasuk,
  totalPengobatan,
  tombol,
}) => {
  const [biayaPerHari, setBiayaPerHari] = useState(0);
  const [total, setTotal] = useState(0);
  const { id } = useParams();
  const navigate = useNavigate();
  const sellTernak = () => {
    navigate(`/admin/penjualan/tambah?cat=Ternak&id=${id}&nilai=${total}`);
  };
  const handleClick = () => {
    const waktu = Math.ceil(
      (new Date().getTime() - new Date(tanggalMasuk).getTime()) /
        (1000 * 3600 * 24)
    );
    setTotal(waktu * biayaPerHari + hargaBeli + totalPengobatan);
  };

  const handleChange = (event) => {
    setBiayaPerHari(event.target.value);
  };
  return (
    <Container>
      <Card sx={{ padding: 2 }}>
        <Typography variant="h6">Perkiraan Nilai Jual</Typography>
        <TextField
          name="biaya"
          label="Biaya per Hari (Rp)"
          placeholder="ex: Rp1.000"
          value={biayaPerHari}
          onChange={handleChange}
          InputProps={{
            inputComponent: NumericFormatCustom,
          }}
          fullWidth
          sx={{
            my: 2,
          }}
        />
        <Button
          variant="contained"
          color="inherit"
          onClick={handleClick}
          sx={{
            display: "block",
            my: 2,
            mx: "auto",
          }}
          size="small"
        >
          Hitung
        </Button>
        {total !== 0 && (
          <Box>
            <Typography>
              Rekomendasi Harga Jual:{" "}
              {new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR",
              }).format(total)}
            </Typography>
            {tombol && (
              <Button
                variant="contained"
                size="medium"
                color="secondary"
                onClick={sellTernak}
                disabled={!tombol}
                sx={{
                  maxWidth: 200,
                  fontSize: "12px",
                  display: "block",
                  mx: "auto",
                }}
              >
                Jual
              </Button>
            )}
          </Box>
        )}
      </Card>
    </Container>
  );
};

PerkiraanHarga.propTypes = {
  hargaBeli: any,
  tanggalMasuk: any,
  totalPengobatan: any,
  kategori: string,
  tombol: bool,
};

export default PerkiraanHarga;
