import { Box, Button, Card, Container, Grid, Typography } from "@mui/material";
import { any, bool } from "prop-types";
import { useNavigate } from "react-router-dom";
import Iconify from "../../../components/iconify";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowDown19,
  faArrowUp91,
  faCalendarDays,
  faCodeBranch,
  faHandshake,
  faMoneyBill,
  faVenusMars,
  faWeightScale,
} from "@fortawesome/free-solid-svg-icons";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState } from "react";

const TernakDetailCard = ({ ternak, tombol }) => {
  const navigate = useNavigate();
  const apiPrivate = useAxiosPrivate();
  const date = new Date(ternak.tanggal_masuk);
  const [btnLoading, setBtnLoading] = useState(false);

  return (
    <Container>
      <Card sx={{ padding: 2, mb: 2 }}>
        <Typography variant="h4">{ternak.kategori}</Typography>
        <hr />
        <Grid container spacing={2} sx={{ display: { md: "none" } }}>
          {ternak.kategori === "Sapi" && (
            <>
              <Grid item xs={5}>
                <FontAwesomeIcon icon={faArrowDown19} /> Nomor
              </Grid>
              <Grid item xs={7}>
                {ternak.nomor}
              </Grid>
            </>
          )}
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faCalendarDays} /> Tanggal Perolehan
          </Grid>
          <Grid item xs={7}>
            {`${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faCodeBranch} /> Jenis
          </Grid>
          <Grid item xs={7}>
            {ternak.jenis}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faArrowUp91} />
            Umur Saat ini
          </Grid>
          <Grid item xs={7}>
            {Math.ceil(
              (new Date().getTime() - new Date(ternak.tanggal_masuk)) /
                (1000 * 3600 * 24)
            ) + ternak.umur}{" "}
            hari
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faArrowUp91} />
            Umur Masuk
          </Grid>
          <Grid item xs={7}>
            {ternak.umur} hari
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faHandshake} /> Cara Mendapatkan
          </Grid>
          <Grid item xs={7}>
            {ternak.cara_perolehan}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faVenusMars} /> Jenis Kelamin
          </Grid>
          <Grid item xs={7}>
            {ternak.kelamin}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faWeightScale} /> Berat
          </Grid>
          <Grid item xs={7}>
            {ternak.berat} kg
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faCodeBranch} /> Induk Jantan
          </Grid>
          <Grid item xs={7}>
            {ternak.induk_jantan ? ternak.induk_jantan : `Tidak Diketahui`}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faCodeBranch} /> Induk Betina
          </Grid>
          <Grid item xs={7}>
            {ternak.induk_betina ? ternak.induk_betina : `Tidak Diketahui`}
          </Grid>
          <Grid item xs={5}>
            <FontAwesomeIcon icon={faMoneyBill} /> Harga Beli
          </Grid>
          <Grid item xs={7}>
            {ternak.harga_beli !== 0
              ? new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(ternak.harga_beli)
              : `Dihasilkan dari breeding`}
          </Grid>
        </Grid>
        <Box sx={{ display: { xs: "none", md: "flex" } }}>
          <Grid container spacing={2}>
            {ternak.kategori === "Sapi" && (
              <>
                <Grid item sm={7}>
                  <FontAwesomeIcon icon={faArrowDown19} /> Nomor
                </Grid>
                <Grid item sm={5}>
                  {ternak.nomor}
                </Grid>
              </>
            )}
            <Grid item sm={7}>
              <FontAwesomeIcon icon={faCalendarDays} /> Tanggal Perolehan
            </Grid>
            <Grid item sm={5}>
              {`${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`}
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faArrowUp91} />
              Umur Saat Ini
            </Grid>
            <Grid item xs={5}>
              {Math.ceil(
                (new Date().getTime() - new Date(ternak.tanggal_masuk)) /
                  (1000 * 3600 * 24)
              ) + ternak.umur}{" "}
              hari
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faArrowUp91} />
              Umur Masuk
            </Grid>
            <Grid item xs={5}>
              {ternak.umur} hari
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faHandshake} /> Cara Mendapatkan
            </Grid>
            <Grid item xs={5}>
              {ternak.cara_perolehan}
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faMoneyBill} /> Harga Beli
            </Grid>
            <Grid item xs={5}>
              {ternak.harga_beli !== 0
                ? new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                  }).format(ternak.harga_beli)
                : `Dihasilkan dari breeding`}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faCodeBranch} /> Jenis
            </Grid>
            <Grid item xs={5}>
              {ternak.jenis}
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faVenusMars} /> Jenis Kelamin
            </Grid>
            <Grid item xs={5}>
              {ternak.kelamin}
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faWeightScale} /> Berat
            </Grid>
            <Grid item xs={5}>
              {ternak.berat} kg
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faCodeBranch} /> Induk Jantan
            </Grid>
            <Grid item xs={5}>
              {ternak.induk_jantan ? ternak.induk_jantan : `Tidak Diketahui`}
            </Grid>
            <Grid item xs={7}>
              <FontAwesomeIcon icon={faCodeBranch} /> Induk Betina
            </Grid>
            <Grid item xs={5}>
              {ternak.induk_betina ? ternak.induk_betina : `Tidak Diketahui`}
            </Grid>
          </Grid>
        </Box>
        {tombol && (
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              gap: 1,
              justifyContent: "center",
              margin: 2,
            }}
          >
            <Button
              variant="contained"
              size="medium"
              color="success"
              onClick={() => {
                navigate(`/admin/ternak/${ternak.id}/edit`);
              }}
              startIcon={<Iconify icon="eva:edit-fill" />}
              sx={{ maxWidth: "6rem", fontSize: "12px" }}
            >
              Edit
            </Button>
            <LoadingButton
              loading={btnLoading}
              loadingPosition="start"
              variant="contained"
              size="medium"
              color="error"
              onClick={async () => {
                const result = await Swal.fire({
                  title: "Apakah Anda Yakin?",
                  text: "Anda akan menghapus ternak ini. Data ternak akan dihapus secara permanen.",
                  icon: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#C70039",
                  confirmButtonText: "Hapus",
                });

                if (result.value) {
                  const input = await Swal.fire({
                    title: "Masukkan catatan penghapusan pegawai!",
                    html:
                      `<div style="display: flex; flex-direction: column; width: 100%; justify-content: start; gap: 1rem;">` +
                      `<div style="display: flex; flex-direction: column; width: 100%;">` +
                      `<label for="swal-date">Tanggal Penghapusan</label>` +
                      `<input type='date' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-date' placeholder='Masukkan tanggal penghapusan' required />` +
                      `</div>` +
                      `<div style="display: flex; flex-direction: column; width: 100%;">` +
                      `<label for="swal-text" style="display: block;">Catatan Penghapusan</label>` +
                      `<input type='text' classname='swal2-input' style="display: block; height: 2rem; border: 1px solid #ccc; border-radius: 4px; padding: 8px;" id='swal-text' placeholder='Masukkan catatan penghapusan' required />` +
                      `</div>` +
                      `</div>`,
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#C70039",
                    confirmButtonText: "Hapus",
                    preConfirm: () => {
                      const textValue =
                        document.getElementById("swal-text").value;
                      const dateValue =
                        document.getElementById("swal-date").value;

                      if (!textValue) {
                        Swal.showValidationMessage("Catatn tidak boleh kosong");
                        return false;
                      }

                      // Validasi input tanggal (gunakan aturan validasi sesuai kebutuhan)
                      if (!dateValue) {
                        Swal.showValidationMessage(
                          "Tanggal penghapusan tidak boleh kosong"
                        );
                        return false;
                      }

                      if (
                        new Date(dateValue).getTime() > new Date().getTime()
                      ) {
                        Swal.showValidationMessage(
                          "Tanggal penghapusan tidak boleh melebihi hari ini"
                        );
                        return false;
                      }

                      if (
                        new Date(dateValue).getTime() <
                        new Date(ternak.tanggal_masuk).getTime()
                      ) {
                        Swal.showValidationMessage(
                          "Tanggal penghapusan tidak boleh sebelum ternak masuk"
                        );
                        return false;
                      }

                      // Mengembalikan objek dengan nilai dari kedua input
                      return { text: textValue, date: dateValue };
                    },
                  });

                  if (input.isConfirmed) {
                    setBtnLoading(true);
                    try {
                      await apiPrivate.post(`ternak/hapus`, {
                        id: ternak.id, // Ganti dengan properti yang sesuai untuk mengidentifikasi ternak yang akan dihapus
                        deskripsi: input.value.text,
                        tanggal: input.value.date,
                        jenis: ternak.jenis,
                        kategori: ternak.kategori,
                        id_peternakan: ternak.id_peternakan,
                      });

                      toast.fire({
                        icon: "success",
                        text: "Ternak berhasil dihapus",
                      });

                      navigate("/admin/ternak");
                    } catch (error) {
                      setBtnLoading(false);
                      toast.fire({
                        icon: "error",
                        text: error.response.data.message,
                      });
                    }
                  }
                }
              }}
              startIcon={<Iconify icon="eva:trash-2-outline" />}
              sx={{ maxWidth: "6rem", fontSize: "12px" }}
            >
              Hapus
            </LoadingButton>
          </Box>
        )}
      </Card>
    </Container>
  );
};

TernakDetailCard.propTypes = {
  ternak: any,
  tombol: bool,
};

export default TernakDetailCard;
