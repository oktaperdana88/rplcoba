import { Grid } from "@mui/material";
import AppWidgetSummary from "../../../components/card";
import { object } from "prop-types";

const TernakSummary = ({ peternakan }) => {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          route="/admin/ternak/tambah?kategori=Sapi"
          title="Sapi"
          total={peternakan.jumlah_sapi}
          max={peternakan.peternakan?.kapasitas_sapi}
          icon={<img alt="icon" src="/assets/icons/glass/ic_glass_bag.png" />}
        />
      </Grid>

      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          route="/admin/ternak/tambah?kategori=Kambing"
          title="Kambing"
          total={peternakan.jumlah_kambing}
          max={peternakan.peternakan?.kapasitas_kambing}
          icon={<img alt="icon" src="/assets/icons/glass/ic_glass_users.png" />}
        />
      </Grid>

      <Grid item xs={12} md={4}>
        <AppWidgetSummary
          route="/admin/ternak/tambah?kategori=Ayam"
          title="Ayam"
          total={peternakan.jumlah_ayam}
          max={peternakan.peternakan?.kapasitas_ayam}
          icon={<img alt="icon" src="/assets/icons/glass/ic_glass_buy.png" />}
        />
      </Grid>
    </Grid>
  );
};

TernakSummary.propTypes = {
  peternakan: object,
};

export default TernakSummary;
