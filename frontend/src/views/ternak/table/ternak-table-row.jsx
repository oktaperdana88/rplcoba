import PropTypes from "prop-types";

import Stack from "@mui/material/Stack";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

// ----------------------------------------------------------------------

const TernakTableRow = ({
  id,
  tanggal,
  kategori,
  jenis,
  nomor,
  umur,
  berat,
  no,
}) => {
  const navigate = useNavigate()

  const handleOpenMenu = () => {
    navigate(`/admin/ternak/${id}`)
  };

  const now = new Date(tanggal);

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox">
        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2} sx={{ pl: 2 }}>
            <Typography variant="subtitle2" noWrap>
              {no}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${now.getDate()}-${now.getMonth()+1}-${now.getFullYear()}`}</TableCell>

        <TableCell>{kategori}</TableCell>

        <TableCell>{jenis}</TableCell>

        <TableCell align="center">{nomor}</TableCell>
        <TableCell align="center">{umur} hari</TableCell>
        <TableCell align="center">{berat} kg</TableCell>

        <TableCell>
          <IconButton onClick={handleOpenMenu} aria-label="Detail" title="Detail">
            <FontAwesomeIcon icon={faArrowRight} />
          </IconButton>
        </TableCell>
      </TableRow>
    </>
  );
};

TernakTableRow.propTypes = {
  id: PropTypes.any,
  tanggal: PropTypes.any,
  kategori: PropTypes.any,
  jenis: PropTypes.any,
  nomor: PropTypes.any,
  umur: PropTypes.any,
  berat: PropTypes.any,
  no: PropTypes.number,
};

export default TernakTableRow;
