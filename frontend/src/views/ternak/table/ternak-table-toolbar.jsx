import {
  Box,
  FormControl,
  MenuItem,
  Select,
  Toolbar,
  Typography,
} from "@mui/material";
import { any, func } from "prop-types";
import {
  jenisAyam,
  jenisKambing,
  jenisSapi,
  kategoriTernak,
} from "../../../constant";

const TernakTableToolbar = ({
  onFilterKategori,
  onFilterJenis,
  kategori,
  jenis,
}) => {
  return (
    <Toolbar
      sx={{
        height: "max",
        padding: 2,
        display: "flex",
        flexDirection: {xs: "column", sm: "row"},
        alignItems: "start",
        justifyContent: {xs: "normal", sm: "space-between"},
      }}
    >
      <Typography variant="h4">Daftar Ternak</Typography>
      <Box
        title="Filter list"
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "end",
          maxWidth: {xs: "100%",sm: "50%"},
        }}
      >
        <FormControl
          variant="standard"
          sx={{ width: "max", textAlign: "right" }}
        >
          <Select
            id="kategori"
            value={kategori}
            onChange={onFilterKategori}
            variant="standard"
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
          >
            <MenuItem value="Semua Kategori">Semua Kategori</MenuItem>
            {kategoriTernak.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl
          variant="standard"
          sx={{ width: "max", textAlign: "right" }}
        >
          <Select
            id="jenis"
            value={jenis}
            onChange={onFilterJenis}
            variant="standard"
            disabled={kategori === "Semua Kategori" ? true : false}
            disableUnderline={true}
            sx={{
              borderBottom: "none",
            }}
          >
            <MenuItem value="Semua Jenis">Semua Jenis</MenuItem>
            {kategori === "Sapi"
              ? jenisSapi.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))
              : kategori === "Kambing"
              ? jenisKambing.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))
              : jenisAyam.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
          </Select>
        </FormControl>
      </Box>
    </Toolbar>
  );
};

TernakTableToolbar.propTypes = {
  onFilterKategori: func,
  onFilterJenis: func,
  kategori: any,
  jenis: any,
};

export default TernakTableToolbar;
