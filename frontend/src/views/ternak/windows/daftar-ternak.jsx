import { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Container from "@mui/material/Container";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";

import Scrollbar from "../../../components/scrollbar";

import { emptyRows } from "../../../components/table/utils";
import {
  TableEmptyRow,
  TableHeadCustom,
  TableNoData,
} from "../../../components/table";
import TernakTableRow from "../table/ternak-table-row";
import TernakTableToolbar from "../table/ternak-table-toolbar";
import TernakSummary from "../summary/ternak-summary";
import { Box, CircularProgress } from "@mui/material";
import { applyFilter, getComparator } from "../table/utils";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";

// ----------------------------------------------------------------------

const TernakView = () => {
  const {auth} = useAuth()
  const apiPrivate = useAxiosPrivate();

  const [data, setData] = useState([]);
  const [peternakan, setPeternakan] = useState({});

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState("asc");

  const [orderBy, setOrderBy] = useState("id");

  const [filterKategori, setFilterKategori] = useState("Semua Kategori");

  const [filterJenis, setFilterJenis] = useState("Semua Jenis");

  const dataFiltered = applyFilter({
    inputData: data,
    comparator: getComparator(order, orderBy),
    filterKategori,
    filterJenis,
  });

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [loading, setLoading] = useState(true);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === "asc";
    if (id !== "") {
      setOrder(isAsc ? "desc" : "asc");
      setOrderBy(id);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleFilterByKategori = (event) => {
    setPage(0);
    if (event.target.value === "Semua Kategori") setFilterJenis("Semua Jenis");
    setFilterKategori(event.target.value);
  };

  const handleFilterByJenis = (event) => {
    setPage(0);
    setFilterJenis(event.target.value);
  };

  const notFound = !dataFiltered.length && !!filterKategori && !!filterJenis;

  const headLabel = [
    { id: "id", label: "No" },
    { id: "tanggal_masuk", label: "Tanggal Masuk" },
    { id: "kategori", label: "Kategori" },
    { id: "jenis", label: "Jenis" },
    { id: "nomor", label: "Nomor Sapi", align: "center" },
    { id: "umur", label: "Umur (hari)", align: "center" },
    { id: "berat", label: "Berat (kg)", align: "center" },
    { id: "", label: "Detail" },
  ];

  const fetcher = async (apiPrivate, peternakan, controller) => {
    try {
      setLoading(true);
      const res = await apiPrivate.get(`ternak?peternakan=${peternakan}`, {
        signal: controller.signal,
      });
      const data = res.data.data.map((item) => {
        const umur =
          item.umur +
          Math.ceil(
            (new Date().getTime() - new Date(item.tanggal_masuk).getTime()) /
              (1000 * 3600 * 24)
          );
        return {
          ...item,
          umur: umur,
        };
      });
      setData(data);
      const response = await apiPrivate.get(`peternakan/${peternakan}`); // TODO: ganti 1 menjadi id peternakan dari user yg login
      setPeternakan(response.data);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    const controller = new AbortController();
    fetcher(apiPrivate, auth.peternakan, controller);
    return () => {
      controller.abort();
    };
  }, [apiPrivate, auth.peternakan]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <Container>
      <Box sx={{ mb: 3 }}>
        <TernakSummary peternakan={peternakan} />
      </Box>

      <Card>
        <TernakTableToolbar
          onFilterKategori={handleFilterByKategori}
          kategori={filterKategori}
          onFilterJenis={handleFilterByJenis}
          jenis={filterJenis}
        />

        <Scrollbar>
          <TableContainer sx={{ overflow: "unset" }}>
            <Table sx={{ minWidth: 800 }}>
              <TableHeadCustom
                order={order}
                orderBy={orderBy}
                rowCount={data.length}
                numSelected={data.length}
                onRequestSort={handleSort}
                headLabel={headLabel}
              />
              <TableBody>
                {dataFiltered
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <TernakTableRow
                      key={row.id}
                      id={row.id}
                      tanggal={row.tanggal_masuk}
                      kategori={row.kategori}
                      jenis={row.jenis}
                      nomor={row.kategori === "Sapi" ? row.nomor : "-"}
                      umur={row.umur}
                      berat={row.berat}
                      no={page * rowsPerPage + index + 1}
                    />
                  ))}

                <TableEmptyRow
                  height={77}
                  emptyRows={emptyRows(0, rowsPerPage, data.length)}
                />

                {notFound && <TableNoData query={filterKategori} col={8} />}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          page={page}
          component="div"
          count={dataFiltered.length}
          labelRowsPerPage={"Baris tiap Halaman"}
          rowsPerPage={rowsPerPage}
          onPageChange={handleChangePage}
          rowsPerPageOptions={[5, 10, 25]}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Container>
  );
};

export default TernakView;
