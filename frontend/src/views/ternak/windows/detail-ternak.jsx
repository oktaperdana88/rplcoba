import { Box, CircularProgress, Grid } from "@mui/material";
import TabelPengobatan from "../pengobatan/pengobatan-tabel";
import TernakDetailCard from "../summary/ternak-detail-card";
import PerkiraanHarga from "../price/ternak-hitung-harga";
import { useEffect, useState } from "react";
import { Navigate, useParams } from "react-router-dom";
import useAxiosPrivate from "../../../hooks/use-axios-private";
import useAuth from "../../../hooks/use-auth";

const DetailTernakView = () => {
  const apiPrivate = useAxiosPrivate();
  const [tanggal, setTanggal] = useState("");
  const [hargaAwal, setHargaAwal] = useState(0);
  const [biayaPengobatan, setBiayaPengobatan] = useState(0);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const { auth } = useAuth();
  const [ternak, setTernak] = useState({});
  const [forbidden, setForbidden] = useState(false);

  const fetcher = async (id, apiPrivate, peternakan) => {
    setLoading(true);
    const response = await apiPrivate.get(`ternak/${id}`);
    if (response.data.data.id_peternakan != peternakan) {
      setForbidden(true);
    }
    setTernak(response.data.data);
    setTanggal(response.data.data.tanggal_masuk);
    setHargaAwal(response.data.data.harga_beli);
    setLoading(false);
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/ternak"} />
  ) : (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TernakDetailCard ternak={ternak} tombol={true} />
        <PerkiraanHarga
          tanggalMasuk={tanggal}
          hargaBeli={hargaAwal}
          totalPengobatan={biayaPengobatan}
          tombol={true}
        />
      </Grid>
      <Grid item xs={12}>
        <TabelPengobatan sum={biayaPengobatan} setSum={setBiayaPengobatan} />
      </Grid>
    </Grid>
  );
};

export default DetailTernakView;
