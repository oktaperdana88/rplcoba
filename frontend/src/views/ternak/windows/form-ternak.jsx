import {
  Box,
  Card,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import {
  caraDapatSapi,
  jenisAyam,
  jenisKambing,
  jenisKelamin,
  jenisSapi,
  kategoriTernak,
} from "../../../constant";
import { date, number, object, string } from "yup";
import { useFormik } from "formik";
import {
  Navigate,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import Swal from "sweetalert2";
import { toast } from "../../../components/alert";
import { useEffect, useState } from "react";
import NumericFormatCustom from "../../../components/currency_field";
import useAuth from "../../../hooks/use-auth";
import LoadingButton from "@mui/lab/LoadingButton";
import useAxiosPrivate from "../../../hooks/use-axios-private";

const FormTernak = () => {
  const apiPrivate = useAxiosPrivate();
  const { auth } = useAuth();
  const validationSchema = object({
    tanggal: date()
      .min(
        new Date(auth.tanggal),
        "Tanggal Masuk tidak bisa mendahului tanggal berdiri peternakan"
      )
      .max(new Date(), "Tanggal masuk tidak boleh lebih dari hari ini")
      .required("Tanggal masuk tidak boleh kosong"),
    kategori: string().required("Kategori tidak boleh kosong"),
    jenis: string().required("Jenis tidak boleh kosong"),
    nomor: number().min(0, "Nomor Sapi minimal 1"),
    jk: string().required("Jenis Kelamin tidak boleh kosong"),
    berat: number().required("Berat tidak boleh kosong"),
    cara_dapat: string().required("Cara memperoleh tidak boleh kosong"),
    induk_jantan: string(),
    induk_betina: string(),
    pihak_terkait: string(),
    umur: number().min(0, "Umur tidak boleh negatif"),
    harga: number().min(0, "Harga beli tidak boleh negatif"),
  });

  const [ternak, setTernak] = useState({});
  const [btnLoading, setBtnLoading] = useState(false);
  const [forbidden, setForbidden] = useState(false);
  const [searchParams] = useSearchParams();
  const { id } = useParams();
  const navigate = useNavigate();

  const initialValues = {
    tanggal: ternak?.tanggal_masuk || "",
    kategori: ternak?.kategori || searchParams.get("kategori") || "",
    jenis: ternak?.jenis || "",
    nomor: ternak?.nomor || "",
    jk: ternak?.kelamin || "",
    berat: ternak?.berat || "",
    cara_dapat: ternak?.cara_perolehan || "",
    induk_jantan: ternak?.induk_jantan || "",
    induk_betina: ternak?.induk_betina || "",
    pihak_terkait: "",
    umur: ternak?.umur || "",
    harga: ternak?.harga_beli || "",
    peternakan: ternak?.id_peternakan || 1,
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    enableReinitialize: true,
    onSubmit: async (val) => {
      const controller = new AbortController();
      const result = await Swal.fire({
        title: "Apakah Anda Yakin?",
        text: `Ternak akan ${id ? "diperbaharui" : "ditambahkan"}!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#219C90",
        confirmButtonText: "Ya",
      });

      if (result.value) {
        setBtnLoading(true);
        try {
          if (!id) {
            await apiPrivate.post(
              `ternak/create`,
              {
                nomor: val.nomor,
                berat: val.berat,
                jenis: val.jenis,
                kategori: val.kategori,
                kelamin: val.jk,
                cara_perolehan: val.cara_dapat,
                induk_jantan: val.induk_jantan,
                induk_betina: val.induk_betina,
                harga_beli: val.harga,
                tanggal_transaksi: val.tanggal,
                pihak_terkait: val.pihak_terkait,
                umur: val.umur,
                id_peternakan: auth.peternakan,
              },
              {
                signal: controller.signal,
              }
            );
          } else {
            await apiPrivate.put(
              `ternak/${id}`,
              {
                nomor: val.nomor,
                berat: val.berat,
                jenis: val.jenis,
                kategori: val.kategori,
                kelamin: val.jk,
                cara_perolehan: val.cara_dapat,
                induk_jantan: val.induk_jantan,
                induk_betina: val.induk_betina,
                harga_beli: val.harga,
                tanggal_transaksi: val.tanggal,
                umur: val.umur,
                id_peternakan: val.peternakan,
              },
              {
                signal: controller.signal,
              }
            );
          }

          toast.fire({
            icon: "success",
            text: `Ternak berhasil ${id ? "diperbaharui" : "ditambahkan"}`,
          });

          navigate("/admin/ternak");
        } catch (error) {
          setBtnLoading(false);
          toast.fire({
            icon: "error",
            text: error.response.data.message,
          });
        }
      }
    },
  });

  const [loading, setLoading] = useState(true);

  const fetcher = async (id, apiPrivate, peternakan) => {
    if (id) {
      setLoading(true);
      const response = await apiPrivate.get(`ternak/${id}`);
      if (response.data.data.id_peternakan != peternakan) {
        setForbidden(true);
      }
      setTernak(response.data.data);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetcher(id, apiPrivate, auth.peternakan);
  }, [id, apiPrivate, auth.peternakan]);

  const [options, setOptions] = useState([]);

  const fetcherOpt = async (apiPrivate) => {
    const res = await apiPrivate.get(`peternakan`);
    setOptions(res.data.data);
  };

  useEffect(() => {
    fetcherOpt(apiPrivate);
  }, [apiPrivate]);

  return loading ? (
    <Box
      component="div"
      sx={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : forbidden ? (
    <Navigate to={"/admin/ternak"} />
  ) : (
    <Card sx={{ margin: 2, padding: 2 }}>
      <Typography variant="h3" sx={{ textAlign: "center" }}>
        {id ? "Edit" : "Tambah"} Ternak
      </Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {!id && (
          <TextField
            label="Tanggal Masuk"
            name="tanggal"
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="dd/mm/yyyy"
            variant="outlined"
            value={formik.values.tanggal}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.tanggal && Boolean(formik.errors.tanggal)}
            helperText={formik.touched.tanggal && formik.errors.tanggal}
            fullWidth
            required
            sx={{
              my: 2,
            }}
          />
        )}
        <FormControl fullWidth>
          <InputLabel id="kategori" sx={{ my: 2 }}>
            Kategori *
          </InputLabel>
          <Select
            id="kategori"
            label="Kategori"
            name="kategori"
            variant="outlined"
            value={formik.values.kategori}
            onChange={
              id
                ? null
                : (e) => {
                    formik.values.nomor = "";
                    formik.handleChange(e);
                  }
            }
            onBlur={formik.handleBlur}
            error={formik.touched.kategori && Boolean(formik.errors.kategori)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {kategoriTernak.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box component={"span"} sx={{ fontSize: 12, color: "red" }}>
            {formik.touched.kategori && formik.errors.kategori}
          </Box>
        </FormControl>
        {formik.values.kategori === "Sapi" && (
          <>
            <FormControl fullWidth>
              <InputLabel id="jenis" sx={{ my: 2 }}>
                Jenis *
              </InputLabel>
              <Select
                id="jenis"
                label="Jenis"
                name="jenis"
                variant="outlined"
                value={formik.values.jenis}
                onChange={id ? null : formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.jenis && Boolean(formik.errors.jenis)}
                sx={{
                  borderBottom: "none",
                  my: 2,
                }}
                required
              >
                <MenuItem selected disabled>
                  Pilih Salah Satu
                </MenuItem>
                {jenisSapi.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
              <Box
                component={"div"}
                sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
              >
                {formik.touched.jenis && formik.errors.jenis}
              </Box>
            </FormControl>
            <TextField
              label="Nomor Sapi"
              type="number"
              name="nomor"
              placeholder="ex: 7"
              variant="outlined"
              value={formik.values.nomor}
              onChange={id ? null : formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.nomor && Boolean(formik.errors.nomor)}
              helperText={formik.touched.nomor && formik.errors.nomor}
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        {formik.values.kategori === "Kambing" && (
          <>
            <FormControl fullWidth>
              <InputLabel id="jenis" sx={{ my: 2 }}>
                Jenis *
              </InputLabel>
              <Select
                id="jenis"
                label="Jenis"
                name="jenis"
                variant="outlined"
                value={formik.values.jenis}
                onChange={id ? null : formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.jenis && Boolean(formik.errors.jenis)}
                sx={{
                  borderBottom: "none",
                  my: 2,
                }}
                required
              >
                <MenuItem selected disabled>
                  Pilih Salah Satu
                </MenuItem>
                {jenisKambing.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
              <Box
                component={"div"}
                sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
              >
                {formik.touched.jenis && formik.errors.jenis}
              </Box>
            </FormControl>
          </>
        )}
        {formik.values.kategori === "Ayam" && (
          <>
            <FormControl fullWidth>
              <InputLabel id="jenis" sx={{ my: 2 }}>
                Jenis *
              </InputLabel>
              <Select
                id="jenis"
                label="Jenis"
                name="jenis"
                variant="outlined"
                value={formik.values.jenis}
                onChange={id ? null : formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.jenis && Boolean(formik.errors.jenis)}
                sx={{
                  borderBottom: "none",
                  my: 2,
                }}
                required
              >
                <MenuItem selected disabled>
                  Pilih Salah Satu
                </MenuItem>
                {jenisAyam.map((item) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
              <Box
                component={"div"}
                sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
              >
                {formik.touched.jenis && formik.errors.jenis}
              </Box>
            </FormControl>
          </>
        )}
        <FormControl fullWidth>
          <InputLabel id="jk" sx={{ my: 2 }}>
            Jenis Kelamin *
          </InputLabel>
          <Select
            id="jk"
            label="Jenis Kelamin"
            name="jk"
            variant="outlined"
            value={formik.values.jk}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.jk && Boolean(formik.errors.jk)}
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {jenisKelamin.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.jk && formik.errors.jk}
          </Box>
        </FormControl>
        <TextField
          label="Berat (kg)"
          type="number"
          name="berat"
          placeholder="ex: 70"
          variant="outlined"
          value={formik.values.berat}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.berat && Boolean(formik.errors.berat)}
          helperText={formik.touched.berat && formik.errors.berat}
          fullWidth
          required
          sx={{
            my: 2,
          }}
        />
        <FormControl fullWidth>
          <InputLabel id="cara_dapat" sx={{ my: 2 }}>
            Cara Mendapatkan *
          </InputLabel>
          <Select
            id="cara_dapat"
            label="Cara Mendapatkan"
            name="cara_dapat"
            variant="outlined"
            value={formik.values.cara_dapat}
            onChange={
              id
                ? null
                : (e) => {
                    if (e.target.value === "Beli") {
                      formik.values.induk_jantan = "";
                      formik.values.induk_betina = "";
                    } else {
                      formik.values.harga = 0;
                      formik.values.umur = 0;
                    }
                    formik.handleChange(e);
                  }
            }
            onBlur={formik.handleBlur}
            error={
              formik.touched.cara_dapat && Boolean(formik.errors.cara_dapat)
            }
            sx={{
              borderBottom: "none",
              my: 2,
            }}
            required
          >
            <MenuItem selected disabled>
              Pilih Salah Satu
            </MenuItem>
            {caraDapatSapi.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          <Box
            component={"div"}
            sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
          >
            {formik.touched.cara_dapat && formik.errors.cara_dapat}
          </Box>
        </FormControl>
        {formik.values.cara_dapat === "Breeding" &&
          formik.values.kategori === "Sapi" && (
            <>
              <FormControl fullWidth>
                <InputLabel id="induk_jantan" sx={{ my: 2 }}>
                  Induk Jantan *
                </InputLabel>
                <Select
                  id="induk_jantan"
                  label="Induk Jantan"
                  name="induk_jantan"
                  variant="outlined"
                  value={formik.values.induk_jantan}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_jantan &&
                    Boolean(formik.errors.induk_jantan)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisSapi.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_jantan && formik.errors.induk_jantan}
                </Box>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel id="induk_betina" sx={{ my: 2 }}>
                  Induk Betina *
                </InputLabel>
                <Select
                  id="induk_betina"
                  label="Induk Betina"
                  name="induk_betina"
                  variant="outlined"
                  value={formik.values.induk_betina}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_betina &&
                    Boolean(formik.errors.induk_betina)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisSapi.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_betina && formik.errors.induk_betina}
                </Box>
              </FormControl>
            </>
          )}
        {formik.values.cara_dapat === "Breeding" &&
          formik.values.kategori === "Kambing" && (
            <>
              <FormControl fullWidth>
                <InputLabel id="induk_jantan" sx={{ my: 2 }}>
                  Induk Jantan *
                </InputLabel>
                <Select
                  id="induk_jantan"
                  label="Induk Jantan"
                  name="induk_jantan"
                  variant="outlined"
                  value={formik.values.induk_jantan}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_jantan &&
                    Boolean(formik.errors.induk_jantan)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisKambing.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_jantan && formik.errors.induk_jantan}
                </Box>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel id="induk_betina" sx={{ my: 2 }}>
                  Induk Betina *
                </InputLabel>
                <Select
                  id="induk_betina"
                  label="Induk Betina"
                  name="induk_betina"
                  variant="outlined"
                  value={formik.values.induk_betina}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_betina &&
                    Boolean(formik.errors.induk_betina)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisKambing.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_betina && formik.errors.induk_betina}
                </Box>
              </FormControl>
            </>
          )}
        {formik.values.cara_dapat === "Breeding" &&
          formik.values.kategori === "Ayam" && (
            <>
              <FormControl fullWidth>
                <InputLabel id="induk_jantan" sx={{ my: 2 }}>
                  Induk Jantan *
                </InputLabel>
                <Select
                  id="induk_jantan"
                  label="Induk Jantan"
                  name="induk_jantan"
                  variant="outlined"
                  value={formik.values.induk_jantan}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_jantan &&
                    Boolean(formik.errors.induk_jantan)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisAyam.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_jantan && formik.errors.induk_jantan}
                </Box>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel id="induk_betina" sx={{ my: 2 }}>
                  Induk Betina *
                </InputLabel>
                <Select
                  id="induk_betina"
                  label="Induk Betina"
                  name="induk_betina"
                  variant="outlined"
                  value={formik.values.induk_betina}
                  onChange={id ? null : formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.induk_betina &&
                    Boolean(formik.errors.induk_betina)
                  }
                  sx={{
                    borderBottom: "none",
                    my: 2,
                  }}
                  required
                >
                  <MenuItem selected disabled>
                    Pilih Salah Satu
                  </MenuItem>
                  {jenisAyam.map((item) => (
                    <MenuItem key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Select>
                <Box
                  component={"div"}
                  sx={{ fontSize: 12, color: "orangered", mt: -1.5, ml: 2 }}
                >
                  {formik.touched.induk_betina && formik.errors.induk_betina}
                </Box>
              </FormControl>
            </>
          )}
        {formik.values.cara_dapat === "Beli" && (
          <>
            {!id && (
              <TextField
                label="Penjual"
                name="pihak_terkait"
                placeholder="Adib"
                variant="outlined"
                value={formik.values.pihak_terkait}
                onChange={id ? null : formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.pihak_terkait &&
                  Boolean(formik.errors.pihak_terkait)
                }
                helperText={
                  formik.touched.pihak_terkait && formik.errors.pihak_terkait
                }
                fullWidth
                required
                sx={{
                  my: 2,
                }}
              />
            )}
            <TextField
              label="Umur Ketika Masuk (hari)"
              type="number"
              name="umur"
              placeholder="ex: 365"
              variant="outlined"
              value={formik.values.umur}
              onChange={id ? null : formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.umur && Boolean(formik.errors.umur)}
              helperText={formik.touched.umur && formik.errors.umur}
              fullWidth
              required
              sx={{
                my: 2,
              }}
            />
          </>
        )}
        {!id && formik.values.cara_dapat === "Beli" && (
          <TextField
            label="Harga"
            name="harga"
            placeholder="Rp10.000.000"
            variant="outlined"
            value={formik.values.harga}
            onChange={id ? null : formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.harga && Boolean(formik.errors.harga)}
            helperText={formik.touched.harga && formik.errors.harga}
            InputProps={{
              inputComponent: NumericFormatCustom,
            }}
            fullWidth
            required
            sx={{
              my: 2,
            }}
          />
        )}
        {id && (
          <FormControl fullWidth>
            <InputLabel id="peternakan" sx={{ my: 2 }}>
              Lokasi Peternakan
            </InputLabel>
            <Select
              id="peternakan"
              label="Lokasi Peternakan"
              name="peternakan"
              variant="outlined"
              value={formik.values.peternakan}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.peternakan && Boolean(formik.errors.peternakan)
              }
              sx={{
                borderBottom: "none",
                my: 2,
              }}
              required
            >
              <MenuItem selected disabled>
                Pilih Salah Satu
              </MenuItem>
              {options.map((item) => (
                <MenuItem key={item.id} value={item.id}>
                  {item.nama}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
        <LoadingButton
          loadingPosition="start"
          loading={btnLoading}
          size="medium"
          type="submit"
          variant="contained"
          color="inherit"
          sx={{
            width: 120,
            display: "block",
            mx: "auto",
          }}
        >
          Submit
        </LoadingButton>
      </form>
    </Card>
  );
};

export default FormTernak;
